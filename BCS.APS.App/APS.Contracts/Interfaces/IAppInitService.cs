﻿using APS.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.Contracts.Interfaces
{
    public interface IAppInitService
    {
        ApiResult SysDbInit();
    }
}
