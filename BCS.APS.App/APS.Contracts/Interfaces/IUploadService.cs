﻿using APS.Contracts.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.Contracts.Interfaces
{
    public interface IUploadService
    {
        IUploadService SetConfig(UploadConfig config);
        Task<UploadSuccessResponse> FileUploadAsync(IFormFile iFormFile);
        Task<List<UploadSuccessResponse>> FileUploadAsync(IFormFileCollection iFormFiles);
    }
}
