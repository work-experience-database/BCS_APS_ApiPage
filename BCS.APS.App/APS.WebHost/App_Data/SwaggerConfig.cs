﻿namespace APS.WebHost.App_Data
{
    public class SwaggerConfig
    {
        public string GroupName { get; set; }
        public string Title { get; set; }
        public string Version { get; set; }
    }
}
