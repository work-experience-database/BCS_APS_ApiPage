﻿using APS.AdminApplication.Application;
using APS.AdminApplication.Entitys;
using APS.AspNetCore.Controllers;
using APS.Utils.Attributes;
using APS.Utils.Models;
using Microsoft.AspNetCore.Mvc;
using System.Configuration;

namespace APS.AdminApplication.Controllers
{
    /// <summary>菜单控制器</summary>
    [ApiExplorerSettings(GroupName = "Sys")]
    [Route("api/[controller]"), PermissionGroup("菜单Curd")]
    public class MenuController : AppCurdController<SysMenu>
    {
        private SysMainApplication mainApplication;

        public MenuController(AdminDbContext db, SysMainApplication mainApplication) : base(db)
        {
            this.mainApplication = mainApplication;
        }

        /// <summary>获取授权树</summary>
        /// <returns></returns>
        [HttpGet("menutree"), Permission("menutree", "权限树")]
        public ApiResult GetMenuTree()
        {
            return ApiResult.Success(mainApplication.GetMenuTrees(LoginUser));
        }
    }
}