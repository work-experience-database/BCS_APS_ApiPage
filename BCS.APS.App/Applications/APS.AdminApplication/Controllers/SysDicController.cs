﻿using APS.AdminApplication.Entitys;
using APS.AspNetCore.Controllers;
using APS.EFCore;
using APS.Utils.Attributes;
using APS.Utils.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Configuration;


namespace APS.AdminApplication.Controllers
{
    /// <summary>菜单控制器</summary>
    [ApiExplorerSettings(GroupName = "Sys")]
    [Route("api/[controller]"), PermissionGroup("字典Curd")]
    public class SysDicController : AppCurdController<SysDicType>
    {
        private ICurdService<SysDicValue> sysDicValueService;

        public SysDicController(AdminDbContext db) : base(db)
        {
            this.sysDicValueService = new BaseCurdService<SysDicValue>(db);
        }
    }
}