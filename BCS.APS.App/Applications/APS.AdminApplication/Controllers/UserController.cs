﻿using APS.AdminApplication.Entitys;
using APS.AspNetCore.Controllers;
using APS.Utils.Attributes;
using Microsoft.AspNetCore.Mvc;

namespace APS.AdminApplication.Controllers
{
    /// <summary>用户控制器</summary>
    [ApiExplorerSettings(GroupName = "Sys")]
    [Route("api/[controller]"), PermissionGroup("用户Curd")]
    public class UserController : AppCurdController<SysUser>
    {
        public UserController(AdminDbContext db) : base(db)
        {
        }
    }
}