﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.AdminApplication.Entitys
{
    /// <summary>字典类型</summary>
    [Table("SysDicType", Schema = "APS")]
    public class SysDicType : DefaultEntityLong
    {
        public SysDicType()
        {
            SysDicValues = new HashSet<SysDicValue>();
        }

        /// <summary>名称</summary>
        public string Name { get; set; }

        /// <summary>备注</summary>
        public string? Remark { get; set; }

        public long Sort { get; set; } = 0;

        public virtual ICollection<SysDicValue> SysDicValues { get; set; }
    }
}
