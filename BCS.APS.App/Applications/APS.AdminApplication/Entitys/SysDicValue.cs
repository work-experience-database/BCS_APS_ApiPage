﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.AdminApplication.Entitys
{
    /// <summary>字典值</summary>
    [Table("SysDicValue", Schema = "APS")]
    public class SysDicValue : DefaultEntityLong
    {
        /// <summary>名称</summary>
        public string? Name { get; set; }

        public string Value { get; set; }

        /// <summary>备注</summary>
        public string? Remark { get; set; }

        public long SysDicTypeId { get; set; }

        public virtual SysDicType SysDicType { get; set; }
    }
}
