﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.AdminApplication.Entitys
{
    [Table("SysJobLog", Schema = "APS")]
    public class SysJobLog : DefaultEntityLong
    {
        public string? ModuleName { get; set; }
        public string? Name { get; set; }
        public DateTime StartTime { get; set; }
        public string? Duration { get; set; }
        public DateTime? NextRun { get; set; }
    }
}
