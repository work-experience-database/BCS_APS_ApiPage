﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.AdminApplication.Entitys
{
    /// <summary>
    /// 菜单的功能
    /// </summary>
    [Table("SysMenuFunction", Schema = "APS")]
    public class SysMenuFunction : DefaultEntityLong
    {
        public long SysMenuId { get; set; }
        public virtual SysMenu SysMenu { get; set; }

        public long SysFunctionId { get; set; }
        public virtual SysFunction SysFunction { get; set; }
    }
}
