﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.AdminApplication.Entitys
{
    /// <summary>
    /// 角色权限
    /// </summary>
    [Table("SysRoleFunction", Schema = "APS")]
    public class SysRoleFunction : DefaultEntityLong
    {
        public long SysRoleId { get; set; }
        public long SysFunctionId { get; set; }
        public virtual SysFunction SysFunction { get; set; }
        public virtual SysRole SysRole { get; set; }
    }
}
