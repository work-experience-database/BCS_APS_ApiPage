﻿using APS.Utils.Models.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace APS.AdminApplication.Entitys
{
    /// <summary>角色菜单</summary>
    [Table("SysRoleMenu", Schema = "APS")]
    public class SysRoleMenu : DefaultEntityLong
    {
        public long SysRoleId { get; set; }
        public long SysMenuId { get; set; }
        public virtual SysMenu SysMenu { get; set; }
        public virtual SysRole SysRole { get; set; }
    }
}
