﻿using APS.AdminApplication.Entitys;
using APS.AspNetCore.EventBus;
using APS.Utils.Helper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.AdminApplication.Events
{
    public class JobLogStorageHandle : INotificationHandler<AppDomainEvent<SysJobLog>>
    {
        private AdminDbContext db;

        public JobLogStorageHandle(AdminDbContext db)
        {
            this.db = db;
        }

        public Task Handle(AppDomainEvent<SysJobLog> jobInfo, CancellationToken cancellationToken)
        {
            db.SysJobLog.Add(jobInfo.Value);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                LogHelper.Error("保存定时任务日志异常", ex);
            }
            return Task.CompletedTask;
        }
    }
}
