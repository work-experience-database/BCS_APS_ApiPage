﻿using APS.Jobs;
using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.AdminApplication.Jobs
{
    internal class TestJob : IJobSchedule
    {
        public string Name { get; set; } = "APS.AdminApplication|TestJob";

        public void ScheduleConfig(Schedule schedule)
        {
            schedule.ToRunEvery(30).Minutes();
        }

        public void Execute()
        {
            Console.WriteLine($"{DateTime.Now} 执行了一次测试任务");
        }
    }
}
