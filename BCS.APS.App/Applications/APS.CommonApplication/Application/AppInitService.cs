﻿using APS.AdminApplication.Entitys;
using APS.Contracts.Interfaces;
using APS.Utils.Attributes;
using APS.Utils.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.CommonApplication.Application
{
    [Transient]
    public class AppInitService : IAppInitService
    {
        private AdminDbContext adminDb;

        public AppInitService(AdminDbContext adminDb)
        {
            this.adminDb = adminDb;
        }

        public ApiResult SysDbInit()
        {
            var result = ApiResult.Default;
            return result;
        }
    }
}
