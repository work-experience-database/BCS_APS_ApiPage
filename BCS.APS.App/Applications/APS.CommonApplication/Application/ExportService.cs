﻿using APS.Contracts.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.CommonApplication.Application
{
    public class ExportService : IExportService
    {
        [HttpGet("Export"), AllowAnonymous]
        public static FileResult Export()
        {
            //先获取要导出的数据
            var data = new List<string>();
            //创建Excel文件的对象
            HSSFWorkbook book = new HSSFWorkbook();
            //添加一个sheet
            ISheet sheet1 = book.CreateSheet("Sheet1");
            //给sheet1添加第一行的头部标题
            IRow row1 = sheet1.CreateRow(0);
            row1.CreateCell(0).SetCellValue("ID");
            row1.CreateCell(1).SetCellValue("名称");
            //......以此类推
            //将数据逐步写入sheet1各个行
            for (int i = 0; i < data.Count; i++)
            {
                IRow rowtemp = sheet1.CreateRow(i + 1);
                //rowtemp.CreateCell(0).SetCellValue(data[i].Id.ToString());
                //rowtemp.CreateCell(1).SetCellValue(data[i].Name.ToString());
            }
            // 写入到客户端 
            MemoryStream ms = new MemoryStream();
            book.Write(ms);
            ms.Seek(0, SeekOrigin.Begin);
            DateTime time = DateTime.Now;
            string Time = time.ToString("yyMMddHHmmssfff");
            string fileName = "Excel" + Time + ".xls";

            return File(ms, "application/vnd.ms-excel", fileName);
        }

        private static FileResult File(MemoryStream ms, string v, string fileName)
        {
            throw new NotImplementedException();
        }
    }
}
