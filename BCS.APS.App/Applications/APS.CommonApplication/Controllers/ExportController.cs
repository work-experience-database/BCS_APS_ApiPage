﻿using APS.CommonApplication.Application;
using APS.CommonApplication.Model;
using APS.SchedulerApplication.Entitys;
using APS.SchedulerApplication.Interfaces;
using APS.Utils.Helper;
using APS.Utils.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Metadata;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.CommonApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [ApiExplorerSettings(GroupName = "Common")]
    public class ExportController : ControllerBase
    {
        private readonly IMasterDataService _masterDataService;
        public ExportController(IMasterDataService masterDataService)
        {
            _masterDataService = masterDataService;
        }

        /// <summary>
        /// 导入Excel文件
        /// </summary>
        /// <param name="excelFile"></param>
        /// <returns></returns>
        [HttpPost("UploadUserInfo")]
        public IActionResult UploadUserInfo(IFormFile excelFile)
        {
            try
            {
                var postFile = Request.Form.Files[0];
                string extName = Path.GetExtension(postFile.FileName);
                if (!new string[] { ".xls", ".xlsx" }.Contains(extName))
                {
                    return Ok(new
                    {
                        error = 1,
                        msg = "必须是Excel文件"
                    });
                }

                MemoryStream ms = new MemoryStream();
                postFile.CopyTo(ms);
                ms.Position = 0;
                IWorkbook wb = null;
                if (extName.ToLower().Equals(".xls")) // 97-2003版本
                {
                    wb = new HSSFWorkbook(ms);
                }
                else
                {
                    wb = new XSSFWorkbook(ms); // 2007以上版本
                }

                ISheet sheet = wb.GetSheetAt(0);

                //总行数（0开始）
                int totalRow = sheet.LastRowNum;
                // 总列数（1开始）
                int totalColumn = sheet.GetRow(0).LastCellNum;

                //List<Stu> stuList = new();
                for (int i = 1; i <= totalRow; i++)
                {

                    IRow row = sheet.GetRow(i);
                    // 判定第5列的值是不是日期，日期的值类型可以按日期来读，也可以用数据的方式来读
                    var isDate = DateUtil.IsCellDateFormatted(row.GetCell(4));

                    string StuName = row.GetCell(0).StringCellValue;
                    int Sex = row.GetCell(1).StringCellValue == "男" ? 0 : 1;
                    string Phone = ((long)row.GetCell(2).NumericCellValue).ToString();
                    int CId = (int)row.GetCell(3).NumericCellValue;
                    DateTime InDate = row.GetCell(4).DateCellValue;
                    decimal JF = (decimal)row.GetCell(5).NumericCellValue;

                    // 第6列有可能是空的
                    string Pic = "";
                    if (row.GetCell(6) != null)
                    {
                        CellType type = row.GetCell(6).CellType;
                        if (type != CellType.Blank)
                        {
                            Pic = row.GetCell(6).StringCellValue;
                        }
                    }

                    int State = (int)row.GetCell(7).NumericCellValue;
                }

                wb.Close();
                return Ok(new
                {
                    error = 0,
                    importCount = 10,
                    msg = ""
                });
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 导出所有的信息为Excel
        /// </summary>
        /// <returns></returns>
        [HttpPost("ExportExcel")]
        public IActionResult ExportExcel([FromBody] object fromBody)
        {
            List<Dictionary<string, object>> deserializedList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(fromBody.ToString());
            var filePath = NpoiExcelService.ExcelDataExport(deserializedList);
            var contentType = "application/octet-stream"; 
            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            var fileDownloadName = Path.GetFileName(filePath);
            return File(fileBytes, contentType, fileDownloadName);
        }

        /// <summary>
        /// 导出Excel（使用模板）
        /// </summary>
        /// <returns></returns>
        [HttpGet("ExportExcelByTemplate")]
        public IActionResult ExportExcelByTemplate()
        {
            try
            {
                IWorkbook wb = null;
                var template = Directory.GetCurrentDirectory() + @"\wwwroot\Template\template.xlsx";
                // 按模板内容创建 IWorkbook
                using (FileStream fs = new FileStream(template, FileMode.OpenOrCreate))
                {
                    wb = new XSSFWorkbook(fs);
                }

                //var list =  db.Stu.Where(s => s.IsOk).ToList();
                var list = new List<object>();

                ISheet sheet = wb.GetSheetAt(0);
                int i = 1;
                IRow row = null;


                byte[] buffer = null;
                using (MemoryStream ms = new MemoryStream())
                {
                    wb.Write(ms);
                    buffer = ms.ToArray();
                }
                // .xlsx文件对应的Mime信息
                var mime = new FileExtensionContentTypeProvider().Mappings[".xlsx"];
                return File(buffer, mime, "学生信息.xlsx");

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
