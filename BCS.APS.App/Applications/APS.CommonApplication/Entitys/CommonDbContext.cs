﻿using APS.EFCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.CommonApplication.Entitys
{
    public class CommonDbContext : AppDbContext
    {
        public CommonDbContext(DbContextOptions<CommonDbContext> options) : base(options)
        {
        }

        public DbSet<OperateLog> OperateLog { get; set; }
        public DbSet<SAPDemand> SAPDemand { get; set; }
        public DbSet<FSTestTable> FSTable { get; set; }
    }
}
