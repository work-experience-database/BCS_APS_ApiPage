﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace APS.CommonApplication.Entitys
{
    [Table("FSTable", Schema = "APS")]
    //[Table("ODB.FSTable")]
    public class FSTestTable
    {
        public long Id { get; set; }
        [Column("Name")]
        public string FullName { get; set; }
        public DateTime Date { get; set; }
        public bool Deleted { get; set; }
    }
}
