﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.CommonApplication.Entitys
{
    [Table("OperateLog", Schema = "APS")]
    public class OperateLog : DefaultEntityLong
    {
        /// <summary>
        /// 操作类型
        /// </summary>
        public string OperateType { get; set; }

        /// <summary>
        /// 操作描述
        /// </summary>
        public string Description { get; set; }
    }
}
