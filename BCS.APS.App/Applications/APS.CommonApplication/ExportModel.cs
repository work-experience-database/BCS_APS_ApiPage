﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.CommonApplication
{
    public class ExportModel
    {
        public string FileName { get; set; }
        public string FileData { get; set; }
    }
}
