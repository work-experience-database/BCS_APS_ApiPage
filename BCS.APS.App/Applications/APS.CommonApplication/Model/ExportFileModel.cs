﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.CommonApplication.Model
{
    public class ExportFileModel
    {
        public string FileName { get; set; }
        public string FileData { get; set; }
    }
}
