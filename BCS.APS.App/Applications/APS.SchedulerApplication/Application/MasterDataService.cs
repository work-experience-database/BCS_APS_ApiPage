﻿using APS.SchedulerApplication.Entitys;
using APS.SchedulerApplication.Interfaces;
using APS.SchedulerApplication.Model;
using APS.Utils.Attributes;
using APS.Utils.Helper;
using APS.Utils.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Application
{
    [Transient]
    public class MasterDataService : IMasterDataService
    {
        private APSDbContext _apsDbContext;
        public MasterDataService(APSDbContext apsDbContext) { _apsDbContext = apsDbContext; }


        public List<MaterialNumberLineMapRelations> GetMaterialInfo()
        {
            List<MaterialNumberLineMapRelations> materialNumberLineMapRelations = [];
            var result = _apsDbContext.MaterialNumberLineMapRelations.ToList();
            materialNumberLineMapRelations = result.OrderBy(x => x.MaterialNumber).OrderBy(x => x.Order).ToList();

            return materialNumberLineMapRelations;
        }
        public List<MaterialNumberLineMapRelations> GetMaterialInfo(MaterialInfoViewModel materialInfoViewModel)
        {
            List<MaterialNumberLineMapRelations> materialNumberLineMapRelations = [];
            var result = _apsDbContext.MaterialNumberLineMapRelations.ToList();
            if (result.Count != 0)
            {
                if (!string.IsNullOrEmpty(materialInfoViewModel.Line))
                {
                    result = result.Where(x => x.Line.Replace(" ", "").ToString() == materialInfoViewModel.Line.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(materialInfoViewModel.PN))
                {
                    result = result.Where(x => x.MaterialNumber.Replace(" ", "").ToString() == materialInfoViewModel.PN.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(materialInfoViewModel.Area))
                {
                    result = result.Where(x => x.Area.Replace(" ", "").ToString() == materialInfoViewModel.Area.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(materialInfoViewModel.User))
                {
                    result = result.Where(x => x.User.Replace(" ", "").ToString() == materialInfoViewModel.User.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(materialInfoViewModel.SPQ))
                {
                    result = result.Where(x => x.SPQ.ToString() == materialInfoViewModel.SPQ.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(materialInfoViewModel.UPH))
                {
                    result = result.Where(x => x.UPH.Replace(" ", "").ToString() == materialInfoViewModel.UPH.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(materialInfoViewModel.HC))
                {
                    result = result.Where(x => x.HC.ToString() == materialInfoViewModel.HC.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(materialInfoViewModel.Order))
                {
                    result = result.Where(x => x.Order.ToString() == materialInfoViewModel.Order.Replace(" ", "").ToUpper()).ToList();
                }
                materialNumberLineMapRelations = result.OrderBy(x => x.MaterialNumber).ThenBy(x => x.Order).ToList();
            }
            return materialNumberLineMapRelations;
        }
        public List<MaterialNumberLineMapRelations> AddMaterialInfo(MaterialInfoViewModel materialInfoViewModel)
        {
            var queryEntity = _apsDbContext.MaterialMainLine.Where(x => x.MaterialNumber == materialInfoViewModel.PN.Replace(" ", "").ToUpper()).FirstOrDefault();
            MaterialNumberLineMapRelations newEntity = new MaterialNumberLineMapRelations();
            newEntity.MaterialNumber = materialInfoViewModel.PN.Replace(" ", "").ToUpper();
            newEntity.Plant = materialInfoViewModel.Plant;
            newEntity.User = materialInfoViewModel.User;
            newEntity.Line = materialInfoViewModel.Line;
            newEntity.Order = materialInfoViewModel.Order;
            newEntity.Area = materialInfoViewModel.Area;
            newEntity.SPQ = materialInfoViewModel.SPQ;
            newEntity.UPH = materialInfoViewModel.UPH;
            newEntity.HC = materialInfoViewModel.HC;
            newEntity.Description = materialInfoViewModel.Description;
            newEntity.CreateTime = DateTime.Now;
            newEntity.UpdateTime = DateTime.Now;

            if (queryEntity == null)
            {
                _apsDbContext.MaterialNumberLineMapRelations.Add(newEntity);
            }
            else
            {
                _apsDbContext.MaterialNumberLineMapRelations.Update(newEntity);
            }
            //_apsDbContext.SaveChanges();
            var result = _apsDbContext.MaterialNumberLineMapRelations.ToList();
            return result;
        }
        public List<MaterialNumberLineMapRelations> UpdateMaterialInfoById(MaterialInfoViewModel materialInfoViewModel)
        {
            List<MaterialNumberLineMapRelations> materialNumberLineMapRelations  = [];
            int id = int.Parse(materialInfoViewModel.Id);
            var queryEntity = _apsDbContext.MaterialNumberLineMapRelations.Where(x => x.Id == id).FirstOrDefault();
            if (queryEntity != null)
            {
                queryEntity.Line = materialInfoViewModel.Line;
                queryEntity.Area = materialInfoViewModel.Area;
                queryEntity.User = materialInfoViewModel.User;
                queryEntity.SPQ = materialInfoViewModel.SPQ == null ? "0" : materialInfoViewModel.SPQ;
                queryEntity.UPH = materialInfoViewModel.UPH == null ? "0" : materialInfoViewModel.UPH;
                queryEntity.HC = materialInfoViewModel.HC == null ? "0" : materialInfoViewModel.HC;
                queryEntity.Description = materialInfoViewModel.Description;
                _apsDbContext.MaterialNumberLineMapRelations.Update(queryEntity);
                materialNumberLineMapRelations = _apsDbContext.MaterialNumberLineMapRelations.ToList();
            }
            else
            {
                var newEntity = AutoMapperHelper.Map<MaterialInfoViewModel, MaterialNumberLineMapRelations>(materialInfoViewModel);
                materialNumberLineMapRelations.Add(newEntity);
                _apsDbContext.MaterialNumberLineMapRelations.Update(newEntity);
            }
            _apsDbContext.SaveChanges();
            return materialNumberLineMapRelations;
        }


        public List<FactoryCalendar> GetFactoryCalendarInfo()
        {
            List<FactoryCalendar> factoryCalendars = [];
            var result = _apsDbContext.FactoryCalendar.ToList();

            if (result.Count != 0)
            {
                factoryCalendars = result;
            }
            return factoryCalendars;
        }
        public List<FactoryCalendar> UpdateFactoryCalendarInfo(FactoryCalendarViewModel factoryCalendarViewModel)
        {
            var queryEntity = _apsDbContext.FactoryCalendar.Where(x => x.Date == factoryCalendarViewModel.Date).FirstOrDefault();
            var newEntity = AutoMapperHelper.Map<FactoryCalendarViewModel, FactoryCalendar>(factoryCalendarViewModel);
            if (queryEntity == null)
            {
                newEntity.CreateTime = DateTime.Now;
                newEntity.UpdateTime = DateTime.Now;
                _apsDbContext.FactoryCalendar.Add(newEntity);
            }
            else
            {
                queryEntity.UpdateTime = DateTime.Now;
                queryEntity.IsWork = factoryCalendarViewModel.IsWork;
                queryEntity.Type = factoryCalendarViewModel.Type;
                queryEntity.Comments = factoryCalendarViewModel.Comments;
                queryEntity.KeyUser = factoryCalendarViewModel.KeyUser;
                _apsDbContext.FactoryCalendar.Update(queryEntity);
            }
            _apsDbContext.SaveChanges();
            var result = _apsDbContext.FactoryCalendar.ToList();
            return result;
        }


        public List<EachDayHumanQuantityViewModel> GetHCResource()
        {
            List<EachDayHumanQuantity> eachDayHumanQuantities = [];
            eachDayHumanQuantities = _apsDbContext.EachDayHumanQuantity.ToList();
            List<EachDayHumanQuantityViewModel> eachDayHumanQuantityViewModels = [];
            foreach (var item in eachDayHumanQuantities)
            {
                EachDayHumanQuantityViewModel eachDayHumanQuantityViewModel = new EachDayHumanQuantityViewModel();
                eachDayHumanQuantityViewModel.Date = item.Date.ToString("yyyy-MM-dd");
                eachDayHumanQuantityViewModel.CostCenter = item.CostCenter == null ? "" : item.CostCenter.ToString();
                eachDayHumanQuantityViewModel.EmployeeGroup = item.EmployeeGroup;
                eachDayHumanQuantityViewModel.Quantity = item.Quantity.ToString();
                eachDayHumanQuantityViewModel.OEE = item.OEE.ToString();
                eachDayHumanQuantityViewModel.KeyUser = item.KeyUser == null ? "" : item.KeyUser.ToString();
                eachDayHumanQuantityViewModel.Remark = item.Remark == null ? "" : item.Remark.ToString();
                eachDayHumanQuantityViewModels.Add(eachDayHumanQuantityViewModel);
            }
            return eachDayHumanQuantityViewModels;
        }
        public List<EachDayHumanQuantity> GetHCResourceForJob(string startDateTime, string endDateTime, string costCenter)
        {
            HttpClient _httpClient = new HttpClient();
            _httpClient.Timeout = TimeSpan.FromSeconds(120);
            List<HCResourceModel> hCResourceModels = [];
            List<EachDayHumanQuantity> eachDayHumanQuantities = [];
            //var url = $"http://localhost:8500/api/Sap/GetHCResourceInfo?startDateTime={startDateTime}&endDateTime={endDateTime}&costCenter={costCenter}";
            var url = $"http://10.2.34.21:8100/api/Sap/GetHCResourceInfo?startDateTime={startDateTime}&endDateTime={endDateTime}&costCenter={costCenter}";
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            HttpResponseMessage response = _httpClient.Send(request);
            try
            {
                // 确保HTTP响应成功  
                HttpResponseMessage resmsg = response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    var responseBody = response.Content.ReadAsStream();
                    StreamReader reader = new(responseBody);
                    string text = reader.ReadToEnd();
                    var apiResult = JsonConvert.DeserializeObject<ApiResult>(text);
                    if (apiResult != null && apiResult.Data != null)
                    {
                        hCResourceModels = JsonConvert.DeserializeObject<List<HCResourceModel>>(apiResult.Data.ToString());
                    }
                }

                if (hCResourceModels.Count == 0) { return eachDayHumanQuantities; }
                foreach (var item in hCResourceModels)
                {
                    //只关注D类人
                    if (!item.PERSG.Replace(" ", "").Equals("D", StringComparison.CurrentCultureIgnoreCase)) { continue; }
                    if (string.IsNullOrEmpty(item.DATUM.Replace(" ", ""))) { continue; }
                    var date = DateTime.ParseExact(item.DATUM.Replace(" ", "").ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                    EachDayHumanQuantity eachDayHumanQuantity = new EachDayHumanQuantity();
                    eachDayHumanQuantity.Date = date;
                    eachDayHumanQuantity.CostCenter = item.KOSTL;
                    eachDayHumanQuantity.EmployeeGroup = item.PERSG;
                    eachDayHumanQuantity.Quantity = double.Parse(item.COUNTER.ToString());
                    eachDayHumanQuantity.OEE = 0.9;
                    eachDayHumanQuantity.KeyUser = "Admin";
                    eachDayHumanQuantity.CreateTime = DateTime.Now;
                    eachDayHumanQuantity.UpdateTime = DateTime.Now;
                    eachDayHumanQuantities.Add(eachDayHumanQuantity);
                }

                if (eachDayHumanQuantities.Count != 0)
                {
                    foreach (var item in eachDayHumanQuantities)
                    {
                        var queryEntity = _apsDbContext.EachDayHumanQuantity.Where(x => x.Date == item.Date).FirstOrDefault();
                        var newEntity = AutoMapperHelper.Map<EachDayHumanQuantity, EachDayHumanQuantity>(item);
                        if (queryEntity == null)
                        {
                            _apsDbContext.EachDayHumanQuantity.Update(newEntity);
                        }
                        else
                        {
                            _apsDbContext.EachDayHumanQuantity.Update(queryEntity);
                        }
                    }
                    _apsDbContext.SaveChanges();
                }
                return eachDayHumanQuantities;
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
                return eachDayHumanQuantities;
            }
        }


        public List<LineMaintain> GetLineInfo()
        {
            List<LineMaintain> lineMaintains = [];
            var result = _apsDbContext.LineMaintain.ToList();
            lineMaintains = result.OrderBy(x => x.Line).ToList();
            return lineMaintains;
        }
        public List<LineMaintain> GetLineInfo(LineMaintainViewModel lineMaintainViewModel)
        {
            List<LineMaintain> lineMaintains = [];
            var result = _apsDbContext.LineMaintain.ToList();
            if (result.Count != 0)
            {
                if (!string.IsNullOrEmpty(lineMaintainViewModel.Line))
                {
                    result = result.Where(x => x.Line.Replace(" ", "").ToString() == lineMaintainViewModel.Line.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(lineMaintainViewModel.Area))
                {
                    result = result.Where(x => x.Area.Replace(" ", "").ToString() == lineMaintainViewModel.Area.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(lineMaintainViewModel.IsWork))
                {
                    var _enable = lineMaintainViewModel.IsWork.Replace(" ", "").ToUpper() == "OK";
                    result = result.Where(x => x.Enable == _enable).ToList();
                }
                if (!string.IsNullOrEmpty(lineMaintainViewModel.User))
                {
                    result = result.Where(x => x.KeyUser.Replace(" ", "").ToString() == lineMaintainViewModel.User.Replace(" ", "").ToUpper()).ToList();
                }
                lineMaintains = result.OrderBy(x => x.Line).ToList();
            }
            return lineMaintains;
        }
        public List<LineMaintain> AddLineInfo(LineMaintainViewModel lineMaintainViewModel)
        {
            var queryEntity = _apsDbContext.LineMaintain.Where(x => x.Line == lineMaintainViewModel.Line).FirstOrDefault();
            var newEntity = AutoMapperHelper.Map<LineMaintainViewModel, LineMaintain>(lineMaintainViewModel);
            newEntity.StartTime = DateTime.Parse(lineMaintainViewModel.StartDate);
            newEntity.EndTime = DateTime.Parse(lineMaintainViewModel.EndDate);
            if (queryEntity == null)
            {
                newEntity.CreateTime = DateTime.Now;
                newEntity.UpdateTime = DateTime.Now;
                _apsDbContext.LineMaintain.Update(newEntity);
            }
            else
            {
                queryEntity.StartTime = DateTime.Parse(lineMaintainViewModel.StartDate);
                queryEntity.EndTime = DateTime.Parse(lineMaintainViewModel.EndDate);
                queryEntity.Enable = lineMaintainViewModel.IsWork == "OK";
                queryEntity.KeyUser = lineMaintainViewModel.User;
                _apsDbContext.LineMaintain.Update(queryEntity);
            }
            _apsDbContext.SaveChanges();
            var result = _apsDbContext.LineMaintain.ToList();
            return result;
        }
        public List<LineMaintain> UpdateLineInfo(LineMaintainViewModel lineMaintainViewModel)
        {
            var queryEntity = _apsDbContext.LineMaintain.Where(x => x.Line == lineMaintainViewModel.Line).FirstOrDefault();
            var newEntity = AutoMapperHelper.Map<LineMaintainViewModel, LineMaintain>(lineMaintainViewModel);
            if (queryEntity == null)
            {
                newEntity.CreateTime = DateTime.Now;
                newEntity.UpdateTime = DateTime.Now;
                _apsDbContext.LineMaintain.Add(newEntity);
            }
            else
            {
                queryEntity.StartTime = DateTime.Parse(lineMaintainViewModel.StartDate);
                queryEntity.EndTime = DateTime.Parse(lineMaintainViewModel.EndDate);
                queryEntity.Area = lineMaintainViewModel.Area;
                queryEntity.Enable = lineMaintainViewModel.IsWork == "OK";
                queryEntity.KeyUser = lineMaintainViewModel.User;
                _apsDbContext.LineMaintain.Update(queryEntity);
            }
            _apsDbContext.SaveChanges();
            var result = _apsDbContext.LineMaintain.ToList();
            return result;
        }


        public List<SAPMaterial> GetMaterialInfoFromSapThenInsertIntoAPSDb()
        {
            HttpClient _httpClient = new HttpClient();
            _httpClient.Timeout = TimeSpan.FromSeconds(120);
            List<SAPMaterial> sapMaterials = [];
            List<SapOriginalMaterial> sapOriginalMaterials = [];

            var uri = "http://10.2.34.21:8100/api/sap/GetMaterialInfo";
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            HttpResponseMessage response = _httpClient.Send(request);
            try
            {
                // 确保HTTP响应成功  
                HttpResponseMessage resmsg = response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    var responseBody = response.Content.ReadAsStream();
                    StreamReader reader = new(responseBody);
                    string text = reader.ReadToEnd();
                    var apiResult = JsonConvert.DeserializeObject<ApiResult>(text);
                    if (apiResult != null && apiResult.Data != null)
                    {
                        sapOriginalMaterials = JsonConvert.DeserializeObject<List<SapOriginalMaterial>>(apiResult.Data.ToString());
                    }
                }

                if (sapOriginalMaterials.Count == 0) { return sapMaterials; }

                foreach (var item in sapOriginalMaterials)
                {
                    SAPMaterial sapMaterial = new()
                    {
                        Plant = item.WERKS,
                        MaterialNumber = item.MATNR,
                        MaterialType = item.MTART,
                        MaterialDescription = item.MAKTX,
                        ProcurementType = item.BESKZ,
                        SpecialProcurementType = item.SOBSL,
                        ProductionSupervisor = item.FEVOR,
                        ProductionSupervisorName = item.TXT,
                        PostToInspectionStock = item.INSMK,
                        MRPController = item.DISPO,
                        MRPControllerName = item.DSNAM,
                        PackingMaterialNumber = item.PMATNR1,
                        PackingQuantityOfSPQ = item.TRGQTY1,
                    };
                    sapMaterials.Add(sapMaterial);
                }

                var result = _apsDbContext.SAPMaterial.ToList();
                if (sapMaterials.Count != 0 && result.Count != 0)
                {
                    foreach (var item in sapMaterials)
                    {
                        var queryEntity = result.Where(x => x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).FirstOrDefault();
                        var newEntity = AutoMapperHelper.Map<SAPMaterial, SAPMaterial>(item);
                        if (queryEntity == null)
                        {
                            newEntity.CreateTime = DateTime.Now;
                            newEntity.UpdateTime = DateTime.Now;
                            _apsDbContext.SAPMaterial.Add(newEntity);
                        }
                        else
                        {
                            queryEntity.UpdateTime = DateTime.Now;
                            _apsDbContext.SAPMaterial.Update(queryEntity);
                        }
                    }
                    _apsDbContext.SaveChanges();
                }
                return sapMaterials;
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
                return sapMaterials;
            }
        }


        public List<SapBomModel> SendMasterDataToSapBom()
        {
            var result = _apsDbContext.MaterialMainLine.ToList();
            List<SapBomModel> sapBomModels = new List<SapBomModel>();
            if (result.Count != 0)
            {
                foreach (var item in result)
                {
                    SapBomModel sapBomModel = new SapBomModel();
                    sapBomModel.MATNR = item.MaterialNumber;
                    sapBomModel.WERKS = item.Plant;
                    sapBomModel.ARBPL = item.MainLine;
                    sapBomModel.ALPRF = item.MainLine != null ? "1" : "2";
                    sapBomModel.UPH = item.UPH.ToString();
                    sapBomModel.LKENZ = item.Deleted.ToString();
                    sapBomModels.Add(sapBomModel);
                }
            }

            HttpClient _httpClient = new HttpClient();
            _httpClient.Timeout = TimeSpan.FromSeconds(120);
            List<SAPMaterial> sapMaterials = [];
            List<SapOriginalMaterial> sapOriginalMaterials = [];

            var uri = "http://10.2.34.21:8100/api/Sap/SendScheduleInfoToSap";
            var request = new HttpRequestMessage(HttpMethod.Post, uri);
            HttpResponseMessage response = _httpClient.Send(request);
            try
            {
                HttpResponseMessage resmsg = response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    var responseBody = response.Content.ReadAsStream();
                    StreamReader reader = new(responseBody);
                    string text = reader.ReadToEnd();
                    var apiResult = JsonConvert.DeserializeObject<ApiResult>(text);
                    if (apiResult.IsSuccess)
                    {
                        return sapBomModels;
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
            }
            return sapBomModels;
        }
        public List<SapBomListModel> GetSapBomInfo(SapBomListModel sapBomListModel)
        {
            HttpClient _httpClient = new HttpClient();
            _httpClient.Timeout = TimeSpan.FromSeconds(120);
            List<SapBomListModel> sapBomListModels = [];
            var url = $"http://10.2.34.21:8100/api/Sap/GetSapBomInfo";
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            HttpResponseMessage response = _httpClient.Send(request);
            try
            {
                // 确保HTTP响应成功  
                HttpResponseMessage resmsg = response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    var responseBody = response.Content.ReadAsStream();
                    StreamReader reader = new(responseBody);
                    string text = reader.ReadToEnd();
                    var apiResult = JsonConvert.DeserializeObject<ApiResult>(text);
                    if (apiResult != null && apiResult.Data != null)
                    {
                        sapBomListModels = JsonConvert.DeserializeObject<List<SapBomListModel>>(apiResult.Data.ToString());
                    }
                }

                if (sapBomListModels.Count == 0) { return sapBomListModels; }

                if (!string.IsNullOrEmpty(sapBomListModel.MATNR))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.MATNR.Replace(" ", "").ToUpper() == sapBomListModel.MATNR.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(sapBomListModel.WERKS))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.WERKS.Replace(" ", "").ToUpper() == sapBomListModel.WERKS.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(sapBomListModel.POSNR))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.POSNR.Replace(" ", "").ToUpper() == sapBomListModel.POSNR.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(sapBomListModel.IDNRK))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.IDNRK.Replace(" ", "").ToUpper() == sapBomListModel.IDNRK.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(sapBomListModel.MENGE))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.MENGE.Replace(" ", "").ToUpper() == sapBomListModel.MENGE.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(sapBomListModel.MEINS))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.MEINS.Replace(" ", "").ToUpper() == sapBomListModel.MEINS.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(sapBomListModel.SOBSL))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.SOBSL.Replace(" ", "").ToUpper() == sapBomListModel.SOBSL.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(sapBomListModel.DUMPS))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.DUMPS.Replace(" ", "").ToUpper() == sapBomListModel.DUMPS.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(sapBomListModel.DATUV))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.DATUV.Replace(" ", "").ToUpper() == sapBomListModel.DATUV.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(sapBomListModel.DATUB))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.DATUB.Replace(" ", "").ToUpper() == sapBomListModel.DATUB.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(sapBomListModel.MMSTA))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.MMSTA.Replace(" ", "").ToUpper() == sapBomListModel.MMSTA.Replace(" ", "").ToUpper()).ToList();
                }
                if (!string.IsNullOrEmpty(sapBomListModel.AUSCH))
                {
                    sapBomListModels = sapBomListModels.Where(x => x.AUSCH.Replace(" ", "").ToUpper() == sapBomListModel.AUSCH.Replace(" ", "").ToUpper()).ToList();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
            }
            return sapBomListModels;
        }


        public string DownloadMaterialInfo(DownloadModel downloadModel)
        {
            string excelName = "Template";
            string excelPath = string.Empty;
            try
            {
                //首先创建Excel文件对象
                var workbook = new HSSFWorkbook();

                //创建工作表，也就是Excel中的sheet，给工作表赋一个名称(Excel底部名称)
                var sheet = workbook.CreateSheet("sheet1");

                sheet.ForceFormulaRecalculation = false;//TODO:是否开始Excel导出后公式仍然有效（非必须）

                //二级标题列样式设置
                var headTopStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 15, true, 700, "楷体", true, false, false, true, FillPattern.SolidForeground, HSSFColor.Grey25Percent.Index, HSSFColor.Black.Index,
                FontUnderlineType.None, FontSuperScript.None, false);
                var headerlist = downloadModel.title.ToArray();
                var maxRows = headerlist.Count();
                var row = NpoiExcelExportHelper._.CreateRow(sheet, 0, 24);  //第二行
                var cell = row.CreateCell(0);
                for (var i = 0; i < headerlist.Length; i++)
                {
                    cell = NpoiExcelExportHelper._.CreateCells(row, headTopStyle, i, headerlist[i]);
                    sheet.SetColumnWidth(i, 5000); //设置单元格宽度
                }

                //单元格边框样式
                var cellStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 10, true, 400);

                var currentDate = DateTime.Now;
                int rowNumber = 0;
                var listData = downloadModel.data;
                foreach (var diclist in listData)
                {
                    rowNumber++;
                    row = NpoiExcelExportHelper._.CreateRow(sheet, rowNumber, 24);  //第二行
                    int columnNumber = 0;
                    List<int> existedColumnIndexlist = [];
                    foreach (var key in diclist.Keys.OrderBy(x => x.Length))
                    {
                        var content = diclist[key] == null ? "" : diclist[key].ToString();
                        var columnIndex = headerlist.ToList().FindIndex(x => x.Replace(" ", "").ToUpper() == key.Replace(" ", "").ToUpper());
                        if (columnIndex == -1) { continue; }
                        cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, columnIndex, content);
                        existedColumnIndexlist.Add(columnIndex);
                    }

                    //补充空白
                    var newMaxRowsCopy = maxRows;
                    if (maxRows >= 1)
                    {
                        for (int i = 0; i < newMaxRowsCopy; i++)
                        {
                            if (!existedColumnIndexlist.Contains(i))
                            {
                                cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, i, string.Empty);
                            }
                        }
                    }
                }

                string folder = DateTime.Now.ToString("yyyyMMdd");


                var uploadPath = AppDomain.CurrentDomain.BaseDirectory + "UploadFile\\" + folder + "\\";

                //excel保存文件名
                string excelFileName = excelName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";

                //创建目录文件夹
                if (!Directory.Exists(uploadPath))
                {
                    Directory.CreateDirectory(uploadPath);
                }

                //Excel的路径及名称
                excelPath = uploadPath + excelFileName;

                //使用FileStream文件流来写入数据（传入参数为：文件所在路径，对文件的操作方式，对文件内数据的操作）
                var fileStream = new FileStream(excelPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                //向Excel文件对象写入文件流，生成Excel文件
                workbook.Write(fileStream);

                //关闭文件流
                fileStream.Close();

                //释放流所占用的资源
                fileStream.Dispose();

                //excel文件保存的相对路径，提供前端下载
                var relativePositioning = "/UploadFile/" + folder + "/" + excelFileName;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return excelPath;
        }
        public string DownloadLineInfo(DownloadModel downloadModel)
        {
            string excelName = "Template";
            string excelPath = string.Empty;
            try
            {
                //首先创建Excel文件对象
                var workbook = new HSSFWorkbook();

                //创建工作表，也就是Excel中的sheet，给工作表赋一个名称(Excel底部名称)
                var sheet = workbook.CreateSheet("sheet1");

                sheet.ForceFormulaRecalculation = false;//TODO:是否开始Excel导出后公式仍然有效（非必须）

                //二级标题列样式设置
                var headTopStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 15, true, 700, "楷体", true, false, false, true, FillPattern.SolidForeground, HSSFColor.Grey25Percent.Index, HSSFColor.Black.Index,
                FontUnderlineType.None, FontSuperScript.None, false);
                var headerlist = downloadModel.title.ToArray();
                var maxRows = headerlist.Count();
                var row = NpoiExcelExportHelper._.CreateRow(sheet, 0, 24);  //第二行
                var cell = row.CreateCell(0);
                for (var i = 0; i < headerlist.Length; i++)
                {
                    cell = NpoiExcelExportHelper._.CreateCells(row, headTopStyle, i, headerlist[i]);
                    sheet.SetColumnWidth(i, 5000); //设置单元格宽度
                }

                //单元格边框样式
                var cellStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 10, true, 400);

                var currentDate = DateTime.Now;
                int rowNumber = 0;
                var listData = downloadModel.data;
                foreach (var diclist in listData)
                {
                    rowNumber++;
                    row = NpoiExcelExportHelper._.CreateRow(sheet, rowNumber, 24);  //第二行
                    int columnNumber = 0;
                    List<int> existedColumnIndexlist = [];
                    foreach (var key in diclist.Keys.OrderBy(x => x.Length))
                    {
                        var content = diclist[key] == null ? "" : diclist[key].ToString();
                        var columnIndex = headerlist.ToList().FindIndex(x => x.Replace(" ", "").ToUpper() == key.Replace(" ", "").ToUpper());
                        if (columnIndex == -1) { continue; }
                        cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, columnIndex, content);
                        existedColumnIndexlist.Add(columnIndex);
                    }

                    //补充空白
                    var newMaxRowsCopy = maxRows;
                    if (maxRows >= 1)
                    {
                        for (int i = 0; i < newMaxRowsCopy; i++)
                        {
                            if (!existedColumnIndexlist.Contains(i))
                            {
                                cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, i, string.Empty);
                            }
                        }
                    }
                }

                string folder = DateTime.Now.ToString("yyyyMMdd");


                var uploadPath = AppDomain.CurrentDomain.BaseDirectory + "UploadFile\\" + folder + "\\";

                //excel保存文件名
                string excelFileName = excelName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";

                //创建目录文件夹
                if (!Directory.Exists(uploadPath))
                {
                    Directory.CreateDirectory(uploadPath);
                }

                //Excel的路径及名称
                excelPath = uploadPath + excelFileName;

                //使用FileStream文件流来写入数据（传入参数为：文件所在路径，对文件的操作方式，对文件内数据的操作）
                var fileStream = new FileStream(excelPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                //向Excel文件对象写入文件流，生成Excel文件
                workbook.Write(fileStream);

                //关闭文件流
                fileStream.Close();

                //释放流所占用的资源
                fileStream.Dispose();

                //excel文件保存的相对路径，提供前端下载
                var relativePositioning = "/UploadFile/" + folder + "/" + excelFileName;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return excelPath;
        }
    }
}
