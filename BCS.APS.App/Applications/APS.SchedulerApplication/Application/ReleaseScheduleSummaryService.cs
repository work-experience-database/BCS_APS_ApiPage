﻿using APS.SchedulerApplication.Entitys;
using APS.SchedulerApplication.Interfaces;
using APS.SchedulerApplication.Model;
using APS.Utils.Attributes;
using APS.Utils.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Application
{
    [Transient]
    public class ReleaseScheduleSummaryService : IReleaseScheduleSummaryService
    {
        private APSDbContext _apsDbContext;
        static readonly HttpClient _httpClient = new HttpClient();

        public ReleaseScheduleSummaryService(APSDbContext apsDbContext)
        {
            _apsDbContext = apsDbContext;
        }

        public LineAndKeyUserSelectedModel GetLineAndKeyUserSelectedModelInfo()
        {
            var result = _apsDbContext.MaterialMainLine.Where(x => !x.Deleted).ToList();
            LineAndKeyUserSelectedModel lineAndKeyUserSelectedModel = new LineAndKeyUserSelectedModel();
            if (result.Count != 0)
            {
                lineAndKeyUserSelectedModel.Lines = result.Select(x => x.MainLine).Distinct().OrderBy(x => x).ToList();
                lineAndKeyUserSelectedModel.KeyUsers = result.Select(x => x.KeyUser).Distinct().OrderBy(x => x).ToList();
            }
            return lineAndKeyUserSelectedModel;
        }

        /// <summary>
        /// 获取即将释放给Sap的排程记录
        /// </summary>
        /// <param name="releaseSchedulerSummaryToSapViewModel"></param>
        /// <returns></returns>
        public List<T_PLDORD> GetSchedulerSummaryInfoReleaseToSap(ReleaseSchedulerSummaryToSapViewModel releaseSchedulerSummaryToSapViewModel)
        {
            var lines = releaseSchedulerSummaryToSapViewModel.Line.Where(x => !string.IsNullOrEmpty(x) && x.Length >= 2).Select(x => x.Replace(" ", "").ToUpper()).Distinct().ToList();
            var keyUsers = releaseSchedulerSummaryToSapViewModel.PP.Where(x => string.IsNullOrEmpty(x) && x.Length >= 2).Select(x => x.Replace(" ", "").ToUpper()).Distinct().ToList();
            List<T_PLDORD> lst = new List<T_PLDORD>();
            List<SchedulerSummary> schedulerSummaries = new List<SchedulerSummary>();
            if (releaseSchedulerSummaryToSapViewModel.selectDate.Count != 2) { return lst; }

            var startDate = DateTime.Parse(releaseSchedulerSummaryToSapViewModel.selectDate[0]);
            var endDate = DateTime.Parse(releaseSchedulerSummaryToSapViewModel.selectDate[1]);
            schedulerSummaries = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= startDate.Date && x.SchedulerDate < endDate.AddDays(1).Date).ToList();

            if (lines.Count != 0)
            {
                schedulerSummaries = schedulerSummaries.Where(x => lines.Contains(x.Line.Replace(" ", "").ToUpper())).ToList();
            }

            if (keyUsers.Count != 0)
            {
                schedulerSummaries = schedulerSummaries.Where(x => keyUsers.Contains(x.KeyUser.Replace(" ", "").ToUpper())).ToList();
            }

            if (schedulerSummaries.Count != 0)
            {
                foreach (var item in schedulerSummaries)
                {
                    T_PLDORD t_PLDORD = new T_PLDORD();
                    t_PLDORD.ORDER = item.SchedulerId;
                    t_PLDORD.MATNR = item.MaterialNumber;
                    t_PLDORD.WERKS = item.Plant;
                    t_PLDORD.ARBPL = item.Line;
                    t_PLDORD.PSTTR = item.SchedulerDate.Date.ToString("yyyy-MM-dd");
                    t_PLDORD.PEDTR = item.SchedulerDate.Date.ToString("yyyy-MM-dd");
                    t_PLDORD.GSMNG = item.PlanedQuantity.ToString();
                    t_PLDORD.PLNUM = item.PlanOrder == null ? "" : item.PlanOrder.ToString();
                    t_PLDORD.TYPE = string.Empty;
                    t_PLDORD.MESSAGE = string.Empty;
                    lst.Add(t_PLDORD);
                }
            }
            return lst;
        }

        /// <summary>
        /// 将排程数据发送至Sap从而获取PlanOrder
        /// </summary>
        /// <param name="releaseSchedulerSummaryToSapViewModel"></param>
        /// <returns></returns>
        public List<T_PLDORD> ExecuteSchedulerSummaryReleaseToSap(ReleaseSchedulerSummaryToSapViewModel releaseSchedulerSummaryToSapViewModel)
        {
            var lst = GetSchedulerSummaryInfoReleaseToSap(releaseSchedulerSummaryToSapViewModel);
            //将结果发送给SAP获取PlanOrder
            ReleaseToSapRFCModel releaseToSapRFCModel = new ReleaseToSapRFCModel();
            releaseToSapRFCModel.T_PLDORDList = lst;
            releaseToSapRFCModel.IV_WERKS = "535A";
            releaseToSapRFCModel.IV_PSTTR = DateTime.Parse(releaseSchedulerSummaryToSapViewModel.selectDate[0]).ToString("yyyy-MM-dd");
            releaseToSapRFCModel.IV_PEDTR = DateTime.Parse(releaseSchedulerSummaryToSapViewModel.selectDate[1]).ToString("yyyy-MM-dd");

            var jsonContent = JsonConvert.SerializeObject(releaseToSapRFCModel);
            var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
            var uri = "http://10.2.34.21:8100/api/sap/GetPlanOrder";
            //var uri = "http://localhost:63732//api/sap/GetPlanOrder";
            var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

            HttpResponseMessage response = _httpClient.Send(request);

            // 确保HTTP响应成功  
            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                var responseBody = response.Content.ReadAsStream();
                StreamReader reader = new(responseBody);
                string text = reader.ReadToEnd();
                var result = JsonConvert.DeserializeObject<ApiResult>(text);
                lst.Clear();
                if (result.Data != null)
                {
                    lst = JsonConvert.DeserializeObject<List<T_PLDORD>>(result.Data.ToString());

                    if (lst.Count() != 0)
                    {
                        //将Plan Order保存至APS数据库
                        foreach (var item in lst)
                        {
                            var queryResult = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerId == item.ORDER.Replace(" ", "").ToUpper()).FirstOrDefault();
                            if (queryResult != null)
                            {
                                queryResult.ReleaseToSAP = DateTime.Now;
                                queryResult.Status = SchedulerStatus.ReleasedToSap.ToString();
                                queryResult.PlanOrder = item.PLNUM == null ? string.Empty : item.PLNUM?.ToString();
                                _apsDbContext.SchedulerSummary.Update(queryResult);
                            }
                        }
                        _apsDbContext.SaveChanges();
                    }
                }
                return lst;
            }
            else
            {
                lst.Clear();
                return lst;
            }
        }


        /// <summary>
        /// 获取排程数据，释放至IMES
        /// </summary>
        /// <param name="releaseSchedulerSummaryToIMesViewModel"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public List<T_PLDORD> GetSchedulerSummaryInfoReleaseToIMes(ReleaseSchedulerSummaryToIMesViewModel releaseSchedulerSummaryToIMesViewModel)
        {
            var lines = releaseSchedulerSummaryToIMesViewModel.Line.Where(x => !string.IsNullOrEmpty(x) && x.Length >= 2).Select(x => x.Replace(" ", "").ToUpper()).Distinct().ToList();
            var keyUsers = releaseSchedulerSummaryToIMesViewModel.PP.Where(x => string.IsNullOrEmpty(x) && x.Length >= 2).Select(x => x.Replace(" ", "").ToUpper()).Distinct().ToList();
            List<T_PLDORD> lst = [];
            List<SchedulerSummary> schedulerSummaries = [];
            if (releaseSchedulerSummaryToIMesViewModel.selectDate.Count != 2) { return lst; }

            var startDate = DateTime.Parse(releaseSchedulerSummaryToIMesViewModel.selectDate[0]);
            var endDate = DateTime.Parse(releaseSchedulerSummaryToIMesViewModel.selectDate[1]);
            schedulerSummaries = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= startDate.Date && x.SchedulerDate < endDate.AddDays(1).Date).ToList();

            if (lines.Count != 0)
            {
                schedulerSummaries = schedulerSummaries.Where(x => lines.Contains(x.Line.Replace(" ", "").ToUpper())).ToList();
            }

            if (keyUsers.Count != 0)
            {
                schedulerSummaries = schedulerSummaries.Where(x => keyUsers.Contains(x.KeyUser.Replace(" ", "").ToUpper())).ToList();
            }

            if (schedulerSummaries.Count != 0)
            {
                foreach (var item in schedulerSummaries)
                {
                    T_PLDORD t_PLDORD = new T_PLDORD();
                    t_PLDORD.ORDER = item.SchedulerId;
                    t_PLDORD.MATNR = item.MaterialNumber;
                    t_PLDORD.WERKS = item.Plant;
                    t_PLDORD.ARBPL = item.Line;
                    t_PLDORD.PSTTR = item.SchedulerDate.Date.ToString("yyyy-MM-dd");
                    t_PLDORD.PEDTR = item.SchedulerDate.Date.ToString("yyyy-MM-dd");
                    t_PLDORD.GSMNG = item.PlanedQuantity.ToString();
                    t_PLDORD.PLNUM = item.PlanOrder.ToString();
                    t_PLDORD.TYPE = string.Empty;
                    t_PLDORD.MESSAGE = string.Empty;
                    lst.Add(t_PLDORD);
                }
            }
            return lst;
        }

        /// <summary>
        /// 将排程数据释放至IMES
        /// </summary>
        /// <param name="releaseSchedulerSummaryToIMesViewModel"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public List<T_PLDORD> ExecuteSchedulerSummaryReleaseToIMes(ReleaseSchedulerSummaryToIMesViewModel releaseSchedulerSummaryToIMesViewModel)
        {
            var lst = GetSchedulerSummaryInfoReleaseToIMes(releaseSchedulerSummaryToIMesViewModel);
            //将结果发送给SAP获取PlanOrder
            ReleaseToIMESModel releaseToIMESModel = new ReleaseToIMESModel();
            releaseToIMESModel.WERKS = "535A";
            releaseToIMESModel.GSTRP = DateTime.Parse(releaseSchedulerSummaryToIMesViewModel.selectDate[0]).ToString("yyyy-MM-dd");
            releaseToIMESModel.GLTRP = DateTime.Parse(releaseSchedulerSummaryToIMesViewModel.selectDate[1]).ToString("yyyy-MM-dd");

            //{
            //"MSG": [
            //{
            //  "WERKS": "535A", //工厂
            //  "AUFNR": "L12917040205-022024010401", //工单
            //  "MSGTY": "S", //成功
            //  "MSGTX": "OK"
            //},
            //{
            //  "WERKS": "535A",
            //  "AUFNR": "L12917040205-022024010402",
            //  "MSGTY": "E", //Error
            //  "MSGTX": "Part [17040205-02X] is not exist" //错误信息
            //}
            //]
            //}

            var jsonContent = JsonConvert.SerializeObject(releaseToIMESModel);
            var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
            var uri = "https://suzimesdevapi.ad.one-bcs.com/api/mes/OrderHeader";
            var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

            //HttpResponseMessage response = _httpClient.Send(request);

            ////确保HTTP响应成功
            //response.EnsureSuccessStatusCode();

            //if (response.IsSuccessStatusCode)
            //{
            //    var responseBody = response.Content.ReadAsStream();
            //    StreamReader reader = new(responseBody);
            //    string text = reader.ReadToEnd();
            //    var result = JsonConvert.DeserializeObject<ApiResult>(text);
            //    lst.Clear();
            //    if (result.Data != null)
            //    {
            //        lst = JsonConvert.DeserializeObject<List<T_PLDORD>>(result.Data.ToString());

            //        if (lst.Count() != 0)
            //        {
            //            //将Plan Order保存至APS数据库
            //            foreach (var item in lst)
            //            {
            //                var queryResult = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerId == item.ORDER.Replace(" ", "").ToUpper()).FirstOrDefault();
            //                if (queryResult != null)
            //                {
            //                    queryResult.ReleaseToSAP = DateTime.Now;
            //                    queryResult.Status = SchedulerStatus.ReleasedToSap.ToString();
            //                    queryResult.PlanOrder = item.PLNUM == null ? string.Empty : item.PLNUM?.ToString();
            //                    _apsDbContext.SchedulerSummary.Update(queryResult);
            //                }
            //            }
            //            _apsDbContext.SaveChanges();
            //        }
            //    }
            //    return lst;
            //}
            //else
            //{
            //    lst.Clear();
            //    return lst;
            //}
            return lst;
        }
    }
}
