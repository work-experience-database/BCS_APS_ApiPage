﻿using APS.SchedulerApplication.Entitys;
using APS.SchedulerApplication.Interfaces;
using APS.SchedulerApplication.Model;
using APS.Utils.Attributes;
using APS.Utils.Extensions;
using APS.Utils.Helper;
using APS.Utils.Models;
using Newtonsoft.Json;
using System.Collections;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using APS.SchedulerApplication.Model.ScheduleModel;

namespace APS.SchedulerApplication.Application
{
    [Transient]
    public class SchedulerService : ISchedulerService
    {
        private APSDbContext _apsDbContext;
        static DateTime date = DateTime.Now.Date;
        static DateTime firstMonday;
        static DateTime scheduleLastDay; //挖掘数据的最后一天
        static int weekNumber;
        static double standareTime = 22;
        static double weekStandareTime = 22 * 7;
        static List<SAPDemand> originalPirs = [];
        static List<LineMaintain> lineMaintains = [];
        static List<FactoryCalendar> factoryCalendars = [];
        static List<SchedulerSummary> schedulerSummaries = [];
        static List<SapPirExtensionModel> sapPirExtensionModels = [];
        static List<EachDayHumanQuantity> eachDayHumanQuantitys = [];
        static List<ScheduleErrorMessage> scheduleErrorMessagelst = [];
        static List<SchedulerSummary> ReleaseToSapSchedulerSummaries = [];
        static List<MaterialNumberLineMapRelations> materialNumberLineMapRelations = [];
        static readonly HttpClient _httpClient = new HttpClient();
        private readonly IConfiguration _configuration;

        public SchedulerService(APSDbContext apsDbContext, IConfiguration configuration)
        {
            _apsDbContext = apsDbContext;
            _configuration = configuration;
            firstMonday = WeekHelper.GetMondayDate(date);
            weekNumber = WeekHelper.GetWeekNumber(date);
            scheduleLastDay = firstMonday.AddDays(27);
            eachDayHumanQuantitys = _apsDbContext.EachDayHumanQuantity.ToList();
            lineMaintains = _apsDbContext.LineMaintain.Where(x => !x.Enable).ToList();
            factoryCalendars = _apsDbContext.FactoryCalendar.Where(x => !x.IsWork).ToList();
            materialNumberLineMapRelations = _apsDbContext.MaterialNumberLineMapRelations.Where(x => x.HC != "0" & !string.IsNullOrEmpty(x.HC)).ToList();
        }

        #region 日排程主函数
        /// <summary>Main Service</summary>
        public SchedulerSummaryViewModel ExecuteSchedulerSummary(DateTime startDate = default, DateTime endDate = default, double oee = 0.9, int days = 1)
        {
            //获取原始的PIR需求，并分配线体，排序
            if (startDate == default)
                startDate = DateTime.Now.Date;

            if (endDate == default)
                endDate = firstMonday.AddDays(6);

            eachDayHumanQuantitys.ForEach(x => { x.Date = x.Date.Date; x.Quantity = Math.Ceiling(x.Quantity * oee); });
            lineMaintains.ForEach(x => { x.StartTime = x.StartTime.Date; x.EndTime = x.EndTime.Date; });
            lineMaintains.ForEach(x => { x.StartTime = x.StartTime.Date; x.EndTime = x.EndTime.Date; });
            materialNumberLineMapRelations.ForEach(x => x.MaterialNumber = x.MaterialNumber.Replace(" ", "").ToUpper());

            //获取原始PIR,扣除库存
            originalPirs = GetOriginalPIR(endDate);

            //日期提前，拆分需求日
            List<SAPDemand> sapPirs = SplitAndAdvancePickUpDate(endDate, originalPirs, days);

            //创建需求模型
            sapPirExtensionModels = SortSapPIR(sapPirs);

            //日排程
            List<SchedulerSummary> schedulerSummaries = ScheduleEveryDay(startDate, endDate);
            return SchedulerSummaryTransforFEData(schedulerSummaries);
        }
        #endregion

        #region 处理原始PIR
        /// <summary>#获取原始PIR#扣除上一版本的库存</summary>
        public List<SAPDemand> GetOriginalPIR(DateTime endDate)
        {
            List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Date.ToString() != null & x.Date >= firstMonday & x.Date < firstMonday.AddDays(42) & x.Quantity >= 1).ToList();
            if (sapDemands.Count == 0) { return sapDemands; }

            //扣除库存
            //净需求=Fixed日期范围内的原始PIR-上一版本排程的

            //获取原始PIR
            var lastVersionSchedulesFromPir = sapDemands.Where(x => x.Date.Date < endDate.AddDays(1) & x.Date.Date >= DateTime.Now.Date).ToList();

            //获取上一版本的排程计划
            //值得注意的是,获取PIR的日期是已经扣除当前日期之前的Plan数量的
            var lastVersionSchedules = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= DateTime.Now.Date & x.SchedulerDate < endDate.AddDays(1) & !x.Deleted).ToList();
            schedulerSummaries = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= firstMonday.Date & x.SchedulerDate < endDate.AddDays(1) & !x.Deleted).ToList();
            Dictionary<string, int> materialNumberAcutualQuantitys = [];
            if (lastVersionSchedules.Count != 0)
            {
                List<string> distinctMaterialNumbers = lastVersionSchedules.Select(x => x.MaterialNumber.Replace(" ", "").ToUpper()).Distinct().ToList();
                int _totalQuantity = 0;
                foreach (var materialNumber in distinctMaterialNumbers)
                {
                    _totalQuantity = lastVersionSchedules.Where(x => x.MaterialNumber.Equals(materialNumber)).Sum(x => x.PlanedQuantity).ToInt();
                    materialNumberAcutualQuantitys.TryAdd(materialNumber, _totalQuantity);
                }
            }

            //净需求
            if (lastVersionSchedulesFromPir.Count != 0)
            {
                if (materialNumberAcutualQuantitys.Keys.Count != 0)
                {
                    foreach (var item in lastVersionSchedulesFromPir)
                    {
                        item.Date = item.Date.Date;
                        if (materialNumberAcutualQuantitys.Keys.Contains(item.MaterialNumber.Replace(" ", "").ToUpper()))
                        {
                            item.Quantity -= materialNumberAcutualQuantitys[item.MaterialNumber.Replace(" ", "").ToUpper()];
                        }
                    }
                }
            }

            //扣除净需求
            var newLastVersionSchedulesFromPir = lastVersionSchedulesFromPir.Where(x => x.Quantity != 0).ToList();
            var nextWeeksPirs = sapDemands.Where(x => x.Date >= endDate.AddDays(1)).ToList();

            //将上一版本的净需求一分为二
            var _newLastVersionSchedulesFromPir1 = newLastVersionSchedulesFromPir.Where(x => x.Quantity > 0).ToList(); //没有生产完，如PIR需求100，Plan为90，将此需求放到本周六
            var _newLastVersionSchedulesFromPir2 = newLastVersionSchedulesFromPir.Where(x => x.Quantity < 0).ToList(); //超额生产，如PIR需求100，Plan为110，将接下来的需求进行扣除

            //说明上周没有生产完，有剩余==>没有生产完，如PIR需求100，Plan为90，将此需求放到本周六
            //==>判断Fix周六有无需求
            //==>有需求则累加，无需求则直接添加
            if (_newLastVersionSchedulesFromPir1.Count != 0)
            {
                foreach (var item in _newLastVersionSchedulesFromPir1)
                {
                    var queryExistInFixDays = schedulerSummaries.Where(x => x.SchedulerDate == firstMonday.AddDays(5) & x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList();
                    if (queryExistInFixDays.Count != 0)
                    {
                        schedulerSummaries.Where(x => x.SchedulerDate == firstMonday.AddDays(5) & x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList().ForEach(x => x.PlanedQuantity += item.Quantity);
                    }
                    else
                    {
                        var basicInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).OrderBy(x => x.Order).FirstOrDefault();
                        if (basicInfo != null)
                        {
                            SchedulerSummary schedulerSummary = new SchedulerSummary();
                            schedulerSummary.SchedulerDate = firstMonday.AddDays(5);
                            schedulerSummary.SchedulerId = item.MaterialNumber.Replace(" ", "").ToUpper() + basicInfo.Line.Replace(" ", "").ToUpper() + firstMonday.AddDays(5).ToString("yyyyMMdd");
                            schedulerSummary.Plant = item.Plant;
                            schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
                            schedulerSummary.Line = basicInfo.Line;
                            schedulerSummary.SPQ = basicInfo.SPQ == null ? 0 : int.Parse(basicInfo.SPQ);
                            schedulerSummary.UPH = double.Parse(basicInfo.UPH);
                            schedulerSummary.HC = double.Parse(basicInfo.HC);
                            schedulerSummary.Area = basicInfo.Area;
                            schedulerSummary.Description = basicInfo.Description;
                            schedulerSummary.WeekNumber = WeekHelper.GetWeekNumber(firstMonday);
                            schedulerSummary.KeyUser = basicInfo.User;
                            schedulerSummary.PlanedQuantity = item.Quantity;
                            schedulerSummaries.Add(schedulerSummary);
                        }
                    }
                }
            }

            //说明上周生产完，且多生产了,超额生产，如PIR需求100，Plan为110，将接下来的需求进行扣除
            //#判断接下来有无再生产此料号
            //==>没有生产此料号，则不做任何处理
            //==>有生产，则进行扣除
            if (_newLastVersionSchedulesFromPir2.Count != 0)
            {
                int _currentWeekQuantity = 0;
                int _nextWeeksTotalQuantity = 0;
                foreach (var item in _newLastVersionSchedulesFromPir2)
                {
                    _currentWeekQuantity = item.Quantity * -1;
                    item.Date = item.Date.Date;

                    #region 将需求全部挪到周日
                    var queryExistInFixDays = schedulerSummaries.Where(x => x.SchedulerDate == firstMonday.AddDays(6) & x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList();
                    if (queryExistInFixDays.Count != 0)
                    {
                        schedulerSummaries.Where(x => x.SchedulerDate == firstMonday.AddDays(5) & x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList().ForEach(x => x.PlanedQuantity += item.Quantity);
                    }
                    else
                    {
                        var basicInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).OrderBy(x => x.Order).FirstOrDefault();
                        if (basicInfo != null)
                        {
                            SchedulerSummary schedulerSummary = new SchedulerSummary();
                            schedulerSummary.SchedulerDate = firstMonday.AddDays(6);
                            schedulerSummary.SchedulerId = item.MaterialNumber.Replace(" ", "").ToUpper() + basicInfo.Line.Replace(" ", "").ToUpper() + firstMonday.AddDays(6).ToString("yyyyMMdd");
                            schedulerSummary.Plant = item.Plant;
                            schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
                            schedulerSummary.Line = basicInfo.Line;
                            schedulerSummary.SPQ = basicInfo.SPQ == null ? 0 : int.Parse(basicInfo.SPQ);
                            schedulerSummary.UPH = double.Parse(basicInfo.UPH);
                            schedulerSummary.HC = double.Parse(basicInfo.HC);
                            schedulerSummary.Area = basicInfo.Area;
                            schedulerSummary.Description = basicInfo.Description;
                            schedulerSummary.WeekNumber = WeekHelper.GetWeekNumber(firstMonday);
                            schedulerSummary.KeyUser = basicInfo.User;
                            schedulerSummary.PlanedQuantity = item.Quantity;
                            schedulerSummaries.Add(schedulerSummary);
                        }
                    }

                    #endregion

                    #region Fix周多生产，接下来少生产
                    ////判断下接下来有无生产此料号计划
                    //var _nextWeeksPirs = nextWeeksPirs.Where(x => x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber).ToList();
                    //if (_nextWeeksPirs.Count() == 0) { continue; } //说明接下来五周内不再生产此料号

                    ////接下来仍然会生产此料号
                    ////说明上周生产完，且多生产了 ==>在接下来几周内扣除多生产的
                    ////接下来将上一版本的计划数量全部扣除
                    ////计算出接下来六周内全部排程计划
                    //_nextWeeksTotalQuantity = _nextWeeksPirs.Sum(x => x.Quantity).ToInt();
                    //if (_nextWeeksTotalQuantity == 0) { continue; } //说明接下来五周内不再生产此料号

                    ////说明接下来六周全部被扣除
                    //if (_currentWeekQuantity >= _nextWeeksTotalQuantity)
                    //{
                    //    nextWeeksPirs.Where(x => x.MaterialNumber == item.MaterialNumber).ToList().ForEach(x => x.Quantity = 0);
                    //}
                    //else
                    //{
                    //    //说明接下来六周排程足够扣除库存
                    //    //遍历未来排程日，起始日期为Fixed的第二天
                    //    foreach (var item2 in nextWeeksPirs)
                    //    {
                    //        if (item2.MaterialNumber != item.MaterialNumber) { continue; }
                    //        _currentWeekQuantity -= (int)item2.Quantity;

                    //        if (_currentWeekQuantity >= 0) { continue; }

                    //        if (_currentWeekQuantity < 0)
                    //        {
                    //            //小于,说明到了临界值，将多减的加回来 ，并保留此原始PIR需求
                    //            item2.Quantity = _currentWeekQuantity * -1;
                    //            break;
                    //        }
                    //    }
                    //}
                    #endregion
                }
            }
            return sapDemands;
        }
        /// <summary>第二周日期提前一天#第三，四，五，六周，拆到周一，三，五，然后再提前一天</summary>
        public List<SAPDemand> SplitAndAdvancePickUpDate(DateTime endDate, List<SAPDemand> sapPirs, int days = 1)
        {
            List<SAPDemand> lst = [];
            //对原始需求进行分割
            var secondWeekPIRlst = sapPirs.Where(x => x.Date >= endDate.AddDays(1) & x.Date < firstMonday.AddDays(14)).OrderBy(x => x.Date).ToList();
            var futureWeeksPIRlst = sapPirs.Where(x => x.Date >= firstMonday.AddDays(14)).OrderBy(x => x.Date).ToList();

            //第二周
            secondWeekPIRlst.ForEach(x => x.Date = x.Date.AddDays(-1));

            //第三，四，五，六周
            int _quantity = 0;
            List<SAPDemand> newFutureWeeksPIRlst = [];
            foreach (var item in futureWeeksPIRlst)
            {
                _quantity = item.Quantity;

                if (item.Quantity <= 0) { continue; }
                if (item.Quantity <= 100 & item.Quantity >= 1) //小于等于100不拆分
                {
                    newFutureWeeksPIRlst.Add(item);
                    continue;
                }
                else
                {
                    //拆分的天数
                    int dayCount = 3;
                    //当拆分不能被整除时，余数放到最后一天
                    int avaQuantity = _quantity / dayCount;
                    //根据当前需求日，获取当前周的周一日期
                    var currentMonDayDate = WeekHelper.GetMondayDate(item.Date);

                    #region 由周拆至日
                    SAPDemand newItem1 = new();
                    newItem1 = AutoMapperHelper.Map<SAPDemand, SAPDemand>(item);
                    newItem1.Quantity = avaQuantity;
                    newItem1.Date = currentMonDayDate.AddDays(-days);
                    newFutureWeeksPIRlst.Add(newItem1);

                    SAPDemand newItem2 = new();
                    newItem2 = AutoMapperHelper.Map<SAPDemand, SAPDemand>(item);
                    newItem2.Quantity = avaQuantity;
                    newItem2.Date = currentMonDayDate.AddDays(2).AddDays(-days);
                    newFutureWeeksPIRlst.Add(newItem2);

                    SAPDemand newItem3 = new();
                    newItem3 = AutoMapperHelper.Map<SAPDemand, SAPDemand>(item);
                    newItem3.Quantity = _quantity - 2 * avaQuantity;
                    newItem3.Date = currentMonDayDate.AddDays(4).AddDays(-days);
                    newFutureWeeksPIRlst.Add(newItem3);
                    #endregion  由周拆至日
                }
            }
            lst.AddRange(secondWeekPIRlst);
            lst.AddRange(newFutureWeeksPIRlst);
            return lst;
        }
        /// <summary>将所有MaterialNumber需求汇总到一天</summary>
        public List<SapPirExtensionModel> SortSapPIR(List<SAPDemand> sapPirs)
        {
            List<SapPirExtensionModel> sapPirExtensionModels = [];
            sapPirs = sapPirs.Where(x => x.MaterialNumber != null).OrderBy(x => x.Date).ToList();
            foreach (var sapPir in sapPirs)
            {
                var date = sapPir.Date.Date;
                var line = string.Empty;
                var materialNumber = sapPir.MaterialNumber.Replace(" ", "").ToUpper();
                int quantity = sapPir.Quantity;
                int weekNumber = WeekHelper.GetWeekNumber(date);
                var masterData = materialNumberLineMapRelations.Where(x => x.MaterialNumber.Equals(materialNumber) & x.Order == "0").FirstOrDefault();
                if (masterData == null)
                {
                    ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                    scheduleErrorMessage.MaterialNumber = materialNumber;
                    scheduleErrorMessage.ErrorMessage = "不存在主线.";
                    scheduleErrorMessagelst.Add(scheduleErrorMessage);
                    continue;
                }

                //主线是否在维护中
                var isMaintain = lineMaintains.Where(x => x.Line == masterData.Line & x.Enable).FirstOrDefault() != null;
                var masterDataOfAllLines = materialNumberLineMapRelations.Where(x => x.MaterialNumber.Equals(materialNumber)).OrderBy(x => x.Order).ToList();
                if (masterDataOfAllLines != null)
                {
                    foreach (var item in masterDataOfAllLines)
                    {
                        isMaintain = lineMaintains.Where(x => x.Line == item.Line & x.Enable).FirstOrDefault() != null;
                        if (!isMaintain)
                        {
                            line = item.Line;
                            break;
                        }
                    }
                }
                if (isMaintain)
                {
                    ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                    scheduleErrorMessage.MaterialNumber = materialNumber;
                    scheduleErrorMessage.ErrorMessage = "不存在可用线体.";
                    scheduleErrorMessagelst.Add(scheduleErrorMessage);
                    continue;
                }

                double uph = double.Parse(masterData.UPH);

                var queryExisted = sapPirExtensionModels.Where(x => x.Date == date & x.MaterialNumber == materialNumber).FirstOrDefault();
                if (queryExisted != null)
                {
                    sapPirExtensionModels.Where(x => x.Date == date & x.MaterialNumber == materialNumber).ToList().ForEach(y => y.Quantity += quantity);
                }
                else
                {
                    SapPirExtensionModel sapPirExtensionModel = new SapPirExtensionModel
                    {
                        Date = date,
                        WeekNumber = weekNumber,
                        Line = line,
                        UPH = uph,
                        Quantity = quantity,
                        MaterialNumber = materialNumber
                    };
                    sapPirExtensionModels.Add(sapPirExtensionModel);
                }
            }
            sapPirExtensionModels = sapPirExtensionModels.OrderBy(x => x.Date).ThenBy(x => x.WeekNumber).ToList();
            return sapPirExtensionModels;
        }
        #endregion

        #region 日排程
        /// <summary>正序排程,并将排程结果存储到Db</summary>
        public List<SchedulerSummary> ScheduleEveryDay(DateTime startDate, DateTime endDate)
        {
            //获取上一版的排程计划
            var lastVsersionSchedules = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= firstMonday & x.SchedulerDate < endDate.AddDays(-1)).OrderBy(x => x.SchedulerDate).ToList();

            //排程日开始于endDate次日
            DateTime scheduleDate;
            DateTime end = Convert.ToDateTime(scheduleLastDay.ToShortDateString());
            DateTime start = Convert.ToDateTime(endDate.AddDays(1).ToShortDateString());
            DateTime _endDate = endDate.AddDays(1);
            int days = end.Subtract(start).Days;

            //排程日的第一日，日期提前一天，将其需求排到当前日期
            var previousDateOfFirstScheduleDatePir = sapPirExtensionModels.Where(x => x.Date == endDate & x.Flag == 0).ToList();
            if (previousDateOfFirstScheduleDatePir != null)
            {
                sapPirExtensionModels.Where(x => x.Date == endDate).ToList().ForEach(y => { y.Flag = 1; });
                foreach (var item in previousDateOfFirstScheduleDatePir)
                {
                    var queryExistInFixDays = schedulerSummaries.Where(x => x.SchedulerDate == endDate & x.Line == item.Line & x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList();
                    if (queryExistInFixDays.Count != 0)
                    {
                        schedulerSummaries.Where(x => x.SchedulerDate == endDate & x.Line == item.Line & x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList().ForEach(x => x.PlanedQuantity += item.Quantity);
                    }
                    else
                    {
                        var masterData = materialNumberLineMapRelations.Where(x => x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper() & x.Line == item.Line).FirstOrDefault();
                        if (masterData != null)
                        {
                            SchedulerSummary schedulerSummary = new SchedulerSummary();
                            schedulerSummary.SchedulerDate = firstMonday.AddDays(6);
                            schedulerSummary.SchedulerId = item.MaterialNumber.Replace(" ", "").ToUpper() + masterData.Line.Replace(" ", "").ToUpper() + endDate.ToString("yyyyMMdd");
                            schedulerSummary.Plant = masterData.Plant;
                            schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
                            schedulerSummary.Line = item.Line;
                            schedulerSummary.SPQ = masterData.SPQ == null ? 0 : int.Parse(masterData.SPQ);
                            schedulerSummary.UPH = double.Parse(masterData.UPH);
                            schedulerSummary.HC = double.Parse(masterData.HC);
                            schedulerSummary.Area = masterData.Area;
                            schedulerSummary.Description = masterData.Description;
                            schedulerSummary.WeekNumber = WeekHelper.GetWeekNumber(firstMonday.AddDays(6));
                            schedulerSummary.KeyUser = masterData.User;
                            schedulerSummary.PlanedQuantity = item.Quantity;
                            schedulerSummaries.Add(schedulerSummary);
                        }
                    }
                }
            }

            for (int i = 0; i < days; i++)
            {
                if (sapPirExtensionModels.Count == 0) { break; }
                scheduleDate = _endDate.AddDays(i);
                var firstScheduleDate = WeekHelper.GetMondayDate(scheduleDate);
                var currentWeekNumber = WeekHelper.GetWeekNumber(scheduleDate);
                var currentWeekDataSource = sapPirExtensionModels.Where(x => x.WeekNumber == currentWeekNumber & x.Flag == 0 & x.Quantity >= 1).ToList();
                i += 6;
                //分线别
                List<string> distinctLineList = currentWeekDataSource.OrderBy(x => x.Line).Select(x => x.Line).Distinct().ToList();
                if (distinctLineList.Count == 0) { continue; }

                foreach (var line in distinctLineList)
                {
                    double realTotalTime = currentWeekDataSource.Where(x => x.Line == line).Sum(x => x.Quantity / x.UPH);
                    if (realTotalTime > weekStandareTime)
                    {
                        ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                        scheduleErrorMessage.MaterialNumber = "NA";
                        scheduleErrorMessage.ErrorMessage = $"该线在第{currentWeekNumber}周的PIR需求({realTotalTime / weekStandareTime})超出全周.";
                        scheduleErrorMessagelst.Add(scheduleErrorMessage);
                        continue;
                    }
                    sapPirExtensionModels.Where(x => x.WeekNumber == currentWeekNumber & x.Line == line).ToList().ForEach(y => y.Flag = 1);

                    //找到需求最大的MaterialNumber
                    var currentLinePIR = currentWeekDataSource.Where(x => x.Line == line).ToList();

                    //计算一周内，这条线，这个MaterialNumber的总需求
                    Dictionary<string, int> materialNumberQuantity = [];
                    foreach (var item in currentLinePIR)
                    {
                        if (materialNumberQuantity.ContainsKey(item.MaterialNumber))
                        {
                            materialNumberQuantity[item.MaterialNumber] += item.Quantity;
                        }
                        else
                        {
                            materialNumberQuantity.Add(item.MaterialNumber, item.Quantity);
                        }
                    }

                    //寻找需求最大的MaterialNumber
                    var maxMaterialNumberKeyValue = materialNumberQuantity.OrderBy(x => x.Key).OrderByDescending(x => x.Value).FirstOrDefault();
                    //开始每天满额排程
                    var masterDataOfMaxMaterialNumber = materialNumberLineMapRelations.Where(x => x.Order == "0" & x.MaterialNumber == maxMaterialNumberKeyValue.Key).FirstOrDefault();
                    int dayCount = (int)Math.Ceiling((maxMaterialNumberKeyValue.Value / (double.Parse(masterDataOfMaxMaterialNumber.UPH) * standareTime)));
                    if ((maxMaterialNumberKeyValue.Value / (double.Parse(masterDataOfMaxMaterialNumber.UPH) * standareTime)) <= 1) { dayCount = 0; }

                    var daySpan = dayCount >= 1 ? dayCount - 1 : dayCount;
                    DateTime fullScheduleLastDate = firstScheduleDate.AddDays(daySpan);
                    var maxMaterialNumberLastDate = currentWeekDataSource.Where(x => x.MaterialNumber == maxMaterialNumberKeyValue.Key).OrderByDescending(x => x.Date).FirstOrDefault();
                    if (maxMaterialNumberLastDate != null)
                    {
                        if (fullScheduleLastDate > maxMaterialNumberLastDate.Date)
                        {
                            ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                            scheduleErrorMessage.MaterialNumber = "NA";
                            scheduleErrorMessage.ErrorMessage = $"线别({line})不能如期完成最大需求料号({maxMaterialNumberKeyValue.Key}).";
                            scheduleErrorMessagelst.Add(scheduleErrorMessage);
                            continue;
                        }
                    }

                    dayCount = dayCount == 0 ? 1 : dayCount;
                    for (int j = 0; j < dayCount; j++)
                    {
                        var _scheduleDate = firstScheduleDate.AddDays(j);
                        //跳过周六，周日
                        if (scheduleDate.DayOfWeek == DayOfWeek.Saturday || scheduleDate.DayOfWeek == DayOfWeek.Sunday) { continue; }

                        //跳过工厂日历
                        var _factoryCalendars = factoryCalendars.Where(x => x.IsWork).Select(x => x.Date).ToList();
                        if (_factoryCalendars.Contains(scheduleDate)) { continue; }

                        //人力
                        var eachDayHumanQuantityInfo = eachDayHumanQuantitys.Where(x => x.Date == scheduleDate).FirstOrDefault();
                        if (eachDayHumanQuantityInfo == null) { continue; }
                        if (eachDayHumanQuantityInfo.Quantity <= 0)
                        {
                            ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                            scheduleErrorMessage.MaterialNumber = "NA";
                            scheduleErrorMessage.ErrorMessage = "当日sap人力不存在.";
                            scheduleErrorMessagelst.Add(scheduleErrorMessage);
                            continue;
                        }
                        double scheduleDateHumanResourceFromSap = eachDayHumanQuantityInfo.Quantity;
                        double scheduleDateHumanResourceFromPir = 0;
                        double restHumanResource = scheduleDateHumanResourceFromSap - scheduleDateHumanResourceFromPir;

                        //线体维护
                        var isMaintain = lineMaintains.Where(x => x.Line == masterDataOfMaxMaterialNumber.Line & x.StartTime <= DateTime.Now & x.EndTime >= DateTime.Now & x.Enable).ToList().Count != 0;
                        if (isMaintain) { continue; }

                        int standaredTimeCount = (int)(double.Parse(masterDataOfMaxMaterialNumber.UPH) * standareTime);
                        int _quantity = 0;
                        if (standaredTimeCount >= materialNumberQuantity[maxMaterialNumberKeyValue.Key])
                        {
                            _quantity = materialNumberQuantity[maxMaterialNumberKeyValue.Key];
                            materialNumberQuantity[maxMaterialNumberKeyValue.Key] = 0;
                        }
                        else
                        {
                            _quantity = standaredTimeCount;
                            materialNumberQuantity[maxMaterialNumberKeyValue.Key] -= standaredTimeCount;
                        }

                        SchedulerSummary schedulerSummary = new SchedulerSummary();
                        schedulerSummary.SchedulerDate = _scheduleDate;
                        schedulerSummary.Line = masterDataOfMaxMaterialNumber.Line.Replace(" ", "").ToUpper();
                        schedulerSummary.SchedulerId = $"{masterDataOfMaxMaterialNumber.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataOfMaxMaterialNumber.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                        schedulerSummary.MaterialNumber = masterDataOfMaxMaterialNumber.MaterialNumber.Replace(" ", "").ToUpper();
                        schedulerSummary.Plant = masterDataOfMaxMaterialNumber.Plant;
                        schedulerSummary.PlanedQuantity = _quantity;
                        schedulerSummary.Description = masterDataOfMaxMaterialNumber.Description;
                        schedulerSummary.SPQ = masterDataOfMaxMaterialNumber.SPQ == null ? 0 : int.Parse(masterDataOfMaxMaterialNumber.SPQ);
                        schedulerSummary.UPH = masterDataOfMaxMaterialNumber.UPH == null ? 0 : double.Parse(masterDataOfMaxMaterialNumber.UPH);
                        schedulerSummary.HC = masterDataOfMaxMaterialNumber.HC == null ? 0 : double.Parse(masterDataOfMaxMaterialNumber.HC);
                        schedulerSummary.WeekNumber = currentWeekNumber;
                        schedulerSummary.Area = masterDataOfMaxMaterialNumber.Area;
                        schedulerSummary.KeyUser = masterDataOfMaxMaterialNumber.User;
                        schedulerSummary.CreateTime = DateTime.Now;
                        schedulerSummary.UpdateTime = DateTime.Now;
                        schedulerSummaries.Add(schedulerSummary);
                    }

                    //需求最大的料号在满额排程后，必须能够如期出货
                    var maxMaterialNumberMaxDate = currentLinePIR.Where(x => x.MaterialNumber == maxMaterialNumberKeyValue.Key).OrderByDescending(x => x.Date).FirstOrDefault().Date;
                    if (fullScheduleLastDate > maxMaterialNumberMaxDate)
                    {
                        ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                        scheduleErrorMessage.MaterialNumber = masterDataOfMaxMaterialNumber.MaterialNumber;
                        scheduleErrorMessage.ErrorMessage = $"料号{masterDataOfMaxMaterialNumber.MaterialNumber}在满额排程后，仍然无法如期出货,放弃线体{line}排程计划.";
                        scheduleErrorMessagelst.Add(scheduleErrorMessage);
                        continue;
                    }

                    //逐天插入
                    var otherMaterialNumberDates = currentLinePIR.Where(x => x.MaterialNumber != maxMaterialNumberKeyValue.Key).OrderBy(x => x.Date).Select(x => x.Date).Distinct().ToList();
                    foreach (var date in otherMaterialNumberDates)
                    {
                        if (line == "L129")
                        {

                        }
                        if (line == "L26" & date >= DateTime.Now.AddDays(12))
                        {

                        }
                        var oneDateAllPIR = currentLinePIR.Where(x => x.Date == date & x.MaterialNumber != maxMaterialNumberKeyValue.Key).OrderBy(x => x.Quantity).ToList();

                        if (fullScheduleLastDate.AddDays(1) <= maxMaterialNumberMaxDate)
                        {
                            //剩下的小需求在最大需求料号的最后排程日之前或重合
                            if (date <= fullScheduleLastDate)
                            {
                                /* 小需求能否全部插入当天,若垂直方向能全部插入，则考虑横向是否能够全部插入
                                 * --垂直方向PIR能够全部插入至当天
                                 *   1.遍历，插入所有小需求料号
                                 *   2.遍历，判断水平需求能否插入
                                 *   --插入水平需求,水平方向的PIR插入多少？取决于最大料号占用的时间+当天剩余的时间==能够挤进来的总工时
                                 *   --剩余的水平PIR占用时间<=当天剩余的时间==>直接将水平需求插入
                                 *   --剩余的水平PIR占用时间> 当天剩余的时间==>即将挤进来的时间T1= 剩余的水平PIR占用时间-当天剩余的时间
                                 *   --判断即将挤进来的时间T1与向后插入的时间TT1是否充足，大白话就是：既然要挤进来，那么就要消耗当天的时间，那么当天的时间就要向后插入
                                 *   --特别需要注意 挤进来的时间必须是小于等于当日最大料号占用的工时，否则水平方向的PIR则放弃插入
                                 * --垂直方向PIR不能全部插入至当天
                                 *   1.说明将当天最大料号全部挤走，仍然不能够完成当前PIR，接下来就要判断需求挪走多少最大料号的总需求
                                 *   ==>计算即将挤进来的小需求料号总工时T1
                                 *   ==>计算理论向前插入的总工时T2,T1-T2>0则需要将多余的工时，转移至备用线，同时T1也不一定能够全部挤进来，还得考虑向后插入总工时是否充足
                                 *   ==>计算向后能够插入的总工时TT1，若TT2>=T1,那么挤进来的时间ResultT=T1,若 T1-T2>=0,ResultT=T1;若T1-T2<0,ResultT=T1-T2;
                                 *   ==>判断T1与向前能够挤入总工时，
                                 *   ==>判断向后插入总工时是否能够全部容纳即将挤进来的总工时 
                                 *   那那么首先将当前最大料号全部挪到最后排程日第二天，为当日小需求料号PIR腾工时
                                 *   2.遍历小需求料号PIR,检查当前是否是满额排程
                                 * 
                                 */
                                //若不能够全部插入，则考虑向前插入，向前插入要保证最大需求料号如期出货，若仍然有剩余，则考虑备用线，若无可用线体，放到当日，不做处理
                                //由于动态插入，挤压空间也会动态变化
                                DateTime dynamicFullScheduleLastDate = fullScheduleLastDate;
                                var queryDynamicFullScheduleLastDate = schedulerSummaries.Where(x => x.WeekNumber == currentWeekNumber & x.MaterialNumber == maxMaterialNumberKeyValue.Key & x.Line == line).OrderByDescending(x => x.SchedulerDate).FirstOrDefault();
                                if (queryDynamicFullScheduleLastDate != null)
                                {
                                    dynamicFullScheduleLastDate = queryDynamicFullScheduleLastDate.SchedulerDate;
                                }

                                var afterDateScope1 = (maxMaterialNumberMaxDate - dynamicFullScheduleLastDate).Days;

                                //只要有向后的挤压空间，并且当天的需求能够全部挤到当天，就全部挤进来
                                double? currentMinPIRsUsedTotalTime = oneDateAllPIR.Sum(x => x.Quantity / x.UPH);
                                double? fullScheduleLastDateUsedTime = schedulerSummaries.Where(x => x.SchedulerDate == fullScheduleLastDate & x.Line == line).Sum(x => x.PlanedQuantity / x.UPH);

                                double? currentCanUsedTotalTime = standareTime - fullScheduleLastDateUsedTime;
                                double? afterCanUsedTotalTime = standareTime - fullScheduleLastDateUsedTime;
                                double _temp = afterDateScope1 >= 1 ? standareTime * afterDateScope1 : 0;
                                afterCanUsedTotalTime += _temp;

                                for (int j = 0; j < oneDateAllPIR.Count; j++)
                                {
                                    var _scheduleDate = date;
                                    var _plant = oneDateAllPIR[j].Plant;
                                    int _quantity = oneDateAllPIR[j].Quantity;
                                    double _uph = oneDateAllPIR[j].UPH;
                                    var _materialNumber = oneDateAllPIR[j].MaterialNumber.Replace(" ", "").ToUpper();
                                    var _masterData = materialNumberLineMapRelations.Where(x => x.MaterialNumber == _materialNumber & x.Order == "0").FirstOrDefault();
                                    if (_masterData == null)
                                    {
                                        ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                                        scheduleErrorMessage.MaterialNumber = _materialNumber;
                                        scheduleErrorMessage.ErrorMessage = $"{_materialNumber}未维护主线.";
                                        scheduleErrorMessagelst.Add(scheduleErrorMessage);
                                        continue;
                                    }

                                    //当天能够全部插入，即垂直全部插入,然后横向保证料号的连续性
                                    if (currentMinPIRsUsedTotalTime <= standareTime)
                                    {
                                        double currentMaterialNumberSpendTime = _quantity / _uph; //当前料号所消耗工时
                                        if (currentMaterialNumberSpendTime <= afterCanUsedTotalTime)
                                        {
                                            SchedulerSummary schedulerSummary = new SchedulerSummary();
                                            schedulerSummary.SchedulerDate = _scheduleDate;
                                            schedulerSummary.Line = masterDataOfMaxMaterialNumber.Line.Replace(" ", "").ToUpper();
                                            schedulerSummary.SchedulerId = $"{_materialNumber}{masterDataOfMaxMaterialNumber.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                            schedulerSummary.MaterialNumber = _materialNumber;
                                            schedulerSummary.Plant = _plant;
                                            schedulerSummary.PlanedQuantity = _quantity;
                                            schedulerSummary.Description = _masterData.Description;
                                            schedulerSummary.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                            schedulerSummary.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                            schedulerSummary.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                            schedulerSummary.WeekNumber = currentWeekNumber;
                                            schedulerSummary.KeyUser = _masterData.User;
                                            schedulerSummary.Area = _masterData.Area;
                                            schedulerSummary.CreateTime = DateTime.Now;
                                            schedulerSummary.UpdateTime = DateTime.Now;
                                            schedulerSummaries.Add(schedulerSummary);
                                            //查询当日最大需求料号的Quantity
                                            var queryMaxMaterialNumberQuantityInfo = schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).FirstOrDefault();
                                            if (queryMaxMaterialNumberQuantityInfo != null)
                                            {
                                                //当天的扣除
                                                //先不管三七二十一，加进来再说，假如加进来后，存在超出工时，则把超出的部分进行扣除
                                                double? currentDateRestTime = standareTime - schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line).Sum(x => x.PlanedQuantity / x.UPH);
                                                //没有剩余工时
                                                //存在剩余时间，先将剩余工时用掉，然后开始挤进来
                                                var _currentDateRestTime = Math.Truncate((double)currentDateRestTime * 100) / 100;

                                                if (_currentDateRestTime >= 0)
                                                {
                                                    //continue;
                                                }

                                                if (_currentDateRestTime < 0)
                                                {
                                                    var _currentDateRestTime1 = Math.Abs((double)currentDateRestTime);
                                                    int? _tempQuantity = (int)(_currentDateRestTime1 * double.Parse(masterDataOfMaxMaterialNumber.UPH));

                                                    //扣除当天的数量
                                                    if (queryMaxMaterialNumberQuantityInfo.PlanedQuantity >= _tempQuantity)
                                                    {
                                                        schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).ToList().ForEach(y =>
                                                        {
                                                            y.PlanedQuantity -= _tempQuantity;
                                                        });
                                                    }
                                                    else
                                                    {
                                                        _tempQuantity = queryMaxMaterialNumberQuantityInfo.PlanedQuantity;
                                                        schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).ToList().ForEach(y =>
                                                        {
                                                            y.PlanedQuantity = 0;
                                                        });
                                                    }

                                                    //向后插入
                                                    for (int h = 0; h < afterDateScope1; h++)
                                                    {
                                                        var _afterDate = fullScheduleLastDate.AddDays(h);
                                                        var afterDateRestTime = standareTime - schedulerSummaries.Where(x => x.SchedulerDate == _afterDate.AddDays(1) & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).Sum(x => x.PlanedQuantity / x.UPH);
                                                        var _afterDateRestTime = Math.Truncate((double)afterDateRestTime * 100) / 100;

                                                        if (_afterDateRestTime <= 0) { continue; }
                                                        if (_tempQuantity == 0) { break; }

                                                        int? afterDateRestQuantity = (int)(_afterDateRestTime * double.Parse(masterDataOfMaxMaterialNumber.UPH));
                                                        if (_tempQuantity <= afterDateRestQuantity)
                                                        {
                                                            var queryAfterSchedulerSummaries = schedulerSummaries.Where(x => x.SchedulerDate == _afterDate.AddDays(1) & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).FirstOrDefault();
                                                            if (queryAfterSchedulerSummaries == null)
                                                            {
                                                                SchedulerSummary schedulerSummary1 = new SchedulerSummary();
                                                                schedulerSummary1.SchedulerDate = fullScheduleLastDate.AddDays(1);
                                                                schedulerSummary1.Line = masterDataOfMaxMaterialNumber.Line.Replace(" ", "").ToUpper();
                                                                schedulerSummary1.SchedulerId = $"{masterDataOfMaxMaterialNumber.MaterialNumber}{masterDataOfMaxMaterialNumber.Line.Replace(" ", "").ToUpper()}{_afterDate.AddDays(1).ToString("yyyyMMdd")}";
                                                                schedulerSummary1.MaterialNumber = masterDataOfMaxMaterialNumber.MaterialNumber;
                                                                schedulerSummary1.Plant = _plant;
                                                                schedulerSummary1.PlanedQuantity = _tempQuantity;
                                                                schedulerSummary1.Description = _masterData.Description;
                                                                schedulerSummary1.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                                schedulerSummary1.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                                schedulerSummary1.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                                schedulerSummary1.WeekNumber = currentWeekNumber;
                                                                schedulerSummary1.KeyUser = _masterData.User;
                                                                schedulerSummary1.Area = _masterData.Area;
                                                                schedulerSummary1.CreateTime = DateTime.Now;
                                                                schedulerSummary1.UpdateTime = DateTime.Now;
                                                                schedulerSummaries.Add(schedulerSummary1);
                                                            }
                                                            else
                                                            {
                                                                schedulerSummaries.Where(x => x.SchedulerDate == fullScheduleLastDate.AddDays(1) & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).ToList().ForEach(y =>
                                                                {
                                                                    y.PlanedQuantity += _tempQuantity;
                                                                });
                                                            }
                                                            _tempQuantity = 0;
                                                        }
                                                        else
                                                        {
                                                            _tempQuantity -= afterDateRestQuantity;
                                                        }
                                                    }
                                                }
                                            }

                                            //水平插入,当遍历所有料号后，需要判断是否还有剩余工时,此时就需要横向找PIR需求，先找当天生产的料号，若还有剩余，则继续挖掘
                                            if (j == oneDateAllPIR.Count - 1)
                                            {
                                                foreach (var item in oneDateAllPIR)
                                                {
                                                    var restTime = standareTime - schedulerSummaries.Where(x => x.SchedulerDate == date & x.Line == line).Sum(x => x.PlanedQuantity / x.UPH);
                                                    double currentMaterialNumberTotalQuantity = materialNumberQuantity[item.MaterialNumber] - item.Quantity;

                                                    //var _restTime = Math.Truncate((double)restTime * 100) / 100;
                                                    //if (_restTime <= 0) { continue; }

                                                    //只要被挤压的空间存在，就一直挤
                                                    //剩余的料号占用的时间
                                                    double restTimeOfCurrentMaterialNumber = currentMaterialNumberTotalQuantity / item.UPH;

                                                    //计算挤压空间工时
                                                    DateTime dynamicFullScheduleLastDate1 = fullScheduleLastDate;
                                                    var queryDynamicFullScheduleLastDate1 = schedulerSummaries.Where(x => x.WeekNumber == currentWeekNumber & x.MaterialNumber == maxMaterialNumberKeyValue.Key & x.Line == line).OrderByDescending(x => x.SchedulerDate).FirstOrDefault();
                                                    if (queryDynamicFullScheduleLastDate1 != null)
                                                    {
                                                        dynamicFullScheduleLastDate1 = queryDynamicFullScheduleLastDate1.SchedulerDate.Date;
                                                    }
                                                    var restTimeMaxMaterialNumber = standareTime - schedulerSummaries.Where(x => x.SchedulerDate == dynamicFullScheduleLastDate1 & x.Line == line).Sum(x => x.PlanedQuantity / x.UPH);
                                                    var dateSpan = (maxMaterialNumberLastDate.Date - dynamicFullScheduleLastDate).Days;
                                                    var _tempTime = dateSpan >= 1 ? standareTime * dateSpan : 0;
                                                    restTimeMaxMaterialNumber += _tempTime;

                                                    //当天剩余的工时能够让小需求剩余的水平方向的需求直接插入进来
                                                    if (restTime >= restTimeOfCurrentMaterialNumber)
                                                    {
                                                        schedulerSummaries.Where(x => x.SchedulerDate == date & x.Line == line & x.MaterialNumber == item.MaterialNumber).ToList().ForEach(y => y.PlanedQuantity = materialNumberQuantity[item.MaterialNumber]);
                                                        materialNumberQuantity[item.MaterialNumber] = 0;
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        //当天剩余的工时，无法让小需求剩余的水平方向需求直接插入进来
                                                        //首先判断水平方向剩余需求能够插入进来，即向后的剩余挤压空间是否充足
                                                        if (restTimeOfCurrentMaterialNumber <= restTimeMaxMaterialNumber)
                                                        {
                                                            var minPIRInsertSpendTime = restTimeOfCurrentMaterialNumber - restTime;
                                                            var maxMaterialNumberRemovedQuantity = (int)(minPIRInsertSpendTime * double.Parse(masterDataOfMaxMaterialNumber.UPH));
                                                            if (maxMaterialNumberRemovedQuantity <= 0) { continue; }

                                                            //把剩余的水平需求全部挤进来，前提是最大需求料号当天占用的时间够它挤
                                                            double? queryMaxMaterialNumberCurrentDateSpendTime = schedulerSummaries.Where(x => x.SchedulerDate == date & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).Sum(x => x.PlanedQuantity / x.UPH);
                                                            if (queryMaxMaterialNumberCurrentDateSpendTime >= minPIRInsertSpendTime)
                                                            {
                                                                //扣除当天的
                                                                schedulerSummaries.Where(x => x.SchedulerDate == date & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).ToList().ForEach(y =>
                                                                {
                                                                    y.PlanedQuantity -= maxMaterialNumberRemovedQuantity;
                                                                });

                                                                //插入水平需求
                                                                schedulerSummaries.Where(x => x.SchedulerDate == date & x.Line == line & x.MaterialNumber == item.MaterialNumber).ToList().ForEach(y => y.PlanedQuantity = materialNumberQuantity[item.MaterialNumber]);
                                                                materialNumberQuantity[item.MaterialNumber] = 0;
                                                                sapPirExtensionModels.Where(x => x.WeekNumber == currentWeekNumber & x.Line == line & x.MaterialNumber == item.MaterialNumber).ToList().ForEach(y =>
                                                                {
                                                                    y.Flag = 1;
                                                                    y.Quantity = 0;
                                                                });

                                                                //向后插入
                                                                int? _tempQuantity = maxMaterialNumberRemovedQuantity;

                                                                for (int h = 0; h < afterDateScope1; h++)
                                                                {
                                                                    var _afterDate = fullScheduleLastDate.AddDays(h);
                                                                    var afterDateRestTime = standareTime - schedulerSummaries.Where(x => x.SchedulerDate == _afterDate.AddDays(1) & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).Sum(x => x.PlanedQuantity / x.UPH);
                                                                    var _afterDateRestTime = Math.Truncate((double)afterDateRestTime * 100) / 100;

                                                                    if (_tempQuantity <= 0) { break; }
                                                                    if (_afterDateRestTime <= 0) { continue; }
                                                                    if (maxMaterialNumberRemovedQuantity == 0) { break; }

                                                                    int? afterDateRestQuantity = (int)(_afterDateRestTime * double.Parse(masterDataOfMaxMaterialNumber.UPH));
                                                                    if (_tempQuantity <= afterDateRestQuantity)
                                                                    {
                                                                        var queryAfterSchedulerSummaries = schedulerSummaries.Where(x => x.SchedulerDate == _afterDate.AddDays(1) & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).FirstOrDefault();
                                                                        if (queryAfterSchedulerSummaries == null)
                                                                        {
                                                                            SchedulerSummary schedulerSummary1 = new SchedulerSummary();
                                                                            schedulerSummary1.SchedulerDate = fullScheduleLastDate.AddDays(1);
                                                                            schedulerSummary1.Line = masterDataOfMaxMaterialNumber.Line.Replace(" ", "").ToUpper();
                                                                            schedulerSummary1.SchedulerId = $"{masterDataOfMaxMaterialNumber.MaterialNumber}{masterDataOfMaxMaterialNumber.Line.Replace(" ", "").ToUpper()}{_afterDate.AddDays(1).ToString("yyyyMMdd")}";
                                                                            schedulerSummary1.MaterialNumber = masterDataOfMaxMaterialNumber.MaterialNumber;
                                                                            schedulerSummary1.Plant = _plant;
                                                                            schedulerSummary1.PlanedQuantity = _tempQuantity;
                                                                            schedulerSummary1.Description = _masterData.Description;
                                                                            schedulerSummary1.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                                            schedulerSummary1.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                                            schedulerSummary1.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                                            schedulerSummary1.WeekNumber = currentWeekNumber;
                                                                            schedulerSummary1.KeyUser = _masterData.User;
                                                                            schedulerSummary1.Area = _masterData.Area;
                                                                            schedulerSummary1.CreateTime = DateTime.Now;
                                                                            schedulerSummary1.UpdateTime = DateTime.Now;
                                                                            schedulerSummaries.Add(schedulerSummary1);
                                                                        }
                                                                        else
                                                                        {
                                                                            schedulerSummaries.Where(x => x.SchedulerDate == fullScheduleLastDate.AddDays(1) & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).ToList().ForEach(y =>
                                                                            {
                                                                                y.PlanedQuantity += _tempQuantity;
                                                                            });
                                                                        }
                                                                        _tempQuantity = 0;
                                                                    }
                                                                    else
                                                                    {
                                                                        _tempQuantity -= afterDateRestQuantity;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //大前天是，当天所有需求都能插入进来
                                                            //此时水平需求只能插入部分，便结束了
                                                            //此处直接放弃水平插入，因为不存在挤压空间
                                                            //schedulerSummaries.Where(x => x.SchedulerDate == date & x.Line == line & x.MaterialNumber == item.MaterialNumber).ToList().ForEach(y => y.PlanedQuantity += spareQuantity);
                                                            //materialNumberQuantity[item.MaterialNumber] -= maxMaterialNumberRemovedQuantity;
                                                            //sapPirExtensionModels.Where(x => x.Date == date & x.Line == line & x.MaterialNumber == item.MaterialNumber).ToList().ForEach(y =>
                                                            //{
                                                            //    y.Flag = 1;
                                                            //    y.Quantity -= maxMaterialNumberRemovedQuantity;
                                                            //});

                                                            //schedulerSummaries.Where(x => x.SchedulerDate == fullScheduleLastDate.AddDays(1) & x.Line == line & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).ToList().ForEach(y =>
                                                            //{
                                                            //    y.PlanedQuantity += maxMaterialNumberRemovedQuantity;
                                                            //});
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //当天不能够全部插入，即垂直只能部分插入,然后横向直接放弃连续性
                                        //找到临界值
                                        //临界值考虑是否向前插入
                                        //向前插入两条必要条件：1.存在向前插入的日期范围 2.向后存在挤压空间

                                        double? restTime = standareTime - schedulerSummaries.Where(x => x.SchedulerDate == date & x.Line == line).Sum(x => x.PlanedQuantity / x.UPH);
                                        double? currentMaterialNumberSpendTime = _quantity / double.Parse(_masterData.UPH);

                                        if (restTime >= currentMaterialNumberSpendTime)
                                        {
                                            //能够插入
                                            SchedulerSummary schedulerSummary = new SchedulerSummary();
                                            schedulerSummary.SchedulerDate = _scheduleDate;
                                            schedulerSummary.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                            schedulerSummary.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                            schedulerSummary.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                            schedulerSummary.Plant = _masterData.Plant;
                                            schedulerSummary.PlanedQuantity = _quantity;
                                            schedulerSummary.Description = _masterData.Description;
                                            schedulerSummary.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                            schedulerSummary.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                            schedulerSummary.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                            schedulerSummary.WeekNumber = weekNumber;
                                            schedulerSummary.KeyUser = _masterData.User;
                                            schedulerSummary.CreateTime = DateTime.Now;
                                            schedulerSummary.UpdateTime = DateTime.Now;
                                            schedulerSummaries.Add(schedulerSummary);
                                        }
                                        else
                                        {
                                            //临界值
                                            int _quantity1 = (int)(restTime * int.Parse(_masterData.UPH));  //保留在当日
                                            int _quantity2 = _quantity - _quantity1;  //向前插入


                                            //既然要向前插入，首先判断有无向前插入的日期
                                            //被挤压的最大需求料号，向后填充需要有足够的挤压空间，二者缺一不可

                                            //查找向前插入的日期范围
                                            var beforeDateScope = (date - firstScheduleDate).Days; //日期相同为0，也可能为负数

                                            //查找向后插入的日期范围
                                            var afterDateScope = (maxMaterialNumberLastDate.Date - fullScheduleLastDate).Days;

                                            //不可以向前插入
                                            if (beforeDateScope < 0 || afterDateScope < 0)
                                            {
                                                /*向前无法挪，向后无处挤
                                                 *考虑备用线,若无可用备用线，再考虑放到原始需求日
                                                 *找到一条可用的线体
                                                 *找到该线体最晚的一天进行插入
                                                 */
                                                var masterData1 = materialNumberLineMapRelations.Where(x => x.MaterialNumber == oneDateAllPIR[j].MaterialNumber & x.Order != "0").OrderBy(x => x.Order).ToList();
                                                var standybyLine = string.Empty;
                                                if (masterData1.Count >= 1)
                                                {
                                                    foreach (var item in masterData1)
                                                    {
                                                        var isMaintain = lineMaintains.Where(x => x.Line == item.Line & x.StartTime <= DateTime.Now & x.EndTime >= DateTime.Now & x.Enable).ToList().Count != 0;
                                                        if (isMaintain) { continue; }
                                                        standybyLine = item.Line;
                                                        break;
                                                    }
                                                }

                                                //没有找到可用备用线,将其排程至原始需求日
                                                if (string.IsNullOrEmpty(standybyLine))
                                                {
                                                    SchedulerSummary schedulerSummary = new SchedulerSummary();
                                                    schedulerSummary.SchedulerDate = _scheduleDate;
                                                    schedulerSummary.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                    schedulerSummary.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                                    schedulerSummary.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                    schedulerSummary.Plant = _masterData.Plant;
                                                    schedulerSummary.PlanedQuantity = _quantity;
                                                    schedulerSummary.Description = _masterData.Description;
                                                    schedulerSummary.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                    schedulerSummary.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                    schedulerSummary.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                    schedulerSummary.WeekNumber = currentWeekNumber;
                                                    schedulerSummary.KeyUser = _masterData.User;
                                                    schedulerSummary.Area = _masterData.Area;
                                                    schedulerSummary.CreateTime = DateTime.Now;
                                                    schedulerSummary.UpdateTime = DateTime.Now;
                                                    schedulerSummaries.Add(schedulerSummary);

                                                    ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                                                    scheduleErrorMessage.MaterialNumber = _masterData.MaterialNumber;
                                                    scheduleErrorMessage.Type = Types.Info;
                                                    scheduleErrorMessage.ErrorMessage = $"{_masterData.MaterialNumber}无法向前插入且无备用线,故放至原始需求日.";
                                                    scheduleErrorMessagelst.Add(scheduleErrorMessage);
                                                }
                                                else
                                                {
                                                    //放于备用线
                                                    var queryStandbyLineSchedulerSummaries = schedulerSummaries.Where(x => x.MaterialNumber == _masterData.MaterialNumber & x.Line == standybyLine & x.SchedulerDate <= date).OrderByDescending(x => x.SchedulerDate).FirstOrDefault();
                                                    if (queryStandbyLineSchedulerSummaries == null)
                                                    {
                                                        SchedulerSummary schedulerSummary1 = new SchedulerSummary();
                                                        var _scheduleDate1 = firstScheduleDate;
                                                        schedulerSummary1.SchedulerDate = _scheduleDate1;
                                                        schedulerSummary1.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                        schedulerSummary1.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate1.ToString("yyyyMMdd")}";
                                                        schedulerSummary1.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                        schedulerSummary1.Plant = _masterData.Plant;
                                                        schedulerSummary1.PlanedQuantity = _quantity2;
                                                        schedulerSummary1.Description = _masterData.Description;
                                                        schedulerSummary1.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                        schedulerSummary1.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                        schedulerSummary1.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                        schedulerSummary1.WeekNumber = currentWeekNumber;
                                                        schedulerSummary1.KeyUser = _masterData.User;
                                                        schedulerSummary1.Area = _masterData.Area;
                                                        schedulerSummary1.CreateTime = DateTime.Now;
                                                        schedulerSummary1.UpdateTime = DateTime.Now;
                                                        schedulerSummaries.Add(schedulerSummary1);
                                                    }
                                                    else
                                                    {
                                                        //放置于备用线也是需要考虑备用线的剩余工时，暂时不做考虑
                                                        var _scheduleDate2 = queryStandbyLineSchedulerSummaries.SchedulerDate;
                                                        schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate2 & x.MaterialNumber == _masterData.MaterialNumber & x.Line == standybyLine & x.SchedulerDate <= date).ToList().ForEach(y =>
                                                        {
                                                            y.PlanedQuantity += _quantity2;
                                                        });
                                                    }

                                                    SchedulerSummary schedulerSummary = new SchedulerSummary();
                                                    schedulerSummary.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                    schedulerSummary.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                                    schedulerSummary.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                    schedulerSummary.Plant = _masterData.Plant;
                                                    schedulerSummary.PlanedQuantity = _quantity1;
                                                    schedulerSummary.Description = _masterData.Description;
                                                    schedulerSummary.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                    schedulerSummary.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                    schedulerSummary.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                    schedulerSummary.WeekNumber = currentWeekNumber;
                                                    schedulerSummary.KeyUser = _masterData.User;
                                                    schedulerSummary.Area = _masterData.Area;
                                                    schedulerSummary.CreateTime = DateTime.Now;
                                                    schedulerSummary.UpdateTime = DateTime.Now;
                                                    schedulerSummaries.Add(schedulerSummary);
                                                }
                                                continue;
                                            }

                                            //存在向前插入日期范围=>判断有无挤压空间==>可挤压的工时够不够向后插入
                                            for (int h = 0; h < beforeDateScope; h++)
                                            {
                                                var _scheduleDate1 = date;
                                                var _beforeScheduleDate = date.AddDays(-h);

                                                //判断能否插入,计算可供挤压的总工时
                                                double? canUsedTotalTime = 0;
                                                double? usedTotalTime = standareTime;
                                                for (int p = 0; p < afterDateScope; p++)
                                                {
                                                    var currentDate = fullScheduleLastDate.AddDays(p);
                                                    var query = schedulerSummaries.Where(x => x.SchedulerDate == currentDate & x.Line == line).FirstOrDefault();
                                                    if (query != null)
                                                    {
                                                        usedTotalTime = schedulerSummaries.Where(x => x.SchedulerDate == currentDate & x.Line == line).Sum(x => x.PlanedQuantity / x.UPH);
                                                        canUsedTotalTime += standareTime - usedTotalTime;
                                                    }
                                                    else
                                                    {
                                                        canUsedTotalTime += usedTotalTime;
                                                    }
                                                }

                                                double oneMaterialNumberUsedTime = _quantity / double.Parse(_masterData.UPH);

                                                if (oneMaterialNumberUsedTime <= canUsedTotalTime)
                                                {
                                                    //保留原始
                                                    //向前插入=>添加一笔
                                                    //被挤走=>减少对应的数量
                                                    //向后插入=>一笔

                                                    SchedulerSummary schedulerSummary1 = new SchedulerSummary();
                                                    schedulerSummary1.SchedulerDate = _beforeScheduleDate;
                                                    schedulerSummary1.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                    schedulerSummary1.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_beforeScheduleDate.ToString("yyyyMMdd")}";
                                                    schedulerSummary1.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                    schedulerSummary1.Plant = _masterData.Plant;
                                                    schedulerSummary1.PlanedQuantity = _quantity1;
                                                    schedulerSummary1.Description = _masterData.Description;
                                                    schedulerSummary1.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                    schedulerSummary1.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                    schedulerSummary1.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                    schedulerSummary1.WeekNumber = currentWeekNumber;
                                                    schedulerSummary1.KeyUser = _masterData.User;
                                                    schedulerSummary1.Area = _masterData.Area;
                                                    schedulerSummary1.CreateTime = DateTime.Now;
                                                    schedulerSummary1.UpdateTime = DateTime.Now;
                                                    schedulerSummaries.Add(schedulerSummary1);

                                                    //向前插入一笔
                                                    SchedulerSummary schedulerSummary2 = new SchedulerSummary();
                                                    schedulerSummary2.SchedulerDate = _beforeScheduleDate;
                                                    schedulerSummary2.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                    schedulerSummary2.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_beforeScheduleDate.ToString("yyyyMMdd")}";
                                                    schedulerSummary2.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                    schedulerSummary2.Plant = _masterData.Plant;
                                                    schedulerSummary2.PlanedQuantity = _quantity2;
                                                    schedulerSummary2.Description = _masterData.Description;
                                                    schedulerSummary2.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                    schedulerSummary2.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                    schedulerSummary2.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                    schedulerSummary2.WeekNumber = currentWeekNumber;
                                                    schedulerSummary2.KeyUser = _masterData.User;
                                                    schedulerSummary2.Area = _masterData.Area;
                                                    schedulerSummary2.CreateTime = DateTime.Now;
                                                    schedulerSummary2.UpdateTime = DateTime.Now;
                                                    schedulerSummaries.Add(schedulerSummary2);
                                                    materialNumberQuantity[_masterData.MaterialNumber] -= _quantity2;

                                                    //将原始排程做扣除
                                                    var queryBeforeSchedulerSummaries = schedulerSummaries.Where(x => x.SchedulerDate == _beforeScheduleDate & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).FirstOrDefault();
                                                    if (queryBeforeSchedulerSummaries != null)
                                                    {
                                                        if (queryBeforeSchedulerSummaries.PlanedQuantity >= _quantity2)
                                                        {
                                                            schedulerSummaries.Where(x => x.SchedulerDate == _beforeScheduleDate & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).ToList().ForEach(y =>
                                                            {
                                                                y.PlanedQuantity -= _quantity2;
                                                            });
                                                            materialNumberQuantity[_masterData.MaterialNumber] -= _quantity2;
                                                        }
                                                        else
                                                        {
                                                            schedulerSummaries.Where(x => x.SchedulerDate == _beforeScheduleDate & x.MaterialNumber == masterDataOfMaxMaterialNumber.MaterialNumber).ToList().ForEach(y =>
                                                            {
                                                                y.PlanedQuantity = 0;
                                                            });
                                                            materialNumberQuantity[_masterData.MaterialNumber] -= _quantity2;
                                                        }
                                                    }

                                                    //向后插入一笔  
                                                    for (int p = 0; p < afterDateScope; p++)
                                                    {
                                                        var _currentDate = fullScheduleLastDate.AddDays(p);
                                                        var queryAfterSchedulerSummaries = schedulerSummaries.Where(x => x.SchedulerDate == _currentDate & x.Line == line & x.MaterialNumber == _materialNumber).FirstOrDefault();
                                                        if (_quantity2 <= 0) { break; }
                                                        if (queryAfterSchedulerSummaries == null)
                                                        {
                                                            SchedulerSummary schedulerSummary3 = new SchedulerSummary();
                                                            schedulerSummary3.SchedulerDate = _beforeScheduleDate;
                                                            schedulerSummary3.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                            schedulerSummary3.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                                            schedulerSummary3.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                            schedulerSummary3.Plant = _masterData.Plant;
                                                            schedulerSummary3.PlanedQuantity = _quantity2;
                                                            schedulerSummary3.Description = _masterData.Description;
                                                            schedulerSummary3.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                            schedulerSummary3.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                            schedulerSummary3.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                            schedulerSummary3.WeekNumber = currentWeekNumber;
                                                            schedulerSummary3.KeyUser = _masterData.User;
                                                            schedulerSummary3.Area = _masterData.Area;
                                                            schedulerSummary3.CreateTime = DateTime.Now;
                                                            schedulerSummary3.UpdateTime = DateTime.Now;
                                                            schedulerSummaries.Add(schedulerSummary3);
                                                            materialNumberQuantity[_masterData.MaterialNumber] -= _quantity2;
                                                        }
                                                        else
                                                        {
                                                            var restTime1 = standareTime - schedulerSummaries.Where(x => x.SchedulerDate == _currentDate & x.Line == line).Sum(x => x.PlanedQuantity / x.UPH);
                                                            if (restTime1 >= oneMaterialNumberUsedTime)
                                                            {
                                                                schedulerSummaries.Where(x => x.SchedulerDate == _currentDate & x.Line == line & x.MaterialNumber == _materialNumber).ToList().ForEach(y => { y.PlanedQuantity += _quantity2; });
                                                                materialNumberQuantity[_masterData.MaterialNumber] -= _quantity2;
                                                                _quantity2 = 0;
                                                            }
                                                            else
                                                            {
                                                                int _tempQuantity = (int)(restTime1 * int.Parse(_masterData.UPH));
                                                                schedulerSummaries.Where(x => x.SchedulerDate == _currentDate & x.Line == line & x.MaterialNumber == _materialNumber).ToList().ForEach(y => { y.PlanedQuantity += _tempQuantity; });
                                                                _quantity2 -= _tempQuantity;
                                                                materialNumberQuantity[_masterData.MaterialNumber] -= _tempQuantity;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //不存在可挤压的空间，则考虑备用线
                                                    var masterData1 = materialNumberLineMapRelations.Where(x => x.MaterialNumber == oneDateAllPIR[j].MaterialNumber & x.Order != "0").OrderBy(x => x.Order).ToList();
                                                    var standybyLine = string.Empty;
                                                    if (masterData1.Count >= 1)
                                                    {
                                                        foreach (var item in masterData1)
                                                        {
                                                            var isMaintain = lineMaintains.Where(x => x.Line == item.Line & x.StartTime <= DateTime.Now & x.EndTime >= DateTime.Now & x.Enable).ToList().Count != 0;
                                                            if (isMaintain) { continue; }
                                                            standybyLine = item.Line;
                                                            break;
                                                        }
                                                    }
                                                    //没有找到可用备用线,将其排程至原始需求日
                                                    //没有找到可用备用线,将其排程至原始需求日
                                                    if (string.IsNullOrEmpty(standybyLine))
                                                    {
                                                        SchedulerSummary schedulerSummary = new SchedulerSummary();
                                                        schedulerSummary.SchedulerDate = _scheduleDate;
                                                        schedulerSummary.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                        schedulerSummary.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                                        schedulerSummary.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                        schedulerSummary.Plant = _masterData.Plant;
                                                        schedulerSummary.PlanedQuantity = _quantity;
                                                        schedulerSummary.Description = _masterData.Description;
                                                        schedulerSummary.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                        schedulerSummary.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                        schedulerSummary.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                        schedulerSummary.WeekNumber = currentWeekNumber;
                                                        schedulerSummary.KeyUser = _masterData.User;
                                                        schedulerSummary.Area = _masterData.Area;
                                                        schedulerSummary.CreateTime = DateTime.Now;
                                                        schedulerSummary.UpdateTime = DateTime.Now;
                                                        schedulerSummaries.Add(schedulerSummary);
                                                        materialNumberQuantity[_masterData.MaterialNumber] -= _quantity;

                                                        ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                                                        scheduleErrorMessage.MaterialNumber = _masterData.MaterialNumber;
                                                        scheduleErrorMessage.Type = Types.Info;
                                                        scheduleErrorMessage.ErrorMessage = $"{_masterData.MaterialNumber}无法向前插入且无备用线,故放至原始需求日.";
                                                        scheduleErrorMessagelst.Add(scheduleErrorMessage);
                                                    }
                                                    else
                                                    {
                                                        //放于备用线
                                                        var queryStandbyLineSchedulerSummaries = schedulerSummaries.Where(x => x.MaterialNumber == _masterData.MaterialNumber & x.Line == standybyLine & x.SchedulerDate <= date).OrderByDescending(x => x.SchedulerDate).FirstOrDefault();
                                                        if (queryStandbyLineSchedulerSummaries == null)
                                                        {
                                                            SchedulerSummary schedulerSummary1 = new SchedulerSummary();
                                                            var _scheduleDate11 = firstScheduleDate;
                                                            schedulerSummary1.SchedulerDate = _scheduleDate11;
                                                            schedulerSummary1.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                            schedulerSummary1.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate11.ToString("yyyyMMdd")}";
                                                            schedulerSummary1.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                            schedulerSummary1.Plant = _masterData.Plant;
                                                            schedulerSummary1.PlanedQuantity = _quantity2;
                                                            schedulerSummary1.Description = _masterData.Description;
                                                            schedulerSummary1.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                            schedulerSummary1.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                            schedulerSummary1.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                            schedulerSummary1.WeekNumber = currentWeekNumber;
                                                            schedulerSummary1.KeyUser = _masterData.User;
                                                            schedulerSummary1.Area = _masterData.Area;
                                                            schedulerSummary1.CreateTime = DateTime.Now;
                                                            schedulerSummary1.UpdateTime = DateTime.Now;
                                                            schedulerSummaries.Add(schedulerSummary1);
                                                            materialNumberQuantity[_masterData.MaterialNumber] -= _quantity2;
                                                        }
                                                        else
                                                        {
                                                            //放置于备用线也是需要考虑备用线的剩余工时，暂时不做考虑
                                                            var _scheduleDate2 = queryStandbyLineSchedulerSummaries.SchedulerDate;
                                                            schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate2 & x.MaterialNumber == _masterData.MaterialNumber & x.Line == standybyLine & x.SchedulerDate <= date).ToList().ForEach(y =>
                                                            {
                                                                y.PlanedQuantity += _quantity2;
                                                            });
                                                            materialNumberQuantity[_masterData.MaterialNumber] -= _quantity2;
                                                        }

                                                        SchedulerSummary schedulerSummary = new SchedulerSummary();
                                                        schedulerSummary.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                        schedulerSummary.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                                        schedulerSummary.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                        schedulerSummary.Plant = _masterData.Plant;
                                                        schedulerSummary.PlanedQuantity = _quantity1;
                                                        schedulerSummary.Description = _masterData.Description;
                                                        schedulerSummary.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                        schedulerSummary.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                        schedulerSummary.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                        schedulerSummary.WeekNumber = currentWeekNumber;
                                                        schedulerSummary.KeyUser = _masterData.User;
                                                        schedulerSummary.Area = _masterData.Area;
                                                        schedulerSummary.CreateTime = DateTime.Now;
                                                        schedulerSummary.UpdateTime = DateTime.Now;
                                                        schedulerSummaries.Add(schedulerSummary);
                                                        materialNumberQuantity[_masterData.MaterialNumber] -= _quantity1;
                                                    }
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //只插入小需求
                            //首先垂直插入所有小需求
                            //若有剩余工时，横向插入
                            if (date > fullScheduleLastDate)
                            {
                                //向前可插入的日期范围
                                var beforeDateScope = (date - firstScheduleDate).Days;
                                DateTime dynamicFullScheduleLastDate1 = fullScheduleLastDate;
                                var queryDynamicFullScheduleLastDate1 = schedulerSummaries.Where(x => x.WeekNumber == currentWeekNumber & x.MaterialNumber == maxMaterialNumberKeyValue.Key & x.Line == line).OrderByDescending(x => x.SchedulerDate).FirstOrDefault();
                                if (queryDynamicFullScheduleLastDate1 != null)
                                {
                                    dynamicFullScheduleLastDate1 = queryDynamicFullScheduleLastDate1.SchedulerDate.Date;
                                }
                                var afterDateScope = (maxMaterialNumberMaxDate - dynamicFullScheduleLastDate1).Days; //日期相同为0，也可能为负数

                                //查找可插入空间,计算总工时
                                //可插入空间能否将垂直需求全部插入
                                //垂直需求全部可插入,考虑水平需求，能否插入
                                //垂直需求全部不可插入
                                double currentMinPIRsUsedTotalTime = oneDateAllPIR.Sum(x => x.Quantity / x.UPH);
                                double? fullScheduleLastDateUsedTime = schedulerSummaries.Where(x => x.SchedulerDate == fullScheduleLastDate & x.Line == line).Sum(x => x.PlanedQuantity / x.UPH);
                                double? canUsedTotalTime = standareTime - fullScheduleLastDateUsedTime;
                                double _temp = beforeDateScope >= 1 ? standareTime * beforeDateScope : 0;
                                canUsedTotalTime += _temp;

                                if (currentMinPIRsUsedTotalTime <= canUsedTotalTime)
                                {
                                    //垂直需求全部可插入,考虑水平需求，能否插入
                                    for (int j = 0; j < oneDateAllPIR.Count; j++)
                                    {
                                        var _pirDate = date;
                                        var _plant = oneDateAllPIR[j].Plant;
                                        int _quantity = oneDateAllPIR[j].Quantity;
                                        var _materialNumber = oneDateAllPIR[j].MaterialNumber.Replace(" ", "").ToUpper();
                                        var _masterData = materialNumberLineMapRelations.Where(x => x.MaterialNumber == _materialNumber & x.Order == "0").FirstOrDefault();
                                        if (_masterData == null)
                                        {
                                            ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                                            scheduleErrorMessage.MaterialNumber = _materialNumber;
                                            scheduleErrorMessage.ErrorMessage = $"{_materialNumber}未维护主线.";
                                            scheduleErrorMessagelst.Add(scheduleErrorMessage);
                                            continue;
                                        }

                                        var isLastPIR = false;
                                        beforeDateScope = beforeDateScope + 1;
                                        for (int h = 0; h < beforeDateScope; h++)
                                        {
                                            var _scheduleDate = fullScheduleLastDate.AddDays(h);

                                            //排程日不能晚于出货日
                                            if (_scheduleDate > _pirDate) { continue; }

                                            if (_quantity <= 0) { break; }

                                            double? restTime = standareTime - schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line).Sum(x => x.PlanedQuantity / x.UPH);
                                            double? currentMaterialNumberSpendTime = _quantity / double.Parse(_masterData.UPH);

                                            var _restTime = Math.Truncate((double)restTime * 100) / 100;
                                            if (_restTime <= 0) { continue; }

                                            if (restTime >= currentMaterialNumberSpendTime)
                                            {
                                                //判断是否能够水平插入
                                                double? newCurrentMaterialNumberSpendTime = materialNumberQuantity[_masterData.MaterialNumber] / double.Parse(_masterData.UPH);
                                                int _tempQuantity = 0;

                                                if (restTime >= newCurrentMaterialNumberSpendTime & !isLastPIR)
                                                {
                                                    _quantity = materialNumberQuantity[_masterData.MaterialNumber];
                                                    _tempQuantity = materialNumberQuantity[_masterData.MaterialNumber];
                                                    sapPirExtensionModels.Where(x => x.WeekNumber == currentWeekNumber & x.MaterialNumber == _masterData.MaterialNumber).ToList().ForEach(y => { y.Flag = 1; y.Quantity = 0; });
                                                }
                                                else if (!isLastPIR)
                                                {
                                                    _quantity = materialNumberQuantity[_masterData.MaterialNumber];
                                                    _tempQuantity = (int)(restTime * int.Parse(_masterData.UPH));
                                                    sapPirExtensionModels.Where(x => x.WeekNumber == currentWeekNumber & x.MaterialNumber == _masterData.MaterialNumber).ToList().ForEach(y => { y.Flag = 1; });
                                                }

                                                if (isLastPIR)
                                                {
                                                    _tempQuantity = materialNumberQuantity[_masterData.MaterialNumber];
                                                }

                                                var queryIsExisted = schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line & x.MaterialNumber == _materialNumber).FirstOrDefault();
                                                if (queryIsExisted == null)
                                                {
                                                    SchedulerSummary schedulerSummary = new SchedulerSummary();
                                                    schedulerSummary.SchedulerDate = _scheduleDate;
                                                    schedulerSummary.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                    schedulerSummary.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                                    schedulerSummary.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                    schedulerSummary.Plant = _masterData.Plant;
                                                    schedulerSummary.PlanedQuantity = _tempQuantity;
                                                    schedulerSummary.Description = _masterData.Description;
                                                    schedulerSummary.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                    schedulerSummary.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                    schedulerSummary.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                    schedulerSummary.WeekNumber = currentWeekNumber;
                                                    schedulerSummary.KeyUser = _masterData.User;
                                                    schedulerSummary.Area = _masterData.Area;
                                                    schedulerSummary.CreateTime = DateTime.Now;
                                                    schedulerSummary.UpdateTime = DateTime.Now;
                                                    schedulerSummaries.Add(schedulerSummary);

                                                    materialNumberQuantity[_masterData.MaterialNumber] -= _tempQuantity;
                                                }
                                                else
                                                {
                                                    schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line & x.MaterialNumber == _materialNumber).ToList().ForEach(y => y.PlanedQuantity += _quantity);
                                                    materialNumberQuantity[_masterData.MaterialNumber] -= _tempQuantity;
                                                }

                                                _quantity -= _tempQuantity;
                                                isLastPIR = false;
                                            }
                                            else
                                            {
                                                int _tempQuantity = (int)(restTime * int.Parse(_masterData.UPH));
                                                var queryIsExsited = schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line & x.MaterialNumber == _materialNumber).FirstOrDefault();
                                                if (queryIsExsited != null)
                                                {
                                                    schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line & x.MaterialNumber == _materialNumber).ToList().ForEach(y => { y.PlanedQuantity += _tempQuantity; });
                                                }
                                                else
                                                {
                                                    SchedulerSummary schedulerSummary = new SchedulerSummary();
                                                    schedulerSummary.SchedulerDate = _scheduleDate;
                                                    schedulerSummary.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                    schedulerSummary.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                                    schedulerSummary.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                    schedulerSummary.Plant = _masterData.Plant;
                                                    schedulerSummary.PlanedQuantity = _tempQuantity;
                                                    schedulerSummary.Description = _masterData.Description;
                                                    schedulerSummary.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                    schedulerSummary.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                    schedulerSummary.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                    schedulerSummary.WeekNumber = currentWeekNumber;
                                                    schedulerSummary.KeyUser = _masterData.User;
                                                    schedulerSummary.Area = _masterData.Area;
                                                    schedulerSummary.CreateTime = DateTime.Now;
                                                    schedulerSummary.UpdateTime = DateTime.Now;
                                                    schedulerSummaries.Add(schedulerSummary);
                                                }
                                                _quantity -= _tempQuantity;
                                                isLastPIR = true;
                                                materialNumberQuantity[_masterData.MaterialNumber] -= _tempQuantity;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //垂直需求全部不可插入
                                    //若是最晚排程日,找到临界值
                                    //逐个料号寻找备用线，若无备用线,则放置主线，不予理会
                                    for (int j = 0; j < oneDateAllPIR.Count; j++)
                                    {
                                        var _pirDate = date;
                                        var _plant = oneDateAllPIR[j].Plant;
                                        int _quantity = oneDateAllPIR[j].Quantity;
                                        var _materialNumber = oneDateAllPIR[j].MaterialNumber.Replace(" ", "").ToUpper();
                                        var _masterData = materialNumberLineMapRelations.Where(x => x.MaterialNumber == _materialNumber & x.Order == "0").FirstOrDefault();
                                        if (_masterData == null)
                                        {
                                            ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                                            scheduleErrorMessage.MaterialNumber = _materialNumber;
                                            scheduleErrorMessage.ErrorMessage = $"{_materialNumber}未维护主线.";
                                            scheduleErrorMessagelst.Add(scheduleErrorMessage);
                                            continue;
                                        }

                                        for (int h = 0; h < beforeDateScope; h++)
                                        {
                                            var _scheduleDate = fullScheduleLastDate.AddDays(h);

                                            //排程日不能晚于出货日
                                            if (_scheduleDate > _pirDate)
                                            {
                                                //判断备用线
                                                var masterData1 = materialNumberLineMapRelations.Where(x => x.MaterialNumber == oneDateAllPIR[j].MaterialNumber & x.Order != "0").OrderBy(x => x.Order).ToList();
                                                var standybyLine = string.Empty;
                                                if (masterData1.Count >= 1)
                                                {
                                                    foreach (var item in masterData1)
                                                    {
                                                        var isMaintain = lineMaintains.Where(x => x.Line == item.Line & x.StartTime <= DateTime.Now & x.EndTime >= DateTime.Now & x.Enable).ToList().Count != 0;
                                                        if (isMaintain) { continue; }
                                                        standybyLine = item.Line;
                                                        break;
                                                    }
                                                }

                                                //没有找到可用备用线,将其排程至原始需求日
                                                if (string.IsNullOrEmpty(standybyLine))
                                                {
                                                    SchedulerSummary schedulerSummary = new SchedulerSummary();
                                                    schedulerSummary.SchedulerDate = _scheduleDate;
                                                    schedulerSummary.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                    schedulerSummary.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                                    schedulerSummary.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                    schedulerSummary.Plant = _masterData.Plant;
                                                    schedulerSummary.PlanedQuantity = _quantity;
                                                    schedulerSummary.Description = _masterData.Description;
                                                    schedulerSummary.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                    schedulerSummary.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                    schedulerSummary.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                    schedulerSummary.WeekNumber = currentWeekNumber;
                                                    schedulerSummary.KeyUser = _masterData.User;
                                                    schedulerSummary.Area = _masterData.Area;
                                                    schedulerSummary.CreateTime = DateTime.Now;
                                                    schedulerSummary.UpdateTime = DateTime.Now;
                                                    schedulerSummaries.Add(schedulerSummary);
                                                    materialNumberQuantity[_masterData.MaterialNumber] -= _quantity;
                                                }
                                                else
                                                {
                                                    //放于备用线
                                                    var queryStandbyLineSchedulerSummaries = schedulerSummaries.Where(x => x.MaterialNumber == _masterData.MaterialNumber & x.Line == standybyLine & x.SchedulerDate <= date).OrderByDescending(x => x.SchedulerDate).FirstOrDefault();
                                                    if (queryStandbyLineSchedulerSummaries == null)
                                                    {
                                                        SchedulerSummary schedulerSummary1 = new SchedulerSummary();
                                                        var _scheduleDate1 = firstScheduleDate;
                                                        schedulerSummary1.SchedulerDate = _scheduleDate1;
                                                        schedulerSummary1.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                        schedulerSummary1.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate1.ToString("yyyyMMdd")}";
                                                        schedulerSummary1.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                        schedulerSummary1.Plant = _masterData.Plant;
                                                        schedulerSummary1.PlanedQuantity = _quantity;
                                                        schedulerSummary1.Description = _masterData.Description;
                                                        schedulerSummary1.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                        schedulerSummary1.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                        schedulerSummary1.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                        schedulerSummary1.WeekNumber = currentWeekNumber;
                                                        schedulerSummary1.KeyUser = _masterData.User;
                                                        schedulerSummary1.Area = _masterData.Area;
                                                        schedulerSummary1.CreateTime = DateTime.Now;
                                                        schedulerSummary1.UpdateTime = DateTime.Now;
                                                        schedulerSummaries.Add(schedulerSummary1);
                                                        materialNumberQuantity[_masterData.MaterialNumber] -= _quantity;
                                                    }
                                                    else
                                                    {
                                                        //放置于备用线也是需要考虑备用线的剩余工时，暂时不做考虑
                                                        var _scheduleDate2 = queryStandbyLineSchedulerSummaries.SchedulerDate;
                                                        schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate2 & x.MaterialNumber == _masterData.MaterialNumber & x.Line == standybyLine & x.SchedulerDate <= date).ToList().ForEach(y =>
                                                        {
                                                            y.PlanedQuantity += _quantity;
                                                        });
                                                        materialNumberQuantity[_masterData.MaterialNumber] -= _quantity;
                                                    }
                                                }
                                                continue;
                                            }

                                            if (_quantity <= 0) { break; }

                                            double? restTime = standareTime - schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line).Sum(x => x.PlanedQuantity / x.UPH);
                                            double? currentMaterialNumberSpendTime = _quantity / double.Parse(_masterData.UPH);

                                            if (restTime <= 0) { continue; }

                                            if (restTime >= currentMaterialNumberSpendTime)
                                            {
                                                //判断是否能够水平插入
                                                double? newCurrentMaterialNumberSpendTime = materialNumberQuantity[_masterData.MaterialNumber] / double.Parse(_masterData.UPH);
                                                if (restTime >= newCurrentMaterialNumberSpendTime)
                                                {
                                                    _quantity = materialNumberQuantity[_masterData.MaterialNumber];
                                                    sapPirExtensionModels.Where(x => x.WeekNumber == currentWeekNumber & x.MaterialNumber == _masterData.MaterialNumber).ToList().ForEach(y => { y.Flag = 1; y.Quantity = 0; });
                                                }

                                                SchedulerSummary schedulerSummary = new SchedulerSummary();
                                                schedulerSummary.SchedulerDate = _scheduleDate;
                                                schedulerSummary.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                schedulerSummary.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                                schedulerSummary.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                schedulerSummary.Plant = _masterData.Plant;
                                                schedulerSummary.PlanedQuantity = _quantity;
                                                schedulerSummary.Description = _masterData.Description;
                                                schedulerSummary.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                schedulerSummary.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                schedulerSummary.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                schedulerSummary.WeekNumber = currentWeekNumber;
                                                schedulerSummary.KeyUser = _masterData.User;
                                                schedulerSummary.Area = _masterData.Area;
                                                schedulerSummary.CreateTime = DateTime.Now;
                                                schedulerSummary.UpdateTime = DateTime.Now;
                                                schedulerSummaries.Add(schedulerSummary);
                                                _quantity = 0;
                                                materialNumberQuantity[_masterData.MaterialNumber] -= _quantity;
                                            }
                                            else
                                            {
                                                int _tempQuantity = (int)(restTime * int.Parse(_masterData.UPH));
                                                var queryIsExsited = schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line & x.MaterialNumber == _materialNumber).FirstOrDefault();
                                                if (queryIsExsited != null)
                                                {
                                                    schedulerSummaries.Where(x => x.SchedulerDate == _scheduleDate & x.Line == line & x.MaterialNumber == _materialNumber).ToList().ForEach(y => { y.PlanedQuantity += _tempQuantity; });
                                                }
                                                else
                                                {
                                                    SchedulerSummary schedulerSummary = new SchedulerSummary();
                                                    schedulerSummary.SchedulerDate = _scheduleDate;
                                                    schedulerSummary.Line = _masterData.Line.Replace(" ", "").ToUpper();
                                                    schedulerSummary.SchedulerId = $"{_masterData.MaterialNumber.Replace(" ", "").ToUpper()}{_masterData.Line.Replace(" ", "").ToUpper()}{_scheduleDate.ToString("yyyyMMdd")}";
                                                    schedulerSummary.MaterialNumber = _masterData.MaterialNumber.Replace(" ", "").ToUpper();
                                                    schedulerSummary.Plant = _masterData.Plant;
                                                    schedulerSummary.PlanedQuantity = _tempQuantity;
                                                    schedulerSummary.Description = _masterData.Description;
                                                    schedulerSummary.SPQ = _masterData.SPQ == null ? 0 : int.Parse(_masterData.SPQ);
                                                    schedulerSummary.UPH = _masterData.UPH == null ? 0 : double.Parse(_masterData.UPH);
                                                    schedulerSummary.HC = _masterData.HC == null ? 0 : double.Parse(_masterData.HC);
                                                    schedulerSummary.WeekNumber = currentWeekNumber;
                                                    schedulerSummary.KeyUser = _masterData.User;
                                                    schedulerSummary.Area = _masterData.Area;
                                                    schedulerSummary.CreateTime = DateTime.Now;
                                                    schedulerSummary.UpdateTime = DateTime.Now;
                                                    schedulerSummaries.Add(schedulerSummary);
                                                }
                                                _quantity -= _tempQuantity;
                                                materialNumberQuantity[_masterData.MaterialNumber] -= _tempQuantity;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
                            scheduleErrorMessage.MaterialNumber = masterDataOfMaxMaterialNumber.MaterialNumber;
                            scheduleErrorMessage.ErrorMessage = $"料号{masterDataOfMaxMaterialNumber.MaterialNumber}在满额排程后，仍然无法如期出货,放弃线体{line}排程计划.";
                            scheduleErrorMessagelst.Add(scheduleErrorMessage);
                            continue;
                        }
                    }
                }
            }

            //垂直方向，逐个料号插入；横向考虑能否让料号一次性排程
            //假如垂直料号都插入了，还是不能如期出货，是否考虑将最大需求料号向后挪==>此时采取的策略是放弃向后挪

            #region 插入数据库
            if (schedulerSummaries.Count != 0)
            {
                //删除上一次排程记录
                //List<SchedulerSummary> lastScheduleResult = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= endDate.AddDays(1)).ToList();

                //if (lastScheduleResult.Count != 0)
                //{
                //    _apsDbContext.SchedulerSummary.RemoveRange(lastScheduleResult);
                //}
                //_apsDbContext.SchedulerSummary.UpdateRange(schedulerSummaries);
                //_apsDbContext.SaveChanges();
            }
            #endregion

            return schedulerSummaries;
        }
        #endregion

        #region 将排程结果传递至前端
        /// <summary>将数据格式进行转换,传递至前端</summary>
        public SchedulerSummaryViewModel SchedulerSummaryTransforFEData(List<SchedulerSummary> schedulerSummaries)
        {
            SchedulerSummaryViewModel schedulerSummaryForUI = new SchedulerSummaryViewModel();
            List<Hashtable> lst = new List<Hashtable>();
            var distinctMaterialNumberlst = schedulerSummaries.Select(x => x.MaterialNumber?.Replace(" ", "").ToUpper()).OrderBy(x => x).Distinct().ToList();
            var distinctLinelst = schedulerSummaries.Select(x => x.Line?.Replace(" ", "").ToUpper()).OrderBy(x => x).Distinct().ToList();
            var startDate = firstMonday;

            string[] weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            List<TitleElement> titleElements = new List<TitleElement>();
            for (int i = 0; i < 28; i++)
            {
                var scheduleDate = startDate.AddDays(i);
                var dateFormate = scheduleDate.ToString("MM-dd");
                string week = weekdays[Convert.ToInt32(scheduleDate.DayOfWeek)];

                Hashtable hashtable = new Hashtable();
                hashtable.Add("title", $"{dateFormate}-{week}");
                List<Hashtable> _children = [];
                Hashtable hashtable1 = new Hashtable();
                hashtable1.Add("key", $"{dateFormate}-Plan");
                Hashtable hashtable2 = new Hashtable();
                hashtable2.Add("key", $"{dateFormate}-Actual");
                Hashtable hashtable3 = new Hashtable();
                hashtable3.Add("key", $"{dateFormate}-Mark");
                _children.Add(hashtable1);
                _children.Add(hashtable2);
                _children.Add(hashtable3);

                titleElements.Add(new TitleElement() { title = hashtable, children = _children });
            }
            schedulerSummaryForUI.title = titleElements;

            foreach (var line in distinctLinelst)
            {
                foreach (var materialNumber in distinctMaterialNumberlst)
                {
                    if (string.IsNullOrEmpty(materialNumber) || string.IsNullOrEmpty(line)) { continue; }

                    //查询相同MaterialNumber,Line对应的排程计划数据
                    var queryResultByMaterialNumberLinelst = schedulerSummaries.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber & x.Line?.Replace(" ", "").ToUpper() == line).DistinctBy(x => x.SchedulerDate).ToList();
                    if (queryResultByMaterialNumberLinelst.Count != 0)
                    {
                        Hashtable hashtable = new Hashtable();
                        foreach (var item in queryResultByMaterialNumberLinelst)
                        {
                            var dateFormate = item.SchedulerDate.ToString("MM-dd");

                            //basic info
                            if (hashtable.Keys.Count == 0)
                            {
                                hashtable.Add("PN", materialNumber);
                                hashtable.Add("key", line + materialNumber);
                                hashtable.Add("Line", line);
                                hashtable.Add("UPH", item.UPH);
                                hashtable.Add("HC", item.HC);
                                hashtable.Add("SPQ", item.SPQ);
                                //hashtable.Add("Site", item.Site);
                                //hashtable.Add("PCBA", item.PCBA);
                                hashtable.Add("Description", item.Description);
                            }

                            //dynamic info
                            if (!hashtable.ContainsKey($"{dateFormate}-Plan"))
                            {
                                hashtable.Add($"{dateFormate}-Plan", item.PlanedQuantity);
                                hashtable.Add($"{dateFormate}-Actual", item.ActualQuantity);
                                hashtable.Add($"{dateFormate}-Mark", item.Remark);
                            }
                        }
                        lst.Add(hashtable);
                    }
                }
            }
            schedulerSummaryForUI.list = lst;
            return schedulerSummaryForUI;
        }
        /// <summary>获取已经做过排程的记录</summary>
        public SchedulerSummaryViewModel GetSchedulerSummary(DateTime startDate = default(DateTime))
        {
            if (startDate == default(DateTime))
                startDate = firstMonday.AddDays(7);
            var _scheduleSummaryResult = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= startDate).ToList();
            SchedulerSummaryViewModel schedulerSummaryViewModel = new SchedulerSummaryViewModel();
            if (_scheduleSummaryResult.Count > 0)
            {
                schedulerSummaryViewModel = SchedulerSummaryTransforFEData(_scheduleSummaryResult);
            }
            return schedulerSummaryViewModel;
        }
        /// <summary>更新排程计划</summary>
        public List<UpdateSchedulerSummaryModel> UpdateSchedulerSummary(UpdateSchedulerSummaryViewModel updateSchedulerSummaryViewModel)
        {
            updateSchedulerSummaryViewModel.list.ToList();
            List<Hashtable> _list = new List<Hashtable>();
            List<UpdateSchedulerSummaryModel> schedulerSummaryModellst = new List<UpdateSchedulerSummaryModel>();
            if (updateSchedulerSummaryViewModel.list.Count == 0) { return schedulerSummaryModellst; }

            //通过PN+Line+Date查找
            _list = updateSchedulerSummaryViewModel.list;

            foreach (Hashtable item in _list)
            {
                if (!item.ContainsKey("PN") || !item.ContainsKey("Line")) { continue; }   //保存失败

                foreach (var key in item.Keys)
                {
                    var _key = key.ToString();
                    var _value = item[key] == null ? string.Empty : item[key].ToString();
                    if (!_key.Contains('-')) { continue; }
                    var _keySplit = _key.Split('-');
                    var _dateStr = _keySplit.Length >= 4 ? _keySplit[0] + _keySplit[1] + _keySplit[2] : DateTime.Now.ToString("yyyy") + _keySplit[0] + _keySplit[1];
                    var _schedulerId = item["PN"]?.ToString()?.Replace(" ", "").ToUpper() + item["Line"]?.ToString()?.Replace(" ", "").ToUpper() + _dateStr;

                    var queryResult = schedulerSummaryModellst.Where(x => x.SchedulerId == _schedulerId).FirstOrDefault();
                    if (queryResult != null)
                    {
                        if (_keySplit[^1].ToString().Replace(" ", "").Equals("PLAN", StringComparison.CurrentCultureIgnoreCase))
                        {
                            queryResult.PlanedQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
                            continue;
                        }

                        if (_keySplit[^1].ToString().Replace(" ", "").Equals("ACTUAL", StringComparison.CurrentCultureIgnoreCase))
                        {
                            queryResult.ActualQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
                            continue;
                        }

                        if (_keySplit[^1].ToString().Replace(" ", "").Equals("MARK", StringComparison.CurrentCultureIgnoreCase))
                        {
                            queryResult.Remark = _value;
                            continue;
                        }
                    }
                    else
                    {
                        UpdateSchedulerSummaryModel updateSchedulerSummaryModel = new UpdateSchedulerSummaryModel();
                        updateSchedulerSummaryModel.SchedulerId = _schedulerId;
                        if (_keySplit[_keySplit.Length - 1].ToString().Replace(" ", "").ToUpper() == "PLAN")
                        {
                            updateSchedulerSummaryModel.PlanedQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
                            schedulerSummaryModellst.Add(updateSchedulerSummaryModel);
                            continue;
                        }

                        if (_keySplit[_keySplit.Length - 1].ToString().Replace(" ", "").ToUpper() == "ACTUAL")
                        {
                            updateSchedulerSummaryModel.ActualQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
                            schedulerSummaryModellst.Add(updateSchedulerSummaryModel);
                            continue;
                        }

                        if (_keySplit[_keySplit.Length - 1].ToString().Replace(" ", "").ToUpper() == "MARK")
                        {
                            updateSchedulerSummaryModel.Remark = _value;
                            schedulerSummaryModellst.Add(updateSchedulerSummaryModel);
                            continue;
                        }
                    }
                }
            }

            if (schedulerSummaryModellst.Count == 0) { return schedulerSummaryModellst; }

            //更新数据
            foreach (var item in schedulerSummaryModellst)
            {
                var queryResult = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerId == item.SchedulerId).FirstOrDefault();
                if (queryResult != null)
                {
                    queryResult.PlanedQuantity = item.PlanedQuantity;
                    queryResult.ActualQuantity = item.ActualQuantity;
                    queryResult.Remark = item.Remark;
                    if (!string.IsNullOrEmpty(item.Remark))
                    {

                    }
                    _apsDbContext.SchedulerSummary.Update(queryResult);

                    //保存历史记录
                    var queryHistoryResult = _apsDbContext.SchedulerSummaryHistory.Where(x => x.SchedulerId == item.SchedulerId).OrderByDescending(x => x.UpdateTime).FirstOrDefault();
                    if (queryHistoryResult != null)
                    {
                        if (queryHistoryResult.PlanedQuantity != item.PlanedQuantity || queryHistoryResult.ActualQuantity != item.ActualQuantity || queryHistoryResult.Remark != item.Remark)
                        {
                            queryHistoryResult.PlanedQuantity = item.PlanedQuantity;
                            queryHistoryResult.ActualQuantity = item.ActualQuantity;
                            queryHistoryResult.Remark = item.Remark;
                            _apsDbContext.SchedulerSummaryHistory.Update(queryHistoryResult);
                        }
                    }
                }
                _apsDbContext.SaveChanges();
            }

            return schedulerSummaryModellst;
        }

        #endregion

        #region 获取PIR数据
        /// <summary>获取原始PIR数据</summary>
        public SapPIRInfoViewModel GetPirInfo()
        {
            List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Quantity >= 1 & x.Date != null).ToList();

            var distinctLinelst = materialNumberLineMapRelations.Where(x => x.Order == "0").Select(x => x.Line.Replace(" ", "").ToUpper()).Distinct().ToList();
            var distinctMaterialNumberlst = sapDemands.Select(x => x.MaterialNumber?.Replace(" ", "").ToUpper()).Distinct().ToList();

            SapPIRInfoViewModel sapPIRInfoViewModel = new SapPIRInfoViewModel();
            var distinctDateTotallst = sapDemands.Where(x => x.Date != null).Select(x => x.Date.Date.ToString("yyyy-MM-dd")).Distinct().OrderBy(x => x).ToList();
            sapPIRInfoViewModel.title = distinctDateTotallst;

            if (sapDemands.Count > 0 & distinctMaterialNumberlst.Count != 0)
            {
                List<Hashtable> _list = [];
                List<Dictionary<string, string?>> dictionaries = new List<Dictionary<string, string?>>();
                foreach (var materialNumber in distinctMaterialNumberlst)
                {
                    var distinctDatelst = sapDemands.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber).DistinctBy(x => x.Date).ToList();
                    Dictionary<string, string?> keyValuePairs = [];
                    if (!keyValuePairs.ContainsKey(materialNumber))
                    {
                        var queryResult = materialNumberLineMapRelations.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber & x.Order == "0").FirstOrDefault();
                        if (queryResult != null)
                        {
                            keyValuePairs.Add("Line", queryResult.Line.Replace(" ", "").ToUpper());
                        }
                        else
                        {
                            keyValuePairs.Add("Line", "NA");
                        }
                        keyValuePairs.Add("key", materialNumber);
                        keyValuePairs.Add("PN", materialNumber);
                    }
                    if (distinctDatelst.Count != 0)
                    {
                        foreach (var item in distinctDatelst)
                        {
                            var _key = item.Date.ToString("yyyy-MM-dd");
                            if (!keyValuePairs.ContainsKey(_key))
                            {
                                keyValuePairs.Add(_key, item.Quantity.ToString());
                            }
                        }
                    }
                    dictionaries.Add(keyValuePairs);
                }
                sapPIRInfoViewModel.current = 1;
                sapPIRInfoViewModel.pageSize = 20;
                sapPIRInfoViewModel.startDate = DateTime.Now.ToString("yyyy-MM-dd");

                var sortedDictionaries = dictionaries.OrderBy(dict => dict["Line"]).ToList();
                sapPIRInfoViewModel.total = dictionaries.Count;
                sapPIRInfoViewModel.list = sortedDictionaries;
            }
            return sapPIRInfoViewModel;
        }
        /// <summary>获取原始PIR数据</summary>
        public SapPIRInfoViewModel GetAllPirInfo()
        {
            List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Quantity >= 1 & x.Date != null).ToList();
            List<MaterialNumberLineMapRelations> materialNumberLineMapRelations = _apsDbContext.MaterialNumberLineMapRelations.Where(x => x.Line != null & x.Order == "0").ToList();
            var distinctLinelst = materialNumberLineMapRelations.Where(x => x.Line != null).Select(x => x.Line?.Replace(" ", "").ToUpper()).Distinct().ToList();
            var distinctMaterialNumberlst = sapDemands.Select(x => x.MaterialNumber?.Replace(" ", "").ToUpper()).Distinct().ToList();

            SapPIRInfoViewModel sapPIRInfoViewModel = new SapPIRInfoViewModel();
            var distinctDateTotallst = sapDemands.Where(x => x.Date != null).Select(x => x.Date.Date.ToString("yyyy-MM-dd")).Distinct().OrderBy(x => x).ToList();
            sapPIRInfoViewModel.title = distinctDateTotallst;
            //行转列
            if (sapDemands.Count > 0 & distinctMaterialNumberlst.Count != 0)
            {
                List<Hashtable> _list = [];
                List<Dictionary<string, string?>> dictionaries = new List<Dictionary<string, string?>>();
                foreach (var materialNumber in distinctMaterialNumberlst)
                {
                    var distinctDatelst = sapDemands.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber).DistinctBy(x => x.Date).ToList();
                    Dictionary<string, string?> keyValuePairs = [];
                    if (!keyValuePairs.ContainsKey(materialNumber))
                    {
                        var queryResult = materialNumberLineMapRelations.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber).FirstOrDefault();
                        if (queryResult != null)
                        {
                            keyValuePairs.Add("Line", queryResult.Line?.Replace(" ", "").ToUpper());
                        }
                        else
                        {
                            keyValuePairs.Add("Line", "NA");
                        }
                        keyValuePairs.Add("key", materialNumber);
                        keyValuePairs.Add("PN", materialNumber);
                    }
                    if (distinctDatelst.Count != 0)
                    {
                        foreach (var item in distinctDatelst)
                        {
                            var _key = item.Date.ToString("yyyy-MM-dd");
                            if (!keyValuePairs.ContainsKey(_key))
                            {
                                keyValuePairs.Add(_key, item.Quantity.ToString());
                            }
                        }
                    }
                    dictionaries.Add(keyValuePairs);
                }
                sapPIRInfoViewModel.current = 1;
                sapPIRInfoViewModel.pageSize = 20;
                sapPIRInfoViewModel.startDate = DateTime.Now.ToString("yyyy-MM-dd");

                var sortedDictionaries = dictionaries.OrderBy(dict => dict["Line"]).ThenBy(dict => dict["PN"]).ToList();
                sapPIRInfoViewModel.total = dictionaries.Count;
                sapPIRInfoViewModel.list = sortedDictionaries;
            }
            return sapPIRInfoViewModel;
        }
        /// <summary>及时获取PIR数据</summary>
        public List<SapOriginalPir> GetPirInfoInTime()
        {
            HttpClient _httpClient = new HttpClient();
            _httpClient.Timeout = TimeSpan.FromSeconds(120);
            List<SapOriginalPir> sapOriginalPirs = [];
            var uri = "http://10.2.34.21:8100/api/sap/GetPIRInfo";
            //var uri = _configuration.GetSection("SapPir").ToString(); 
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            HttpResponseMessage response = _httpClient.Send(request);
            try
            {
                // 确保HTTP响应成功  
                HttpResponseMessage resmsg = response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    var responseBody = response.Content.ReadAsStream();
                    StreamReader reader = new(responseBody);
                    string text = reader.ReadToEnd();
                    var apiResult = JsonConvert.DeserializeObject<ApiResult>(text);
                    if (apiResult != null & apiResult.Data != null)
                    {
                        sapOriginalPirs = JsonConvert.DeserializeObject<List<SapOriginalPir>>(apiResult.Data.ToString());
                    }
                }

                if (sapOriginalPirs.Count == 0) { return sapOriginalPirs; }

                List<SAPDemand> sapDemands = [];
                foreach (var item in sapOriginalPirs)
                {
                    if (string.IsNullOrEmpty(item.Date.Replace(" ", "")) || string.IsNullOrEmpty(item.Quantity.Replace(" ", "").ToString())) { continue; }
                    var date = DateTime.ParseExact(item.Date.Replace(" ", "").ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                    SAPDemand sAPDemand = new SAPDemand();
                    sAPDemand.Plant = item.Plant;
                    sAPDemand.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
                    sAPDemand.Type = item.Type.Replace(" ", "").ToUpper();
                    sAPDemand.Date = date;
                    sAPDemand.Quantity = int.Parse(item.Quantity.Replace(" ", "").ToUpper());
                    sAPDemand.ReleaseTime = DateTime.Now;
                    sAPDemand.CreateTime = DateTime.Now;
                    sAPDemand.UpdateTime = DateTime.Now;
                    sapDemands.Add(sAPDemand);
                }

                if (sapDemands.Count != 0)
                {
                    //_apsDbContext.GetTableFromSql("TRUNCATE TABLE [AP_SuZhou_APS_DevDb].[APS].[SAPDemand]");
                    _apsDbContext.Database.ExecuteSqlRaw("TRUNCATE TABLE [AP_SuZhou_APS_DevDb].[APS].[SAPDemand]");
                    _apsDbContext.SAPDemand.UpdateRange(sapDemands);
                    _apsDbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex.Message);
                return sapOriginalPirs;
            }
            return sapOriginalPirs;
        }
        #endregion

        #region 日，周达成率
        /// <summary>每条线的损耗工时</summary>
        public EachLineTotalTimeViewModel GetEachLineTotalTimesInfo()
        {
            //获取原始需求 //计算
            DateTime dt = DateTime.Now;
            var result = from x in _apsDbContext.SAPDemand
                         join y in _apsDbContext.MaterialMainLine
                         on x.MaterialNumber.Replace(" ", "").ToUpper() equals y.MaterialNumber.Replace(" ", "").ToUpper()
                         where x.Date >= dt & x.Quantity >= 1 & x.MaterialNumber != null & y.MainLine != null
                         select new
                         {
                             Date = x.Date,
                             Quantity = x.Quantity,
                             UPH = y.UPH,
                             Line = y.MainLine.Replace(" ", "").ToUpper(),
                             StandbyLineA = y.StandbyLineA == null ? y.StandbyLineA.Replace(" ", "").ToUpper() : string.Empty,
                             StandbyLineB = y.StandbyLineB == null ? y.StandbyLineB.Replace(" ", "").ToUpper() : string.Empty,
                             StandbyLineC = y.StandbyLineC == null ? y.StandbyLineC.Replace(" ", "").ToUpper() : string.Empty,
                             MaterialNumber = y.MaterialNumber
                         };
            EachLineTotalTimeViewModel eachLineTotalTimeViewModel = new EachLineTotalTimeViewModel();
            List<Hashtable> hashtables = [];

            if (result.Count() != 0)
            {
                var distinctLineslst = result.Select(x => x.Line).Distinct().OrderBy(x => x).ToList();
                if (distinctLineslst.Count() != 0)
                {
                    foreach (var line in distinctLineslst)
                    {
                        //相同线别,日期进行汇总
                        var samelineResult = result.Where(x => x.Line == line).ToList();
                        if (samelineResult.Count() != 0)
                        {
                            Hashtable hashtable = new Hashtable();
                            var querySeminline = result.Where(x => x.Line == line & x.StandbyLineA != null).FirstOrDefault();

                            var _lineQty = 1;
                            var _line = line;

                            if (querySeminline != null)
                            {
                                _line = querySeminline.Line + querySeminline.StandbyLineA;
                                _lineQty = 2;
                            }

                            hashtable.Add("key", line);
                            hashtable.Add("Line", line);
                            hashtable.Add("Workday", 6);
                            hashtable.Add("LineQty", _lineQty);
                            hashtable.Add("OEE", 0.9);

                            var distinctDateResult = samelineResult.Select(x => x.Date).Distinct().ToList();
                            if (distinctDateResult.Count() != 0)
                            {
                                foreach (var date in distinctDateResult)
                                {
                                    //相同线体，日期，不同MaterialNumber
                                    //var sameLineAndDateResult = samelineResult.Where(x => x.Date == item).ToList();
                                    var sameLineAndDateResult = samelineResult.Where(x => x.Date < date.AddDays(7) & x.Date >= date).ToList();

                                    if (sameLineAndDateResult.Count() != 0)
                                    {
                                        var values = sameLineAndDateResult.Select(x => new { _ = x.Quantity / x.UPH }).ToArray();
                                        double totalCount = values.Sum(x => x._);

                                        //累加至周一
                                        //星期一为第一天  
                                        int weeknow = Convert.ToInt32(date.DayOfWeek);

                                        //因为是以星期一为第一天，所以要判断weeknow等于0时，要向前推6天。  
                                        weeknow = (weeknow == 0 ? (7 - 1) : (weeknow - 1));
                                        int daydiff = (-1) * weeknow;
                                        //本周第一天  
                                        DateTime? firstDay = date.AddDays(daydiff);

                                        var _key = firstDay?.ToString("yyyy-MM-dd");
                                        //hashtable.Add(item?.ToString("yyyy-MM-dd"), totalCount);
                                        if (hashtable.ContainsKey(_key))
                                        {
                                            var _res = (double)hashtable[_key] + totalCount;
                                            hashtable[_key] = _res;
                                        }
                                        else
                                        {
                                            hashtable.Add(firstDay?.ToString("yyyy-MM-dd"), totalCount);
                                        }
                                    }

                                }
                            }
                            hashtables.Add(hashtable);
                        }
                        eachLineTotalTimeViewModel.current = 1;
                        eachLineTotalTimeViewModel.pageSize = 20;
                        eachLineTotalTimeViewModel.startDate = dt.Date.ToString("yyyy-MM-dd");

                    }

                    eachLineTotalTimeViewModel.total = hashtables.Count;
                    eachLineTotalTimeViewModel.list = hashtables;
                }
            }
            return eachLineTotalTimeViewModel;
        }
        #endregion

        #region Download
        public string DownloadPIRInfo(SapPIRInfoViewModel sapPIRInfoViewModel)
        {
            //<<<<<<< HEAD
            string excelName = "Template";
            string excelPath = string.Empty;
            try
            {
                //首先创建Excel文件对象
                var workbook = new HSSFWorkbook();

                //创建工作表，也就是Excel中的sheet，给工作表赋一个名称(Excel底部名称)
                var sheet = workbook.CreateSheet("sheet1");

                //sheet.DefaultColumnWidth = 20;//默认列宽

                sheet.ForceFormulaRecalculation = false;//TODO:是否开始Excel导出后公式仍然有效（非必须）

                //二级标题列样式设置
                var listData = sapPIRInfoViewModel.list;
                var headTopStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 15, true, 700, "楷体", true, false, false, true, FillPattern.SolidForeground, HSSFColor.Grey25Percent.Index, HSSFColor.Black.Index, FontUnderlineType.None, FontSuperScript.None, false);

                var _headerlist = new List<string>() { "PN", "Line" };
                _headerlist.AddRange(sapPIRInfoViewModel.title);
                var maxRows = _headerlist.Count();
                var headerArr = _headerlist.ToArray();
                var row = NpoiExcelExportHelper._.CreateRow(sheet, 0, 24);  //第二行
                var cell = row.CreateCell(0);
                for (var i = 0; i < headerArr.Length; i++)
                //=======
                //    DateOnly endDate = new DateOnly(2024, 4, 21);
                //    DateOnly FiexdDate = new DateOnly(2024, 3, 31);

                //    TestVO vo = new TestVO();
                //    List<TestVOEle> list = new List<TestVOEle>();


                //    List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Quantity >= 1 && x.Date != null && x.MaterialNumber == m && x.Date > FiexdDate.AddDays(1).ToDateTime(TimeOnly.MinValue) && x.Date < endDate.AddDays(15).ToDateTime(TimeOnly.MinValue)).ToList();
                //    LogHelper.Info("==============");
                //    foreach (var item in sapDemands)
                //    {
                //        LogHelper.Info("Date->" + item.Date);
                //        LogHelper.Info("Number->" + item.Quantity);
                //    }

                //    list = sapDemands.Select(ele => new TestVOEle()
                //    {
                //        Date = DateOnly.FromDateTime((DateTime)ele.Date),
                //        Number = ele.Quantity
                //    }).ToList();
                //    foreach (var item in list)
                //    {
                //        LogHelper.Info("Date->" + item.Date);
                //        LogHelper.Info("Number->" + item.Number);
                //    }
                //    vo.pir = list;
                //    vo.pirNumber = vo.pir.Where(ele => ele.Date > FiexdDate).Sum(ele => ele.Number);
                //    list = new List<TestVOEle>();
                //    FactoryPlan p = TestScheduler();
                //    foreach (var n in p.LinePlanDictionary.Values)
                //    {
                //        foreach (var d in n.DayPlanList)
                //        {
                //            foreach (var mm in d.MaterialPlanDictionary.Values)
                //            {
                //                if (mm.Material == m)
                //                {

                //                    list.Add(new TestVOEle()
                //                    {
                //                        Date = mm.Date,
                //                        Number = mm.Number
                //                    });
                //                }
                //            }
                //        }
                //    }
                //    list.Sort((p1, p2) => p1.Date.CompareTo(p2.Date));
                //    vo.plan = list.Where(ele => ele.Date > FiexdDate).ToList();
                //    vo.planNumber = vo.plan.Sum(ele => ele.Number);
                //    return vo;
                //}
                //DateOnly pirDate = new DateOnly(2024, 3, 26);
                //DateOnly endDate = new DateOnly(2024, 4, 21);
                //DateOnly FiexdDate = new DateOnly(2024, 3, 31);
                //DateOnly startDate = new DateOnly(2024, 3, 25);
                //public FactoryPlan TestScheduler()
                //{

                //    try
                //    {

                //        List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Quantity >= 1 && x.Date != null).ToList();
                //        List<SAPDemand> pirList = SplitPir(sapDemands, pirDate, endDate);
                //        List<MaterialMainLine> materialMainLines = _apsDbContext.MaterialMainLine.Where(ele => ele.MaterialNumber != null).ToList();
                //        Dictionary<string, MaterialMainLine> dictionary = materialMainLines.GroupBy(item => item.MaterialNumber).Select(group => group.Last()).ToDictionary(p => p.MaterialNumber, p => p);
                //        List<SchedulerSummary> schedulerSummaries = _apsDbContext.SchedulerSummary.ToList();
                //        List<SchedulerSummary> schedulerSummarieList = schedulerSummaries.Where(x => IsBetween(x.SchedulerDate, pirDate, FiexdDate)).Where(x=>x.UPH!=null&&x.HC!=null&&x.PlanedQuantity!=null).ToList();
                //        foreach (var item in schedulerSummarieList) {
                //            if (item.Line=="L26")
                //            {
                //                LogHelper.Info(System.Text.Json.JsonSerializer.Serialize(item));
                //            }
                //        }
                //        FactoryPlan factoryPlan = new FactoryPlan(pirList, schedulerSummarieList, dictionary, FiexdDate);
                //        factoryPlan.EndDate = endDate;
                //        factoryPlan.StartDate = startDate;
                //        factoryPlan.RestDaySet = new HashSet<DateOnly>() { new DateOnly(2024, 3, 23), new DateOnly(2024, 3, 24), new DateOnly(2024, 3, 30), new DateOnly(2024, 3, 31), new DateOnly(2024, 4, 4), new DateOnly(2024, 4, 5), new DateOnly(2024, 4, 6), new DateOnly(2024, 4, 13), new DateOnly(2024, 4, 14), new DateOnly(2024, 4, 20), new DateOnly(2024, 4, 21), new DateOnly(2024, 4, 27), new DateOnly(2024, 4, 28), };
                //        factoryPlan.LineRestDayDictionary = new Dictionary<string, ISet<DateOnly>>() { };
                //        Dictionary<DateOnly, double> hcDictionary = new Dictionary<DateOnly, double>();
                //        for (DateOnly d = startDate; d <= endDate.AddDays(20); d = d.AddDays(1))
                //        {
                //            hcDictionary.Add(d, 700);
                //        }
                //        factoryPlan.DebugScheduler(hcDictionary);
                //        //_apsDbContext.SchedulerSummary.AddRange(factoryPlan.ToSchedulerSummaryList());
                //        //_apsDbContext.SaveChangesAsync();
                //        return factoryPlan;
                //    }
                //    catch (Exception ex)
                //    {
                //        LogHelper.Info(ex.ToString());
                //    }
                //    return null;
                //}
                //public FactoryPlan GetSchedulerResult() {
                //    List<SchedulerSummary> schedulerSummaries = _apsDbContext.SchedulerSummary.ToList();
                //    List<SchedulerSummary> schedulerSummarieList = schedulerSummaries.Where(x => IsBetween(x.SchedulerDate, startDate, endDate)).Where(x => x.UPH != null && x.HC != null && x.PlanedQuantity != null).ToList();
                //    FactoryPlan factoryPlan = new FactoryPlan( schedulerSummarieList);
                //    return factoryPlan;
                //}
                //private List<MaterialPlan> CheckMaterial(FactoryPlan factoryPlan, string material)
                //{
                //    List<MaterialPlan> list = new List<MaterialPlan>();
                //    foreach (var line in factoryPlan.LinePlanDictionary.Values)
                //    {
                //        foreach (var day in line.DayPlanList)
                //>>>>>>> 6a2e649962f0cb1df92b3121ac1f0ec4805a2e93
                {
                    cell = NpoiExcelExportHelper._.CreateCells(row, headTopStyle, i, headerArr[i]);
                    sheet.SetColumnWidth(i, 5000); //设置单元格宽度
                }

                //单元格边框样式
                var cellStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 10, true, 400);

                var scheduleDate = DateTime.Now;
                int rowNumber = 0;
                foreach (var diclist in listData)
                {
                    rowNumber++;
                    row = NpoiExcelExportHelper._.CreateRow(sheet, rowNumber, 24);  //第二行
                    int columnNumber = 0;
                    List<int> existedColumnIndexlist = [];
                    foreach (var key in diclist.Keys.OrderBy(x => x.Length))
                    {
                        var content = diclist[key] == null ? "" : diclist[key].ToString();
                        var columnIndex = _headerlist.FindIndex(x => x.Replace(" ", "").ToUpper() == key.Replace(" ", "").ToUpper());
                        if (columnIndex == -1) { continue; }
                        cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, columnIndex, content);
                        existedColumnIndexlist.Add(columnIndex);
                    }

                    //补充空白
                    var newMaxRowsCopy = maxRows;
                    if (maxRows >= 1)
                    {
                        for (int i = 0; i < newMaxRowsCopy; i++)
                        {
                            if (!existedColumnIndexlist.Contains(i))
                            {
                                Date = mm.Date,
                                Number = mm.Number
                            });
                        }
                    }
                }
            }
            list.Sort((p1, p2) => p1.Date.CompareTo(p2.Date));
            vo.plan = list.Where(ele => ele.Date > FiexdDate).ToList();
            vo.planNumber = vo.plan.Sum(ele => ele.Number);
            return vo;
        }
        public FactoryPlan TestScheduler()
        {
            DateOnly pirDate = new DateOnly(2024, 3, 26);
            DateOnly endDate = new DateOnly(2024, 4, 21);
            DateOnly FiexdDate = new DateOnly(2024, 3, 31);
            DateOnly startDate = new DateOnly(2024, 3, 25);
            List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Quantity >= 1 && x.Date != null).ToList();
            List<SAPDemand> pirList = SplitPir(sapDemands, pirDate, endDate);
            List<MaterialMainLine> materialMainLines = _apsDbContext.MaterialMainLine.Where(ele => ele.MaterialNumber != null).ToList();
            Dictionary<string, MaterialMainLine> dictionary = materialMainLines.GroupBy(item => item.MaterialNumber).Select(group => group.Last()).ToDictionary(p => p.MaterialNumber, p => p);

            FactoryPlan factoryPlan = new FactoryPlan(pirList, new List<SchedulerSummary>(), dictionary);
            factoryPlan.FiexdDate = FiexdDate;
            factoryPlan.EndDate = endDate;
            factoryPlan.StartDate = startDate;
            factoryPlan.RestDaySet = new HashSet<DateOnly>() { new DateOnly(2024, 3, 23), new DateOnly(2024, 3, 24), new DateOnly(2024, 3, 30), new DateOnly(2024, 3, 31), new DateOnly(2024, 4, 4), new DateOnly(2024, 4, 5), new DateOnly(2024, 4, 6) };
            factoryPlan.LineRestDayDictionary = new Dictionary<string, ISet<DateOnly>>() { };
            Dictionary<DateOnly, double> hcDictionary = new Dictionary<DateOnly, double>();
            for (DateOnly d = startDate; d <= endDate.AddDays(20); d = d.AddDays(1))
            {
                hcDictionary.Add(d, 1500);
            }
            try
            {
                factoryPlan.DebugScheduler(hcDictionary);
            }
            catch (Exception ex)
            {
                LogHelper.Info(ex.ToString());
            }
            string m = "17048955-01M1";
            List<MaterialPlan> finishedList = CheckMaterial(factoryPlan, m);
            LogHelper.Info("finished list:" + System.Text.Json.JsonSerializer.Serialize(finishedList));
            LogHelper.Info("finished list sum-->:" + finishedList.Sum(ele => ele.Number));
            return factoryPlan;
        }
        private List<MaterialPlan> CheckMaterial(FactoryPlan factoryPlan, string material)
        {
            List<MaterialPlan> list = new List<MaterialPlan>();
            foreach (var line in factoryPlan.LinePlanDictionary.Values)
            {
                foreach (var day in line.DayPlanList)
                {
                    foreach (MaterialPlan m in day.MaterialPlanDictionary.Values)
                    {
                        if (m.Material == material && m.Date > factoryPlan.FiexdDate)
                        {
                            list.Add(m);
                        }
                    }
                }
            }
            return excelPath;
        }
        public string DownloadStaffTimeByWeekInfo(DownloadModel downloadModel)
        {
            string excelName = "Template";
            string excelPath = string.Empty;
            try
            {
                //首先创建Excel文件对象
                var workbook = new HSSFWorkbook();

                //创建工作表，也就是Excel中的sheet，给工作表赋一个名称(Excel底部名称)
                var sheet = workbook.CreateSheet("sheet1");

                //sheet.DefaultColumnWidth = 20;//默认列宽

                sheet.ForceFormulaRecalculation = false;//TODO:是否开始Excel导出后公式仍然有效（非必须）

                //二级标题列样式设置
                var headTopStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 15, true, 700, "楷体", true, false, false, true, FillPattern.SolidForeground, HSSFColor.Grey25Percent.Index, HSSFColor.Black.Index,
                FontUnderlineType.None, FontSuperScript.None, false);
                var headerlist = downloadModel.title.ToArray();
                var maxRows = headerlist.Count();
                var row = NpoiExcelExportHelper._.CreateRow(sheet, 0, 24);  //第二行
                var cell = row.CreateCell(0);
                for (var i = 0; i < headerlist.Length; i++)
                {
                    cell = NpoiExcelExportHelper._.CreateCells(row, headTopStyle, i, headerlist[i]);
                    sheet.SetColumnWidth(i, 5000); //设置单元格宽度
                }

                //单元格边框样式
                var cellStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 10, true, 400);

                var scheduleDate = DateTime.Now;
                int rowNumber = 0;
                var listData = downloadModel.data;
                foreach (var diclist in listData)
                {
                    rowNumber++;
                    row = NpoiExcelExportHelper._.CreateRow(sheet, rowNumber, 24);  //第二行
                    int columnNumber = 0;
                    List<int> existedColumnIndexlist = [];
                    foreach (var key in diclist.Keys.OrderBy(x => x.Length))
                    {
                        var content = diclist[key] == null ? "" : diclist[key].ToString();
                        var columnIndex = headerlist.ToList().FindIndex(x => x.Replace(" ", "").ToUpper() == key.Replace(" ", "").ToUpper());
                        if (columnIndex == -1) { continue; }
                        cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, columnIndex, content);
                        existedColumnIndexlist.Add(columnIndex);
                    }

                    //补充空白
                    var newMaxRowsCopy = maxRows;
                    if (maxRows >= 1)
                    {
                        for (int i = 0; i < newMaxRowsCopy; i++)
                        {
                            if (!existedColumnIndexlist.Contains(i))
                            {
                                cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, i, string.Empty);
                            }
                        }
                    }
                }

                string folder = DateTime.Now.ToString("yyyyMMdd");


                var uploadPath = AppDomain.CurrentDomain.BaseDirectory + "UploadFile\\" + folder + "\\";

                //excel保存文件名
                string excelFileName = excelName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";

                //创建目录文件夹
                if (!Directory.Exists(uploadPath))
                {
                    Directory.CreateDirectory(uploadPath);
                }

                //Excel的路径及名称
                excelPath = uploadPath + excelFileName;

                //使用FileStream文件流来写入数据（传入参数为：文件所在路径，对文件的操作方式，对文件内数据的操作）
                var fileStream = new FileStream(excelPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                //向Excel文件对象写入文件流，生成Excel文件
                workbook.Write(fileStream);

                //关闭文件流
                fileStream.Close();

                //释放流所占用的资源
                fileStream.Dispose();

                //excel文件保存的相对路径，提供前端下载
                var relativePositioning = "/UploadFile/" + folder + "/" + excelFileName;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return excelPath;
        }
        #endregion
    }
}