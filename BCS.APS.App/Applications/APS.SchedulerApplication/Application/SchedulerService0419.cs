﻿//using APS.SchedulerApplication.Entitys;
//using APS.SchedulerApplication.Interfaces;
//using APS.SchedulerApplication.Model;
//using APS.Utils.Attributes;
//using APS.Utils.Extensions;
//using APS.Utils.Helper;
//using APS.Utils.Models;
//using Newtonsoft.Json;
//using StackExchange.Redis;
//using System.Collections;
//using APS.SchedulerApplication.Model.BO;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.Configuration;
//using NPOI.HSSF.UserModel;
//using NPOI.HSSF.Util;
//using NPOI.SS.UserModel;
//using static NPOI.HSSF.Util.HSSFColor;
//using System.Numerics;
//using Microsoft.AspNetCore.Http;
//namespace APS.SchedulerApplication.Application
//{
//    [Transient]
//    public class SchedulerService0419 : ISchedulerService
//    {
//        private APSDbContext _apsDbContext;
//        static DateTime date = DateTime.Now.Date;
//        static DateTime firstMonday;
//        static DateTime scheduleLastDay; //挖掘数据的最后一天
//        static int weekNumber;
//        static List<EachDayHumanQuantity> eachDayHumanQuantitys;
//        static List<FactoryCalendar> factoryCalendars;
//        static List<LineMaintain> lineMaintains;
//        static List<SchedulerSummary> ReleaseToSapSchedulerSummaries;
//        static List<SapPirModel> originalPirs;
//        static List<SapPirExtensionModel> idealSapPirExtensions;
//        static readonly HttpClient _httpClient = new HttpClient();
//        private readonly IConfiguration _configuration;

//        public SchedulerService0419(APSDbContext apsDbContext, IConfiguration configuration)
//        {
//            _apsDbContext = apsDbContext;
//            _configuration = configuration;
//            firstMonday = WeekHelper.GetMondayDate(date);
//            weekNumber = WeekHelper.GetWeekNumber(date);
//            scheduleLastDay = firstMonday.AddDays(27);
//            eachDayHumanQuantitys = _apsDbContext.EachDayHumanQuantity.ToList();
//            lineMaintains = _apsDbContext.LineMaintain.Where(x => x.Enable).ToList();
//            factoryCalendars = _apsDbContext.FactoryCalendar.Where(x => x.IsWork).ToList();
//        }

//        #region 日排程

//        #region 日排程主函数
//        /// <summary>
//        /// Main Service
//        /// </summary>
//        /// <param name="dateTime"></param>
//        /// <returns></returns>
//        public SchedulerSummaryViewModel ExecuteSchedulerSummary(DateTime startDate = default, DateTime endDate = default)
//        {
//            double oee = 0.9;
//            //获取原始的PIR需求，并分配线体，排序
//            if (startDate == default)
//                startDate = DateTime.Now.Date;

//            if (endDate == default)
//                endDate = firstMonday.AddDays(6);

//            eachDayHumanQuantitys.ForEach(x => x.Date = x.Date.Date);
//            lineMaintains.ForEach(x => { x.StartTime = x.StartTime.Date; x.EndTime = x.EndTime.Date; });
//            factoryCalendars.ForEach(x => x.Date = x.Date.Date);

//            //获取原始PIR,分配线体，扣除库存
//            originalPirs = GetOriginalPIR(endDate);

//            //日期提前，拆分需求日
//            List<SapPirModel> sapPirs = SplitAndAdvancePickUpDate(endDate, originalPirs);

//            //创建需求模型
//            idealSapPirExtensions = ComputedEachPirThenDispatch(sapPirs);

//            //预警日提前处理
//            List<SapPirExtensionModel> sapPirExtensions = SplitFullPir(idealSapPirExtensions, endDate);

//            //日排程
//            List<SchedulerSummary> schedulerSummaries = ScheduleEachDay(sapPirExtensions, startDate, endDate);
//            return SchedulerSummaryTransforFEData(schedulerSummaries);
//        }
//        #endregion

//        #region 处理原始PIR

//        /// <summary>
//        /// #获取原始PIR
//        /// #分配线体
//        /// #扣除上一版本的库存
//        /// </summary>
//        public List<SapPirModel> GetOriginalPIR(DateTime endDate)
//        {
//            List<SapPirModel> lst = [];
//            //获取原始PIR并分配线体
//            var _result = from x in _apsDbContext.SAPDemand
//                          join y in _apsDbContext.MaterialMainLine
//                          on x.MaterialNumber.Replace(" ", "").ToUpper() equals y.MaterialNumber.Replace(" ", "").ToUpper()
//                          where x.Date != null && x.Date >= firstMonday && x.Date < firstMonday.AddDays(42) && x.Quantity >= 1 && x.MaterialNumber != null && y.MainLine != null
//                          select new SapPirModel
//                          {
//                              MaterialNumber = y.MaterialNumber.Replace(" ", "").ToUpper(),
//                              Plant = x.Plant,
//                              DeliveryDate = (DateTime)x.Date,
//                              PlanedQuantity = x.Quantity,
//                              Type = x.Type,
//                              ReleaseTime = x.ReleaseTime,

//                              UPH = y.UPH,
//                              HC = y.HC,
//                              SPQ = y.SPQ,
//                              Site = y.Site,
//                              Area = y.Area,
//                              KeyUser = y.KeyUser,
//                              PCBA = y.PCBA,
//                              Description = y.Description,
//                              Line = y.MainLine.Replace(" ", "").ToUpper(),
//                              StandbyLineA = y.StandbyLineA,
//                              StandbyLineB = y.StandbyLineB,
//                              StandbyLineC = y.StandbyLineC,

//                              CreateTime = DateTime.Now,
//                              UpdateTime = DateTime.Now,
//                              Flag = 0
//                          };
//            var result = _result.ToList();
//            if (result.Count == 0) { return lst; }

//            //扣除库存
//            //净需求=Fixed日期范围内的原始PIR-上一版本排程的

//            //获取原始PIR
//            var lastVersionSchedulesFromPir = result.Where(x => x.DeliveryDate.Date < endDate.AddDays(1)).ToList();

//            //获取上一版本的排程计划
//            var lastVersionSchedules = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= firstMonday.Date && x.SchedulerDate < endDate.AddDays(1) && !x.Deleted).ToList();
//            Dictionary<string, int> materialNumberAcutualQuantitys = [];
//            if (lastVersionSchedules.Count != 0)
//            {
//                List<string> distinctMaterialNumbers = lastVersionSchedules.Select(x => x.MaterialNumber).Distinct().ToList();
//                int _totalQuantity = 0;
//                foreach (var materialNumber in distinctMaterialNumbers)
//                {
//                    _totalQuantity = lastVersionSchedules.Where(x => x.MaterialNumber.Equals(materialNumber)).Sum(x => x.PlanedQuantity).ToInt();
//                    materialNumberAcutualQuantitys.TryAdd(materialNumber, _totalQuantity);
//                }
//            }

//            //净需求
//            if (lastVersionSchedulesFromPir.Count != 0)
//            {
//                if (materialNumberAcutualQuantitys.Keys.Count != 0)
//                {
//                    foreach (var item in lastVersionSchedulesFromPir)
//                    {
//                        item.DeliveryDate = item.DeliveryDate.Date;
//                        if (materialNumberAcutualQuantitys.Keys.TakeWhile(x => x == item.MaterialNumber).Count() > 0)
//                        {
//                            item.PlanedQuantity -= materialNumberAcutualQuantitys[item.MaterialNumber];
//                        }
//                    }
//                }
//            }

//            //扣除净需求
//            var newLastVersionSchedulesFromPir = lastVersionSchedulesFromPir.Where(x => x.PlanedQuantity != 0).ToList();
//            var nextWeeksPirs = result.Where(x => x.DeliveryDate >= endDate.AddDays(1)).ToList();

//            //将上一版本的净需求一分为二
//            var _newLastVersionSchedulesFromPir1 = newLastVersionSchedulesFromPir.Where(x => x.PlanedQuantity > 0).ToList();
//            var _newLastVersionSchedulesFromPir2 = newLastVersionSchedulesFromPir.Where(x => x.PlanedQuantity < 0).ToList();

//            //说明上周没有生产完，有剩余
//            //==>判断接下来判断首日有无需求
//            //==>有需求则累加，无需求则直接添加
//            //上一版没有生产完成的，全部挪到第二周周二，然后日期提前一天
//            if (_newLastVersionSchedulesFromPir1.Count != 0)
//            {
//                foreach (var item in _newLastVersionSchedulesFromPir1)
//                {
//                    var queryExistInFirstDay = nextWeeksPirs.Where(x => x.DeliveryDate == endDate.AddDays(2) && x.MaterialNumber == item.MaterialNumber && x.Line == item.Line).ToList();
//                    item.DeliveryDate = item.DeliveryDate.Date;

//                    if (queryExistInFirstDay.Count != 0)
//                    {
//                        nextWeeksPirs.Where(x => x.DeliveryDate == endDate.AddDays(1) && x.MaterialNumber == item.MaterialNumber && x.Line == item.Line).ToList().ForEach(x => x.PlanedQuantity += item.PlanedQuantity);
//                    }
//                    else
//                    {
//                        item.DeliveryDate = endDate.DayOfWeek == DayOfWeek.Saturday ? endDate.AddDays(3) : endDate.AddDays(2);
//                        nextWeeksPirs.Add(item);
//                    }
//                }
//                lst.AddRange(nextWeeksPirs);
//            }

//            //说明上周生产完，且多生产了
//            //#判断接下来有无再生产此料号
//            //==>没有生产此料号，则不做任何处理
//            //==>有生产，则进行扣除
//            //#如何扣除
//            if (_newLastVersionSchedulesFromPir2.Count != 0)
//            {
//                int _currentWeekQuantity = 0;
//                int _nextWeeksTotalQuantity = 0;
//                foreach (var item in _newLastVersionSchedulesFromPir2)
//                {
//                    _currentWeekQuantity = item.PlanedQuantity * -1;
//                    item.DeliveryDate = item.DeliveryDate.Date;

//                    //判断下接下来有无生产此料号计划
//                    var _nextWeeksPirs = nextWeeksPirs.Where(x => x.MaterialNumber == item.MaterialNumber && x.Line == item.Line).ToList();
//                    if (_nextWeeksPirs.Count() == 0) { continue; } //说明接下来五周内不再生产此料号

//                    //接下来仍然会生产此料号
//                    //说明上周生产完，且多生产了 ==>在接下来几周内扣除多生产的
//                    //接下来将上一版本的计划数量全部扣除
//                    //计算出接下来六周内全部排程计划
//                    _nextWeeksTotalQuantity = _nextWeeksPirs.Sum(x => x.PlanedQuantity).ToInt();
//                    if (_nextWeeksTotalQuantity == 0) { continue; } //说明接下来五周内不再生产此料号

//                    //说明接下来六周全部被扣除
//                    if (_currentWeekQuantity >= _nextWeeksTotalQuantity)
//                    {
//                        nextWeeksPirs.Where(x => x.MaterialNumber == item.MaterialNumber && x.Line == item.Line).ToList().ForEach(x => x.PlanedQuantity = 0);
//                    }
//                    else
//                    {
//                        //说明接下来六周排程足够扣除库存
//                        //遍历未来排程日，起始日期为Fixed的第二天
//                        foreach (var item2 in nextWeeksPirs)
//                        {
//                            //if (item2.MaterialNumber != item.MaterialNumber && item2.Line != item.Line) { continue; }
//                            //无需考虑线别，因为这个料号可能安排到别的线生产
//                            if (item2.MaterialNumber != item.MaterialNumber) { continue; }
//                            _currentWeekQuantity -= (int)item2.PlanedQuantity;
//                            item2.Flag = 1; //表示已经被扣过库存

//                            if (_currentWeekQuantity >= 0)
//                            {
//                                //等于,说明刚好扣除完库存
//                                //继续扣除库存
//                                continue;
//                            }

//                            if (_currentWeekQuantity < 0)
//                            {
//                                //小于,说明到了临界值，将多减的加回来 ，并保留此原始PIR需求
//                                item2.PlanedQuantity = _currentWeekQuantity * -1;
//                                break;
//                            }
//                        }
//                        var restNextWeeksPirs = nextWeeksPirs.Where(x => x.MaterialNumber == item.MaterialNumber && x.Flag == 0).ToList();
//                        lst.AddRange(restNextWeeksPirs);
//                    }
//                }
//            }

//            return lst;
//        }

//        /// <summary>
//        /// 第二周日期提前一天
//        /// 第三，四，五，六周，拆到周一，三，五，然后再提前一天
//        /// </summary>
//        /// <param name="endDate"></param>
//        /// <param name="sapPirs"></param>
//        /// <returns></returns>
//        public List<SapPirModel> SplitAndAdvancePickUpDate(DateTime endDate, List<SapPirModel> sapPirs)
//        {
//            List<SapPirModel> lst = [];
//            //对原始需求进行分割
//            var secondWeekPIRlst = sapPirs.Where(x => x.DeliveryDate >= endDate.AddDays(1) && x.DeliveryDate < firstMonday.AddDays(14)).OrderBy(x => x.DeliveryDate).ToList();
//            var futureWeeksPIRlst = sapPirs.Where(x => x.DeliveryDate >= firstMonday.AddDays(14)).OrderBy(x => x.DeliveryDate).ToList();

//            //第二周
//            secondWeekPIRlst.ForEach(x => x.DeliveryDate = x.DeliveryDate.AddDays(-1));

//            //第三，四，五，六周
//            int _quantity = 0;
//            List<SapPirModel> newFutureWeeksPIRlst = [];
//            foreach (var item in futureWeeksPIRlst)
//            {
//                _quantity = item.PlanedQuantity;
//                if (item.PlanedQuantity <= 0) { continue; }
//                if (item.PlanedQuantity <= 50 && item.PlanedQuantity >= 1)
//                {
//                    //小于等于2不拆分
//                    newFutureWeeksPIRlst.Add(item);
//                    continue;
//                }
//                else
//                {
//                    //拆分的天数
//                    int dayCount = 3;
//                    //当拆分不能被整除时，余数放到最后一天
//                    int avaQuantity = _quantity / dayCount;
//                    //根据当前需求日，获取当前周的周一日期
//                    var currentMonDayDate = WeekHelper.GetMondayDate(item.DeliveryDate);

//                    #region 由周拆至日
//                    SapPirModel newItem1 = new();
//                    newItem1 = AutoMapperHelper.Map<SapPirModel, SapPirModel>(item);
//                    newItem1.PlanedQuantity = avaQuantity;
//                    newItem1.DeliveryDate = currentMonDayDate.AddDays(-1);
//                    newFutureWeeksPIRlst.Add(newItem1);

//                    SapPirModel newItem2 = new();
//                    newItem2 = AutoMapperHelper.Map<SapPirModel, SapPirModel>(item);
//                    newItem2.PlanedQuantity = avaQuantity;
//                    newItem2.DeliveryDate = currentMonDayDate.AddDays(1);
//                    newFutureWeeksPIRlst.Add(newItem2);

//                    SapPirModel newItem3 = new();
//                    newItem3 = AutoMapperHelper.Map<SapPirModel, SapPirModel>(item);
//                    newItem3.PlanedQuantity = _quantity - 2 * avaQuantity;
//                    newItem3.DeliveryDate = currentMonDayDate.AddDays(3);
//                    newFutureWeeksPIRlst.Add(newItem3);
//                    #endregion  由周拆至日
//                }
//            }
//            lst.AddRange(secondWeekPIRlst);
//            lst.AddRange(newFutureWeeksPIRlst);
//            return lst;
//        }

//        /// <summary>
//        /// # 人力
//        /// # 工厂日历
//        /// # 线体维护
//        /// </summary>
//        /// <param name="sapPirModels"></param>
//        public List<SapPirExtensionModel> ComputedEachPirThenDispatch(List<SapPirModel> sapPirs)
//        {
//            List<SapPirExtensionModel> sapPirExtensions = new();
//            //对其进行分组，排程日，线体

//            var distinctDates = sapPirs.Select(x => x.DeliveryDate).Distinct().OrderBy(x => x).ToList();
//            foreach (var date in distinctDates)
//            {
//                SapPirExtensionModel sapPirExtensionModel = new SapPirExtensionModel();
//                sapPirExtensionModel.Date = date;

//                var sameDateResult = sapPirs.Where(x => x.DeliveryDate == date).ToList();
//                var distinctlines = sameDateResult.Select(x => x.Line).OrderBy(x => x).Distinct().ToList();

//                List<LineInfo> lineInfos = [];

//                foreach (var line in distinctlines)
//                {
//                    List<MaterialInfo> materialInfos = [];
//                    LineInfo lineInfo = new() { Line = line };

//                    //相同线别不同料号
//                    var distinctMaterialNumbers = sameDateResult.Where(x => x.Line == line).ToList();
//                    sapPirExtensionModel.Plant = distinctMaterialNumbers.FirstOrDefault().Plant;
//                    foreach (var item in distinctMaterialNumbers)
//                    {
//                        MaterialInfo materialInfo = new MaterialInfo
//                        {
//                            MaterialNumber = item.MaterialNumber,

//                            UPH = item.UPH,
//                            PlanedQuantity = item.PlanedQuantity,
//                            ActualQuantity = item.ActualQuantity,
//                            EachMaterialTime = Math.Truncate(item.PlanedQuantity / item.UPH * 1000) / 1000,

//                            SPQ = item.SPQ,
//                            HC = item.HC,
//                            Site = item.Site,
//                            Area = item.Area,
//                            KeyUser = item.KeyUser,
//                            PCBA = item.PCBA,
//                            Description = item.Description
//                        };
//                        materialInfos.Add(materialInfo);
//                    }

//                    if (materialInfos.Count != 0)
//                    {
//                        lineInfo.MaterialNumberQty = materialInfos.Count;
//                        lineInfo.EachLineHC = materialInfos.Max(x => x.HC);
//                        lineInfo.EachLineTime = Math.Truncate(materialInfos.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                        lineInfo.EachLineSpareTime = Math.Truncate((lineInfo.StandardTime - materialInfos.Sum(x => x.EachMaterialTime)) * 1000) / 1000;

//                        lineInfo.MaterialInfoList = materialInfos;
//                        lineInfos.Add(lineInfo);
//                    }
//                }
//                if (lineInfos.Count != 0)
//                {
//                    sapPirExtensionModel.LineQty = lineInfos.Count;
//                    sapPirExtensionModel.TotalHCOfEachLine = (double)lineInfos.Sum(x => x.EachLineHC);
//                    sapPirExtensionModel.EachDayTime = Math.Truncate(lineInfos.Sum(x => x.EachLineTime) * 1000) / 1000;
//                    sapPirExtensionModel.LineInfoList = lineInfos;
//                    sapPirExtensions.Add(sapPirExtensionModel);
//                }
//            }
//            return sapPirExtensions;
//        }
//        #endregion

//        #region 预警日处理

//        /// <summary>
//        /// 将每天,每条线预警的需求进行分拆，向前面的日期排
//        /// </summary>
//        public static List<SapPirExtensionModel> SplitFullPir(List<SapPirExtensionModel> sapPirExtensions, DateTime endDate)
//        {
//            List<SapPirExtensionModel> sapPirExtensionModels = [];

//            //存储预警扣除后的需求
//            List<SapPirExtensionModel> _restSapPirExtensions = [];
//            //存储预警需要挪走的需求
//            List<SapPirExtensionModel> _morePartSapPirExtensions = [];
//            //汇总挪走后的需求
//            List<SapPirExtensionModel> _totalSapPirExtensions = [];

//            sapPirExtensions = sapPirExtensions.Where(x => x.Date >= endDate.AddDays(1)).OrderBy(x => x.Date).ToList();
//            foreach (var item in sapPirExtensions)
//            {
//                //目标日基本信息
//                var _currentDateDate = item.Date;
//                var _currentDatePlant = item.Plant;
//                var _currentDateLineQty = item.LineQty;
//                var _currentDateEachDayTime = item.EachDayTime;
//                var _currentDateTotalHCOfEachLine = item.TotalHCOfEachLine;
//                var _currentDateLineInfoList = item.LineInfoList;

//                if (_currentDateDate == endDate.AddDays(1)) { _restSapPirExtensions.Add(item); continue; }

//                //寻找某日,某线,某料号集合
//                List<LineInfo> _restPartLineInfoList = [];
//                List<LineInfo> _morePartLineInfoList = [];
//                foreach (var lineInfo in _currentDateLineInfoList)
//                {
//                    if (lineInfo.Line == "L26")
//                    {

//                    }

//                    //说明这条线刚好能够完成该线体下所有料号的生产
//                    if (lineInfo.EachLineSpareTime >= 0)
//                    {
//                        _restPartLineInfoList.Add(lineInfo);
//                    }

//                    //说明当天，这条线，存在工时不够，那么就需要将多余的需求向前挪
//                    if (lineInfo.EachLineSpareTime < 0)
//                    {
//                        //特别说明，预警日，默认其工时能够做满    //先挪主线//然后挪副线
//                        var _currentDateMaterilInfoList = lineInfo.MaterialInfoList.OrderBy(x => x.EachMaterialTime).ToList();
//                        var _currentDateLineRestTime = Math.Abs(lineInfo.StandardTime);
//                        List<MaterialInfo> _restPartMaterialInfoList = [];     //剩下的
//                        List<MaterialInfo> _morePartMaterialInfoList = [];     //需要挪走的

//                        foreach (var materialInfo in _currentDateMaterilInfoList)
//                        {
//                            //获取到需要挪日期的所有料号列表
//                            _currentDateLineRestTime = Math.Truncate((_currentDateLineRestTime - materialInfo.EachMaterialTime) * 1000) / 1000;
//                            if (_currentDateLineRestTime > 0)     //说明还有超时需求
//                            {
//                                materialInfo.Flag = 1;
//                                var _restPartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);
//                                _restPartMaterialInfo.Flag = 0;
//                                _restPartMaterialInfoList.Add(_restPartMaterialInfo);     //挪走
//                            }
//                            else
//                            {
//                                materialInfo.Flag = 1;
//                                //临界需要拆分 --先恢复
//                                _currentDateLineRestTime = Math.Truncate((_currentDateLineRestTime + materialInfo.EachMaterialTime) * 1000) / 1000;

//                                //留下的
//                                var _restPartMaterialEachMaterialTime = Math.Truncate(_currentDateLineRestTime * 1000) / 1000;
//                                var _restPartMaterialPlanedQuantity = _restPartMaterialEachMaterialTime * materialInfo.UPH;
//                                var _restPartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);
//                                _restPartMaterialInfo.Flag = 0;
//                                _restPartMaterialInfo.PlanedQuantity = (int)_restPartMaterialPlanedQuantity;
//                                _restPartMaterialInfo.EachMaterialTime = _restPartMaterialEachMaterialTime;
//                                _restPartMaterialInfoList.Add(_restPartMaterialInfo);

//                                var _morePartMaterialEachMaterialTime = Math.Truncate((materialInfo.EachMaterialTime - _restPartMaterialEachMaterialTime) * 1000) / 1000;
//                                var _morePartMaterialPlanedQuantity = materialInfo.PlanedQuantity - (int)_restPartMaterialPlanedQuantity;

//                                var _morePartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);
//                                _morePartMaterialInfo.Flag = 0;
//                                _morePartMaterialInfo.PlanedQuantity = (int)_morePartMaterialPlanedQuantity;
//                                _morePartMaterialInfo.EachMaterialTime = _morePartMaterialEachMaterialTime;
//                                _morePartMaterialInfoList.Add(_morePartMaterialInfo);

//                                var restMateriallst = _currentDateMaterilInfoList.Where(x => x.Flag == 0).ToList();
//                                if (restMateriallst.Count != 0)
//                                {
//                                    _morePartMaterialInfoList.AddRange(restMateriallst);
//                                }

//                                break;
//                            }
//                        }

//                        if (_morePartMaterialInfoList.Count != 0)   //挪走
//                        {
//                            var morePartlineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(lineInfo);
//                            morePartlineInfo.EachLineTime = Math.Truncate(_morePartMaterialInfoList.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                            morePartlineInfo.EachLineSpareTime = Math.Truncate((morePartlineInfo.StandardTime - morePartlineInfo.EachLineTime) * 1000) / 1000;
//                            morePartlineInfo.MaterialInfoList = _morePartMaterialInfoList;
//                            morePartlineInfo.MaterialNumberQty = _morePartMaterialInfoList.Count;
//                            morePartlineInfo.EachLineHC = _morePartMaterialInfoList.Max(x => x.HC);
//                            _morePartLineInfoList.Add(morePartlineInfo);
//                        }

//                        //重新计算剩余时间
//                        if (_restPartMaterialInfoList.Count != 0)
//                        {
//                            var restPartlineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(lineInfo);
//                            restPartlineInfo.EachLineTime = Math.Truncate(_restPartMaterialInfoList.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                            restPartlineInfo.EachLineSpareTime = Math.Truncate((lineInfo.StandardTime - lineInfo.EachLineTime) * 1000) / 1000;
//                            restPartlineInfo.MaterialInfoList = _restPartMaterialInfoList;
//                            restPartlineInfo.MaterialNumberQty = _restPartMaterialInfoList.Count;
//                            restPartlineInfo.EachLineHC = _restPartMaterialInfoList.Max(x => x.HC);
//                            _restPartLineInfoList.Add(restPartlineInfo);
//                        }
//                    }
//                }

//                //重新计算剩余时间
//                var newPartItem = AutoMapperHelper.Map<SapPirExtensionModel, SapPirExtensionModel>(item);
//                newPartItem.EachDayTime = Math.Truncate(_morePartLineInfoList.Sum(x => x.EachLineTime) * 1000) / 1000;
//                newPartItem.LineInfoList = _morePartLineInfoList;
//                newPartItem.LineQty = _morePartLineInfoList.Count;
//                newPartItem.TotalHCOfEachLine = _morePartLineInfoList.Sum(x => x.EachLineHC);
//                if (_morePartLineInfoList.Count != 0) { _morePartSapPirExtensions.Add(newPartItem); }

//                item.EachDayTime = Math.Truncate(_restPartLineInfoList.Sum(x => x.EachLineTime) * 1000) / 1000;
//                item.LineInfoList = _restPartLineInfoList;
//                item.LineQty = _restPartLineInfoList.Count;
//                item.TotalHCOfEachLine = _restPartLineInfoList.Sum(x => x.EachLineHC);
//                _restSapPirExtensions.Add(item);
//            }

//            sapPirExtensionModels = DistributePirToMainLinendSubLine(_restSapPirExtensions, _morePartSapPirExtensions, endDate);
//            return sapPirExtensionModels;
//        }

//        /// <summary>
//        /// 将预警日主线向前挪
//        /// 剩余部分放到副线，若没有副线则恢复到原始需求线
//        /// </summary>
//        public static List<SapPirExtensionModel> DistributePirToMainLinendSubLine(List<SapPirExtensionModel> _oldSapPirExtensions, List<SapPirExtensionModel> _newSapPirExtensions, DateTime endDate)
//        {
//            List<SapPirExtensionModel> sapPirExtensionModels = [];
//            _oldSapPirExtensions.ForEach(x => x.LineInfoList.ForEach(y => y.MaterialInfoList.ForEach(z => z.Flag = 0)));
//            _newSapPirExtensions.ForEach(x => x.LineInfoList.ForEach(y => y.MaterialInfoList.ForEach(z => z.Flag = 0)));
//            _newSapPirExtensions = _newSapPirExtensions.Where(x => x.LineInfoList.Count != 0).ToList();

//            //将预警的需求挪走
//            if (_newSapPirExtensions.Count != 0)
//            {
//                foreach (var newSapPirItem in _newSapPirExtensions)
//                {
//                    #region _dynamicSapPirItem
//                    var _currentDate = newSapPirItem.Date;
//                    var _firstScheduleDate = endDate.AddDays(1);
//                    DateTime _endDate = Convert.ToDateTime(_currentDate.ToShortDateString());
//                    DateTime _startDate = Convert.ToDateTime(_firstScheduleDate.ToShortDateString());
//                    int days = _endDate.Subtract(_startDate).Days + 1;

//                    var _dynamicSapPirItem = AutoMapperHelper.Map<SapPirExtensionModel, SapPirExtensionModel>(newSapPirItem);
//                    _dynamicSapPirItem.LineInfoList = newSapPirItem.LineInfoList;
//                    #endregion

//                    for (int i = 1; i < days; i++)
//                    {
//                        #region filter Info
//                        var previousDate = _currentDate.AddDays(-i);
//                        //周六，周日不排计划 ==>跳过
//                        if (previousDate.DayOfWeek == DayOfWeek.Saturday || previousDate.DayOfWeek == DayOfWeek.Sunday) { continue; }

//                        //工厂日历  ==>被锁住
//                        if (factoryCalendars.Where(x => x.Date == previousDate).FirstOrDefault() != null) { continue; }

//                        //人力  ==>人力不足跳过
//                        var currentDateHCModel = eachDayHumanQuantitys.Where(x => x.Date.Date == previousDate).FirstOrDefault();
//                        if (currentDateHCModel == null) { continue; }
//                        var currentDateHC = currentDateHCModel.Quantity * currentDateHCModel.OEE;
//                        if (currentDateHCModel.OEE > 1 || currentDateHCModel.OEE < 0) { continue; }
//                        #endregion

//                        //将预警日需求提前
//                        var _targetDateModel = _oldSapPirExtensions.Where(x => x.Date == previousDate).FirstOrDefault();
//                        var AddPartModel = AutoMapperHelper.Map<SapPirExtensionModel, SapPirExtensionModel>(_dynamicSapPirItem);
//                        AddPartModel.LineInfoList = _dynamicSapPirItem.LineInfoList;
//                        AddPartModel.Date = previousDate;
//                        if (_targetDateModel == null)
//                        {
//                            //判断全天,即整天挪 ==>然后逐条线别判断
//                            if (_dynamicSapPirItem.EachDayTime <= _dynamicSapPirItem.LineQty * _dynamicSapPirItem.StandardTime)
//                            {
//                                _oldSapPirExtensions.Add(AddPartModel);  //说明被挪的目标本身就完全能够在这一天生产完
//                                break;
//                            }
//                            else
//                            {
//                                //判断整条线,即整条线挪==>然后逐个物料挪
//                                var lineInfoList = _dynamicSapPirItem.LineInfoList.OrderByDescending(x => x.EachLineTime).ToList();
//                                List<LineInfo> _restPartLineInfoList = [];
//                                List<LineInfo> _morePartLineInfoList = [];
//                                foreach (var lineInfo in lineInfoList)
//                                {
//                                    //这条线是否在维护中
//                                    //能够开线
//                                    if (currentDateHC >= lineInfo.EachLineHC)
//                                    {
//                                        if (lineInfo.EachLineSpareTime >= 0)
//                                        {
//                                            currentDateHC -= lineInfo.EachLineHC;
//                                            _restPartLineInfoList.Add(lineInfo);     ////整条线挪走
//                                            continue;
//                                        }

//                                        //被挪的线体，需求报红，那么就需要将多余的需求向前挪
//                                        if (lineInfo.EachLineSpareTime < 0)
//                                        {
//                                            var _currentDateMaterilInfoList = lineInfo.MaterialInfoList.OrderByDescending(x => x.EachMaterialTime).ToList();
//                                            var _currentDateLineRestTime = lineInfo.StandardTime;

//                                            List<MaterialInfo> _restPartMaterialInfoList = [];
//                                            List<MaterialInfo> _morePartMaterialInfoList = [];
//                                            foreach (var materialInfo in _currentDateMaterilInfoList)
//                                            {
//                                                //获取到需要挪日期的所有料号列表
//                                                _currentDateLineRestTime = Math.Truncate((_currentDateLineRestTime - materialInfo.EachMaterialTime) * 1000) / 1000;
//                                                if (_currentDateLineRestTime > 0)
//                                                {
//                                                    materialInfo.Flag = 1;
//                                                    var _restPartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);
//                                                    _restPartMaterialInfo.Flag = 0;
//                                                    _restPartMaterialInfoList.Add(_restPartMaterialInfo);
//                                                }
//                                                else
//                                                {
//                                                    //临界需要拆分--先恢复
//                                                    materialInfo.Flag = 1;
//                                                    _currentDateLineRestTime = Math.Truncate((_currentDateLineRestTime + materialInfo.EachMaterialTime) * 1000) / 1000;

//                                                    var _restPartMaterialInfoEachMaterialTime = _currentDateLineRestTime;
//                                                    var _restPartMaterialInfoPlanedQuantity = (int)(_currentDateLineRestTime * materialInfo.UPH);
//                                                    var _restPartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);
//                                                    _restPartMaterialInfo.Flag = 0;
//                                                    _restPartMaterialInfo.PlanedQuantity = _restPartMaterialInfoPlanedQuantity;
//                                                    _restPartMaterialInfo.EachMaterialTime = _restPartMaterialInfoEachMaterialTime;
//                                                    _restPartMaterialInfoList.Add(_restPartMaterialInfo);

//                                                    var _morePartMaterialInfoEachMaterialTime = Math.Truncate((materialInfo.EachMaterialTime - _restPartMaterialInfoEachMaterialTime) * 1000) / 1000;
//                                                    var _morePartMaterialInfoPlanedQuantity = materialInfo.PlanedQuantity - _restPartMaterialInfoPlanedQuantity;
//                                                    var _morePartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);
//                                                    _morePartMaterialInfo.Flag = 0;
//                                                    _morePartMaterialInfo.PlanedQuantity = _morePartMaterialInfoPlanedQuantity;
//                                                    _morePartMaterialInfo.EachMaterialTime = _morePartMaterialInfoEachMaterialTime;
//                                                    _morePartMaterialInfoList.Add(_morePartMaterialInfo);

//                                                    var _othersMateriallst = _currentDateMaterilInfoList.Where(x => x.Flag == 0).ToList();
//                                                    //将剩余的都添加到没有挪走的
//                                                    var restMateriallst = _currentDateMaterilInfoList.Where(x => x.Flag == 0).ToList();
//                                                    _morePartMaterialInfoList.AddRange(restMateriallst);
//                                                    break;
//                                                }
//                                            }

//                                            if (_restPartMaterialInfoList.Count != 0)
//                                            {
//                                                var _restPartLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(lineInfo);
//                                                _restPartLineInfo.MaterialInfoList = _restPartMaterialInfoList;
//                                                _restPartLineInfo.MaterialNumberQty = _restPartMaterialInfoList.Count;
//                                                _restPartLineInfo.EachLineHC = _restPartMaterialInfoList.Max(x => x.HC);
//                                                _restPartLineInfo.EachLineTime = Math.Truncate(_restPartMaterialInfoList.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                                _restPartLineInfo.EachLineSpareTime = Math.Truncate((_restPartLineInfo.StandardTime - _restPartLineInfo.EachLineTime) * 1000) / 1000;
//                                                _restPartLineInfoList.Add(_restPartLineInfo);
//                                            }

//                                            if (_morePartMaterialInfoList.Count != 0)
//                                            {
//                                                var _morePartLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(lineInfo);
//                                                _morePartLineInfo.MaterialInfoList = _morePartMaterialInfoList;
//                                                _morePartLineInfo.MaterialNumberQty = _morePartMaterialInfoList.Count;
//                                                _morePartLineInfo.EachLineHC = _morePartMaterialInfoList.Max(x => x.HC);
//                                                _morePartLineInfo.EachLineTime = Math.Truncate(_morePartMaterialInfoList.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                                _morePartLineInfo.EachLineSpareTime = Math.Truncate((_morePartLineInfo.StandardTime - _morePartLineInfo.EachLineTime) * 1000) / 1000;
//                                                _morePartLineInfoList.Add(lineInfo);
//                                            }
//                                        }
//                                    }
//                                }

//                                AddPartModel.LineInfoList = _restPartLineInfoList;
//                                AddPartModel.LineQty = _restPartLineInfoList.Count;
//                                AddPartModel.EachDayTime = _restPartLineInfoList.Sum(x => x.EachLineTime);
//                                AddPartModel.TotalHCOfEachLine = _restPartLineInfoList.Sum(x => x.EachLineHC);
//                                _oldSapPirExtensions.Add(AddPartModel);

//                                //初始化
//                                _dynamicSapPirItem.LineInfoList = _morePartLineInfoList;
//                                _dynamicSapPirItem.LineQty = _morePartLineInfoList.Count;
//                                _dynamicSapPirItem.EachDayTime = _morePartLineInfoList.Sum(x => x.EachLineTime);
//                                _dynamicSapPirItem.TotalHCOfEachLine = _morePartLineInfoList.Sum(x => x.EachLineHC);
//                            }
//                        }
//                        else
//                        {
//                            List<string> _existedLinelst = _targetDateModel.LineInfoList.Select(x => x.Line).Distinct().ToList();
//                            List<string> _newLinelst = _dynamicSapPirItem.LineInfoList.Select(x => x.Line).Distinct().ToList();
//                            List<string> _commonLinelst = _existedLinelst.Intersect(_newLinelst).ToList();
//                            List<string> _differentLinelst = _newLinelst.Except(_existedLinelst).ToList();

//                            //不论目标日是否有满额排程，都要进行排程计划--判断当天已经开了哪些线,==>若存在被挪的线体已经开线，则插入
//                            List<LineInfo> _morePartLineInfoList = [];
//                            List<LineInfo> _restPartLineInfoList = _targetDateModel.LineInfoList;

//                            //人力是否足够开新线
//                            if (_differentLinelst.Count != 0)
//                            {
//                                List<LineInfo> oldPartLineInfoList = [];
//                                List<LineInfo> newPartLineInfoList = [];
//                                foreach (var line in _differentLinelst)
//                                {
//                                    if (_dynamicSapPirItem.LineInfoList.Count == 0) { continue; }
//                                    var _newLineInfo = _dynamicSapPirItem.LineInfoList.Where(x => x.Line == line).First();
//                                    var _newLineTime = _newLineInfo.EachLineTime;

//                                    if (currentDateHC < _newLineInfo.EachLineHC) { continue; }

//                                    if (currentDateHC >= _newLineInfo.EachLineHC)
//                                    {
//                                        //开新线
//                                        currentDateHC -= _newLineInfo.EachLineHC;
//                                        var newLineEachLineSpareTime = _newLineInfo.EachLineSpareTime;
//                                        if (newLineEachLineSpareTime >= 0)
//                                        {
//                                            oldPartLineInfoList.Add(_newLineInfo);
//                                        }
//                                        else
//                                        {
//                                            //没有办法将这条线的所有物料都添加
//                                            var _newLineMaterialInfoList = _newLineInfo.MaterialInfoList.OrderByDescending(x => x.EachMaterialTime).ToList();

//                                            List<MaterialInfo> oldMateriallst = [];
//                                            List<MaterialInfo> newMateriallst = [];
//                                            foreach (var _newLineMaterialInfo in _newLineMaterialInfoList)
//                                            {
//                                                newLineEachLineSpareTime = Math.Truncate((newLineEachLineSpareTime + _newLineMaterialInfo.EachMaterialTime) * 1000) / 1000;
//                                                if (newLineEachLineSpareTime < 0)
//                                                {
//                                                    _newLineMaterialInfo.Flag = 1;
//                                                    newMateriallst.Add(_newLineMaterialInfo);
//                                                }
//                                                else
//                                                {
//                                                    //新增的线体需求吃不完
//                                                    _newLineMaterialInfo.Flag = 1;

//                                                    //先恢复
//                                                    newLineEachLineSpareTime = Math.Abs(newLineEachLineSpareTime);
//                                                    var originPlanedQuantity = _newLineMaterialInfo.PlanedQuantity;
//                                                    var originEachMaterialTime = Math.Truncate(_newLineMaterialInfo.EachMaterialTime * 1000) / 1000;
//                                                    //留下的的部分  
//                                                    var restPlanedQuantity = (int)Math.Truncate(newLineEachLineSpareTime * _newLineMaterialInfo.UPH * 1000) / 1000;
//                                                    var restEachMaterialTime = Math.Truncate(newLineEachLineSpareTime * 1000) / 1000;
//                                                    var restNewLineMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(_newLineMaterialInfo);
//                                                    restNewLineMaterialInfo.EachMaterialTime = restEachMaterialTime;
//                                                    restNewLineMaterialInfo.PlanedQuantity = restPlanedQuantity;
//                                                    oldMateriallst.Add(restNewLineMaterialInfo);

//                                                    //多的部分 --要挪走
//                                                    _newLineMaterialInfo.EachMaterialTime = Math.Truncate((originEachMaterialTime - restEachMaterialTime) * 1000) / 1000;
//                                                    _newLineMaterialInfo.PlanedQuantity = originPlanedQuantity - restPlanedQuantity;
//                                                    newMateriallst.Add(_newLineMaterialInfo);

//                                                    var restMaterialInfos = _newLineMaterialInfoList.Where(x => x.Flag == 0).ToList();
//                                                    newMateriallst.AddRange(restMaterialInfos);
//                                                    break;
//                                                }
//                                            }

//                                            var oldPartLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(_newLineInfo);
//                                            var newPartLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(_newLineInfo);

//                                            if (oldMateriallst.Count != 0)
//                                            {
//                                                oldPartLineInfo.MaterialNumberQty = oldMateriallst.Count;
//                                                oldPartLineInfo.EachLineHC = oldMateriallst.Max(x => x.HC);
//                                                oldPartLineInfo.EachLineTime = Math.Truncate(oldMateriallst.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                                oldPartLineInfo.EachLineSpareTime = Math.Truncate((oldPartLineInfo.StandardTime - oldPartLineInfo.EachLineTime) * 1000) / 1000;
//                                                oldPartLineInfo.MaterialInfoList = oldMateriallst;
//                                                oldPartLineInfoList.Add(oldPartLineInfo);
//                                            }

//                                            if (newMateriallst.Count != 0)
//                                            {
//                                                newPartLineInfo.MaterialNumberQty = newMateriallst.Count;
//                                                newPartLineInfo.EachLineHC = newMateriallst.Max(x => x.HC);
//                                                newPartLineInfo.EachLineTime = Math.Truncate(newMateriallst.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                                newPartLineInfo.EachLineSpareTime = Math.Truncate((oldPartLineInfo.StandardTime - oldPartLineInfo.EachLineTime) * 1000) / 1000;
//                                                newPartLineInfo.MaterialInfoList = newMateriallst;
//                                                newPartLineInfoList.Add(_newLineInfo);
//                                            }
//                                        }
//                                    }
//                                }

//                                //新增线体
//                                var oldSapPirModel = AutoMapperHelper.Map<SapPirExtensionModel, SapPirExtensionModel>(_dynamicSapPirItem);
//                                oldSapPirModel.Date = previousDate;
//                                oldSapPirModel.EachDayTime = oldPartLineInfoList.Sum(x => x.EachLineTime);
//                                oldSapPirModel.LineInfoList = oldPartLineInfoList;
//                                oldSapPirModel.LineQty = oldPartLineInfoList.Count;
//                                oldSapPirModel.TotalHCOfEachLine = oldPartLineInfoList.Sum(x => x.EachLineHC);
//                                var queryExistedresult = _oldSapPirExtensions.Where(x => x.Date == previousDate).FirstOrDefault();
//                                if (queryExistedresult != null)
//                                {
//                                    _oldSapPirExtensions.Where(x => x.Date == previousDate).First().LineInfoList.AddRange(oldPartLineInfoList);
//                                    _oldSapPirExtensions.Where(x => x.Date == previousDate).First().EachDayTime += oldSapPirModel.EachDayTime;
//                                    _oldSapPirExtensions.Where(x => x.Date == previousDate).First().LineQty += oldSapPirModel.LineQty;
//                                    _oldSapPirExtensions.Where(x => x.Date == previousDate).First().TotalHCOfEachLine += oldSapPirModel.TotalHCOfEachLine;
//                                }
//                                else
//                                {
//                                    _oldSapPirExtensions.Add(oldSapPirModel);
//                                }

//                                //初始化
//                                //_dynamicSapPirItem.EachDayTime = newPartLineInfoList.Sum(x => x.EachLineTime);
//                                //_dynamicSapPirItem.LineInfoList = newPartLineInfoList;
//                                //_dynamicSapPirItem.LineQty = newPartLineInfoList.Count;
//                                //_dynamicSapPirItem.TotalHCOfEachLine = newPartLineInfoList.Sum(x => x.EachLineHC);
//                            }

//                            if (_restPartLineInfoList.Count != 0 && _commonLinelst.Count != 0)
//                            {
//                                foreach (var line in _commonLinelst)
//                                {
//                                    var _targetDateLineInfo = _restPartLineInfoList.Where(x => x.Line == line).FirstOrDefault();
//                                    if (_targetDateLineInfo == null) { continue; }

//                                    List<MaterialInfo> _morePartMaterialInfolst = [];
//                                    List<MaterialInfo> _restPartMaterialInfolst = _targetDateLineInfo.MaterialInfoList;

//                                    if (_targetDateLineInfo.EachLineSpareTime <= 0) { continue; }

//                                    //判断被挪的线体，能否整体挪线
//                                    var _dynamicLineInfo = _dynamicSapPirItem.LineInfoList.Where(x => x.Line == line).First();

//                                    if (_targetDateLineInfo.EachLineSpareTime >= _dynamicLineInfo.EachLineTime)
//                                    {
//                                        //将整条线挪过来
//                                        if (_dynamicLineInfo.MaterialInfoList.Count == 0) { continue; }

//                                        foreach (var _materialInfo in _dynamicLineInfo.MaterialInfoList)
//                                        {
//                                            var _targetDateMaterialInfo = _restPartMaterialInfolst.Where(x => x.MaterialNumber == _materialInfo.MaterialNumber).FirstOrDefault();

//                                            if (_targetDateMaterialInfo == null)
//                                            {
//                                                _restPartMaterialInfolst.Add(_materialInfo);
//                                            }
//                                            else
//                                            {
//                                                _restPartMaterialInfolst.Where(x => x.MaterialNumber == _materialInfo.MaterialNumber).ToList().ForEach(y =>
//                                                {
//                                                    y.PlanedQuantity += _materialInfo.PlanedQuantity;
//                                                    y.EachMaterialTime = Math.Truncate((y.EachMaterialTime + _materialInfo.EachMaterialTime) * 1000) / 1000;
//                                                });
//                                            }
//                                        }
//                                    }
//                                    else
//                                    {
//                                        //说明不能整条线全挪，需要逐个Material挪  --挪Material也有优先级
//                                        var _dynamicdMaterialInfolst = _dynamicLineInfo.MaterialInfoList.OrderByDescending(x => x.EachMaterialTime).ToList();
//                                        var _targetDateEachLineSpareTime = _targetDateLineInfo.EachLineSpareTime;
//                                        foreach (var _dynamicdMaterialInfo in _dynamicdMaterialInfolst)
//                                        {
//                                            _targetDateEachLineSpareTime = Math.Truncate((_targetDateEachLineSpareTime - _dynamicdMaterialInfo.EachMaterialTime) * 1000) / 1000;
//                                            if (_targetDateEachLineSpareTime >= 0)
//                                            {
//                                                var _targetDateMaterialInfo = _restPartMaterialInfolst.Where(x => x.MaterialNumber == _dynamicdMaterialInfo.MaterialNumber).FirstOrDefault();
//                                                if (_targetDateMaterialInfo != null)
//                                                {
//                                                    _dynamicdMaterialInfo.Flag = 1;
//                                                    _restPartMaterialInfolst.Where(x => x.MaterialNumber == _dynamicdMaterialInfo.MaterialNumber).ToList().ForEach(x =>
//                                                    {
//                                                        x.PlanedQuantity += _dynamicdMaterialInfo.PlanedQuantity;
//                                                        x.EachMaterialTime = Math.Truncate((x.EachMaterialTime + _dynamicdMaterialInfo.EachMaterialTime) * 1000) / 1000;
//                                                    });
//                                                }
//                                                else
//                                                {
//                                                    _dynamicdMaterialInfo.Flag = 1;
//                                                    var _dynamicdMaterialInfoCopy = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(_dynamicdMaterialInfo);
//                                                    _dynamicdMaterialInfoCopy.Flag = 0;
//                                                    _restPartMaterialInfolst.Add(_dynamicdMaterialInfoCopy);
//                                                }
//                                            }
//                                            else
//                                            {
//                                                _dynamicdMaterialInfo.Flag = 1;
//                                                //先恢复
//                                                _targetDateEachLineSpareTime = Math.Truncate((_targetDateEachLineSpareTime + _dynamicdMaterialInfo.EachMaterialTime) * 1000) / 1000;

//                                                //多的material，留下的material
//                                                //留下的
//                                                var _restPartMaterialInfoEachMaterialTime = _targetDateEachLineSpareTime;
//                                                var _restPartMaterialPlanedQuantity = (int)(_targetDateEachLineSpareTime * _dynamicdMaterialInfo.UPH);

//                                                var _targetDateMaterialInfo = _restPartMaterialInfolst.Where(x => x.MaterialNumber == _dynamicdMaterialInfo.MaterialNumber).FirstOrDefault();
//                                                if (_targetDateMaterialInfo != null)
//                                                {
//                                                    _dynamicdMaterialInfo.Flag = 1;
//                                                    _restPartMaterialInfolst.Where(x => x.MaterialNumber == _dynamicdMaterialInfo.MaterialNumber).ToList().ForEach(x =>
//                                                    {
//                                                        x.PlanedQuantity += _restPartMaterialPlanedQuantity;
//                                                        x.EachMaterialTime = Math.Truncate((x.EachMaterialTime + _restPartMaterialInfoEachMaterialTime) * 1000) / 1000;
//                                                    });
//                                                }
//                                                else
//                                                {
//                                                    _dynamicdMaterialInfo.Flag = 1;
//                                                    var _restPartdMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(_dynamicdMaterialInfo);
//                                                    _restPartdMaterialInfo.Flag = 0;
//                                                    _restPartdMaterialInfo.PlanedQuantity = _restPartMaterialPlanedQuantity;
//                                                    _restPartdMaterialInfo.EachMaterialTime = _restPartMaterialInfoEachMaterialTime;
//                                                    _restPartMaterialInfolst.Add(_restPartdMaterialInfo);
//                                                }

//                                                //多余的
//                                                var morePartMaterialInfoEachMaterialTime = Math.Truncate((_dynamicdMaterialInfo.EachMaterialTime - _targetDateEachLineSpareTime) * 1000) / 1000;
//                                                var morePartMaterialInfoPlanedQuantity = _dynamicdMaterialInfo.PlanedQuantity - _restPartMaterialPlanedQuantity;
//                                                var _morePartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(_dynamicdMaterialInfo);

//                                                _morePartMaterialInfo.Flag = 0;
//                                                _morePartMaterialInfo.PlanedQuantity = morePartMaterialInfoPlanedQuantity;
//                                                _morePartMaterialInfo.EachMaterialTime = morePartMaterialInfoEachMaterialTime;
//                                                _morePartMaterialInfolst.Add(_morePartMaterialInfo);

//                                                var othersMaterialInfolst = _dynamicdMaterialInfolst.Where(x => x.Flag == 0).ToList();
//                                                if (othersMaterialInfolst.Count != 0) { _morePartMaterialInfolst.AddRange(othersMaterialInfolst); }
//                                                break;
//                                            }
//                                        }
//                                    }

//                                    if (_restPartMaterialInfolst.Count != 0)
//                                    {
//                                        var _restPartLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(_targetDateLineInfo);
//                                        _restPartLineInfo.Flag = 0;
//                                        _restPartLineInfo.MaterialInfoList = _restPartMaterialInfolst;
//                                        _restPartLineInfo.MaterialNumberQty = _restPartMaterialInfolst.Count;
//                                        _restPartLineInfo.EachLineHC = _restPartMaterialInfolst.Max(x => x.HC);
//                                        _restPartLineInfo.EachLineTime = Math.Truncate(_restPartMaterialInfolst.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                        _restPartLineInfo.EachLineSpareTime = Math.Truncate((_restPartLineInfo.StandardTime - _restPartLineInfo.EachLineTime) * 1000) / 1000;
//                                        _restPartLineInfoList.Add(_restPartLineInfo);
//                                    }

//                                    if (_morePartMaterialInfolst.Count != 0)
//                                    {
//                                        var _morePartLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(_targetDateLineInfo);
//                                        _morePartLineInfo.Flag = 0;
//                                        _morePartLineInfo.MaterialInfoList = _morePartMaterialInfolst;
//                                        _morePartLineInfo.MaterialNumberQty = _morePartMaterialInfolst.Count;
//                                        _morePartLineInfo.EachLineHC = _morePartMaterialInfolst.Max(x => x.HC);
//                                        _morePartLineInfo.EachLineTime = Math.Truncate(_morePartMaterialInfolst.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                        _morePartLineInfo.EachLineSpareTime = Math.Truncate((_morePartLineInfo.StandardTime - _morePartLineInfo.EachLineTime) * 1000) / 1000;
//                                        _morePartLineInfoList.Add(_morePartLineInfo);
//                                    }
//                                }

//                                if (_restPartLineInfoList.Count != 0)
//                                {
//                                    _targetDateModel.LineInfoList = _restPartLineInfoList;
//                                    _targetDateModel.LineQty = _restPartLineInfoList.Count;
//                                    _targetDateModel.EachDayTime = _restPartLineInfoList.Sum(x => x.EachLineTime);
//                                    _targetDateModel.TotalHCOfEachLine = _restPartLineInfoList.Sum(x => x.EachLineHC);
//                                }

//                                if (_morePartLineInfoList.Count != 0)
//                                {
//                                    _dynamicSapPirItem.LineInfoList = _morePartLineInfoList;
//                                    _dynamicSapPirItem.LineQty = _morePartLineInfoList.Count;
//                                    _dynamicSapPirItem.EachDayTime = _morePartLineInfoList.Sum(x => x.EachLineTime);
//                                    _dynamicSapPirItem.TotalHCOfEachLine = _morePartLineInfoList.Sum(x => x.EachLineHC);
//                                }
//                            }
//                        }

//                        //如果预警日已排程结束，则跳出
//                        if (_dynamicSapPirItem.LineInfoList.Count == 0) { break; }

//                        //如果此时已经是第一排程日了，则准备跳出
//                        if (previousDate == _firstScheduleDate)
//                        {
//                            //若还有剩余排程，则加回原始需求日==>此处做变动过，不是加回原始需求日，而是恢复到原始需求并变更备用线，若没有备用线才做加法处理
//                            if (_dynamicSapPirItem.LineInfoList.Count != 0)
//                            {
//                                List<LineInfo> _mainLineInfolst = [];
//                                List<LineInfo> _subLineInfolst = [];
//                                List<LineInfo> _totalLineInfolst = [];
//                                foreach (var _dynamicLineInfo in _dynamicSapPirItem.LineInfoList)
//                                {
//                                    if (_dynamicLineInfo.MaterialInfoList.Count == 0) { continue; }

//                                    List<MaterialInfo> _mainLineMaterialInfolst = [];
//                                    List<MaterialInfo> _subLinetMaterialInfolst = [];
//                                    foreach (var _dynamicMaterialInfo in _dynamicLineInfo.MaterialInfoList)
//                                    {
//                                        var subLineInfo = originalPirs.Where(x => x.MaterialNumber == _dynamicMaterialInfo.MaterialNumber && !string.IsNullOrEmpty(x.StandbyLineA)).FirstOrDefault();
//                                        if (subLineInfo == null)
//                                        {
//                                            //没有备用线  ==>加回原始需求日
//                                            _dynamicMaterialInfo.Flag = 0;
//                                            _mainLineMaterialInfolst.Add(_dynamicMaterialInfo);
//                                        }
//                                        else
//                                        {
//                                            //存在备用线
//                                            if (subLineInfo.StandbyLineA == _dynamicLineInfo.Line)
//                                            {
//                                                //备用线与主线相同
//                                                _dynamicMaterialInfo.Flag = 0;
//                                                _mainLineMaterialInfolst.Add(_dynamicMaterialInfo);
//                                                continue;
//                                            }
//                                            var newPartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(_dynamicMaterialInfo);
//                                            newPartMaterialInfo.Flag = 0;
//                                            newPartMaterialInfo.Subline = subLineInfo.StandbyLineA;
//                                            _subLinetMaterialInfolst.Add(newPartMaterialInfo);
//                                        }
//                                    }
//                                    if (_mainLineMaterialInfolst.Count != 0)
//                                    {
//                                        var _mainLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(_dynamicLineInfo);
//                                        _mainLineInfo.Flag = 0;
//                                        _mainLineInfo.MaterialInfoList = _mainLineMaterialInfolst;
//                                        _mainLineInfo.MaterialNumberQty = _mainLineMaterialInfolst.Count;
//                                        _mainLineInfo.EachLineSpareTime = Math.Truncate((_mainLineInfo.StandardTime - _mainLineInfo.EachLineTime) * 1000) / 1000;
//                                        _mainLineInfo.EachLineTime = Math.Truncate(_mainLineMaterialInfolst.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                        _mainLineInfo.EachLineHC = _mainLineMaterialInfolst.Max(x => x.HC);
//                                        _mainLineInfolst.Add(_mainLineInfo);
//                                    }

//                                    if (_subLinetMaterialInfolst.Count != 0)
//                                    {
//                                        var newAddPartLinelst = _subLinetMaterialInfolst.Select(x => x.Subline).Distinct().ToList();
//                                        if (newAddPartLinelst.Count != 0)
//                                        {
//                                            foreach (var line in newAddPartLinelst)
//                                            {
//                                                var subLineMaterialInfolst = _subLinetMaterialInfolst.Where(x => x.Subline == line).ToList();
//                                                if (subLineMaterialInfolst.Count != 0)
//                                                {
//                                                    var _subLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(_dynamicLineInfo);
//                                                    _subLineInfo.Line = line;
//                                                    _subLineInfo.Flag = 0;
//                                                    _subLineInfo.MaterialInfoList = subLineMaterialInfolst;
//                                                    _subLineInfo.MaterialInfoList = subLineMaterialInfolst;
//                                                    _subLineInfo.MaterialNumberQty = subLineMaterialInfolst.Count;
//                                                    _subLineInfo.EachLineTime = Math.Truncate(subLineMaterialInfolst.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                                    _subLineInfo.EachLineSpareTime = Math.Truncate((_subLineInfo.StandardTime - _subLineInfo.EachLineTime) * 1000) / 1000;
//                                                    _subLineInfo.EachLineHC = subLineMaterialInfolst.Max(x => x.HC);
//                                                    _subLineInfolst.Add(_subLineInfo);
//                                                }
//                                            }
//                                        }
//                                    }
//                                }

//                                if (_mainLineInfolst.Count != 0) { _totalLineInfolst.AddRange(_mainLineInfolst); }
//                                if (_subLineInfolst.Count != 0) { _totalLineInfolst.AddRange(_subLineInfolst); }

//                                foreach (var lineInfo in _totalLineInfolst)
//                                {
//                                    lineInfo.Flag = 0;
//                                    lineInfo.MaterialNumberQty = lineInfo.MaterialInfoList.Count;
//                                    lineInfo.EachLineHC = lineInfo.MaterialInfoList.Max(x => x.HC);
//                                    lineInfo.EachLineTime = Math.Truncate(lineInfo.MaterialInfoList.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                    lineInfo.EachLineSpareTime = Math.Truncate((lineInfo.StandardTime - lineInfo.EachLineTime) * 1000) / 1000;
//                                }

//                                //恢复
//                                foreach (var sapPirExtension in _oldSapPirExtensions)
//                                {
//                                    if (sapPirExtension.Date != _dynamicSapPirItem.Date) { continue; }

//                                    var newPartLineInfolst = _totalLineInfolst;
//                                    var _existedLineInfolst = sapPirExtension.LineInfoList;
//                                    if (newPartLineInfolst.Count != 0)
//                                    {
//                                        foreach (var newPartLineInfo in newPartLineInfolst)
//                                        {
//                                            var query = _existedLineInfolst.Where(x => x.Line == newPartLineInfo.Line).FirstOrDefault();
//                                            if (query == null)
//                                            {
//                                                _existedLineInfolst.Add(newPartLineInfo);
//                                            }
//                                            else
//                                            {
//                                                foreach (var newPartMaterialInfo in newPartLineInfo.MaterialInfoList)
//                                                {
//                                                    foreach (var _existedLineInfo in _existedLineInfolst.Where(x => x.Line == newPartLineInfo.Line))
//                                                    {
//                                                        var queryMaterialInfo = _existedLineInfo.MaterialInfoList.Where(x => x.MaterialNumber == newPartMaterialInfo.MaterialNumber).FirstOrDefault();
//                                                        if (queryMaterialInfo == null)
//                                                        {
//                                                            _existedLineInfo.MaterialInfoList.Add(newPartMaterialInfo);
//                                                        }
//                                                        else
//                                                        {
//                                                            _existedLineInfo.MaterialInfoList.Where(x => x.MaterialNumber == newPartMaterialInfo.MaterialNumber).ToList().ForEach(y =>
//                                                            {
//                                                                y.PlanedQuantity += newPartMaterialInfo.PlanedQuantity;
//                                                                y.EachMaterialTime = Math.Truncate((y.EachMaterialTime + newPartMaterialInfo.EachMaterialTime) * 1000) / 1000;
//                                                            });

//                                                        }
//                                                    }
//                                                }
//                                            }
//                                        }

//                                        foreach (var lineInfo in _existedLineInfolst)
//                                        {
//                                            lineInfo.Flag = 0;
//                                            lineInfo.MaterialNumberQty = lineInfo.MaterialInfoList.Count;
//                                            lineInfo.EachLineHC = lineInfo.MaterialInfoList.Max(x => x.HC);
//                                            lineInfo.EachLineTime = Math.Truncate(lineInfo.MaterialInfoList.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                            lineInfo.EachLineSpareTime = Math.Truncate((lineInfo.StandardTime - lineInfo.EachLineTime) * 1000) / 1000;
//                                        }

//                                        sapPirExtension.LineInfoList = _existedLineInfolst;
//                                    }

//                                    break;
//                                }

//                                _dynamicSapPirItem.LineInfoList = _totalLineInfolst;
//                            }
//                            break;
//                        }
//                    }
//                }
//            }
//            _oldSapPirExtensions = _oldSapPirExtensions.Where(x => x.LineInfoList.Count() != 0).ToList();

//            foreach (var sapPirExtension in _oldSapPirExtensions)
//            {
//                foreach (var lineInfo in sapPirExtension.LineInfoList)
//                {
//                    foreach (var materialInfo in lineInfo.MaterialInfoList)
//                    {
//                        materialInfo.Flag = 0;
//                        materialInfo.EachMaterialTime = Math.Truncate((materialInfo.PlanedQuantity / materialInfo.UPH) * 1000) / 1000;
//                    }
//                    lineInfo.Flag = 0;
//                    lineInfo.MaterialNumberQty = lineInfo.MaterialInfoList.Count;
//                    lineInfo.EachLineHC = lineInfo.MaterialInfoList.Max(x => x.HC);
//                    lineInfo.EachLineTime = Math.Truncate(lineInfo.MaterialInfoList.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                    lineInfo.EachLineSpareTime = Math.Truncate((lineInfo.StandardTime - lineInfo.EachLineTime) * 1000) / 1000;
//                }
//                sapPirExtension.Flag = 0;
//                sapPirExtension.LineQty = sapPirExtension.LineInfoList.Count;
//                sapPirExtension.EachDayTime = Math.Truncate(sapPirExtension.LineInfoList.Sum(x => x.EachLineTime) * 1000) / 1000;
//                sapPirExtension.TotalHCOfEachLine = sapPirExtension.LineInfoList.Sum(x => x.EachLineHC);
//            }

//            sapPirExtensionModels.AddRange(_oldSapPirExtensions);
//            sapPirExtensionModels = sapPirExtensionModels.OrderBy(x => x.Date).ToList();
//            return sapPirExtensionModels;
//        }

//        #endregion

//        #region 每日排程
//        /// <summary>
//        /// 正序排程,并将排程结果存储到Db
//        /// </summary>
//        public List<SchedulerSummary> ScheduleEachDay(List<SapPirExtensionModel> sapPirExtensions, DateTime startDate, DateTime endDate)
//        {
//            List<SchedulerSummary> schedulerSummaries = [];
//            List<SapPirExtensionModel> newSapPirExtensions = [];

//            //获取上一版的排程计划
//            var lastVsersionSchedules = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= firstMonday && x.SchedulerDate < endDate.AddDays(-1)).OrderBy(x => x.SchedulerDate).ToList();
//            sapPirExtensions = sapPirExtensions.OrderBy(x => x.Date).ToList();

//            //排程日开始于endDate次日
//            if (sapPirExtensions.Count != 0)
//            {
//                DateTime end = Convert.ToDateTime(scheduleLastDay.ToShortDateString());
//                DateTime start = Convert.ToDateTime(endDate.AddDays(1).ToShortDateString());
//                int days = end.Subtract(start).Days;
//                var _endDate = endDate.AddDays(1);
//                DateTime currentDate;

//                for (int i = 0; i < days; i++)
//                {
//                    currentDate = _endDate.AddDays(i);

//                    List<LineInfo> _oldLineInfolst = [];
//                    List<LineInfo> _newLineInfolst = [];
//                    List<MaterialInfo> _oldMaterialInfolst = [];
//                    List<MaterialInfo> _newMaterialInfolst = [];
//                    //跳过周六，周日
//                    if (currentDate.DayOfWeek == DayOfWeek.Saturday || currentDate.DayOfWeek == DayOfWeek.Sunday) { continue; }

//                    //全局考虑
//                    //#工厂日历
//                    //#人力资源
//                    //#线体维护
//                    var _factoryCalendars = factoryCalendars.Where(x => x.IsWork).Select(x => x.Date).ToList();
//                    if (_factoryCalendars.Contains(currentDate)) { continue; }

//                    var _lineMaintainInfo = lineMaintains.Where(x => x.Enable).Select(x => x.Line).ToList();

//                    //人力
//                    var eachDayHumanQuantityInfo = eachDayHumanQuantitys.Where(x => x.Date == currentDate).FirstOrDefault();
//                    if (eachDayHumanQuantityInfo == null) { continue; }
//                    if (eachDayHumanQuantityInfo.Quantity <= 0 || eachDayHumanQuantityInfo.OEE < 0) { continue; }
//                    double currentDateRestHC = eachDayHumanQuantityInfo.Quantity * eachDayHumanQuantityInfo.OEE;


//                    //foreach (var item in sapPirExtensions)
//                    //{
//                    //    if (item.Flag == 1) { continue; }
//                    //    foreach (var lineInfo in item.LineInfoList)
//                    //    {
//                    //        var restTime = lineInfo.EachLineSpareTime;
//                    //        if (restTime <= 0)
//                    //        {
//                    //            item.Flag = 1;
//                    //            var newItem = AutoMapperHelper.Map<SapPirExtensionModel, SapPirExtensionModel>(item);
//                    //            newItem.Date = currentDate;
//                    //            newItem.LineInfoList = item.LineInfoList;
//                    //            newSapPirExtensions.Add(newItem);
//                    //        }
//                    //        else
//                    //        {
//                    //            foreach (var material in lineInfo.MaterialInfoList.OrderByDescending(x => x.EachMaterialTime).ToList())
//                    //            {
//                    //            }
//                    //        }
//                    //    }
//                    //}

//                    //将需求填充到排程日
//                    //确定需求范围,用过的需求要标记作废
//                    //首先查询当日需求
//                    //查询当日有无需求，有需求，则直接添加
//                    //有需求，直接添加的，假如是第一排程日，则将多余的排到上周六
//                    //无需求，查找第一个需求日，然后直接添加
//                    var currentDatePirs = sapPirExtensions.Where(x => x.Date == currentDate && x.Flag == 0).FirstOrDefault();
//                    var firstPir = sapPirExtensions.Where(x => x.Flag == 0).OrderBy(x => x.Date).FirstOrDefault();

//                    if (currentDatePirs != null)
//                    {
//                        //当日存在需求,补剩余工时
//                        //=>直接添加
//                        //标记被使用过了
//                        var newCurrentDatePirs = AutoMapperHelper.Map<SapPirExtensionModel, SapPirExtensionModel>(currentDatePirs);

//                        newCurrentDatePirs.LineInfoList = currentDatePirs.LineInfoList;

//                        foreach (var line in newCurrentDatePirs.LineInfoList)
//                        {
//                            if (line.MaterialInfoList.Count == 0) { continue; }
//                            line.Flag = 0;
//                            line.MaterialInfoList.ForEach(x => x.Flag = 0);
//                            line.MaterialNumberQty = line.MaterialInfoList.Count;
//                            line.EachLineHC = line.MaterialInfoList.Max(x => x.HC);
//                            line.EachLineTime = Math.Truncate(line.MaterialInfoList.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                            line.EachLineSpareTime = Math.Truncate((line.StandardTime - line.EachLineTime) * 1000) / 1000;
//                        }

//                        currentDateRestHC -= newCurrentDatePirs.TotalHCOfEachLine;
//                        sapPirExtensions.Where(x => x.Date == currentDate).ToList().ForEach(x => x.Flag = 1);

//                        //补剩余工时
//                        //某条线，有无剩余工时
//                        var queryRestTimeInLines = newCurrentDatePirs.LineInfoList.Where(x => x.EachLineSpareTime > 0).ToList();
//                        var queryNotRestTimeInLines = newCurrentDatePirs.LineInfoList.Where(x => x.EachLineSpareTime <= 0).ToList();
//                        if (queryRestTimeInLines.Count != 0)
//                        {
//                            //补充剩余工时
//                            foreach (var line in queryRestTimeInLines)
//                            {
//                                var newLineInfo = FillRestTimeBySeekPirs(ref sapPirExtensions, line, currentDate.AddDays(1));
//                                _newLineInfolst.Add(newLineInfo);
//                            }

//                            //排程日第一天，把多余的放到上周六，剩余的保留到第一排程日
//                            if (currentDate == endDate.AddDays(1) && queryNotRestTimeInLines.Count != 0)
//                            {
//                                #region 将多余的放到上周六，剩下的保留
//                                List<LineInfo> morePartLineInfolst = [];   //挪走
//                                List<LineInfo> restPartLineInfolst = [];   //保留
//                                foreach (var lineInfo in queryNotRestTimeInLines)
//                                {
//                                    if (lineInfo.Line == "L26")
//                                    {

//                                    };
//                                    double _spareTime = Math.Abs(lineInfo.EachLineSpareTime);
//                                    if (lineInfo.MaterialInfoList.Count == 0) { continue; }
//                                    List<MaterialInfo> morePartMaterialInfolst = [];   //挪走
//                                    List<MaterialInfo> restPartMaterialInfolst = [];   //保留

//                                    foreach (var materialInfo in lineInfo.MaterialInfoList)
//                                    {
//                                        _spareTime = Math.Truncate((_spareTime - materialInfo.EachMaterialTime) * 1000) / 1000;
//                                        if (_spareTime >= 0)
//                                        {
//                                            materialInfo.Flag = 1;
//                                            morePartMaterialInfolst.Add(materialInfo);
//                                        }
//                                        else
//                                        {
//                                            //拆
//                                            materialInfo.Flag = 1;
//                                            _spareTime = Math.Truncate((_spareTime + materialInfo.EachMaterialTime) * 1000) / 1000;
//                                            var moreMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);
//                                            var moreMaterialInfoPlanedQuantity = (int)Math.Truncate(_spareTime * moreMaterialInfo.UPH * 1000) / 1000;
//                                            var moreMaterialInfoEachMaterialTime = Math.Truncate(_spareTime * 1000) / 1000;
//                                            moreMaterialInfo.Flag = 0;
//                                            moreMaterialInfo.PlanedQuantity = (int)moreMaterialInfoPlanedQuantity;
//                                            moreMaterialInfo.EachMaterialTime = moreMaterialInfoEachMaterialTime;
//                                            morePartMaterialInfolst.Add(moreMaterialInfo);

//                                            var restMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);
//                                            var restMaterialInfoPlanedQuantity = restMaterialInfo.PlanedQuantity - moreMaterialInfoPlanedQuantity;
//                                            var restMaterialInfoEachMaterialTime = Math.Truncate((restMaterialInfo.EachMaterialTime - _spareTime) * 1000) / 1000;
//                                            restMaterialInfo.Flag = 0;
//                                            restMaterialInfo.PlanedQuantity = (int)restMaterialInfoPlanedQuantity;
//                                            restMaterialInfo.EachMaterialTime = restMaterialInfoEachMaterialTime;
//                                            restPartMaterialInfolst.Add(restMaterialInfo);

//                                            //剩余的MaterialInfo
//                                            var restAllMaterialInfolst = lineInfo.MaterialInfoList.Where(x => x.Flag == 0).ToList();
//                                            if (restAllMaterialInfolst.Count != 0)
//                                                restPartMaterialInfolst.AddRange(restAllMaterialInfolst);
//                                            break;
//                                        }
//                                    }

//                                    var moreLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(lineInfo);
//                                    moreLineInfo.MaterialNumberQty = morePartMaterialInfolst.Count;
//                                    moreLineInfo.EachLineTime = Math.Truncate(morePartMaterialInfolst.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                    moreLineInfo.EachLineSpareTime = Math.Truncate((moreLineInfo.StandardTime - moreLineInfo.EachLineTime) * 1000) / 1000;
//                                    moreLineInfo.EachLineHC = morePartMaterialInfolst.Max(x => x.HC);
//                                    moreLineInfo.MaterialInfoList = morePartMaterialInfolst;
//                                    morePartLineInfolst.Add(moreLineInfo);

//                                    var restLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(lineInfo);
//                                    restLineInfo.MaterialNumberQty = restPartMaterialInfolst.Count;
//                                    restLineInfo.EachLineTime = Math.Truncate(restPartMaterialInfolst.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                                    restLineInfo.EachLineSpareTime = Math.Truncate((restLineInfo.StandardTime - restLineInfo.EachLineTime) * 1000) / 1000;
//                                    restLineInfo.EachLineHC = restPartMaterialInfolst.Max(x => x.HC);
//                                    restLineInfo.MaterialInfoList = restPartMaterialInfolst;
//                                    restPartLineInfolst.Add(restLineInfo);
//                                }

//                                var moreSapPirExtensionModel = AutoMapperHelper.Map<SapPirExtensionModel, SapPirExtensionModel>(newCurrentDatePirs);
//                                moreSapPirExtensionModel.Date = firstMonday.AddDays(5);
//                                moreSapPirExtensionModel.LineQty = morePartLineInfolst.Count;
//                                moreSapPirExtensionModel.EachDayTime = Math.Truncate(morePartLineInfolst.Sum(x => x.EachLineTime) * 1000) / 1000;
//                                moreSapPirExtensionModel.TotalHCOfEachLine = morePartLineInfolst.Sum(x => x.EachLineHC);
//                                moreSapPirExtensionModel.LineInfoList = morePartLineInfolst;
//                                newSapPirExtensions.Add(moreSapPirExtensionModel);

//                                var restSapPirExtensionModel = AutoMapperHelper.Map<SapPirExtensionModel, SapPirExtensionModel>(newCurrentDatePirs);
//                                if (_newLineInfolst.Count != 0) { restPartLineInfolst.AddRange(_newLineInfolst); }
//                                restSapPirExtensionModel.Date = currentDate;
//                                restSapPirExtensionModel.LineQty = restPartLineInfolst.Count;
//                                restSapPirExtensionModel.EachDayTime = Math.Truncate(restPartLineInfolst.Sum(x => x.EachLineTime) * 1000) / 1000;
//                                restSapPirExtensionModel.TotalHCOfEachLine = restPartLineInfolst.Sum(x => x.EachLineHC);
//                                restSapPirExtensionModel.LineInfoList = restPartLineInfolst;

//                                newSapPirExtensions.Add(restSapPirExtensionModel);
//                                #endregion
//                            }
//                            else
//                            {
//                                _newLineInfolst.AddRange(queryNotRestTimeInLines);
//                                newCurrentDatePirs.LineInfoList = _newLineInfolst;
//                                newSapPirExtensions.Add(newCurrentDatePirs);
//                            }
//                        }
//                        else if (queryNotRestTimeInLines.Count != 0)
//                        {
//                            newCurrentDatePirs.Date = currentDate;
//                            newSapPirExtensions.Add(newCurrentDatePirs);
//                        }
//                    }
//                    else
//                    {
//                        //当日无需求，则查找，把第一个没有排程的需求放到此日
//                        //然后查看此日线体有无剩余工时，剩余人力
//                        sapPirExtensions.Where(x => x.Date == firstPir.Date).ToList().ForEach(x => x.Flag = 1);
//                        var newCurrentDatePirs = AutoMapperHelper.Map<SapPirExtensionModel, SapPirExtensionModel>(firstPir);
//                        newCurrentDatePirs.LineInfoList = currentDatePirs.LineInfoList;
//                        newCurrentDatePirs.Date = currentDate;

//                        if (firstPir != null)
//                        {
//                            currentDateRestHC -= firstPir.TotalHCOfEachLine;

//                            //补剩余工时
//                            //某条线，有无剩余工时
//                            var fillDataResult = firstPir.LineInfoList.Where(x => x.EachLineSpareTime > 0).ToList();
//                            var notFillDataResult = firstPir.LineInfoList.Where(x => x.EachLineSpareTime <= 0).ToList();
//                            if (fillDataResult.Count != 0)
//                            {
//                                foreach (var line in fillDataResult)
//                                {
//                                    var newLineInfo = FillRestTimeBySeekPirs(ref sapPirExtensions, line, currentDate.AddDays(1));
//                                    _newLineInfolst.Add(newLineInfo);
//                                }
//                            }

//                            if (notFillDataResult.Count != 0)
//                            {
//                                _newLineInfolst.AddRange(notFillDataResult.ToList());
//                            }
//                            newCurrentDatePirs.LineInfoList = _newLineInfolst;
//                            newSapPirExtensions.Add(newCurrentDatePirs);
//                        }
//                    }

//                    //是否开新线，有无人力
//                    if (currentDateRestHC >= 1)
//                    {
//                        var _currentDateLineInfolst = newSapPirExtensions.Where(x => x.Date == currentDate).FirstOrDefault();
//                        if (_currentDateLineInfolst != null)
//                        {
//                            var newLineInfolst = AddNewLineToSchedule(ref sapPirExtensions, _currentDateLineInfolst.LineInfoList, currentDateRestHC, currentDate.AddDays(1));

//                            if (newLineInfolst != null)
//                            {
//                                newSapPirExtensions.Where(x => x.Date == currentDate).ToList().ForEach(y =>
//                                {
//                                    y.LineInfoList = newLineInfolst;
//                                });
//                            }
//                        }
//                    }
//                }
//            }

//            #region Fixed周日的需求
//            var lastVersionSunday = idealSapPirExtensions.Where(x => x.Date == firstMonday.AddDays(6)).FirstOrDefault();
//            if (lastVersionSunday != null) { newSapPirExtensions.Add(lastVersionSunday); }
//            #endregion

//            #region 将模型进行转换
//            schedulerSummaries.AddRange(lastVsersionSchedules);
//            if (newSapPirExtensions.Count != 0)
//            {
//                foreach (var item in newSapPirExtensions)
//                {
//                    if (item.LineInfoList.Count == 0) { continue; }

//                    foreach (var lineInfo in item.LineInfoList)
//                    {
//                        if (lineInfo.MaterialInfoList.Count == 0) { continue; }

//                        foreach (var materialInfo in lineInfo.MaterialInfoList)
//                        {
//                            if (string.IsNullOrEmpty(materialInfo.MaterialNumber) || string.IsNullOrEmpty(lineInfo.Line) || item.Date == null) { continue; }

//                            var _weekNumber = WeekHelper.GetWeekNumber(item.Date);
//                            SchedulerSummary schedulerSummary = new SchedulerSummary();
//                            schedulerSummary.SchedulerDate = item.Date;
//                            schedulerSummary.Plant = item.Plant;
//                            schedulerSummary.SchedulerId = $"{materialInfo.MaterialNumber.Replace(" ", "").ToUpper()}{lineInfo.Line.Replace(" ", "").ToUpper()}{item.Date.ToString("yyyyMMdd")}";
//                            schedulerSummary.MaterialNumber = materialInfo.MaterialNumber.Replace(" ", "").ToUpper();
//                            schedulerSummary.Line = lineInfo.Line;
//                            schedulerSummary.SPQ = materialInfo.SPQ;
//                            schedulerSummary.UPH = materialInfo.UPH;
//                            schedulerSummary.HC = materialInfo.HC;
//                            schedulerSummary.Area = materialInfo.Area;
//                            schedulerSummary.Site = materialInfo.Area;
//                            schedulerSummary.PCBA = materialInfo.PCBA;
//                            schedulerSummary.Description = materialInfo.Description;
//                            schedulerSummary.WeekNumber = _weekNumber;
//                            schedulerSummary.Status = SchedulerStatus.Created.ToString();
//                            schedulerSummary.KeyUser = materialInfo.KeyUser;
//                            schedulerSummary.PlanOrder = "";
//                            schedulerSummary.PlanedQuantity = materialInfo.PlanedQuantity;
//                            schedulerSummary.ActualQuantity = materialInfo.ActualQuantity;
//                            schedulerSummary.Remark = materialInfo.Remark;
//                            schedulerSummary.CreateTime = date;
//                            schedulerSummary.UpdateTime = date;
//                            schedulerSummaries.Add(schedulerSummary);
//                        }
//                    }
//                }
//            }
//            schedulerSummaries = schedulerSummaries.OrderBy(x => x.SchedulerDate).ToList();
//            #endregion

//            #region 插入数据库
//            if (schedulerSummaries.Count != 0)
//            {
//                //删除上一次排程记录
//                List<SchedulerSummary> lastScheduleResult = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= endDate.AddDays(1)).ToList();

//                if (lastScheduleResult.Count != 0)
//                {
//                    _apsDbContext.SchedulerSummary.RemoveRange(lastScheduleResult);
//                }
//                _apsDbContext.SchedulerSummary.UpdateRange(schedulerSummaries);
//                _apsDbContext.SaveChanges();
//            }
//            #endregion

//            return schedulerSummaries;
//        }

//        /// <summary>
//        ///  挖掘数据
//        /// </summary>
//        /// <param name="sapPirExtensions"></param>
//        /// <param name="lineInfo"></param>
//        /// <param name="startDate"></param>
//        public static LineInfo FillRestTimeBySeekPirs(ref List<SapPirExtensionModel> sapPirExtensions, LineInfo lineInfo, DateTime startDate)
//        {
//            var newLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(lineInfo);
//            List<MaterialInfo> materialInfoList = lineInfo.MaterialInfoList;
//            var restTime = lineInfo.EachLineSpareTime;

//            bool isOver = false;
//            if (lineInfo.EachLineSpareTime <= 0) { return newLineInfo; }

//            foreach (var item in sapPirExtensions)
//            {
//                if (item.Date < startDate || item.Flag == 1) { continue; }

//                var querySameLineInfo = item.LineInfoList.Where(x => x.Line == lineInfo.Line).FirstOrDefault();
//                if (querySameLineInfo == null) { continue; }

//                var querySameMaterialInfos = querySameLineInfo.MaterialInfoList.Where(x => x.Flag == 0).OrderByDescending(x => x.EachMaterialTime).ToList(); ;
//                if (querySameMaterialInfos.Count == 0) { continue; }

//                foreach (var materialInfo in querySameMaterialInfos)
//                {
//                    restTime = Math.Truncate((restTime - materialInfo.EachMaterialTime) * 1000) / 1000;  //补充剩余工时
//                    if (restTime > 0)
//                    {
//                        materialInfo.Flag = 1;
//                        item.LineInfoList.Where(x => x.Line == lineInfo.Line).First().MaterialInfoList.Where(y => y.MaterialNumber == materialInfo.MaterialNumber).First().Flag = 1;
//                        var querySameMaterialNumber = materialInfoList.Where(x => x.MaterialNumber == materialInfo.MaterialNumber).FirstOrDefault();
//                        if (querySameMaterialNumber != null)
//                        {
//                            var morePartMaterialInfoEachMaterialTime = Math.Truncate((querySameMaterialNumber.EachMaterialTime + materialInfo.EachMaterialTime) * 1000) / 1000;
//                            var morePartMaterialInfoPlanedQuantity = querySameMaterialNumber.PlanedQuantity + materialInfo.PlanedQuantity;

//                            materialInfoList.Where(x => x.MaterialNumber == materialInfo.MaterialNumber).ToList().ForEach(y =>
//                            {
//                                y.PlanedQuantity = morePartMaterialInfoPlanedQuantity;
//                                y.EachMaterialTime = morePartMaterialInfoEachMaterialTime;
//                            });
//                        }
//                        else
//                        {
//                            var newMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);
//                            newMaterialInfo.EachMaterialTime = Math.Truncate(materialInfo.EachMaterialTime * 1000) / 1000;
//                            newMaterialInfo.PlanedQuantity = materialInfo.PlanedQuantity;
//                            materialInfoList.Add(newMaterialInfo);
//                        }
//                    }
//                    else
//                    {
//                        materialInfo.Flag = 1;
//                        restTime += materialInfo.EachMaterialTime;  //拆分//先恢复
//                        var morePartEachMaterialTime = restTime;
//                        var morePartPlanedQuantity = (int)Math.Abs(restTime * materialInfo.UPH);
//                        var morePartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);  //需要挪走的
//                        morePartMaterialInfo.PlanedQuantity = morePartPlanedQuantity;
//                        morePartMaterialInfo.EachMaterialTime = morePartEachMaterialTime;

//                        var restPartEachMaterialTime = Math.Truncate((materialInfo.EachMaterialTime - restTime) * 1000) / 1000;
//                        var restPartPlanedQuantity = materialInfo.PlanedQuantity - morePartPlanedQuantity;
//                        var restPartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);  //需要留下的

//                        var querySameMaterialNumber = materialInfoList.Where(x => x.MaterialNumber == materialInfo.MaterialNumber).FirstOrDefault();  //只需要RestTime
//                        if (querySameMaterialNumber != null)
//                        {
//                            var morePartMaterialInfoEachMaterialTime = Math.Truncate((querySameMaterialNumber.EachMaterialTime + morePartMaterialInfo.EachMaterialTime) * 1000) / 1000;
//                            var morePartMaterialInfoPlanedQuantity = querySameMaterialNumber.PlanedQuantity + morePartMaterialInfo.PlanedQuantity;

//                            materialInfoList.Where(x => x.MaterialNumber == materialInfo.MaterialNumber).ToList().ForEach(y =>
//                            {
//                                y.PlanedQuantity = morePartMaterialInfoPlanedQuantity;
//                                y.EachMaterialTime = morePartMaterialInfoEachMaterialTime;
//                            });
//                        }
//                        else
//                        {
//                            materialInfoList.Add(morePartMaterialInfo);
//                        }

//                        item.LineInfoList.Where(x => x.Line == lineInfo.Line).First().MaterialInfoList.Where(y => y.MaterialNumber == materialInfo.MaterialNumber).ToList().ForEach(z =>
//                        {
//                            z.Flag = 0;
//                            z.EachMaterialTime = restPartEachMaterialTime;
//                            z.PlanedQuantity = restPartPlanedQuantity;
//                        });  //剩下的放回原处

//                        isOver = true;
//                        break;
//                    }
//                }

//                if (isOver) { break; }
//            }
//            newLineInfo.MaterialInfoList = materialInfoList;
//            newLineInfo.EachLineHC = newLineInfo.MaterialInfoList.Max(x => x.HC);
//            newLineInfo.MaterialNumberQty = newLineInfo.MaterialInfoList.Count;
//            newLineInfo.EachLineTime = Math.Truncate(newLineInfo.MaterialInfoList.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//            newLineInfo.EachLineSpareTime = Math.Truncate((newLineInfo.StandardTime - newLineInfo.EachLineTime) * 1000) / 1000;

//            return newLineInfo;
//        }


//        /// <summary>
//        ///  挖掘数据
//        /// </summary>
//        /// <param name="sapPirExtensions"></param>
//        /// <param name="lineInfo"></param>
//        /// <param name="startDate"></param>
//        public static LineInfo NewFillRestTimeBySeekPirs(ref List<SapPirExtensionModel> sapPirExtensions, ref List<SapPirExtensionModel> newSapPirExtensions, LineInfo lineInfo, DateTime startDate)
//        {
//            var newLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(lineInfo);
//            List<MaterialInfo> materialInfoList = lineInfo.MaterialInfoList.OrderByDescending(x => x.EachMaterialTime).ToList();

//            bool isOver = false;
//            if (lineInfo.EachLineSpareTime <= 0) { return newLineInfo; }

//            var restTime = lineInfo.EachLineSpareTime;
//            List<MaterialInfo> materialInfoListFromPir = [];
//            foreach (var item in materialInfoList)
//            {
//                #region 获取Material汇总
//                foreach (var sapPirExtension in sapPirExtensions)
//                {
//                    foreach (var line in sapPirExtension.LineInfoList)
//                    {
//                        foreach (var materialInfo in line.MaterialInfoList)
//                        {
//                            if (materialInfo.MaterialNumber == item.MaterialNumber)
//                            {
//                                materialInfo.Flag = 1;
//                                materialInfoListFromPir.Add(materialInfo);

//                                line.MaterialNumberQty--;
//                                line.EachLineSpareTime += materialInfo.EachMaterialTime;
//                                line.EachLineTime -= materialInfo.EachMaterialTime;
//                                line.EachLineHC = line.MaterialInfoList.Where(x => x.Flag == 0).Max(x => x.HC);
//                            }
//                        }
//                    }
//                }
//                #endregion

//                if (materialInfoListFromPir.Count == 0) { continue; }

//                foreach (var material in materialInfoListFromPir)
//                {
//                    restTime = Math.Truncate((restTime - material.EachMaterialTime) * 1000) / 1000;
//                    if (restTime > 0)
//                    {

//                    }
//                }

//            }

//            return newLineInfo;
//        }

//        /// <summary>
//        ///  排程日新增线体
//        /// </summary>
//        /// <param name="sapPirExtensions"></param>
//        public static List<LineInfo> AddNewLineToSchedule(ref List<SapPirExtensionModel> sapPirExtensions, List<LineInfo> existedLineInfos, double restHC, DateTime startDate)
//        {
//            List<LineInfo> totalLineInfolst = existedLineInfos;

//            List<MaterialInfo> _morePartMaterialInfos = [];
//            List<MaterialInfo> _restPartMaterialInfos = [];
//            var lineslst = existedLineInfos.Select(x => x.Line).Distinct().ToList();
//            var newSapPirExtensions = sapPirExtensions.Where(x => x.Date >= startDate && x.Flag == 0).OrderBy(x => x.Date).OrderByDescending(x => x.EachDayTime).ToList();

//            foreach (var item in newSapPirExtensions)
//            {
//                if (item.Date < startDate || item.Flag == 1) { continue; }

//                var newLineInfolst = item.LineInfoList.OrderByDescending(x => x.EachLineTime).ToList();
//                if (newLineInfolst.Count == 0) { continue; }

//                foreach (var lineInfo in newLineInfolst)
//                {
//                    if (lineslst.Contains(lineInfo.Line) || lineInfo.Flag == 1) { continue; }

//                    _morePartMaterialInfos.Clear();
//                    _restPartMaterialInfos.Clear();
//                    var newLineInfo = AutoMapperHelper.Map<LineInfo, LineInfo>(lineInfo);
//                    newLineInfo.MaterialInfoList = lineInfo.MaterialInfoList;
//                    if (restHC >= 1 && restHC > lineInfo.EachLineHC)
//                    {
//                        //不能整条线加过来
//                        var restSpareTime = Math.Truncate(lineInfo.EachLineSpareTime * 100) / 100;  //小数处理
//                        if (restSpareTime > 0)
//                        {
//                            lineInfo.Flag = 1;
//                            restHC -= lineInfo.EachLineHC;
//                            var _newLineInfo = FillRestTimeBySeekPirs(ref sapPirExtensions, newLineInfo, startDate);
//                            lineslst.Add(lineInfo.Line);
//                            totalLineInfolst.Add(_newLineInfo);
//                        }
//                        else if (restSpareTime == 0)
//                        {
//                            restHC -= lineInfo.EachLineHC;
//                            newLineInfo.Flag = 1;
//                            newLineInfo.EachLineSpareTime = restSpareTime;
//                            lineslst.Add(lineInfo.Line);
//                            totalLineInfolst.Add(newLineInfo);
//                        }
//                        else
//                        {
//                            restHC -= lineInfo.EachLineHC;
//                            lineslst.Add(lineInfo.Line);

//                            //目标线体报红,则需要拆分
//                            var materialInfolst = lineInfo.MaterialInfoList.Where(x => x.Flag == 0).ToList();
//                            double idealRestSpareTime = lineInfo.StandardTime;

//                            if (materialInfolst.Count == 0) { continue; }
//                            foreach (var materialInfo in materialInfolst)
//                            {
//                                idealRestSpareTime = Math.Truncate((idealRestSpareTime - materialInfo.EachMaterialTime) * 1000) / 1000;
//                                if (restSpareTime >= 0)
//                                {
//                                    materialInfo.Flag = 1;
//                                    _morePartMaterialInfos.Add(materialInfo);
//                                }
//                                else
//                                {
//                                    //截取22.28给排程日
//                                    //多的还给原需求日

//                                    //先还原
//                                    idealRestSpareTime = Math.Truncate((idealRestSpareTime + materialInfo.EachMaterialTime) * 1000) / 1000;

//                                    //挪走的部分
//                                    var morePartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);
//                                    var morePartMaterialInfoEachMaterialTime = Math.Abs(idealRestSpareTime);
//                                    var morePartMaterialInfoPlanedQuantity = (int)Math.Truncate(materialInfo.EachMaterialTime * materialInfo.UPH * 1000) / 1000;
//                                    morePartMaterialInfo.Flag = 1;
//                                    morePartMaterialInfo.PlanedQuantity = morePartMaterialInfoPlanedQuantity;
//                                    morePartMaterialInfo.EachMaterialTime = morePartMaterialInfoEachMaterialTime;
//                                    _morePartMaterialInfos.Add(morePartMaterialInfo);

//                                    var restPartMaterialInfo = AutoMapperHelper.Map<MaterialInfo, MaterialInfo>(materialInfo);
//                                    var restPartMaterialInfoEachMaterialTime = restPartMaterialInfo.EachMaterialTime - morePartMaterialInfoEachMaterialTime;
//                                    var restPartMaterialInfoPlanedQuantity = restPartMaterialInfo.PlanedQuantity - morePartMaterialInfoPlanedQuantity;
//                                    restPartMaterialInfo.Flag = 0;
//                                    restPartMaterialInfo.PlanedQuantity = restPartMaterialInfoPlanedQuantity;
//                                    restPartMaterialInfo.EachMaterialTime = restPartMaterialInfoEachMaterialTime;
//                                    _restPartMaterialInfos.Add(restPartMaterialInfo);

//                                    var restMateriallst = materialInfolst.Where(x => x.Flag == 0).ToList();
//                                    if (restMateriallst.Count != 0) { _restPartMaterialInfos.AddRange(restMateriallst); }

//                                    //原始需求日
//                                    item.LineInfoList.Where(x => x.Line == lineInfo.Line).ToList().ForEach(y =>
//                                    {
//                                        y.MaterialInfoList = _restPartMaterialInfos;
//                                    });
//                                    _restPartMaterialInfos.ForEach(x => x.Flag = 0);
//                                    break;
//                                }
//                            }

//                            newLineInfo.MaterialInfoList = _morePartMaterialInfos;
//                            newLineInfo.EachLineHC = _morePartMaterialInfos.Max(x => x.HC);
//                            newLineInfo.EachLineTime = Math.Truncate(_morePartMaterialInfos.Sum(x => x.EachMaterialTime) * 1000) / 1000;
//                            newLineInfo.MaterialNumberQty = _morePartMaterialInfos.Count;
//                            totalLineInfolst.Add(newLineInfo);
//                        }
//                    }

//                    if (restHC < 1) { break; }
//                }
//            }

//            return totalLineInfolst;
//        }

//        /// <summary>
//        /// 将数据格式进行转换,传递至前端
//        /// </summary>
//        public SchedulerSummaryViewModel SchedulerSummaryTransforFEData(List<SchedulerSummary> schedulerSummaries)
//        {
//            SchedulerSummaryViewModel schedulerSummaryForUI = new SchedulerSummaryViewModel();
//            List<Hashtable> lst = new List<Hashtable>();
//            var distinctMaterialNumberlst = schedulerSummaries.Select(x => x.MaterialNumber?.Replace(" ", "").ToUpper()).OrderBy(x => x).Distinct().ToList();
//            var distinctLinelst = schedulerSummaries.Select(x => x.Line?.Replace(" ", "").ToUpper()).OrderBy(x => x).Distinct().ToList();

//            var startDate = firstMonday;

//            string[] weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
//            List<TitleElement> titleElements = new List<TitleElement>();
//            for (int i = 0; i < 28; i++)
//            {
//                var currentDate = startDate.AddDays(i);
//                var dateFormate = currentDate.ToString("MM-dd");
//                string week = weekdays[Convert.ToInt32(currentDate.DayOfWeek)];

//                Hashtable hashtable = new Hashtable();
//                hashtable.Add("title", $"{dateFormate}-{week}");
//                List<Hashtable> _children = [];
//                Hashtable hashtable1 = new Hashtable();
//                hashtable1.Add("key", $"{dateFormate}-Plan");
//                Hashtable hashtable2 = new Hashtable();
//                hashtable2.Add("key", $"{dateFormate}-Actual");
//                Hashtable hashtable3 = new Hashtable();
//                hashtable3.Add("key", $"{dateFormate}-Mark");
//                _children.Add(hashtable1);
//                _children.Add(hashtable2);
//                _children.Add(hashtable3);

//                titleElements.Add(new TitleElement() { title = hashtable, children = _children });
//            }
//            schedulerSummaryForUI.title = titleElements;

//            foreach (var line in distinctLinelst)
//            {
//                foreach (var materialNumber in distinctMaterialNumberlst)
//                {
//                    if (string.IsNullOrEmpty(materialNumber) || string.IsNullOrEmpty(line)) { continue; }

//                    //查询相同MaterialNumber,Line对应的排程计划数据
//                    var queryResultByMaterialNumberLinelst = schedulerSummaries.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber && x.Line?.Replace(" ", "").ToUpper() == line).DistinctBy(x => x.SchedulerDate).ToList();
//                    if (queryResultByMaterialNumberLinelst.Count != 0)
//                    {
//                        Hashtable hashtable = new Hashtable();
//                        foreach (var item in queryResultByMaterialNumberLinelst)
//                        {
//                            var dateFormate = item.SchedulerDate?.ToString("MM-dd");

//                            //basic info
//                            if (hashtable.Keys.Count == 0)
//                            {
//                                hashtable.Add("PN", materialNumber);
//                                hashtable.Add("key", line + materialNumber);
//                                hashtable.Add("Line", line);
//                                hashtable.Add("UPH", item.UPH);
//                                hashtable.Add("HC", item.HC);
//                                hashtable.Add("SPQ", item.SPQ);
//                                hashtable.Add("Site", item.Site);
//                                hashtable.Add("PCBA", item.PCBA);
//                                hashtable.Add("Description", item.Description);
//                            }

//                            //dynamic info
//                            if (!hashtable.ContainsKey($"{dateFormate}-Plan"))
//                            {
//                                hashtable.Add($"{dateFormate}-Plan", item.PlanedQuantity);
//                                hashtable.Add($"{dateFormate}-Actual", item.ActualQuantity);
//                                hashtable.Add($"{dateFormate}-Mark", item.Remark);
//                            }
//                        }
//                        lst.Add(hashtable);
//                    }
//                }
//            }
//            schedulerSummaryForUI.list = lst;
//            return schedulerSummaryForUI;
//        }

//        /// <summary>
//        /// 获取已经做过排程的记录
//        /// </summary>
//        public SchedulerSummaryViewModel GetSchedulerSummary(DateTime startDate = default(DateTime))
//        {
//            if (startDate == default(DateTime))
//                startDate = firstMonday.AddDays(7);
//            var _scheduleSummaryResult = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= startDate).ToList();
//            SchedulerSummaryViewModel schedulerSummaryViewModel = new SchedulerSummaryViewModel();
//            if (_scheduleSummaryResult.Count > 0)
//            {
//                schedulerSummaryViewModel = SchedulerSummaryTransforFEData(_scheduleSummaryResult);
//            }
//            return schedulerSummaryViewModel;
//        }
//        #endregion

//        #endregion

//        #region Others Apis
//        /// <summary>
//        ///  更新排程计划
//        /// </summary>
//        /// <param name="updateSchedulerSummaryViewModel"></param>
//        /// <returns></returns>
//        public List<UpdateSchedulerSummaryModel> UpdateSchedulerSummary(UpdateSchedulerSummaryViewModel updateSchedulerSummaryViewModel)
//        {
//            updateSchedulerSummaryViewModel.list.ToList();
//            List<Hashtable> _list = new List<Hashtable>();
//            List<UpdateSchedulerSummaryModel> schedulerSummaryModellst = new List<UpdateSchedulerSummaryModel>();
//            if (updateSchedulerSummaryViewModel.list.Count == 0) { return schedulerSummaryModellst; }

//            //通过PN+Line+Date查找
//            _list = updateSchedulerSummaryViewModel.list;

//            foreach (Hashtable item in _list)
//            {
//                if (!item.ContainsKey("PN") || !item.ContainsKey("Line")) { continue; }   //保存失败

//                foreach (var key in item.Keys)
//                {
//                    var _key = key.ToString();
//                    var _value = item[key] == null ? string.Empty : item[key].ToString();
//                    if (!_key.Contains('-')) { continue; }
//                    var _keySplit = _key.Split('-');
//                    var _dateStr = _keySplit.Length >= 4 ? _keySplit[0] + _keySplit[1] + _keySplit[2] : DateTime.Now.ToString("yyyy") + _keySplit[0] + _keySplit[1];
//                    var _schedulerId = item["PN"]?.ToString()?.Replace(" ", "").ToUpper() + item["Line"]?.ToString()?.Replace(" ", "").ToUpper() + _dateStr;

//                    var queryResult = schedulerSummaryModellst.Where(x => x.SchedulerId == _schedulerId).FirstOrDefault();
//                    if (queryResult != null)
//                    {
//                        if (_keySplit[^1].ToString().Replace(" ", "").Equals("PLAN", StringComparison.CurrentCultureIgnoreCase))
//                        {
//                            queryResult.PlanedQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
//                            continue;
//                        }

//                        if (_keySplit[^1].ToString().Replace(" ", "").Equals("ACTUAL", StringComparison.CurrentCultureIgnoreCase))
//                        {
//                            queryResult.ActualQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
//                            continue;
//                        }

//                        if (_keySplit[^1].ToString().Replace(" ", "").Equals("MARK", StringComparison.CurrentCultureIgnoreCase))
//                        {
//                            queryResult.Remark = _value;
//                            continue;
//                        }
//                    }
//                    else
//                    {
//                        UpdateSchedulerSummaryModel updateSchedulerSummaryModel = new UpdateSchedulerSummaryModel();
//                        updateSchedulerSummaryModel.SchedulerId = _schedulerId;
//                        if (_keySplit[_keySplit.Length - 1].ToString().Replace(" ", "").ToUpper() == "PLAN")
//                        {
//                            updateSchedulerSummaryModel.PlanedQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
//                            schedulerSummaryModellst.Add(updateSchedulerSummaryModel);
//                            continue;
//                        }

//                        if (_keySplit[_keySplit.Length - 1].ToString().Replace(" ", "").ToUpper() == "ACTUAL")
//                        {
//                            updateSchedulerSummaryModel.ActualQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
//                            schedulerSummaryModellst.Add(updateSchedulerSummaryModel);
//                            continue;
//                        }

//                        if (_keySplit[_keySplit.Length - 1].ToString().Replace(" ", "").ToUpper() == "MARK")
//                        {
//                            updateSchedulerSummaryModel.Remark = _value;
//                            schedulerSummaryModellst.Add(updateSchedulerSummaryModel);
//                            continue;
//                        }
//                    }
//                }
//            }

//            if (schedulerSummaryModellst.Count == 0) { return schedulerSummaryModellst; }

//            //更新数据
//            foreach (var item in schedulerSummaryModellst)
//            {
//                var queryResult = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerId == item.SchedulerId).FirstOrDefault();
//                if (queryResult != null)
//                {
//                    queryResult.PlanedQuantity = item.PlanedQuantity;
//                    queryResult.ActualQuantity = item.ActualQuantity;
//                    queryResult.Remark = item.Remark;
//                    if (!string.IsNullOrEmpty(item.Remark))
//                    {

//                    }
//                    _apsDbContext.SchedulerSummary.Update(queryResult);

//                    //保存历史记录
//                    var queryHistoryResult = _apsDbContext.SchedulerSummaryHistory.Where(x => x.SchedulerId == item.SchedulerId).OrderByDescending(x => x.UpdateTime).FirstOrDefault();
//                    if (queryHistoryResult != null)
//                    {
//                        if (queryHistoryResult.PlanedQuantity != item.PlanedQuantity || queryHistoryResult.ActualQuantity != item.ActualQuantity || queryHistoryResult.Remark != item.Remark)
//                        {
//                            queryHistoryResult.PlanedQuantity = item.PlanedQuantity;
//                            queryHistoryResult.ActualQuantity = item.ActualQuantity;
//                            queryHistoryResult.Remark = item.Remark;
//                            _apsDbContext.SchedulerSummaryHistory.Update(queryHistoryResult);
//                        }
//                    }
//                }
//                _apsDbContext.SaveChanges();
//            }

//            return schedulerSummaryModellst;
//        }

//        /// <summary>
//        /// 获取原始PIR数据
//        /// </summary>
//        /// <param name="current"></param>
//        /// <param name="pageSize"></param>
//        /// <returns></returns>
//        public SapPIRInfoViewModel GetPirInfo()
//        {
//            List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Quantity >= 1 && x.Date != null).ToList();
//            List<MaterialMainLine> materialMainLinelst = _apsDbContext.MaterialMainLine.Where(x => x.MainLine != null).ToList();
//            var distinctLinelst = materialMainLinelst.Where(x => x.MainLine != null).Select(x => x.MainLine?.Replace(" ", "").ToUpper()).Distinct().ToList();
//            var distinctMaterialNumberlst = sapDemands.Select(x => x.MaterialNumber?.Replace(" ", "").ToUpper()).Distinct().ToList();

//            SapPIRInfoViewModel sapPIRInfoViewModel = new SapPIRInfoViewModel();
//            var distinctDateTotallst = sapDemands.Where(x => x.Date != null).Select(x => x.Date.Date.ToString("yyyy-MM-dd")).Distinct().OrderBy(x => x).ToList();
//            sapPIRInfoViewModel.title = distinctDateTotallst;

//            if (sapDemands.Count > 0 && distinctMaterialNumberlst.Count != 0)
//            {
//                List<Hashtable> _list = [];
//                List<Dictionary<string, string?>> dictionaries = new List<Dictionary<string, string?>>();
//                foreach (var materialNumber in distinctMaterialNumberlst)
//                {
//                    var distinctDatelst = sapDemands.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber).DistinctBy(x => x.Date).ToList();
//                    Dictionary<string, string?> keyValuePairs = [];
//                    if (!keyValuePairs.ContainsKey(materialNumber))
//                    {
//                        var queryResult = materialMainLinelst.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber).FirstOrDefault();
//                        if (queryResult != null)
//                        {
//                            keyValuePairs.Add("Line", queryResult.MainLine?.Replace(" ", "").ToUpper());
//                        }
//                        else
//                        {
//                            keyValuePairs.Add("Line", "NA");
//                        }
//                        keyValuePairs.Add("key", materialNumber);
//                        keyValuePairs.Add("PN", materialNumber);
//                    }
//                    if (distinctDatelst.Count != 0)
//                    {
//                        foreach (var item in distinctDatelst)
//                        {
//                            var _key = item.Date.ToString("yyyy-MM-dd");
//                            if (!keyValuePairs.ContainsKey(_key))
//                            {
//                                keyValuePairs.Add(_key, item.Quantity.ToString());
//                            }
//                        }
//                    }
//                    dictionaries.Add(keyValuePairs);
//                }
//                sapPIRInfoViewModel.current = 1;
//                sapPIRInfoViewModel.pageSize = 20;
//                sapPIRInfoViewModel.startDate = DateTime.Now.ToString("yyyy-MM-dd");

//                var sortedDictionaries = dictionaries.OrderBy(dict => dict["Line"]).ToList();
//                sapPIRInfoViewModel.total = dictionaries.Count;
//                sapPIRInfoViewModel.list = sortedDictionaries;
//            }
//            return sapPIRInfoViewModel;
//        }

//        /// <summary>
//        /// 获取原始PIR数据
//        /// </summary>
//        /// <param name="current"></param>
//        /// <param name="pageSize"></param>
//        /// <returns></returns>
//        public SapPIRInfoViewModel GetAllPirInfo()
//        {
//            List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Quantity >= 1 && x.Date != null).ToList();
//            List<MaterialNumberLineMapRelations> materialNumberLineMapRelations = _apsDbContext.MaterialNumberLineMapRelations.Where(x => x.Line != null && x.Order == "0").ToList();
//            var distinctLinelst = materialNumberLineMapRelations.Where(x => x.Line != null).Select(x => x.Line?.Replace(" ", "").ToUpper()).Distinct().ToList();
//            var distinctMaterialNumberlst = sapDemands.Select(x => x.MaterialNumber?.Replace(" ", "").ToUpper()).Distinct().ToList();

//            SapPIRInfoViewModel sapPIRInfoViewModel = new SapPIRInfoViewModel();
//            var distinctDateTotallst = sapDemands.Where(x => x.Date != null).Select(x => x.Date.Date.ToString("yyyy-MM-dd")).Distinct().OrderBy(x => x).ToList();
//            sapPIRInfoViewModel.title = distinctDateTotallst;
//            //行转列
//            if (sapDemands.Count > 0 && distinctMaterialNumberlst.Count != 0)
//            {
//                List<Hashtable> _list = [];
//                List<Dictionary<string, string?>> dictionaries = new List<Dictionary<string, string?>>();
//                foreach (var materialNumber in distinctMaterialNumberlst)
//                {
//                    var distinctDatelst = sapDemands.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber).DistinctBy(x => x.Date).ToList();
//                    Dictionary<string, string?> keyValuePairs = [];
//                    if (!keyValuePairs.ContainsKey(materialNumber))
//                    {
//                        var queryResult = materialNumberLineMapRelations.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber).FirstOrDefault();
//                        if (queryResult != null)
//                        {
//                            keyValuePairs.Add("Line", queryResult.Line?.Replace(" ", "").ToUpper());
//                        }
//                        else
//                        {
//                            keyValuePairs.Add("Line", "NA");
//                        }
//                        keyValuePairs.Add("key", materialNumber);
//                        keyValuePairs.Add("PN", materialNumber);
//                    }
//                    if (distinctDatelst.Count != 0)
//                    {
//                        foreach (var item in distinctDatelst)
//                        {
//                            var _key = item.Date.ToString("yyyy-MM-dd");
//                            if (!keyValuePairs.ContainsKey(_key))
//                            {
//                                keyValuePairs.Add(_key, item.Quantity.ToString());
//                            }
//                        }
//                    }
//                    dictionaries.Add(keyValuePairs);
//                }
//                sapPIRInfoViewModel.current = 1;
//                sapPIRInfoViewModel.pageSize = 20;
//                sapPIRInfoViewModel.startDate = DateTime.Now.ToString("yyyy-MM-dd");

//                var sortedDictionaries = dictionaries.OrderBy(dict => dict["Line"]).ToList();
//                sapPIRInfoViewModel.total = dictionaries.Count;
//                sapPIRInfoViewModel.list = sortedDictionaries;
//            }
//            return sapPIRInfoViewModel;
//        }

//        /// <summary>
//        /// 及时获取PIR数据
//        /// </summary>
//        public List<SapOriginalPir> GetPirInfoInTime()
//        {
//            HttpClient _httpClient = new HttpClient();
//            _httpClient.Timeout = TimeSpan.FromSeconds(120);
//            List<SapOriginalPir> sapOriginalPirs = [];
//            var uri = "http://10.2.34.21:8100/api/sap/GetPIRInfo";
//            //var uri = _configuration.GetSection("SapPir").ToString(); 
//            var request = new HttpRequestMessage(HttpMethod.Get, uri);
//            HttpResponseMessage response = _httpClient.Send(request);
//            try
//            {
//                // 确保HTTP响应成功  
//                HttpResponseMessage resmsg = response.EnsureSuccessStatusCode();
//                if (response.IsSuccessStatusCode)
//                {
//                    var responseBody = response.Content.ReadAsStream();
//                    StreamReader reader = new(responseBody);
//                    string text = reader.ReadToEnd();
//                    var apiResult = JsonConvert.DeserializeObject<ApiResult>(text);
//                    if (apiResult != null && apiResult.Data != null)
//                    {
//                        sapOriginalPirs = JsonConvert.DeserializeObject<List<SapOriginalPir>>(apiResult.Data.ToString());
//                    }
//                }

//                if (sapOriginalPirs.Count == 0) { return sapOriginalPirs; }

//                List<SAPDemand> sapDemands = [];
//                foreach (var item in sapOriginalPirs)
//                {
//                    if (string.IsNullOrEmpty(item.Date.Replace(" ", "")) || string.IsNullOrEmpty(item.Quantity.Replace(" ", "").ToString())) { continue; }
//                    var date = DateTime.ParseExact(item.Date.Replace(" ", "").ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
//                    SAPDemand sAPDemand = new SAPDemand();
//                    sAPDemand.Plant = item.Plant;
//                    sAPDemand.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                    sAPDemand.Type = item.Type.Replace(" ", "").ToUpper();
//                    sAPDemand.Date = date;
//                    sAPDemand.Quantity = int.Parse(item.Quantity.Replace(" ", "").ToUpper());
//                    sAPDemand.ReleaseTime = DateTime.Now;
//                    sAPDemand.CreateTime = DateTime.Now;
//                    sAPDemand.UpdateTime = DateTime.Now;
//                    sapDemands.Add(sAPDemand);
//                }

//                if (sapDemands.Count != 0)
//                {
//                    //_apsDbContext.GetTableFromSql("TRUNCATE TABLE [AP_SuZhou_APS_DevDb].[APS].[SAPDemand]");
//                    _apsDbContext.Database.ExecuteSqlRaw("TRUNCATE TABLE [AP_SuZhou_APS_DevDb].[APS].[SAPDemand]");
//                    _apsDbContext.SAPDemand.UpdateRange(sapDemands);
//                    _apsDbContext.SaveChanges();
//                }
//            }
//            catch (Exception ex)
//            {
//                LogHelper.Error(ex.Message);
//                return sapOriginalPirs;
//            }
//            return sapOriginalPirs;
//        }

//        /// <summary>
//        /// 每条线的损耗工时
//        /// </summary>
//        /// <returns></returns>
//        public EachLineTotalTimeViewModel GetEachLineTotalTimesInfo()
//        {
//            //获取原始需求 //计算
//            DateTime dt = DateTime.Now;
//            var result = from x in _apsDbContext.SAPDemand
//                         join y in _apsDbContext.MaterialMainLine
//                         on x.MaterialNumber.Replace(" ", "").ToUpper() equals y.MaterialNumber.Replace(" ", "").ToUpper()
//                         where x.Date >= dt && x.Quantity >= 1 && x.MaterialNumber != null && y.MainLine != null
//                         select new
//                         {
//                             Date = x.Date,
//                             Quantity = x.Quantity,
//                             UPH = y.UPH,
//                             Line = y.MainLine.Replace(" ", "").ToUpper(),
//                             StandbyLineA = y.StandbyLineA == null ? y.StandbyLineA.Replace(" ", "").ToUpper() : string.Empty,
//                             StandbyLineB = y.StandbyLineB == null ? y.StandbyLineB.Replace(" ", "").ToUpper() : string.Empty,
//                             StandbyLineC = y.StandbyLineC == null ? y.StandbyLineC.Replace(" ", "").ToUpper() : string.Empty,
//                             MaterialNumber = y.MaterialNumber
//                         };
//            EachLineTotalTimeViewModel eachLineTotalTimeViewModel = new EachLineTotalTimeViewModel();
//            List<Hashtable> hashtables = [];

//            if (result.Count() != 0)
//            {
//                var distinctLineslst = result.Select(x => x.Line).Distinct().OrderBy(x => x).ToList();
//                if (distinctLineslst.Count() != 0)
//                {
//                    foreach (var line in distinctLineslst)
//                    {
//                        //相同线别,日期进行汇总
//                        var samelineResult = result.Where(x => x.Line == line).ToList();
//                        if (samelineResult.Count() != 0)
//                        {
//                            Hashtable hashtable = new Hashtable();
//                            var querySeminline = result.Where(x => x.Line == line && x.StandbyLineA != null).FirstOrDefault();

//                            var _lineQty = 1;
//                            var _line = line;

//                            if (querySeminline != null)
//                            {
//                                _line = querySeminline.Line + querySeminline.StandbyLineA;
//                                _lineQty = 2;
//                            }

//                            hashtable.Add("key", line);
//                            hashtable.Add("Line", line);
//                            hashtable.Add("Workday", 6);
//                            hashtable.Add("LineQty", _lineQty);
//                            hashtable.Add("OEE", 0.9);

//                            var distinctDateResult = samelineResult.Select(x => x.Date).Distinct().ToList();
//                            if (distinctDateResult.Count() != 0)
//                            {
//                                foreach (var item in distinctDateResult)
//                                {
//                                    //相同线体，日期，不同MaterialNumber
//                                    //var sameLineAndDateResult = samelineResult.Where(x => x.Date == item).ToList();
//                                    var sameLineAndDateResult = samelineResult.Where(x => x.Date < item.AddDays(7) && x.Date >= item).ToList();

//                                    if (sameLineAndDateResult.Count() != 0)
//                                    {
//                                        var values = sameLineAndDateResult.Select(x => new { _ = x.Quantity / x.UPH }).ToArray();
//                                        double totalCount = values.Sum(x => x._);

//                                        //累加至周一
//                                        //星期一为第一天  
//                                        int weeknow = Convert.ToInt32(item.DayOfWeek);

//                                        //因为是以星期一为第一天，所以要判断weeknow等于0时，要向前推6天。  
//                                        weeknow = (weeknow == 0 ? (7 - 1) : (weeknow - 1));
//                                        int daydiff = (-1) * weeknow;
//                                        //本周第一天  
//                                        DateTime? firstDay = item.AddDays(daydiff);

//                                        var _key = firstDay?.ToString("yyyy-MM-dd");
//                                        //hashtable.Add(item?.ToString("yyyy-MM-dd"), totalCount);
//                                        if (hashtable.ContainsKey(_key))
//                                        {
//                                            var _res = (double)hashtable[_key] + totalCount;
//                                            hashtable[_key] = _res;
//                                        }
//                                        else
//                                        {
//                                            hashtable.Add(firstDay?.ToString("yyyy-MM-dd"), totalCount);
//                                        }
//                                    }

//                                }
//                            }
//                            hashtables.Add(hashtable);
//                        }
//                        eachLineTotalTimeViewModel.current = 1;
//                        eachLineTotalTimeViewModel.pageSize = 20;
//                        eachLineTotalTimeViewModel.startDate = dt.Date.ToString("yyyy-MM-dd");

//                    }

//                    eachLineTotalTimeViewModel.total = hashtables.Count;
//                    eachLineTotalTimeViewModel.list = hashtables;
//                }
//            }
//            return eachLineTotalTimeViewModel;
//        }

//        public TestVO TestScheduler(string m)
//        {
//            DateOnly endDate = new DateOnly(2024, 4, 21);
//            DateOnly FiexdDate = new DateOnly(2024, 3, 31);

//            TestVO vo = new TestVO();
//            List<TestVOEle> list = new List<TestVOEle>();

//            List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Quantity >= 1 && x.Date != null && x.MaterialNumber == m && x.Date > FiexdDate.AddDays(1).ToDateTime(TimeOnly.MinValue) && x.Date < endDate.AddDays(15).ToDateTime(TimeOnly.MinValue)).ToList();
//            LogHelper.Info("==============");
//            foreach (var item in sapDemands)
//            {
//                LogHelper.Info("Date->" + item.Date);
//                LogHelper.Info("Number->" + item.Quantity);
//            }

//            list = sapDemands.Select(ele => new TestVOEle()
//            {
//                Date = DateOnly.FromDateTime((DateTime)ele.Date),
//                Number = ele.Quantity
//            }).ToList();
//            foreach (var item in list)
//            {
//                LogHelper.Info("Date->" + item.Date);
//                LogHelper.Info("Number->" + item.Number);
//            }
//            vo.pir = list;
//            vo.pirNumber = vo.pir.Where(ele => ele.Date > FiexdDate).Sum(ele => ele.Number);
//            list = new List<TestVOEle>();
//            FactoryPlan p = TestScheduler();
//            foreach (var n in p.LinePlanDictionary.Values)
//            {
//                foreach (var d in n.DayPlanList)
//                {
//                    foreach (var mm in d.MaterialPlanDictionary.Values)
//                    {
//                        if (mm.Material == m)
//                        {

//                            list.Add(new TestVOEle()
//                            {
//                                Date = mm.Date,
//                                Number = mm.Number
//                            });
//                        }
//                    }
//                }
//            }
//            list.Sort((p1, p2) => p1.Date.CompareTo(p2.Date));
//            vo.plan = list.Where(ele => ele.Date > FiexdDate).ToList();
//            vo.planNumber = vo.plan.Sum(ele => ele.Number);
//            return vo;
//        }
//        public FactoryPlan TestScheduler()
//        {
//            DateOnly pirDate = new DateOnly(2024, 3, 26);
//            DateOnly endDate = new DateOnly(2024, 4, 21);
//            DateOnly FiexdDate = new DateOnly(2024, 3, 31);
//            DateOnly startDate = new DateOnly(2024, 3, 25);
//            List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Quantity >= 1 && x.Date != null).ToList();
//            List<SAPDemand> pirList = SplitPir(sapDemands, pirDate, endDate);
//            List<MaterialMainLine> materialMainLines = _apsDbContext.MaterialMainLine.Where(ele => ele.MaterialNumber != null).ToList();
//            Dictionary<string, MaterialMainLine> dictionary = materialMainLines.GroupBy(item => item.MaterialNumber).Select(group => group.Last()).ToDictionary(p => p.MaterialNumber, p => p);

//            FactoryPlan factoryPlan = new FactoryPlan(pirList, new List<SchedulerSummary>(), dictionary);
//            factoryPlan.FiexdDate = FiexdDate;
//            factoryPlan.EndDate = endDate;
//            factoryPlan.StartDate = startDate;
//            factoryPlan.RestDaySet = new HashSet<DateOnly>() { new DateOnly(2024, 3, 23), new DateOnly(2024, 3, 24), new DateOnly(2024, 3, 30), new DateOnly(2024, 3, 31), new DateOnly(2024, 4, 4), new DateOnly(2024, 4, 5), new DateOnly(2024, 4, 6) };
//            factoryPlan.LineRestDayDictionary = new Dictionary<string, ISet<DateOnly>>() { };
//            Dictionary<DateOnly, double> hcDictionary = new Dictionary<DateOnly, double>();
//            for (DateOnly d = startDate; d <= endDate.AddDays(20); d = d.AddDays(1))
//            {
//                hcDictionary.Add(d, 1500);
//            }
//            try
//            {
//                factoryPlan.DebugScheduler(hcDictionary);
//            }
//            catch (Exception ex)
//            {
//                LogHelper.Info(ex.ToString());
//            }
//            string m = "17048955-01M1";
//            List<MaterialPlan> finishedList = CheckMaterial(factoryPlan, m);
//            LogHelper.Info("finished list:" + System.Text.Json.JsonSerializer.Serialize(finishedList));
//            LogHelper.Info("finished list sum-->:" + finishedList.Sum(ele => ele.Number));
//            return factoryPlan;
//        }
//        private List<MaterialPlan> CheckMaterial(FactoryPlan factoryPlan, string material)
//        {
//            List<MaterialPlan> list = new List<MaterialPlan>();
//            foreach (var line in factoryPlan.LinePlanDictionary.Values)
//            {
//                foreach (var day in line.DayPlanList)
//                {
//                    foreach (MaterialPlan m in day.MaterialPlanDictionary.Values)
//                    {
//                        if (m.Material == material && m.Date > factoryPlan.FiexdDate)
//                        {
//                            list.Add(m);
//                        }
//                    }
//                }
//            }
//            return list;
//        }
//        private List<SAPDemand> SplitPir(List<SAPDemand> sapDemands, DateOnly pirDate, DateOnly endDate)
//        {
//            List<SAPDemand> pirList = sapDemands.Where(x => IsBetween(x.Date, pirDate, endDate.AddDays(14))).ToList();

//            int dayOfWeekIndex = (int)pirDate.DayOfWeek;
//            DateOnly splitDate = pirDate.AddDays((dayOfWeekIndex <= 1 ? 1 : 8) - dayOfWeekIndex).AddDays(7);
//            List<SAPDemand> newList = new List<SAPDemand>();
//            foreach (var item in pirList)
//            {

//                if (item.Date != null && DateOnly.FromDateTime((DateTime)item.Date) >= splitDate && item.Quantity > 3)
//                {
//                    SAPDemand sAPDemand2 = new SAPDemand()
//                    {
//                        MaterialNumber = item.MaterialNumber,
//                        Plant = item.Plant,
//                        Type = item.Type,
//                        ReleaseTime = item.ReleaseTime,
//                        Quantity = item.Quantity / 3
//                    };
//                    sAPDemand2.Date = item.Date.AddDays(2);
//                    newList.Add(sAPDemand2);
//                    SAPDemand sAPDemand3 = new SAPDemand()
//                    {
//                        MaterialNumber = item.MaterialNumber,
//                        Plant = item.Plant,
//                        Type = item.Type,
//                        ReleaseTime = item.ReleaseTime,
//                        Quantity = item.Quantity / 3
//                    };
//                    sAPDemand3.Date = item.Date.AddDays(4);
//                    newList.Add(sAPDemand3);
//                    item.Quantity = item.Quantity - item.Quantity / 3 - item.Quantity / 3;
//                }
//            }
//            pirList.AddRange(newList);
//            return pirList.OrderBy(p => p.Date).ToList(); ;
//        }

//        private bool IsBetween(DateTime? time, DateOnly startDate, DateOnly endDate)
//        {
//            if (time == null)
//            {
//                return false;
//            }
//            else
//            {
//                DateOnly date = DateOnly.FromDateTime((DateTime)time);
//                return date >= startDate && date <= endDate;
//            }
//        }
//        #endregion


//        public string DownloadPIRInfo(SapPIRInfoViewModel sapPIRInfoViewModel)
//        {
//            string excelName = "Template";
//            string excelPath = string.Empty;
//            try
//            {
//                //首先创建Excel文件对象
//                var workbook = new HSSFWorkbook();

//                //创建工作表，也就是Excel中的sheet，给工作表赋一个名称(Excel底部名称)
//                var sheet = workbook.CreateSheet("sheet1");

//                //sheet.DefaultColumnWidth = 20;//默认列宽

//                sheet.ForceFormulaRecalculation = false;//TODO:是否开始Excel导出后公式仍然有效（非必须）

//                //二级标题列样式设置
//                var listData = sapPIRInfoViewModel.list;
//                var headTopStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 15, true, 700, "楷体", true, false, false, true, FillPattern.SolidForeground, HSSFColor.Grey25Percent.Index, HSSFColor.Black.Index, FontUnderlineType.None, FontSuperScript.None, false);

//                var _headerlist = new List<string>() { "PN", "Line" };
//                _headerlist.AddRange(sapPIRInfoViewModel.title);
//                var maxRows = _headerlist.Count();
//                var headerArr = _headerlist.ToArray();
//                var row = NpoiExcelExportHelper._.CreateRow(sheet, 0, 24);  //第二行
//                var cell = row.CreateCell(0);
//                for (var i = 0; i < headerArr.Length; i++)
//                {
//                    cell = NpoiExcelExportHelper._.CreateCells(row, headTopStyle, i, headerArr[i]);
//                    sheet.SetColumnWidth(i, 5000); //设置单元格宽度
//                }

//                //单元格边框样式
//                var cellStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 10, true, 400);

//                var currentDate = DateTime.Now;
//                int rowNumber = 0;
//                foreach (var diclist in listData)
//                {
//                    rowNumber++;
//                    row = NpoiExcelExportHelper._.CreateRow(sheet, rowNumber, 24);  //第二行
//                    int columnNumber = 0;
//                    List<int> existedColumnIndexlist = [];
//                    foreach (var key in diclist.Keys.OrderBy(x => x.Length))
//                    {
//                        var content = diclist[key] == null ? "" : diclist[key].ToString();
//                        var columnIndex = _headerlist.FindIndex(x => x.Replace(" ", "").ToUpper() == key.Replace(" ", "").ToUpper());
//                        if (columnIndex == -1) { continue; }
//                        cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, columnIndex, content);
//                        existedColumnIndexlist.Add(columnIndex);
//                    }

//                    //补充空白
//                    var newMaxRowsCopy = maxRows;
//                    if (maxRows >= 1)
//                    {
//                        for (int i = 0; i < newMaxRowsCopy; i++)
//                        {
//                            if (!existedColumnIndexlist.Contains(i))
//                            {
//                                cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, i, string.Empty);
//                            }
//                        }
//                    }
//                }

//                string folder = DateTime.Now.ToString("yyyyMMdd");


//                var uploadPath = AppDomain.CurrentDomain.BaseDirectory + "UploadFile\\" + folder + "\\";

//                //excel保存文件名
//                string excelFileName = excelName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";

//                //创建目录文件夹
//                if (!Directory.Exists(uploadPath))
//                {
//                    Directory.CreateDirectory(uploadPath);
//                }

//                //Excel的路径及名称
//                excelPath = uploadPath + excelFileName;

//                //使用FileStream文件流来写入数据（传入参数为：文件所在路径，对文件的操作方式，对文件内数据的操作）
//                var fileStream = new FileStream(excelPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

//                //向Excel文件对象写入文件流，生成Excel文件
//                workbook.Write(fileStream);

//                //关闭文件流
//                fileStream.Close();

//                //释放流所占用的资源
//                fileStream.Dispose();

//                //excel文件保存的相对路径，提供前端下载
//                var relativePositioning = "/UploadFile/" + folder + "/" + excelFileName;
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(e.Message);
//            }
//            return excelPath;
//        }
//        public string DownloadStaffTimeByDayInfo(List<Dictionary<string, object>> listData)
//        {
//            string excelName = "Template";
//            string excelPath = string.Empty;
//            try
//            {
//                //首先创建Excel文件对象
//                var workbook = new HSSFWorkbook();

//                //创建工作表，也就是Excel中的sheet，给工作表赋一个名称(Excel底部名称)
//                var sheet = workbook.CreateSheet("sheet1");

//                //sheet.DefaultColumnWidth = 20;//默认列宽

//                sheet.ForceFormulaRecalculation = false;//TODO:是否开始Excel导出后公式仍然有效（非必须）

//                //二级标题列样式设置
//                var headTopStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 15, true, 700, "楷体", true, false, false, true, FillPattern.SolidForeground, HSSFColor.Grey25Percent.Index, HSSFColor.Black.Index,
//                FontUnderlineType.None, FontSuperScript.None, false);
//                var maxRows = listData.OrderByDescending(x => x.Count).FirstOrDefault().Keys.Count();
//                var headerlist = listData.OrderByDescending(x => x.Count).FirstOrDefault().Keys.OrderBy(x => x.Length).ToArray();  //表头名称

//                //Line 05 - 07 - Plan
//                var _headerlist = headerlist.Where(x => x.ToString() != "key" && x.ToString().EndsWith("Plan")).OrderBy(x => x).ToList();
//                _headerlist.Insert(0, "Line");
//                headerlist = _headerlist.ToArray();
//                var row = NpoiExcelExportHelper._.CreateRow(sheet, 0, 24);  //第二行
//                var cell = row.CreateCell(0);
//                for (var i = 0; i < headerlist.Length; i++)
//                {
//                    cell = NpoiExcelExportHelper._.CreateCells(row, headTopStyle, i, headerlist[i]);
//                    sheet.SetColumnWidth(i, 5000); //设置单元格宽度
//                }

//                //单元格边框样式
//                var cellStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 10, true, 400);

//                var currentDate = DateTime.Now;
//                int rowNumber = 0;
//                foreach (var diclist in listData)
//                {
//                    rowNumber++;
//                    row = NpoiExcelExportHelper._.CreateRow(sheet, rowNumber, 24);  //第二行
//                    int columnNumber = 0;
//                    List<int> existedColumnIndexlist = [];
//                    foreach (var key in diclist.Keys.OrderBy(x => x.Length))
//                    {
//                        if (key == "key") { continue; }
//                        var content = diclist[key] == null ? "" : diclist[key].ToString();

//                        var columnIndex = _headerlist.FindIndex(x => x.Replace(" ", "").ToUpper() == key.Replace(" ", "").ToUpper());
//                        if (columnIndex == -1) { continue; }
//                        cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, columnIndex, content);
//                        existedColumnIndexlist.Add(columnIndex);
//                    }

//                    //补充空白
//                    //var newMaxRowsCopy = maxRows - columnNumber;
//                    var newMaxRowsCopy = maxRows;
//                    if (maxRows >= 1)
//                    {
//                        for (int i = 0; i < newMaxRowsCopy; i++)
//                        {
//                            if (!existedColumnIndexlist.Contains(i))
//                            {
//                                cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, i, string.Empty);
//                            }
//                        }
//                    }
//                }

//                string folder = DateTime.Now.ToString("yyyyMMdd");


//                var uploadPath = AppDomain.CurrentDomain.BaseDirectory + "UploadFile\\" + folder + "\\";

//                //excel保存文件名
//                string excelFileName = excelName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";

//                //创建目录文件夹
//                if (!Directory.Exists(uploadPath))
//                {
//                    Directory.CreateDirectory(uploadPath);
//                }

//                //Excel的路径及名称
//                excelPath = uploadPath + excelFileName;

//                //使用FileStream文件流来写入数据（传入参数为：文件所在路径，对文件的操作方式，对文件内数据的操作）
//                var fileStream = new FileStream(excelPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

//                //向Excel文件对象写入文件流，生成Excel文件
//                workbook.Write(fileStream);

//                //关闭文件流
//                fileStream.Close();

//                //释放流所占用的资源
//                fileStream.Dispose();

//                //excel文件保存的相对路径，提供前端下载
//                var relativePositioning = "/UploadFile/" + folder + "/" + excelFileName;
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(e.Message);
//            }
//            return excelPath;
//        }

//        public string DownloadStaffTimeByWeekInfo(DownloadModel downloadModel)
//        {
//            string excelName = "Template";
//            string excelPath = string.Empty;
//            try
//            {
//                //首先创建Excel文件对象
//                var workbook = new HSSFWorkbook();

//                //创建工作表，也就是Excel中的sheet，给工作表赋一个名称(Excel底部名称)
//                var sheet = workbook.CreateSheet("sheet1");

//                //sheet.DefaultColumnWidth = 20;//默认列宽

//                sheet.ForceFormulaRecalculation = false;//TODO:是否开始Excel导出后公式仍然有效（非必须）

//                //二级标题列样式设置
//                var headTopStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 15, true, 700, "楷体", true, false, false, true, FillPattern.SolidForeground, HSSFColor.Grey25Percent.Index, HSSFColor.Black.Index,
//                FontUnderlineType.None, FontSuperScript.None, false);
//                var headerlist = downloadModel.title.ToArray();
//                var maxRows = headerlist.Count();
//                var row = NpoiExcelExportHelper._.CreateRow(sheet, 0, 24);  //第二行
//                var cell = row.CreateCell(0);
//                for (var i = 0; i < headerlist.Length; i++)
//                {
//                    cell = NpoiExcelExportHelper._.CreateCells(row, headTopStyle, i, headerlist[i]);
//                    sheet.SetColumnWidth(i, 5000); //设置单元格宽度
//                }

//                //单元格边框样式
//                var cellStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 10, true, 400);

//                var currentDate = DateTime.Now;
//                int rowNumber = 0;
//                var listData = downloadModel.data;
//                foreach (var diclist in listData)
//                {
//                    rowNumber++;
//                    row = NpoiExcelExportHelper._.CreateRow(sheet, rowNumber, 24);  //第二行
//                    int columnNumber = 0;
//                    List<int> existedColumnIndexlist = [];
//                    foreach (var key in diclist.Keys.OrderBy(x => x.Length))
//                    {
//                        var content = diclist[key] == null ? "" : diclist[key].ToString();
//                        var columnIndex = headerlist.ToList().FindIndex(x => x.Replace(" ", "").ToUpper() == key.Replace(" ", "").ToUpper());
//                        if (columnIndex == -1) { continue; }
//                        cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, columnIndex, content);
//                        existedColumnIndexlist.Add(columnIndex);
//                    }

//                    //补充空白
//                    var newMaxRowsCopy = maxRows;
//                    if (maxRows >= 1)
//                    {
//                        for (int i = 0; i < newMaxRowsCopy; i++)
//                        {
//                            if (!existedColumnIndexlist.Contains(i))
//                            {
//                                cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, i, string.Empty);
//                            }
//                        }
//                    }
//                }

//                string folder = DateTime.Now.ToString("yyyyMMdd");


//                var uploadPath = AppDomain.CurrentDomain.BaseDirectory + "UploadFile\\" + folder + "\\";

//                //excel保存文件名
//                string excelFileName = excelName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";

//                //创建目录文件夹
//                if (!Directory.Exists(uploadPath))
//                {
//                    Directory.CreateDirectory(uploadPath);
//                }

//                //Excel的路径及名称
//                excelPath = uploadPath + excelFileName;

//                //使用FileStream文件流来写入数据（传入参数为：文件所在路径，对文件的操作方式，对文件内数据的操作）
//                var fileStream = new FileStream(excelPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

//                //向Excel文件对象写入文件流，生成Excel文件
//                workbook.Write(fileStream);

//                //关闭文件流
//                fileStream.Close();

//                //释放流所占用的资源
//                fileStream.Dispose();

//                //excel文件保存的相对路径，提供前端下载
//                var relativePositioning = "/UploadFile/" + folder + "/" + excelFileName;
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(e.Message);
//            }
//            return excelPath;
//        }

//        public SchedulerSummaryViewModel ExecuteSchedulerSummary(DateTime startDate, DateTime endDate, double oee, int days)
//        {
//            throw new NotImplementedException();
//        }
//    }
//}