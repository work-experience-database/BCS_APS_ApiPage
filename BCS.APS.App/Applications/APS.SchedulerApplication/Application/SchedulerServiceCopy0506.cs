﻿//using APS.SchedulerApplication.Entitys;
//using APS.SchedulerApplication.Interfaces;
//using APS.SchedulerApplication.Model;
//using APS.Utils.Attributes;
//using APS.Utils.Extensions;
//using APS.Utils.Helper;
//using APS.Utils.Models;
//using Newtonsoft.Json;
//using System.Collections;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.Configuration;
//using NPOI.HSSF.UserModel;
//using NPOI.HSSF.Util;
//using NPOI.SS.UserModel;
//using APS.SchedulerApplication.Model.ScheduleModel;
//using static NPOI.HSSF.Util.HSSFColor;
//using System.Linq;
//using System.Collections.Generic;
//namespace APS.SchedulerApplication.Application
//{
//    [Transient]
//    public class SchedulerService : ISchedulerService
//    {
//        private APSDbContext _apsDbContext;
//        static DateTime date = DateTime.Now.Date;
//        static DateTime firstMonday;
//        static DateTime scheduleLastDay; //挖掘数据的最后一天
//        static int weekNumber;
//        static double standareTime = 22;
//        static int dateScope = 7;
//        static List<SAPDemand> originalPirs = [];
//        static List<LineMaintain> lineMaintains = [];
//        static List<NewSapPirModel> newSapPirModels = [];
//        static List<FactoryCalendar> factoryCalendars = [];
//        static List<SchedulerSummary> schedulerSummaries = [];
//        static List<EachDayHumanQuantity> eachDayHumanQuantitys = [];
//        static List<ScheduleErrorMessage> scheduleErrorMessagelst = [];
//        static List<SchedulerSummary> ReleaseToSapSchedulerSummaries = [];
//        static List<MaterialNumberLineMapRelations> materialNumberLineMapRelations = [];
//        static readonly HttpClient _httpClient = new HttpClient();
//        private readonly IConfiguration _configuration;

//        public SchedulerService(APSDbContext apsDbContext, IConfiguration configuration)
//        {
//            _apsDbContext = apsDbContext;
//            _configuration = configuration;
//            firstMonday = WeekHelper.GetMondayDate(date);
//            weekNumber = WeekHelper.GetWeekNumber(date);
//            scheduleLastDay = firstMonday.AddDays(27);
//            eachDayHumanQuantitys = _apsDbContext.EachDayHumanQuantity.ToList();
//            lineMaintains = _apsDbContext.LineMaintain.Where(x => !x.Enable).ToList();
//            factoryCalendars = _apsDbContext.FactoryCalendar.Where(x => !x.IsWork).ToList();
//            materialNumberLineMapRelations = _apsDbContext.MaterialNumberLineMapRelations.Where(x => x.HC != "0" && !string.IsNullOrEmpty(x.HC)).ToList();
//        }

//        #region 日排程主函数
//        /// <summary>Main Service</summary>
//        public SchedulerSummaryViewModel ExecuteSchedulerSummary(DateTime startDate = default, DateTime endDate = default, double oee = 0.9, int days = 1)
//        {
//            //获取原始的PIR需求，并分配线体，排序
//            if (startDate == default)
//                startDate = DateTime.Now.Date;

//            if (endDate == default)
//                endDate = firstMonday.AddDays(6);

//            eachDayHumanQuantitys.ForEach(x => { x.Date = x.Date.Date; x.Quantity = Math.Ceiling(x.Quantity * oee); });
//            lineMaintains.ForEach(x => { x.StartTime = x.StartTime.Date; x.EndTime = x.EndTime.Date; });
//            lineMaintains.ForEach(x => { x.StartTime = x.StartTime.Date; x.EndTime = x.EndTime.Date; });
//            materialNumberLineMapRelations.ForEach(x => x.MaterialNumber = x.MaterialNumber.Replace(" ", "").ToUpper());

//            //获取原始PIR,扣除库存
//            originalPirs = GetOriginalPIR(endDate);

//            //日期提前，拆分需求日
//            List<SAPDemand> sapPirs = SplitAndAdvancePickUpDate(endDate, originalPirs, days);

//            //创建需求模型
//            newSapPirModels = SortSapPIR(sapPirs);

//            //日排程
//            List<SchedulerSummary> schedulerSummaries = ScheduleEachDay(newSapPirModels, startDate, endDate);
//            return SchedulerSummaryTransforFEData(schedulerSummaries);
//        }
//        #endregion

//        #region 处理原始PIR
//        /// <summary>#获取原始PIR#扣除上一版本的库存</summary>
//        public List<SAPDemand> GetOriginalPIR(DateTime endDate)
//        {
//            List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Date.ToString() != null && x.Date >= firstMonday && x.Date < firstMonday.AddDays(42) && x.Quantity >= 1).ToList();
//            if (sapDemands.Count == 0) { return sapDemands; }

//            //扣除库存
//            //净需求=Fixed日期范围内的原始PIR-上一版本排程的

//            //获取原始PIR
//            var lastVersionSchedulesFromPir = sapDemands.Where(x => x.Date.Date < endDate.AddDays(1) && x.Date.Date >= DateTime.Now.Date).ToList();

//            //获取上一版本的排程计划
//            //值得注意的是,获取PIR的日期是已经扣除当前日期之前的Plan数量的
//            var lastVersionSchedules = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= DateTime.Now.Date && x.SchedulerDate < endDate.AddDays(1) && !x.Deleted).ToList();
//            schedulerSummaries = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= firstMonday.Date && x.SchedulerDate < endDate.AddDays(1) && !x.Deleted).ToList();
//            Dictionary<string, int> materialNumberAcutualQuantitys = [];
//            if (lastVersionSchedules.Count != 0)
//            {
//                List<string> distinctMaterialNumbers = lastVersionSchedules.Select(x => x.MaterialNumber.Replace(" ", "").ToUpper()).Distinct().ToList();
//                int _totalQuantity = 0;
//                foreach (var materialNumber in distinctMaterialNumbers)
//                {
//                    _totalQuantity = lastVersionSchedules.Where(x => x.MaterialNumber.Equals(materialNumber)).Sum(x => x.PlanedQuantity).ToInt();
//                    materialNumberAcutualQuantitys.TryAdd(materialNumber, _totalQuantity);
//                }
//            }

//            //净需求
//            if (lastVersionSchedulesFromPir.Count != 0)
//            {
//                if (materialNumberAcutualQuantitys.Keys.Count != 0)
//                {
//                    foreach (var item in lastVersionSchedulesFromPir)
//                    {
//                        item.Date = item.Date.Date;
//                        if (materialNumberAcutualQuantitys.Keys.Contains(item.MaterialNumber.Replace(" ", "").ToUpper()))
//                        {
//                            item.Quantity -= materialNumberAcutualQuantitys[item.MaterialNumber.Replace(" ", "").ToUpper()];
//                        }
//                    }
//                }
//            }

//            //扣除净需求
//            var newLastVersionSchedulesFromPir = lastVersionSchedulesFromPir.Where(x => x.Quantity != 0).ToList();
//            var nextWeeksPirs = sapDemands.Where(x => x.Date >= endDate.AddDays(1)).ToList();

//            //将上一版本的净需求一分为二
//            var _newLastVersionSchedulesFromPir1 = newLastVersionSchedulesFromPir.Where(x => x.Quantity > 0).ToList(); //没有生产完，如PIR需求100，Plan为90，将此需求放到本周六
//            var _newLastVersionSchedulesFromPir2 = newLastVersionSchedulesFromPir.Where(x => x.Quantity < 0).ToList(); //超额生产，如PIR需求100，Plan为110，将接下来的需求进行扣除

//            //说明上周没有生产完，有剩余==>没有生产完，如PIR需求100，Plan为90，将此需求放到本周六
//            //==>判断Fix周六有无需求
//            //==>有需求则累加，无需求则直接添加
//            if (_newLastVersionSchedulesFromPir1.Count != 0)
//            {
//                foreach (var item in _newLastVersionSchedulesFromPir1)
//                {
//                    var queryExistInFixDays = schedulerSummaries.Where(x => x.SchedulerDate == firstMonday.AddDays(5) && x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList();
//                    if (queryExistInFixDays.Count != 0)
//                    {
//                        schedulerSummaries.Where(x => x.SchedulerDate == firstMonday.AddDays(5) && x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList().ForEach(x => x.PlanedQuantity += item.Quantity);
//                    }
//                    else
//                    {
//                        var basicInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).OrderBy(x => x.Order).FirstOrDefault();
//                        if (basicInfo != null)
//                        {
//                            SchedulerSummary schedulerSummary = new SchedulerSummary();
//                            schedulerSummary.SchedulerDate = firstMonday.AddDays(5);
//                            schedulerSummary.SchedulerId = item.MaterialNumber.Replace(" ", "").ToUpper() + basicInfo.Line.Replace(" ", "").ToUpper() + firstMonday.AddDays(5).ToString("yyyyMMdd");
//                            schedulerSummary.Plant = item.Plant;
//                            schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                            schedulerSummary.Line = basicInfo.Line;
//                            schedulerSummary.SPQ = basicInfo.SPQ == null ? 0 : int.Parse(basicInfo.SPQ);
//                            schedulerSummary.UPH = double.Parse(basicInfo.UPH);
//                            schedulerSummary.HC = double.Parse(basicInfo.HC);
//                            schedulerSummary.Area = basicInfo.Area;
//                            schedulerSummary.Description = basicInfo.Description;
//                            schedulerSummary.WeekNumber = WeekHelper.GetWeekNumber(firstMonday);
//                            schedulerSummary.KeyUser = basicInfo.User;
//                            schedulerSummary.PlanedQuantity = item.Quantity;
//                            schedulerSummaries.Add(schedulerSummary);
//                        }
//                    }
//                }
//            }

//            //说明上周生产完，且多生产了,超额生产，如PIR需求100，Plan为110，将接下来的需求进行扣除
//            //#判断接下来有无再生产此料号
//            //==>没有生产此料号，则不做任何处理
//            //==>有生产，则进行扣除
//            if (_newLastVersionSchedulesFromPir2.Count != 0)
//            {
//                int _currentWeekQuantity = 0;
//                int _nextWeeksTotalQuantity = 0;
//                foreach (var item in _newLastVersionSchedulesFromPir2)
//                {
//                    _currentWeekQuantity = item.Quantity * -1;
//                    item.Date = item.Date.Date;

//                    #region 将需求全部挪到周日
//                    var queryExistInFixDays = schedulerSummaries.Where(x => x.SchedulerDate == firstMonday.AddDays(6) && x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList();
//                    if (queryExistInFixDays.Count != 0)
//                    {
//                        schedulerSummaries.Where(x => x.SchedulerDate == firstMonday.AddDays(5) && x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList().ForEach(x => x.PlanedQuantity += item.Quantity);
//                    }
//                    else
//                    {
//                        var basicInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).OrderBy(x => x.Order).FirstOrDefault();
//                        if (basicInfo != null)
//                        {
//                            SchedulerSummary schedulerSummary = new SchedulerSummary();
//                            schedulerSummary.SchedulerDate = firstMonday.AddDays(6);
//                            schedulerSummary.SchedulerId = item.MaterialNumber.Replace(" ", "").ToUpper() + basicInfo.Line.Replace(" ", "").ToUpper() + firstMonday.AddDays(6).ToString("yyyyMMdd");
//                            schedulerSummary.Plant = item.Plant;
//                            schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                            schedulerSummary.Line = basicInfo.Line;
//                            schedulerSummary.SPQ = basicInfo.SPQ == null ? 0 : int.Parse(basicInfo.SPQ);
//                            schedulerSummary.UPH = double.Parse(basicInfo.UPH);
//                            schedulerSummary.HC = double.Parse(basicInfo.HC);
//                            schedulerSummary.Area = basicInfo.Area;
//                            schedulerSummary.Description = basicInfo.Description;
//                            schedulerSummary.WeekNumber = WeekHelper.GetWeekNumber(firstMonday);
//                            schedulerSummary.KeyUser = basicInfo.User;
//                            schedulerSummary.PlanedQuantity = item.Quantity;
//                            schedulerSummaries.Add(schedulerSummary);
//                        }
//                    }

//                    #endregion

//                    #region Fix周多生产，接下来少生产
//                    ////判断下接下来有无生产此料号计划
//                    //var _nextWeeksPirs = nextWeeksPirs.Where(x => x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber).ToList();
//                    //if (_nextWeeksPirs.Count() == 0) { continue; } //说明接下来五周内不再生产此料号

//                    ////接下来仍然会生产此料号
//                    ////说明上周生产完，且多生产了 ==>在接下来几周内扣除多生产的
//                    ////接下来将上一版本的计划数量全部扣除
//                    ////计算出接下来六周内全部排程计划
//                    //_nextWeeksTotalQuantity = _nextWeeksPirs.Sum(x => x.Quantity).ToInt();
//                    //if (_nextWeeksTotalQuantity == 0) { continue; } //说明接下来五周内不再生产此料号

//                    ////说明接下来六周全部被扣除
//                    //if (_currentWeekQuantity >= _nextWeeksTotalQuantity)
//                    //{
//                    //    nextWeeksPirs.Where(x => x.MaterialNumber == item.MaterialNumber).ToList().ForEach(x => x.Quantity = 0);
//                    //}
//                    //else
//                    //{
//                    //    //说明接下来六周排程足够扣除库存
//                    //    //遍历未来排程日，起始日期为Fixed的第二天
//                    //    foreach (var item2 in nextWeeksPirs)
//                    //    {
//                    //        if (item2.MaterialNumber != item.MaterialNumber) { continue; }
//                    //        _currentWeekQuantity -= (int)item2.Quantity;

//                    //        if (_currentWeekQuantity >= 0) { continue; }

//                    //        if (_currentWeekQuantity < 0)
//                    //        {
//                    //            //小于,说明到了临界值，将多减的加回来 ，并保留此原始PIR需求
//                    //            item2.Quantity = _currentWeekQuantity * -1;
//                    //            break;
//                    //        }
//                    //    }
//                    //}
//                    #endregion
//                }
//            }
//            return sapDemands;
//        }
//        /// <summary>第二周日期提前一天#第三，四，五，六周，拆到周一，三，五，然后再提前一天</summary>
//        public List<SAPDemand> SplitAndAdvancePickUpDate(DateTime endDate, List<SAPDemand> sapPirs, int days = 1)
//        {
//            List<SAPDemand> lst = [];
//            //对原始需求进行分割
//            var secondWeekPIRlst = sapPirs.Where(x => x.Date >= endDate.AddDays(1) && x.Date < firstMonday.AddDays(14)).OrderBy(x => x.Date).ToList();
//            var futureWeeksPIRlst = sapPirs.Where(x => x.Date >= firstMonday.AddDays(14)).OrderBy(x => x.Date).ToList();

//            //第二周
//            secondWeekPIRlst.ForEach(x => x.Date = x.Date.AddDays(-1));

//            //第三，四，五，六周
//            int _quantity = 0;
//            List<SAPDemand> newFutureWeeksPIRlst = [];
//            foreach (var item in futureWeeksPIRlst)
//            {
//                _quantity = item.Quantity;

//                if (item.Quantity <= 0) { continue; }
//                if (item.Quantity <= 100 && item.Quantity >= 1) //小于等于100不拆分
//                {
//                    newFutureWeeksPIRlst.Add(item);
//                    continue;
//                }
//                else
//                {
//                    //拆分的天数
//                    int dayCount = 3;
//                    //当拆分不能被整除时，余数放到最后一天
//                    int avaQuantity = _quantity / dayCount;
//                    //根据当前需求日，获取当前周的周一日期
//                    var currentMonDayDate = WeekHelper.GetMondayDate(item.Date);

//                    #region 由周拆至日
//                    SAPDemand newItem1 = new();
//                    newItem1 = AutoMapperHelper.Map<SAPDemand, SAPDemand>(item);
//                    newItem1.Quantity = avaQuantity;
//                    newItem1.Date = currentMonDayDate.AddDays(-days);
//                    newFutureWeeksPIRlst.Add(newItem1);

//                    SAPDemand newItem2 = new();
//                    newItem2 = AutoMapperHelper.Map<SAPDemand, SAPDemand>(item);
//                    newItem2.Quantity = avaQuantity;
//                    newItem2.Date = currentMonDayDate.AddDays(2).AddDays(-days);
//                    newFutureWeeksPIRlst.Add(newItem2);

//                    SAPDemand newItem3 = new();
//                    newItem3 = AutoMapperHelper.Map<SAPDemand, SAPDemand>(item);
//                    newItem3.Quantity = _quantity - 2 * avaQuantity;
//                    newItem3.Date = currentMonDayDate.AddDays(4).AddDays(-days);
//                    newFutureWeeksPIRlst.Add(newItem3);
//                    #endregion  由周拆至日
//                }
//            }
//            lst.AddRange(secondWeekPIRlst);
//            lst.AddRange(newFutureWeeksPIRlst);
//            return lst;
//        }
//        /// <summary>将所有MaterialNumber需求汇总到一天</summary>
//        public List<NewSapPirModel> SortSapPIR(List<SAPDemand> sapPirs)
//        {
//            List<NewSapPirModel> newSapPirModels = [];
//            var distinctDates = sapPirs.OrderBy(x => x.Date).Select(x => x.Date).Distinct().ToList();
//            foreach (var date in distinctDates)
//            {
//                NewSapPirModel newSapPirModel = new NewSapPirModel();
//                newSapPirModel.Date = date;
//                newSapPirModel.Plant = "535A";
//                var sameDateResult = sapPirs.Where(x => x.Date == date).OrderByDescending(x => x.Quantity).ToList();
//                if (sameDateResult.Count != 0)
//                {
//                    List<PIRDemand> lst = [];
//                    foreach (var item in sameDateResult)
//                    {
//                        var queryModel = lst.Where(x => x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).FirstOrDefault();
//                        if (queryModel == null)
//                        {
//                            PIRDemand pirDemand = new PIRDemand();
//                            pirDemand.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                            pirDemand.Quantity = item.Quantity;
//                            lst.Add(pirDemand);
//                        }
//                        else
//                        {
//                            lst.Where(x => x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList().ForEach(x => x.Quantity += item.Quantity);
//                        }
//                    }
//                    newSapPirModel.PIRDemands = lst;
//                }
//                newSapPirModels.Add(newSapPirModel);
//            }
//            return newSapPirModels;
//        }
//        #endregion

//        #region 日排程
//        /// <summary>正序排程,并将排程结果存储到Db</summary>
//        public List<SchedulerSummary> ScheduleEachDay(List<NewSapPirModel> newSapPirModels, DateTime startDate, DateTime endDate)
//        {
//            //获取上一版的排程计划
//            var lastVsersionSchedules = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= firstMonday && x.SchedulerDate < endDate.AddDays(-1)).OrderBy(x => x.SchedulerDate).ToList();
//            newSapPirModels = newSapPirModels.OrderBy(x => x.Date).ToList();

//            //排程日开始于endDate次日
//            DateTime scheduleDate;
//            DateTime end = Convert.ToDateTime(scheduleLastDay.ToShortDateString());
//            DateTime start = Convert.ToDateTime(endDate.AddDays(1).ToShortDateString());
//            DateTime _endDate = endDate.AddDays(1);
//            int days = end.Subtract(start).Days;

//            //排程日的第一日，日期提前一天，将其需求排到当前日期
//            var previousDateOfFirstScheduleDatePir = newSapPirModels.Where(x => x.Date == endDate && x.Flag == 0 && x.PIRDemands.Where(y => y.Flag == 0).ToList().Count != 0).FirstOrDefault();
//            if (previousDateOfFirstScheduleDatePir != null)
//            {
//                newSapPirModels.Where(x => x.Date == endDate).ToList().ForEach(y => { y.Flag = 1; y.PIRDemands.ForEach(z => z.Flag = 1); });

//                var materialNumberList = previousDateOfFirstScheduleDatePir.PIRDemands;
//                foreach (var item in materialNumberList)
//                {
//                    var queryExistInFixDays = schedulerSummaries.Where(x => x.SchedulerDate == endDate && x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList();
//                    if (queryExistInFixDays.Count != 0)
//                    {
//                        schedulerSummaries.Where(x => x.SchedulerDate == endDate && x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).ToList().ForEach(x => x.PlanedQuantity += item.Quantity);
//                    }
//                    else
//                    {
//                        var basicInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber.Replace(" ", "").ToUpper() == item.MaterialNumber.Replace(" ", "").ToUpper()).OrderBy(x => x.Order).FirstOrDefault();
//                        if (basicInfo != null)
//                        {
//                            SchedulerSummary schedulerSummary = new SchedulerSummary();
//                            schedulerSummary.SchedulerDate = firstMonday.AddDays(5);
//                            schedulerSummary.SchedulerId = item.MaterialNumber.Replace(" ", "").ToUpper() + basicInfo.Line.Replace(" ", "").ToUpper() + endDate.ToString("yyyyMMdd");
//                            schedulerSummary.Plant = basicInfo.Plant;
//                            schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                            schedulerSummary.Line = basicInfo.Line;
//                            schedulerSummary.SPQ = basicInfo.SPQ == null ? 0 : int.Parse(basicInfo.SPQ);
//                            schedulerSummary.UPH = double.Parse(basicInfo.UPH);
//                            schedulerSummary.HC = double.Parse(basicInfo.HC);
//                            schedulerSummary.Area = basicInfo.Area;
//                            schedulerSummary.Description = basicInfo.Description;
//                            schedulerSummary.WeekNumber = WeekHelper.GetWeekNumber(firstMonday);
//                            schedulerSummary.KeyUser = basicInfo.User;
//                            schedulerSummary.PlanedQuantity = item.Quantity;
//                            schedulerSummaries.Add(schedulerSummary);
//                        }
//                    }
//                }
//            }

//            for (int i = 0; i < days; i++)
//            {
//                if (newSapPirModels.Count == 0) { break; }
//                scheduleDate = _endDate.AddDays(i);
//                var scheduleDatePirs = newSapPirModels.Where(x => x.Date == scheduleDate && x.Flag == 0 && x.PIRDemands.Where(y => y.Flag == 0 && y.Quantity >= 1).ToList().Count != 0).FirstOrDefault();
//                //跳过周六，周日
//                if (scheduleDate.DayOfWeek == DayOfWeek.Saturday || scheduleDate.DayOfWeek == DayOfWeek.Sunday) { continue; }

//                //跳过工厂日历
//                var _factoryCalendars = factoryCalendars.Where(x => x.IsWork).Select(x => x.Date).ToList();
//                if (_factoryCalendars.Contains(scheduleDate)) { continue; }

//                //人力
//                var eachDayHumanQuantityInfo = eachDayHumanQuantitys.Where(x => x.Date == scheduleDate).FirstOrDefault();
//                if (eachDayHumanQuantityInfo == null) { continue; }
//                if (eachDayHumanQuantityInfo.Quantity <= 0)
//                {
//                    ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                    scheduleErrorMessage.MaterialNumber = "NA";
//                    scheduleErrorMessage.ErrorMessage = "当日sap人力不存在.";
//                    scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                    continue;
//                }
//                double scheduleDateHumanResourceFromSap = eachDayHumanQuantityInfo.Quantity;
//                double scheduleDateHumanResourceFromPir = 0;
//                double restHumanResource = scheduleDateHumanResourceFromSap - scheduleDateHumanResourceFromPir;

//                newSapPirModels.Where(x => x.Date == scheduleDate).ToList().ForEach(x => x.Flag = 1);
//                if (scheduleDatePirs != null)
//                {
//                    ScheduleSameDate(scheduleDate, scheduleDatePirs);
//                    var sameScheduleDateScheduleSummary = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).ToList();
//                    var sameScheduleDateUsedLineList = sameScheduleDateScheduleSummary.OrderBy(x => x.Line).Select(x => x.Line).Distinct().ToList();

//                    #region 剩余人力时，则开新的线别
//                    foreach (var line in sameScheduleDateUsedLineList)
//                    {
//                        var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == line).FirstOrDefault();
//                        if (queryEntity == null) { continue; }
//                        var queryModel = materialNumberLineMapRelations.Where(x => x.Line == line && x.MaterialNumber == queryEntity.MaterialNumber).FirstOrDefault();
//                        if (queryModel == null) { continue; }
//                        scheduleDateHumanResourceFromPir += double.Parse(queryModel.HC);
//                    }

//                    restHumanResource = scheduleDateHumanResourceFromSap - scheduleDateHumanResourceFromPir;
//                    if (restHumanResource >= 1) //存在剩余人力
//                    {
//                        foreach (var newSapPirModel in newSapPirModels)
//                        {
//                            bool isFullSchedule = false;
//                            if (restHumanResource <= 0) { break; }
//                            if (newSapPirModel.Flag == 1) { continue; }
//                            if (newSapPirModel.Date <= scheduleDate) { continue; }

//                            //数据挖掘不能超过一周
//                            if (newSapPirModel.Date > scheduleDate.AddDays(dateScope)) { break; }

//                            newSapPirModel.PIRDemands.ForEach(x => x.MaterialNumber = x.MaterialNumber.Replace(" ", "").ToUpper());
//                            var weekNumber = WeekHelper.GetWeekNumber(scheduleDate);
//                            var recentPirMaterialNumberList = newSapPirModel.PIRDemands.Where(x => x.Flag == 0 && x.Quantity >= 1).OrderByDescending(x => x.Quantity).ToList();

//                            //即使新开线别，也要优先考虑昨天已经开的线别
//                            DateTime? previousDate = scheduleDate.AddDays(-1);
//                            var lastDateFromScheduleSummaries = schedulerSummaries.OrderByDescending(x => x.SchedulerDate).FirstOrDefault()?.SchedulerDate;
//                            if (lastDateFromScheduleSummaries != null)
//                            {
//                                previousDate = previousDate?.DayOfWeek == DayOfWeek.Sunday ? scheduleDate.AddDays(-2).Date : scheduleDate.AddDays(-1).Date;
//                                previousDate = previousDate?.DayOfWeek == DayOfWeek.Saturday ? scheduleDate.AddDays(-3).Date : scheduleDate.AddDays(-1).Date;
//                                previousDate = lastDateFromScheduleSummaries?.Date != lastDateFromScheduleSummaries ? lastDateFromScheduleSummaries : previousDate;
//                            }

//                            var lastDateUsedLineList = schedulerSummaries.Where(x => x.SchedulerDate == previousDate).Select(x => x.Line).Distinct().ToList();
//                            var lastDateUsedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == previousDate).Select(x => x.MaterialNumber).Distinct().ToList();


//                            //获取当天已经生产了哪些料号,开了哪些线别
//                            var usedLineList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.Line).Distinct().ToList();
//                            var usedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.MaterialNumber).Distinct().ToList();

//                            if (recentPirMaterialNumberList.Count != 0)
//                            {
//                                //昨天已经生产的料号优先，其次才是线别,最后才是最大需求量
//                                var recentMaterialNumberList = recentPirMaterialNumberList.Select(x => x.MaterialNumber).ToList();
//                                var commonMaterialNumberList = recentMaterialNumberList.Intersect(lastDateUsedMaterialNumberList).ToList();
//                                if (commonMaterialNumberList.Count != 0)
//                                {
//                                    var recentPirMaterialNumberOfFilterdList = recentPirMaterialNumberList.Where(x => commonMaterialNumberList.Contains(x.MaterialNumber.ToUpper().Replace(" ", "")) && x.Flag == 0 && x.Quantity >= 1).OrderByDescending(x => x.Quantity).ToList();
//                                    if (recentPirMaterialNumberOfFilterdList.Count != 0)
//                                    {
//                                        foreach (var item in recentPirMaterialNumberOfFilterdList)
//                                        {
//                                            if (usedMaterialNumberList.Contains(item.MaterialNumber)) { continue; } //这个料号已经开启

//                                            var masterDataInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "") && x.Order == "0").FirstOrDefault();
//                                            if (masterDataInfo == null)
//                                            {
//                                                ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                                                scheduleErrorMessage.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                                scheduleErrorMessage.ErrorMessage = "不存在主线.";
//                                                scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                                                continue;
//                                            }

//                                            if (usedLineList.Contains(masterDataInfo.Line)) { continue; }   //线别已经开启

//                                            var lineMaintainInfo = lineMaintains.Where(x => x.Line == masterDataInfo.Line && x.StartTime >= DateTime.Now && x.EndTime <= DateTime.Now && x.Enable).FirstOrDefault();
//                                            bool isExistedMainline = lineMaintainInfo != null;
//                                            if (isExistedMainline) { continue; }

//                                            //剩余的人力是否足够开线
//                                            if (restHumanResource >= double.Parse(masterDataInfo.HC))
//                                            {
//                                                SchedulerSummary schedulerSummary = new SchedulerSummary();
//                                                int currentQuantity = item.Quantity;
//                                                var targetQuantity = 0;

//                                                if (masterDataInfo.Line == "L26")
//                                                {

//                                                }
//                                                //某一条线，不能无限叠加，计算剩余工时后方可叠加
//                                                double oneLineUsedTime = 0;
//                                                schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterDataInfo.Line).ToList().ForEach(x => oneLineUsedTime += (double)(x.PlanedQuantity / x.UPH));
//                                                double currentLineRestTime = standareTime - oneLineUsedTime;
//                                                if (currentLineRestTime <= 0) { continue; } //说明此条线已经没有剩余工时可用了//当前线别，存在剩余工时

//                                                var fullQuantity = (int)Math.Floor(currentLineRestTime * double.Parse(masterDataInfo.UPH));

//                                                if (currentQuantity >= fullQuantity)
//                                                {
//                                                    targetQuantity = fullQuantity;
//                                                    item.Quantity -= fullQuantity;
//                                                }
//                                                else
//                                                {
//                                                    targetQuantity = currentQuantity;
//                                                    item.Flag = 1;
//                                                    item.Quantity = 0;
//                                                }

//                                                var addPartTime = item.Quantity / int.Parse(masterDataInfo.UPH);
//                                                if (currentLineRestTime >= addPartTime)
//                                                {
//                                                    //剩的时间，足够将此需求生产完
//                                                    var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).FirstOrDefault();
//                                                    if (queryEntity == null)
//                                                    {
//                                                        schedulerSummary.SchedulerDate = scheduleDate;
//                                                        schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                                        schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                                        schedulerSummary.Plant = masterDataInfo.Plant;
//                                                        schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                                        schedulerSummary.PlanedQuantity = targetQuantity;
//                                                        schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                                        schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                                        schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                                        schedulerSummary.WeekNumber = weekNumber;
//                                                        schedulerSummary.KeyUser = masterDataInfo.User;
//                                                        schedulerSummary.CreateTime = DateTime.Now;
//                                                        schedulerSummary.UpdateTime = DateTime.Now;
//                                                        schedulerSummaries.Add(schedulerSummary);
//                                                        if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                                        {
//                                                            restHumanResource -= (double)schedulerSummary.HC;
//                                                        }
//                                                    }
//                                                    else
//                                                    {
//                                                        schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                                    }
//                                                }
//                                                else
//                                                {
//                                                    var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line != masterDataInfo.Line).FirstOrDefault();
//                                                    if (queryEntity == null)
//                                                    {
//                                                        schedulerSummary.SchedulerDate = scheduleDate;
//                                                        schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                                        schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                                        schedulerSummary.Plant = masterDataInfo.Plant;
//                                                        schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                                        schedulerSummary.PlanedQuantity = targetQuantity;
//                                                        schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                                        schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                                        schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                                        schedulerSummary.WeekNumber = weekNumber;
//                                                        schedulerSummary.KeyUser = masterDataInfo.User;
//                                                        schedulerSummary.CreateTime = DateTime.Now;
//                                                        schedulerSummary.UpdateTime = DateTime.Now;
//                                                        schedulerSummaries.Add(schedulerSummary);
//                                                        if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                                        {
//                                                            restHumanResource -= (double)schedulerSummary.HC;
//                                                        }
//                                                    }
//                                                    else
//                                                    {
//                                                        schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                                    }

//                                                    //新开的线别满额拍程
//                                                    usedLineList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.Line).Distinct().ToList();
//                                                    usedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.MaterialNumber).Distinct().ToList();
//                                                }
//                                            }
//                                        }
//                                    }
//                                }

//                                var scheduleDateLineList = materialNumberLineMapRelations.Where(x => recentMaterialNumberList.Contains(x.MaterialNumber.ToUpper().Replace(" ", ""))).Select(x => x.Line).Distinct().ToList();
//                                var commonLineList = scheduleDateLineList.Intersect(lastDateUsedLineList).ToList();
//                                List<string> _commonMaterialNumberAndLineList = [];

//                                if (commonLineList.Count != 0)
//                                {
//                                    //由线别转料号
//                                    _commonMaterialNumberAndLineList = materialNumberLineMapRelations.Where(x => commonLineList.Contains(x.Line)).Select(x => x.MaterialNumber).Distinct().ToList();
//                                    if (_commonMaterialNumberAndLineList.Count != 0)
//                                    {
//                                        var recentPirLineOfFilterdList = recentPirMaterialNumberList.Where(x => !commonMaterialNumberList.Contains(x.MaterialNumber.ToUpper().Replace(" ", "")) && _commonMaterialNumberAndLineList.Contains(x.MaterialNumber.ToUpper().Replace(" ", "")) && x.Flag == 0 && x.Quantity >= 1).OrderByDescending(x => x.Quantity).ToList();

//                                        if (recentPirLineOfFilterdList.Count != 0)
//                                        {
//                                            foreach (var item in recentPirLineOfFilterdList)
//                                            {
//                                                if (usedMaterialNumberList.Contains(item.MaterialNumber)) { continue; } //这个料号已经开启

//                                                var masterDataInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "") && x.Order == "0").FirstOrDefault();
//                                                if (masterDataInfo == null)
//                                                {
//                                                    ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                                                    scheduleErrorMessage.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                                    scheduleErrorMessage.ErrorMessage = "不存在主线.";
//                                                    scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                                                    continue;
//                                                }

//                                                if (usedLineList.Contains(masterDataInfo.Line)) { continue; }   //线别已经开启

//                                                var lineMaintainInfo = lineMaintains.Where(x => x.Line == masterDataInfo.Line && x.StartTime >= DateTime.Now && x.EndTime <= DateTime.Now && x.Enable).FirstOrDefault();
//                                                bool isExistedMainline = lineMaintainInfo != null;
//                                                if (isExistedMainline) { continue; }

//                                                //剩余的人力是否足够开线
//                                                if (restHumanResource >= double.Parse(masterDataInfo.HC))
//                                                {
//                                                    SchedulerSummary schedulerSummary = new SchedulerSummary();
//                                                    int currentQuantity = item.Quantity;
//                                                    var targetQuantity = 0;
//                                                    //某一条线，不能无限叠加，计算剩余工时后方可叠加
//                                                    double oneLineUsedTime = 0;
//                                                    schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterDataInfo.Line).ToList().ForEach(x => oneLineUsedTime += (double)(x.PlanedQuantity / x.UPH));
//                                                    double currentLineRestTime = standareTime - oneLineUsedTime;
//                                                    if (currentLineRestTime <= 0) { continue; } //说明此条线已经没有剩余工时可用了//当前线别，存在剩余工时
//                                                    var fullQuantity = (int)Math.Floor(currentLineRestTime * double.Parse(masterDataInfo.UPH));

//                                                    if (currentQuantity >= fullQuantity)
//                                                    {
//                                                        targetQuantity = fullQuantity;
//                                                        item.Quantity -= fullQuantity;
//                                                    }
//                                                    else
//                                                    {
//                                                        targetQuantity = currentQuantity;
//                                                        item.Flag = 1;
//                                                        item.Quantity = 0;
//                                                    }
//                                                    var addPartTime = item.Quantity / int.Parse(masterDataInfo.UPH);
//                                                    if (currentLineRestTime >= addPartTime)
//                                                    {
//                                                        //剩的时间，足够将此需求生产完
//                                                        var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).FirstOrDefault();
//                                                        if (queryEntity == null)
//                                                        {
//                                                            schedulerSummary.SchedulerDate = scheduleDate;
//                                                            schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                                            schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                                            schedulerSummary.Plant = masterDataInfo.Plant;
//                                                            schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                                            schedulerSummary.PlanedQuantity = targetQuantity;
//                                                            schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                                            schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                                            schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                                            schedulerSummary.WeekNumber = weekNumber;
//                                                            schedulerSummary.KeyUser = masterDataInfo.User;
//                                                            schedulerSummary.CreateTime = DateTime.Now;
//                                                            schedulerSummary.UpdateTime = DateTime.Now;
//                                                            schedulerSummaries.Add(schedulerSummary);
//                                                            if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                                            {
//                                                                restHumanResource -= (double)schedulerSummary.HC;
//                                                            }
//                                                        }
//                                                        else
//                                                        {
//                                                            schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                                        }
//                                                    }
//                                                    else
//                                                    {
//                                                        var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line != masterDataInfo.Line).FirstOrDefault();
//                                                        if (queryEntity == null)
//                                                        {
//                                                            schedulerSummary.SchedulerDate = scheduleDate;
//                                                            schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                                            schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                                            schedulerSummary.Plant = masterDataInfo.Plant;
//                                                            schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                                            schedulerSummary.PlanedQuantity = targetQuantity;
//                                                            schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                                            schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                                            schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                                            schedulerSummary.WeekNumber = weekNumber;
//                                                            schedulerSummary.KeyUser = masterDataInfo.User;
//                                                            schedulerSummary.CreateTime = DateTime.Now;
//                                                            schedulerSummary.UpdateTime = DateTime.Now;
//                                                            schedulerSummaries.Add(schedulerSummary);
//                                                            if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                                            {
//                                                                restHumanResource -= (double)schedulerSummary.HC;
//                                                            }
//                                                        }
//                                                        else
//                                                        {
//                                                            schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                                        }

//                                                        //新开的线别满额拍程
//                                                        usedLineList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.Line).Distinct().ToList();
//                                                        usedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.MaterialNumber).Distinct().ToList();
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                }

//                                //最后考虑最大需求的料号，即先考虑料号，然后考虑线别，最后考虑最大需求数
//                                var recentPirLineAndMaterialNumberOfFilterdList = recentPirMaterialNumberList.Where(x => !commonMaterialNumberList.Contains(x.MaterialNumber.ToUpper().Replace(" ", "")) && !_commonMaterialNumberAndLineList.Contains(x.MaterialNumber.ToUpper().Replace(" ", "")) && x.Flag == 0 && x.Quantity >= 1).OrderByDescending(x => x.Quantity).ToList();
//                                if (recentPirLineAndMaterialNumberOfFilterdList.Count != 0)
//                                {
//                                    //与当天交叉的部分，已经满额排程了,要不然开不了新线别
//                                    foreach (var item in recentPirLineAndMaterialNumberOfFilterdList)
//                                    {
//                                        if (usedMaterialNumberList.Contains(item.MaterialNumber)) { continue; } //这个料号已经开启

//                                        var masterDataInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "") && x.Order == "0").FirstOrDefault();
//                                        if (masterDataInfo == null)
//                                        {
//                                            ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                                            scheduleErrorMessage.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                            scheduleErrorMessage.ErrorMessage = "不存在主线.";
//                                            scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                                            continue;
//                                        }

//                                        if (usedLineList.Contains(masterDataInfo.Line)) { continue; }   //线别已经开启

//                                        var lineMaintainInfo = lineMaintains.Where(x => x.Line == masterDataInfo.Line && x.StartTime >= DateTime.Now && x.EndTime <= DateTime.Now && x.Enable).FirstOrDefault();
//                                        bool isExistedMainline = lineMaintainInfo != null;
//                                        if (isExistedMainline) { continue; }

//                                        //剩余的人力是否足够开线
//                                        if (restHumanResource >= double.Parse(masterDataInfo.HC))
//                                        {
//                                            SchedulerSummary schedulerSummary = new SchedulerSummary();
//                                            int currentQuantity = item.Quantity;
//                                            var targetQuantity = 0;
//                                            //某一条线，不能无限叠加，计算剩余工时后方可叠加
//                                            double oneLineUsedTime = 0;
//                                            schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterDataInfo.Line).ToList().ForEach(x => oneLineUsedTime += (double)(x.PlanedQuantity / x.UPH));
//                                            double currentLineRestTime = standareTime - oneLineUsedTime;
//                                            if (currentLineRestTime <= 0) { continue; } //说明此条线已经没有剩余工时可用了//当前线别，存在剩余工时
//                                            var fullQuantity = (int)Math.Floor(currentLineRestTime * double.Parse(masterDataInfo.UPH));

//                                            if (currentQuantity >= fullQuantity)
//                                            {
//                                                targetQuantity = fullQuantity;
//                                                item.Quantity -= fullQuantity;
//                                            }
//                                            else
//                                            {
//                                                targetQuantity = currentQuantity;
//                                                item.Flag = 1;
//                                                item.Quantity = 0;
//                                            }

//                                            var addPartTime = item.Quantity / int.Parse(masterDataInfo.UPH);
//                                            if (currentLineRestTime >= addPartTime)
//                                            {
//                                                //剩的时间，足够将此需求生产完
//                                                var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).FirstOrDefault();
//                                                if (queryEntity == null)
//                                                {
//                                                    schedulerSummary.SchedulerDate = scheduleDate;
//                                                    schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                                    schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                                    schedulerSummary.Plant = masterDataInfo.Plant;
//                                                    schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                                    schedulerSummary.PlanedQuantity = targetQuantity;
//                                                    schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                                    schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                                    schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                                    schedulerSummary.WeekNumber = weekNumber;
//                                                    schedulerSummary.KeyUser = masterDataInfo.User;
//                                                    schedulerSummary.CreateTime = DateTime.Now;
//                                                    schedulerSummary.UpdateTime = DateTime.Now;
//                                                    schedulerSummaries.Add(schedulerSummary);
//                                                    if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                                    {
//                                                        restHumanResource -= (double)schedulerSummary.HC;
//                                                    }
//                                                }
//                                                else
//                                                {
//                                                    schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                                }
//                                            }
//                                            else
//                                            {
//                                                var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line != masterDataInfo.Line).FirstOrDefault();
//                                                if (queryEntity == null)
//                                                {
//                                                    schedulerSummary.SchedulerDate = scheduleDate;
//                                                    schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                                    schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                                    schedulerSummary.Plant = masterDataInfo.Plant;
//                                                    schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                                    schedulerSummary.PlanedQuantity = targetQuantity;
//                                                    schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                                    schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                                    schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                                    schedulerSummary.WeekNumber = weekNumber;
//                                                    schedulerSummary.KeyUser = masterDataInfo.User;
//                                                    schedulerSummary.CreateTime = DateTime.Now;
//                                                    schedulerSummary.UpdateTime = DateTime.Now;
//                                                    schedulerSummaries.Add(schedulerSummary);
//                                                    if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                                    {
//                                                        restHumanResource -= (double)schedulerSummary.HC;
//                                                    }
//                                                }
//                                                else
//                                                {
//                                                    schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                                }

//                                                //新开的线别满额拍程
//                                                usedLineList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.Line).Distinct().ToList();
//                                                usedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.MaterialNumber).Distinct().ToList();
//                                            }
//                                        }

//                                        if (restHumanResource < 1)
//                                        {
//                                            isFullSchedule = true;
//                                            break;
//                                        }
//                                    }
//                                }
//                            }
//                            if (isFullSchedule) { break; }
//                        }
//                    }
//                    else
//                    {
//                        ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                        scheduleErrorMessage.MaterialNumber = "NA";
//                        scheduleErrorMessage.ErrorMessage = $"SAP提供的人力不足，PIR需要人数:{scheduleDateHumanResourceFromSap},SAP提供的人数:{scheduleDateHumanResourceFromPir}.";
//                        scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                        continue;
//                    }
//                    #endregion

//                    #region 已经开线的，需要保证开线是满额排程
//                    sameScheduleDateScheduleSummary = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).ToList();

//                    if (sameScheduleDateScheduleSummary.Count != 0)
//                    {
//                        var usedLineList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.Line).Distinct().ToList();
//                        foreach (var line in usedLineList)
//                        {
//                            double? oneLineUsedTime = 0;
//                            schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == line).ToList().ForEach(x => oneLineUsedTime += x.PlanedQuantity / x.UPH);

//                            double? currentLineRestTime = standareTime - oneLineUsedTime;
//                            if (currentLineRestTime <= 0) { continue; }

//                            bool isFullScheduleOneLine = false;
//                            foreach (var newSapPirModel in newSapPirModels)
//                            {
//                                if (newSapPirModel.Flag == 1) { continue; }
//                                if (newSapPirModel.Date <= scheduleDate) { continue; }

//                                //获取已经在某条线生产的MaterialNumber清单
//                                var usedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == line).Select(x => x.MaterialNumber).Distinct().ToList();
//                                if (usedMaterialNumberList.Count == 0) { continue; }

//                                //获取最近需求日的相同料号
//                                var recentPirMaterialList = newSapPirModel.PIRDemands.Where(x => x.Flag == 0 && x.Quantity >= 1).OrderByDescending(x => x.Quantity).ToList();
//                                if (recentPirMaterialList.Count == 0) { continue; }

//                                //最近的料号有无相似料号或者相同线别
//                                //判断有无重合的料号，有相同料号，优先生产，无，则安排相同线别料号==>有相同料号以及相同线别，均遵循最大需求优先
//                                List<string> recentPirMaterialList1 = recentPirMaterialList.Select(x => x.MaterialNumber).Distinct().ToList();
//                                List<string> onlyMaterialNumberIsOkList = materialNumberLineMapRelations.Where(x => x.Line == line).Select(x => x.MaterialNumber).ToList();
//                                bool isOk = onlyMaterialNumberIsOkList.Intersect(recentPirMaterialList1).ToList().Count > 0;    //这条线只能生产这些料号集合
//                                if (!isOk) { continue; }

//                                List<string> recentPirIsOkMaterialNumberList = onlyMaterialNumberIsOkList.Intersect(recentPirMaterialList1).ToList();
//                                var commoneMaterialNumberList = usedMaterialNumberList.Intersect(recentPirIsOkMaterialNumberList).ToList();
//                                List<PIRDemand> recentPirMaterialSortedList1 = [];

//                                if (commoneMaterialNumberList.Count != 0)
//                                {
//                                    recentPirMaterialSortedList1 = recentPirMaterialList.Where(x => commoneMaterialNumberList.Contains(x.MaterialNumber) && recentPirIsOkMaterialNumberList.Contains(x.MaterialNumber) && x.Flag != 1 && x.Quantity >= 1).OrderByDescending(y => y.Quantity).ToList();
//                                    if (recentPirMaterialSortedList1.Count != 0)
//                                    {
//                                        foreach (var item in recentPirMaterialSortedList1)
//                                        {
//                                            var masterdata = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "") && x.Line == line).FirstOrDefault();
//                                            if (masterdata == null) { continue; }
//                                            double addPartTime = item.Quantity / double.Parse(masterdata.UPH);
//                                            int targetQuantity = 0;
//                                            if (currentLineRestTime >= addPartTime)
//                                            {
//                                                item.Flag = 1;
//                                                targetQuantity = item.Quantity;
//                                                item.Quantity = 0;
//                                                currentLineRestTime -= addPartTime;
//                                            }
//                                            else
//                                            {
//                                                int addPartQuantity = (int)(currentLineRestTime * double.Parse(masterdata.UPH));    //顺序不能颠倒
//                                                currentLineRestTime -= addPartTime;
//                                                targetQuantity = addPartQuantity;
//                                                item.Quantity -= addPartQuantity;
//                                                isFullScheduleOneLine = true;
//                                            }

//                                            var existedScheduleSummary = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterdata.Line && x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "")).FirstOrDefault();
//                                            if (existedScheduleSummary == null)
//                                            {
//                                                SchedulerSummary schedulerSummary = new SchedulerSummary();
//                                                schedulerSummary.SchedulerDate = scheduleDate;
//                                                schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterdata.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                                schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                                schedulerSummary.Plant = masterdata.Plant;
//                                                schedulerSummary.Line = masterdata.Line.Replace(" ", "").ToUpper();
//                                                schedulerSummary.PlanedQuantity = targetQuantity;
//                                                schedulerSummary.SPQ = masterdata.SPQ == null ? 0 : int.Parse(masterdata.SPQ);
//                                                schedulerSummary.UPH = masterdata.UPH == null ? 0 : double.Parse(masterdata.UPH);
//                                                schedulerSummary.HC = masterdata.HC == null ? 0 : double.Parse(masterdata.HC);
//                                                schedulerSummary.WeekNumber = weekNumber;
//                                                schedulerSummary.Area = masterdata.Area;
//                                                schedulerSummary.KeyUser = masterdata.User;
//                                                schedulerSummary.CreateTime = DateTime.Now;
//                                                schedulerSummary.UpdateTime = DateTime.Now;
//                                                schedulerSummaries.Add(schedulerSummary);
//                                            }
//                                            else
//                                            {
//                                                schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterdata.Line && x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "")).ToList().ForEach(x => { x.PlanedQuantity += targetQuantity; });
//                                            }

//                                            if (isFullScheduleOneLine) { break; }
//                                        }
//                                    }
//                                }

//                                if (isFullScheduleOneLine) { break; }
//                                List<PIRDemand> recentPirMaterialSortedList2 = recentPirMaterialList.Where(x => !commoneMaterialNumberList.Contains(x.MaterialNumber) && recentPirIsOkMaterialNumberList.Contains(x.MaterialNumber) && x.Flag != 1 && x.Quantity >= 1).OrderByDescending(y => y.Quantity).ToList();
//                                if (recentPirMaterialSortedList2.Count != 0)
//                                {
//                                    foreach (var item in recentPirMaterialSortedList2)
//                                    {
//                                        var masterdata = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "") && x.Line == line).FirstOrDefault();
//                                        if (masterdata == null) { continue; }
//                                        double addPartTime = item.Quantity / double.Parse(masterdata.UPH);
//                                        int targetQuantity = 0;
//                                        if (currentLineRestTime >= addPartTime)
//                                        {
//                                            item.Flag = 1;
//                                            targetQuantity = item.Quantity;
//                                            item.Quantity = 0;
//                                            currentLineRestTime -= addPartTime;
//                                        }
//                                        else
//                                        {
//                                            int addPartQuantity = (int)(currentLineRestTime * double.Parse(masterdata.UPH));  //顺序不能颠倒
//                                            currentLineRestTime -= addPartTime;
//                                            targetQuantity = addPartQuantity;
//                                            item.Quantity -= addPartQuantity;
//                                            isFullScheduleOneLine = true;
//                                        }

//                                        var existedScheduleSummary = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterdata.Line && x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "")).FirstOrDefault();
//                                        if (existedScheduleSummary == null)
//                                        {
//                                            SchedulerSummary schedulerSummary = new SchedulerSummary();
//                                            schedulerSummary.SchedulerDate = scheduleDate;
//                                            schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterdata.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                            schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                            schedulerSummary.Plant = masterdata.Plant;
//                                            schedulerSummary.Line = masterdata.Line.Replace(" ", "").ToUpper();
//                                            schedulerSummary.PlanedQuantity = targetQuantity;
//                                            schedulerSummary.SPQ = masterdata.SPQ == null ? 0 : int.Parse(masterdata.SPQ);
//                                            schedulerSummary.UPH = masterdata.UPH == null ? 0 : double.Parse(masterdata.UPH);
//                                            schedulerSummary.HC = masterdata.HC == null ? 0 : double.Parse(masterdata.HC);
//                                            schedulerSummary.WeekNumber = weekNumber;
//                                            schedulerSummary.Area = masterdata.Area;
//                                            schedulerSummary.KeyUser = masterdata.User;
//                                            schedulerSummary.CreateTime = DateTime.Now;
//                                            schedulerSummary.UpdateTime = DateTime.Now;
//                                            schedulerSummaries.Add(schedulerSummary);
//                                        }
//                                        else
//                                        {
//                                            schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterdata.Line && x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "")).ToList().ForEach(x => { x.PlanedQuantity += targetQuantity; });
//                                        }
//                                        if (isFullScheduleOneLine) { break; }
//                                    }
//                                }
//                                if (isFullScheduleOneLine) { break; }
//                                //测试使用
//                                var testVals = sameScheduleDateScheduleSummary = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).ToList();

//                            }
//                        }
//                    }
//                    #endregion
//                }
//                else
//                {
//                    ScheduleNotSameDate(scheduleDate, restHumanResource);

//                    #region 已经开线的，需要保证开线是满额排程
//                    var sameScheduleDateScheduleSummary = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).ToList();

//                    if (sameScheduleDateScheduleSummary.Count != 0)
//                    {
//                        var usedLineList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.Line).Distinct().ToList();
//                        foreach (var line in usedLineList)
//                        {
//                            if (line == "L26")
//                            {

//                            }
//                            double? oneLineUsedTime = 0;
//                            schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == line).ToList().ForEach(x => oneLineUsedTime += x.PlanedQuantity / x.UPH);

//                            double? currentLineRestTime = standareTime - oneLineUsedTime;
//                            if (currentLineRestTime <= 0) { continue; }

//                            bool isFullScheduleOneLine = false;
//                            foreach (var newSapPirModel in newSapPirModels)
//                            {
//                                if (newSapPirModel.Flag == 1) { continue; }
//                                if (newSapPirModel.Date <= scheduleDate) { continue; }

//                                //获取已经在某条线生产的MaterialNumber清单
//                                var usedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == line).Select(x => x.MaterialNumber).Distinct().ToList();
//                                if (usedMaterialNumberList.Count == 0) { continue; }

//                                //获取最近需求日的相同料号
//                                var recentPirMaterialList = newSapPirModel.PIRDemands.Where(x => x.Flag == 0 && x.Quantity >= 1).OrderByDescending(x => x.Quantity).ToList();
//                                if (recentPirMaterialList.Count == 0) { continue; }

//                                //最近的料号有无相似料号或者相同线别
//                                //判断有无重合的料号，有相同料号，优先生产，无，则安排相同线别料号==>有相同料号以及相同线别，均遵循最大需求优先
//                                List<string> recentPirMaterialList1 = recentPirMaterialList.Select(x => x.MaterialNumber).Distinct().ToList();
//                                List<string> onlyMaterialNumberIsOkList = materialNumberLineMapRelations.Where(x => x.Line == line).Select(x => x.MaterialNumber).ToList();
//                                bool isOk = onlyMaterialNumberIsOkList.Intersect(recentPirMaterialList1).ToList().Count > 0;    //这条线只能生产这些料号集合
//                                if (!isOk) { continue; }

//                                List<string> recentPirIsOkMaterialNumberList = onlyMaterialNumberIsOkList.Intersect(recentPirMaterialList1).ToList();
//                                var commoneMaterialNumberList = usedMaterialNumberList.Intersect(recentPirIsOkMaterialNumberList).ToList();
//                                List<PIRDemand> recentPirMaterialSortedList1 = [];

//                                if (commoneMaterialNumberList.Count != 0)
//                                {
//                                    recentPirMaterialSortedList1 = recentPirMaterialList.Where(x => commoneMaterialNumberList.Contains(x.MaterialNumber) && recentPirIsOkMaterialNumberList.Contains(x.MaterialNumber) && x.Flag != 1 && x.Quantity >= 1).OrderByDescending(y => y.Quantity).ToList();
//                                    if (recentPirMaterialSortedList1.Count != 0)
//                                    {
//                                        foreach (var item in recentPirMaterialSortedList1)
//                                        {
//                                            var masterdata = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "") && x.Line == line).FirstOrDefault();
//                                            if (masterdata == null) { continue; }
//                                            double addPartTime = item.Quantity / double.Parse(masterdata.UPH);
//                                            int targetQuantity = 0;
//                                            if (currentLineRestTime >= addPartTime)
//                                            {
//                                                item.Flag = 1;
//                                                targetQuantity = item.Quantity;
//                                                item.Quantity = 0;
//                                                currentLineRestTime -= addPartTime;
//                                            }
//                                            else
//                                            {
//                                                int addPartQuantity = (int)(currentLineRestTime * double.Parse(masterdata.UPH));    //顺序不能颠倒
//                                                currentLineRestTime -= addPartTime;
//                                                targetQuantity = addPartQuantity;
//                                                item.Quantity -= addPartQuantity;
//                                                isFullScheduleOneLine = true;
//                                            }

//                                            var existedScheduleSummary = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterdata.Line && x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "")).FirstOrDefault();
//                                            if (existedScheduleSummary == null)
//                                            {
//                                                SchedulerSummary schedulerSummary = new SchedulerSummary();
//                                                schedulerSummary.SchedulerDate = scheduleDate;
//                                                schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterdata.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                                schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                                schedulerSummary.Plant = masterdata.Plant;
//                                                schedulerSummary.Line = masterdata.Line.Replace(" ", "").ToUpper();
//                                                schedulerSummary.PlanedQuantity = targetQuantity;
//                                                schedulerSummary.SPQ = masterdata.SPQ == null ? 0 : int.Parse(masterdata.SPQ);
//                                                schedulerSummary.UPH = masterdata.UPH == null ? 0 : double.Parse(masterdata.UPH);
//                                                schedulerSummary.HC = masterdata.HC == null ? 0 : double.Parse(masterdata.HC);
//                                                schedulerSummary.WeekNumber = weekNumber;
//                                                schedulerSummary.Area = masterdata.Area;
//                                                schedulerSummary.KeyUser = masterdata.User;
//                                                schedulerSummary.CreateTime = DateTime.Now;
//                                                schedulerSummary.UpdateTime = DateTime.Now;
//                                                schedulerSummaries.Add(schedulerSummary);
//                                            }
//                                            else
//                                            {
//                                                schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterdata.Line && x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "")).ToList().ForEach(x => { x.PlanedQuantity += targetQuantity; });
//                                            }

//                                            if (isFullScheduleOneLine) { break; }
//                                        }
//                                    }
//                                }

//                                if (isFullScheduleOneLine) { break; }
//                                List<PIRDemand> recentPirMaterialSortedList2 = recentPirMaterialList.Where(x => !commoneMaterialNumberList.Contains(x.MaterialNumber) && recentPirIsOkMaterialNumberList.Contains(x.MaterialNumber) && x.Flag != 1 && x.Quantity >= 1).OrderByDescending(y => y.Quantity).ToList();
//                                if (recentPirMaterialSortedList2.Count != 0)
//                                {
//                                    foreach (var item in recentPirMaterialSortedList2)
//                                    {
//                                        var masterdata = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "") && x.Line == line).FirstOrDefault();
//                                        if (masterdata == null) { continue; }
//                                        double addPartTime = item.Quantity / double.Parse(masterdata.UPH);
//                                        int targetQuantity = 0;
//                                        if (currentLineRestTime >= addPartTime)
//                                        {
//                                            item.Flag = 1;
//                                            targetQuantity = item.Quantity;
//                                            item.Quantity = 0;
//                                            currentLineRestTime -= addPartTime;
//                                        }
//                                        else
//                                        {
//                                            int addPartQuantity = (int)(currentLineRestTime * double.Parse(masterdata.UPH));  //顺序不能颠倒
//                                            currentLineRestTime -= addPartTime;
//                                            targetQuantity = addPartQuantity;
//                                            item.Quantity -= addPartQuantity;
//                                            isFullScheduleOneLine = true;
//                                        }

//                                        var existedScheduleSummary = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterdata.Line && x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "")).FirstOrDefault();
//                                        if (existedScheduleSummary == null)
//                                        {
//                                            SchedulerSummary schedulerSummary = new SchedulerSummary();
//                                            schedulerSummary.SchedulerDate = scheduleDate;
//                                            schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterdata.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                            schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                            schedulerSummary.Plant = masterdata.Plant;
//                                            schedulerSummary.Line = masterdata.Line.Replace(" ", "").ToUpper();
//                                            schedulerSummary.PlanedQuantity = targetQuantity;
//                                            schedulerSummary.SPQ = masterdata.SPQ == null ? 0 : int.Parse(masterdata.SPQ);
//                                            schedulerSummary.UPH = masterdata.UPH == null ? 0 : double.Parse(masterdata.UPH);
//                                            schedulerSummary.HC = masterdata.HC == null ? 0 : double.Parse(masterdata.HC);
//                                            schedulerSummary.WeekNumber = weekNumber;
//                                            schedulerSummary.Area = masterdata.Area;
//                                            schedulerSummary.KeyUser = masterdata.User;
//                                            schedulerSummary.CreateTime = DateTime.Now;
//                                            schedulerSummary.UpdateTime = DateTime.Now;
//                                            schedulerSummaries.Add(schedulerSummary);
//                                        }
//                                        else
//                                        {
//                                            schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterdata.Line && x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "")).ToList().ForEach(x => { x.PlanedQuantity += targetQuantity; });
//                                        }
//                                        if (isFullScheduleOneLine) { break; }
//                                    }
//                                }
//                                if (isFullScheduleOneLine) { break; }
//                                //测试使用
//                                var testVals = sameScheduleDateScheduleSummary = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).ToList();

//                            }
//                        }
//                    }
//                    #endregion
//                }
//            }

//            #region 插入数据库
//            if (schedulerSummaries.Count != 0)
//            {
//                //删除上一次排程记录
//                //List<SchedulerSummary> lastScheduleResult = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= endDate.AddDays(1)).ToList();

//                //if (lastScheduleResult.Count != 0)
//                //{
//                //    _apsDbContext.SchedulerSummary.RemoveRange(lastScheduleResult);
//                //}
//                //_apsDbContext.SchedulerSummary.UpdateRange(schedulerSummaries);
//                //_apsDbContext.SaveChanges();
//            }
//            #endregion
//            return schedulerSummaries;
//        }
//        public static void ScheduleSameDate(DateTime scheduleDate, NewSapPirModel newSapPirModel)
//        {
//            var materialNumberlst = newSapPirModel.PIRDemands.Where(x => x.Flag == 0 && x.Quantity >= 1).ToList();
//            var weekNumber = WeekHelper.GetWeekNumber(scheduleDate);
//            if (materialNumberlst.Count > 0)
//            {
//                foreach (var item in materialNumberlst)
//                {
//                    int currentQuantity = item.Quantity;
//                    //是否存在主线
//                    var oneMaterialNumberLineMapRelationlst = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber).OrderBy(x => x.Order).ToList();
//                    if (oneMaterialNumberLineMapRelationlst.Count <= 0)
//                    {
//                        ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                        scheduleErrorMessage.MaterialNumber = item.MaterialNumber;
//                        scheduleErrorMessage.ErrorMessage = "不存在可用线别.";
//                        scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                        continue;
//                    }

//                    var isExistedMainline = oneMaterialNumberLineMapRelationlst.Where(x => x.Order == "0").FirstOrDefault() == null;
//                    if (isExistedMainline)
//                    {
//                        ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                        scheduleErrorMessage.MaterialNumber = item.MaterialNumber;
//                        scheduleErrorMessage.ErrorMessage = "不存在主线.";
//                        scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                        continue;
//                    }

//                    var masterDataInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber).FirstOrDefault();
//                    if (masterDataInfo == null)
//                    {
//                        ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                        scheduleErrorMessage.MaterialNumber = item.MaterialNumber;
//                        scheduleErrorMessage.ErrorMessage = "未维护MasterData.";
//                        scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                        continue;
//                    }

//                    //当前线别是否在维护中
//                    for (var i = 0; i < oneMaterialNumberLineMapRelationlst.Count; i++)
//                    {
//                        var lineMaintainInfo = lineMaintains.Where(x => x.Line == oneMaterialNumberLineMapRelationlst[i].Line && x.Enable).FirstOrDefault();
//                        bool isMaintain = false;    //默认该线别没有维护
//                        bool isLastLine = i == oneMaterialNumberLineMapRelationlst.Count - 1; //是否是最后一条线
//                        isMaintain = lineMaintainInfo != null ? lineMaintainInfo.StartTime >= DateTime.Now && lineMaintainInfo.EndTime <= DateTime.Now : false;

//                        SchedulerSummary schedulerSummary = new SchedulerSummary();
//                        if (isLastLine)
//                        {
//                            var maintainlines = lineMaintains.Where(x => x.StartTime >= DateTime.Now && x.EndTime <= DateTime.Now && x.Enable).Select(x => x.Line.Replace(" ", "").ToUpper()).ToList();//处于维护中的所有线别
//                            var canUseLine = oneMaterialNumberLineMapRelationlst.Where(x => !maintainlines.Contains(x.Line.Replace(" ", "").ToUpper())).OrderByDescending(x => x.Order).FirstOrDefault();
//                            if (canUseLine == null)
//                            {
//                                ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                                scheduleErrorMessage.MaterialNumber = item.MaterialNumber;
//                                scheduleErrorMessage.ErrorMessage = "不存在可用线别.";
//                                scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                                continue;
//                            }
//                            else
//                            {
//                                var queryModel = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == canUseLine.Line && x.MaterialNumber == item.MaterialNumber).FirstOrDefault();
//                                if (queryModel == null)
//                                {
//                                    schedulerSummary.SchedulerDate = scheduleDate;
//                                    schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{canUseLine.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                    schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                    schedulerSummary.Plant = canUseLine.Plant;
//                                    schedulerSummary.Line = canUseLine.Line.Replace(" ", "").ToUpper();
//                                    schedulerSummary.PlanedQuantity = currentQuantity;
//                                    schedulerSummary.SPQ = canUseLine.SPQ == null ? 0 : int.Parse(canUseLine.SPQ);
//                                    schedulerSummary.UPH = canUseLine.UPH == null ? 0 : double.Parse(canUseLine.UPH);
//                                    schedulerSummary.HC = canUseLine.HC == null ? 0 : double.Parse(canUseLine.HC);
//                                    schedulerSummary.WeekNumber = weekNumber;
//                                    schedulerSummary.Area = canUseLine.Area;
//                                    schedulerSummary.KeyUser = canUseLine.User;
//                                    schedulerSummary.CreateTime = DateTime.Now;
//                                    schedulerSummary.UpdateTime = DateTime.Now;
//                                    schedulerSummaries.Add(schedulerSummary);
//                                }
//                                else
//                                {
//                                    schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == canUseLine.Line && x.MaterialNumber == item.MaterialNumber).ToList().ForEach(x => x.PlanedQuantity += currentQuantity);
//                                }
//                            }
//                            break;
//                        }

//                        var existedScheduleSummaries = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == oneMaterialNumberLineMapRelationlst[i].Line).ToList();
//                        if (existedScheduleSummaries.Count != 0)
//                        {
//                            double usedTime = (Double)existedScheduleSummaries.Sum(x => x.PlanedQuantity / x.UPH);
//                            double restTime = standareTime - usedTime;

//                            if (restTime <= 0) { continue; } //说明此条线已经没有剩余工时可用了

//                            //当前线别，存在剩余工时
//                            var addPartTime = item.Quantity / int.Parse(masterDataInfo.UPH);
//                            if (restTime >= addPartTime)
//                            {
//                                //剩的时间，足够将此需求生产完
//                                var queryModel = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == oneMaterialNumberLineMapRelationlst[i].Line).FirstOrDefault();
//                                if (queryModel == null)
//                                {
//                                    schedulerSummary.SchedulerDate = scheduleDate;
//                                    schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                    schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                    schedulerSummary.Plant = masterDataInfo.Plant;
//                                    schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                    schedulerSummary.PlanedQuantity = currentQuantity;
//                                    schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                    schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                    schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                    schedulerSummary.WeekNumber = weekNumber;
//                                    schedulerSummary.KeyUser = masterDataInfo.User;
//                                    schedulerSummary.CreateTime = DateTime.Now;
//                                    schedulerSummary.UpdateTime = DateTime.Now;
//                                    schedulerSummaries.Add(schedulerSummary);
//                                }
//                                else
//                                {
//                                    schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == oneMaterialNumberLineMapRelationlst[i].Line).ToList().ForEach(x => x.PlanedQuantity += currentQuantity);
//                                }
//                                break;
//                            }
//                            else
//                            {
//                                var addPartQuantity = (int)Math.Floor(restTime * double.Parse(masterDataInfo.UPH));
//                                currentQuantity -= addPartQuantity;

//                                //剩余的人力是否足够开线
//                                var queryModel = schedulerSummaries.Where(x => x.MaterialNumber == item.MaterialNumber && x.Line != oneMaterialNumberLineMapRelationlst[i].Line).FirstOrDefault();
//                                if (queryModel == null)
//                                {
//                                    schedulerSummary.SchedulerDate = scheduleDate;
//                                    schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                    schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                    schedulerSummary.Plant = masterDataInfo.Plant;
//                                    schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                    schedulerSummary.PlanedQuantity = addPartQuantity;
//                                    schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                    schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                    schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                    schedulerSummary.WeekNumber = weekNumber;
//                                    schedulerSummary.KeyUser = masterDataInfo.User;
//                                    schedulerSummary.CreateTime = DateTime.Now;
//                                    schedulerSummary.UpdateTime = DateTime.Now;
//                                    schedulerSummaries.Add(schedulerSummary);
//                                }
//                                else
//                                {
//                                    schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == oneMaterialNumberLineMapRelationlst[i].Line).ToList().ForEach(x => x.PlanedQuantity += addPartQuantity);
//                                }
//                            }

//                            if (currentQuantity <= 0)
//                            {
//                                break;
//                            }
//                        }
//                        else
//                        {
//                            //说明没有开线
//                            var targetQuantity = 0;
//                            var fullQuantity = (int)Math.Floor(standareTime * double.Parse(masterDataInfo.UPH));

//                            if (currentQuantity >= fullQuantity)
//                            {
//                                targetQuantity = fullQuantity;
//                                currentQuantity -= fullQuantity;
//                            }
//                            else
//                            {
//                                targetQuantity = currentQuantity;
//                                currentQuantity = 0;
//                            }
//                            schedulerSummary.SchedulerDate = scheduleDate;
//                            schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                            schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                            schedulerSummary.Plant = masterDataInfo.Plant;
//                            schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                            schedulerSummary.PlanedQuantity = targetQuantity;
//                            schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                            schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                            schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                            schedulerSummary.WeekNumber = weekNumber;
//                            schedulerSummary.KeyUser = masterDataInfo.User;
//                            schedulerSummary.Area = masterDataInfo.Area;
//                            schedulerSummary.CreateTime = DateTime.Now;
//                            schedulerSummary.UpdateTime = DateTime.Now;
//                            schedulerSummaries.Add(schedulerSummary);

//                            if (currentQuantity <= 0) { break; }
//                        }
//                    }
//                }
//            }
//        }
//        public static void ScheduleNotSameDate(DateTime scheduleDate, double restHumanResource)
//        {
//            //排程日与需求日不重合
//            DateTime? previousDate = scheduleDate.AddDays(-1);
//            var lastDateFromScheduleSummaries = schedulerSummaries.OrderByDescending(x => x.SchedulerDate).FirstOrDefault()?.SchedulerDate;
//            if (lastDateFromScheduleSummaries != null)
//            {
//                previousDate = previousDate?.DayOfWeek == DayOfWeek.Sunday ? scheduleDate.AddDays(-2).Date : scheduleDate.AddDays(-1).Date;
//                previousDate = previousDate?.DayOfWeek == DayOfWeek.Saturday ? scheduleDate.AddDays(-3).Date : scheduleDate.AddDays(-1).Date;
//                previousDate = lastDateFromScheduleSummaries?.Date != lastDateFromScheduleSummaries ? lastDateFromScheduleSummaries : previousDate;
//            }

//            var lastDateScheduleSummaries = schedulerSummaries.Where(x => x.SchedulerDate == previousDate).ToList();
//            if (lastDateScheduleSummaries.Count == 0)
//            {
//                lastDateScheduleSummaries = schedulerSummaries.Where(x => x.SchedulerDate == lastDateFromScheduleSummaries).ToList();
//            }
//            if (lastDateScheduleSummaries.Count != 0)
//            {
//                var lastDateScheduleSummariesUsedLineList = lastDateScheduleSummaries.Select(x => x.Line).Distinct().ToList();
//                var lastDateScheduleSummariesUsedMaterialNumberList = lastDateScheduleSummaries.Select(x => x.MaterialNumber).Distinct().ToList();
//                foreach (var newSapPirModel in newSapPirModels)
//                {
//                    //当排程日与需求日不重合时，则需要考虑人力的使用情况
//                    bool isFullSchedule = false;
//                    if (restHumanResource <= 0) { break; }
//                    if (newSapPirModel.Flag == 1) { continue; }
//                    if (newSapPirModel.Date <= scheduleDate) { continue; }
//                    var weekNumber = WeekHelper.GetWeekNumber(scheduleDate);
//                    var recentPirMaterialNumberList = newSapPirModel.PIRDemands.Where(x => x.Flag == 0 & x.Quantity >= 1).OrderByDescending(x => x.Quantity).ToList();
//                    if (recentPirMaterialNumberList.Count == 0) { continue; }
//                    var recentPNList = recentPirMaterialNumberList.Select(x => x.MaterialNumber).Distinct().ToList();

//                    //前一天生产了哪些料号，开了哪些线别
//                    var lastDateUsedLineList = schedulerSummaries.Where(x => x.SchedulerDate == previousDate).Select(x => x.Line).Distinct().ToList();
//                    var lastDateUsedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == previousDate).Select(x => x.MaterialNumber).Distinct().ToList();

//                    //当天已经生产了哪些料号,开了哪些线别
//                    var usedLineList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.Line).Distinct().ToList();
//                    var usedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.MaterialNumber).Distinct().ToList();

//                    //优先级别：昨天已经生产的料号，已经开的线别，最大需求数量
//                    List<string?> commonMaterialNumberList = [];
//                    if (usedMaterialNumberList.Count != 0)
//                    {
//                        commonMaterialNumberList = lastDateUsedMaterialNumberList.Intersect(usedMaterialNumberList).ToList();
//                    }

//                    List<string?> commonLineList = [];
//                    //获取已开线别能够生产哪些料号
//                    List<string?> materialNumberOnLineList = [];
//                    if (usedLineList.Count != 0)
//                    {
//                        commonLineList = lastDateUsedLineList.Intersect(usedLineList).ToList();
//                        if (commonLineList.Count != 0)
//                        {
//                            materialNumberOnLineList = materialNumberLineMapRelations.Where(x => commonLineList.Contains(x.Line)).Select(x => x.MaterialNumber).Distinct().ToList();
//                        }
//                    }

//                    List<string?> restPirMaterialNumberList = [];
//                    restPirMaterialNumberList = recentPirMaterialNumberList.Where(x => !commonMaterialNumberList.Contains(x.MaterialNumber.ToUpper().Replace(" ", "")) && !materialNumberOnLineList.Contains(x.MaterialNumber.ToUpper().Replace(" ", ""))).OrderByDescending(x => x.Quantity).Select(x => x.MaterialNumber.ToUpper().Replace(" ", "")).ToList();

//                    //开始排程
//                    if (commonMaterialNumberList.Count != 0)
//                    {
//                        var filteredPirList = recentPirMaterialNumberList.Where(x => commonMaterialNumberList.Contains(x.MaterialNumber.ToUpper().Replace(" ", ""))).OrderByDescending(x => x.Quantity).ToList();
//                        foreach (var item in filteredPirList)
//                        {
//                            //if (usedMaterialNumberList.Contains(item.MaterialNumber)) { continue; } //这个料号已经开启

//                            var masterDataInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "") && x.Order == "0").FirstOrDefault();
//                            if (masterDataInfo == null)
//                            {
//                                ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                                scheduleErrorMessage.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                scheduleErrorMessage.ErrorMessage = "不存在主线.";
//                                scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                                continue;
//                            }

//                            //if (usedLineList.Contains(masterDataInfo.Line)) { continue; }   //线别已经开启

//                            var lineMaintainInfo = lineMaintains.Where(x => x.Line == masterDataInfo.Line && x.StartTime >= DateTime.Now && x.EndTime <= DateTime.Now && x.Enable).FirstOrDefault();
//                            bool isExistedMainline = lineMaintainInfo != null;
//                            if (isExistedMainline) { continue; }

//                            //剩余的人力是否足够开线
//                            if (restHumanResource >= double.Parse(masterDataInfo.HC))
//                            {
//                                SchedulerSummary schedulerSummary = new SchedulerSummary();
//                                int currentQuantity = item.Quantity;
//                                var targetQuantity = 0;
//                                //某一条线，不能无限叠加，计算剩余工时后方可叠加
//                                double oneLineUsedTime = 0;
//                                schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterDataInfo.Line).ToList().ForEach(x => oneLineUsedTime += (double)(x.PlanedQuantity / x.UPH));
//                                double currentLineRestTime = standareTime - oneLineUsedTime;
//                                if (currentLineRestTime <= 0) { continue; } //说明此条线已经没有剩余工时可用了//当前线别，存在剩余工时
//                                var fullQuantity = (int)Math.Floor(currentLineRestTime * double.Parse(masterDataInfo.UPH));

//                                if (currentQuantity >= fullQuantity)
//                                {
//                                    targetQuantity = fullQuantity;
//                                    item.Quantity -= fullQuantity;
//                                }
//                                else
//                                {
//                                    targetQuantity = currentQuantity;
//                                    item.Flag = 1;
//                                    item.Quantity = 0;
//                                }

//                                var addPartTime = item.Quantity / int.Parse(masterDataInfo.UPH);
//                                if (currentLineRestTime >= addPartTime)
//                                {
//                                    //剩的时间，足够将此需求生产完
//                                    var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).FirstOrDefault();
//                                    if (queryEntity == null)
//                                    {
//                                        schedulerSummary.SchedulerDate = scheduleDate;
//                                        schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                        schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                        schedulerSummary.Plant = masterDataInfo.Plant;
//                                        schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                        schedulerSummary.PlanedQuantity = targetQuantity;
//                                        schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                        schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                        schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                        schedulerSummary.WeekNumber = weekNumber;
//                                        schedulerSummary.KeyUser = masterDataInfo.User;
//                                        schedulerSummary.CreateTime = DateTime.Now;
//                                        schedulerSummary.UpdateTime = DateTime.Now;
//                                        schedulerSummaries.Add(schedulerSummary);
//                                        if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                        {
//                                            restHumanResource -= (double)schedulerSummary.HC;
//                                        }
//                                    }
//                                    else
//                                    {
//                                        schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                    }
//                                }
//                                else
//                                {
//                                    var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line != masterDataInfo.Line).FirstOrDefault();
//                                    if (queryEntity == null)
//                                    {
//                                        schedulerSummary.SchedulerDate = scheduleDate;
//                                        schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                        schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                        schedulerSummary.Plant = masterDataInfo.Plant;
//                                        schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                        schedulerSummary.PlanedQuantity = targetQuantity;
//                                        schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                        schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                        schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                        schedulerSummary.WeekNumber = weekNumber;
//                                        schedulerSummary.KeyUser = masterDataInfo.User;
//                                        schedulerSummary.CreateTime = DateTime.Now;
//                                        schedulerSummary.UpdateTime = DateTime.Now;
//                                        schedulerSummaries.Add(schedulerSummary);
//                                        if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                        {
//                                            restHumanResource -= (double)schedulerSummary.HC;
//                                        }
//                                    }
//                                    else
//                                    {
//                                        schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                    }

//                                    //新开的线别满额拍程
//                                    usedLineList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.Line).Distinct().ToList();
//                                    usedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.MaterialNumber).Distinct().ToList();
//                                }
//                            }

//                            if (restHumanResource < 1)
//                            {
//                                isFullSchedule = true;
//                                break;
//                            }
//                        }
//                    }

//                    if (commonLineList.Count != 0 && materialNumberOnLineList.Count != 0)
//                    {
//                        var filteredPirList = recentPirMaterialNumberList.Where(x => !commonMaterialNumberList.Contains(x.MaterialNumber.ToUpper().Replace(" ", "")) && materialNumberOnLineList.Contains(x.MaterialNumber.ToUpper().Replace(" ", ""))).OrderByDescending(x => x.Quantity).ToList();
//                        foreach (var item in filteredPirList)
//                        {
//                            //if (usedMaterialNumberList.Contains(item.MaterialNumber)) { continue; } //这个料号已经开启

//                            var masterDataInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "") && x.Order == "0").FirstOrDefault();
//                            if (masterDataInfo == null)
//                            {
//                                ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                                scheduleErrorMessage.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                scheduleErrorMessage.ErrorMessage = "不存在主线.";
//                                scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                                continue;
//                            }

//                            //if (usedLineList.Contains(masterDataInfo.Line)) { continue; }   //线别已经开启

//                            var lineMaintainInfo = lineMaintains.Where(x => x.Line == masterDataInfo.Line && x.StartTime >= DateTime.Now && x.EndTime <= DateTime.Now && x.Enable).FirstOrDefault();
//                            bool isExistedMainline = lineMaintainInfo != null;
//                            if (isExistedMainline) { continue; }

//                            //剩余的人力是否足够开线
//                            if (restHumanResource >= double.Parse(masterDataInfo.HC))
//                            {
//                                SchedulerSummary schedulerSummary = new SchedulerSummary();
//                                int currentQuantity = item.Quantity;
//                                var targetQuantity = 0;
//                                //某一条线，不能无限叠加，计算剩余工时后方可叠加
//                                double oneLineUsedTime = 0;
//                                schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterDataInfo.Line).ToList().ForEach(x => oneLineUsedTime += (double)(x.PlanedQuantity / x.UPH));
//                                double currentLineRestTime = standareTime - oneLineUsedTime;
//                                if (currentLineRestTime <= 0) { continue; } //说明此条线已经没有剩余工时可用了//当前线别，存在剩余工时
//                                var fullQuantity = (int)Math.Floor(currentLineRestTime * double.Parse(masterDataInfo.UPH));

//                                if (currentQuantity >= fullQuantity)
//                                {
//                                    targetQuantity = fullQuantity;
//                                    item.Quantity -= fullQuantity;
//                                }
//                                else
//                                {
//                                    targetQuantity = currentQuantity;
//                                    item.Flag = 1;
//                                    item.Quantity = 0;
//                                }

//                                var addPartTime = item.Quantity / int.Parse(masterDataInfo.UPH);
//                                if (currentLineRestTime >= addPartTime)
//                                {
//                                    //剩的时间，足够将此需求生产完
//                                    var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).FirstOrDefault();
//                                    if (queryEntity == null)
//                                    {
//                                        schedulerSummary.SchedulerDate = scheduleDate;
//                                        schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                        schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                        schedulerSummary.Plant = masterDataInfo.Plant;
//                                        schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                        schedulerSummary.PlanedQuantity = targetQuantity;
//                                        schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                        schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                        schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                        schedulerSummary.WeekNumber = weekNumber;
//                                        schedulerSummary.KeyUser = masterDataInfo.User;
//                                        schedulerSummary.CreateTime = DateTime.Now;
//                                        schedulerSummary.UpdateTime = DateTime.Now;
//                                        schedulerSummaries.Add(schedulerSummary);
//                                        if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                        {
//                                            restHumanResource -= (double)schedulerSummary.HC;
//                                        }
//                                    }
//                                    else
//                                    {
//                                        schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                    }
//                                }
//                                else
//                                {
//                                    var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line != masterDataInfo.Line).FirstOrDefault();
//                                    if (queryEntity == null)
//                                    {
//                                        schedulerSummary.SchedulerDate = scheduleDate;
//                                        schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                        schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                        schedulerSummary.Plant = masterDataInfo.Plant;
//                                        schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                        schedulerSummary.PlanedQuantity = targetQuantity;
//                                        schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                        schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                        schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                        schedulerSummary.WeekNumber = weekNumber;
//                                        schedulerSummary.KeyUser = masterDataInfo.User;
//                                        schedulerSummary.CreateTime = DateTime.Now;
//                                        schedulerSummary.UpdateTime = DateTime.Now;
//                                        schedulerSummaries.Add(schedulerSummary);
//                                        if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                        {
//                                            restHumanResource -= (double)schedulerSummary.HC;
//                                        }
//                                    }
//                                    else
//                                    {
//                                        schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                    }

//                                    //新开的线别满额拍程
//                                    usedLineList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.Line).Distinct().ToList();
//                                    usedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.MaterialNumber).Distinct().ToList();
//                                }
//                            }

//                            if (restHumanResource < 1)
//                            {
//                                isFullSchedule = true;
//                                break;
//                            }
//                        }
//                    }

//                    if (restPirMaterialNumberList.Count != 0)
//                    {
//                        var filteredPirList = recentPirMaterialNumberList.Where(x => restPirMaterialNumberList.Contains(x.MaterialNumber.ToUpper().Replace(" ", ""))).OrderByDescending(x => x.Quantity).ToList();
//                        //与当天交叉的部分，已经满额排程了,要不然开不了新线别
//                        foreach (var item in filteredPirList)
//                        {
//                            //if (usedMaterialNumberList.Contains(item.MaterialNumber)) { continue; } //这个料号已经开启

//                            var masterDataInfo = materialNumberLineMapRelations.Where(x => x.MaterialNumber == item.MaterialNumber.ToUpper().Replace(" ", "") && x.Order == "0").FirstOrDefault();
//                            if (masterDataInfo == null)
//                            {
//                                ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                                scheduleErrorMessage.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                scheduleErrorMessage.ErrorMessage = "不存在主线.";
//                                scheduleErrorMessagelst.Add(scheduleErrorMessage);
//                                continue;
//                            }

//                            //if (usedLineList.Contains(masterDataInfo.Line)) { continue; }   //线别已经开启

//                            var lineMaintainInfo = lineMaintains.Where(x => x.Line == masterDataInfo.Line && x.StartTime >= DateTime.Now && x.EndTime <= DateTime.Now && x.Enable).FirstOrDefault();
//                            bool isExistedMainline = lineMaintainInfo != null;
//                            if (isExistedMainline) { continue; }

//                            //剩余的人力是否足够开线
//                            if (restHumanResource >= double.Parse(masterDataInfo.HC))
//                            {
//                                SchedulerSummary schedulerSummary = new SchedulerSummary();
//                                int currentQuantity = item.Quantity;
//                                var targetQuantity = 0;
//                                //某一条线，不能无限叠加，计算剩余工时后方可叠加
//                                double oneLineUsedTime = 0;
//                                schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.Line == masterDataInfo.Line).ToList().ForEach(x => oneLineUsedTime += (double)(x.PlanedQuantity / x.UPH));
//                                double currentLineRestTime = standareTime - oneLineUsedTime;
//                                if (currentLineRestTime <= 0) { continue; } //说明此条线已经没有剩余工时可用了//当前线别，存在剩余工时
//                                var fullQuantity = (int)Math.Floor(currentLineRestTime * double.Parse(masterDataInfo.UPH));

//                                if (currentQuantity >= fullQuantity)
//                                {
//                                    targetQuantity = fullQuantity;
//                                    item.Quantity -= fullQuantity;
//                                }
//                                else
//                                {
//                                    targetQuantity = currentQuantity;
//                                    item.Flag = 1;
//                                    item.Quantity = 0;
//                                }

//                                var addPartTime = item.Quantity / int.Parse(masterDataInfo.UPH);
//                                if (currentLineRestTime >= addPartTime)
//                                {
//                                    //剩的时间，足够将此需求生产完
//                                    var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).FirstOrDefault();
//                                    if (queryEntity == null)
//                                    {
//                                        schedulerSummary.SchedulerDate = scheduleDate;
//                                        schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                        schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                        schedulerSummary.Plant = masterDataInfo.Plant;
//                                        schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                        schedulerSummary.PlanedQuantity = targetQuantity;
//                                        schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                        schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                        schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                        schedulerSummary.WeekNumber = weekNumber;
//                                        schedulerSummary.KeyUser = masterDataInfo.User;
//                                        schedulerSummary.CreateTime = DateTime.Now;
//                                        schedulerSummary.UpdateTime = DateTime.Now;
//                                        schedulerSummaries.Add(schedulerSummary);
//                                        if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                        {
//                                            restHumanResource -= (double)schedulerSummary.HC;
//                                        }
//                                    }
//                                    else
//                                    {
//                                        schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                    }
//                                }
//                                else
//                                {
//                                    var queryEntity = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line != masterDataInfo.Line).FirstOrDefault();
//                                    if (queryEntity == null)
//                                    {
//                                        schedulerSummary.SchedulerDate = scheduleDate;
//                                        schedulerSummary.SchedulerId = $"{item.MaterialNumber.Replace(" ", "").ToUpper()}{masterDataInfo.Line.Replace(" ", "").ToUpper()}{scheduleDate.ToString("yyyyMMdd")}";
//                                        schedulerSummary.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                                        schedulerSummary.Plant = masterDataInfo.Plant;
//                                        schedulerSummary.Line = masterDataInfo.Line.Replace(" ", "").ToUpper();
//                                        schedulerSummary.PlanedQuantity = targetQuantity;
//                                        schedulerSummary.SPQ = masterDataInfo.SPQ == null ? 0 : int.Parse(masterDataInfo.SPQ);
//                                        schedulerSummary.UPH = masterDataInfo.UPH == null ? 0 : double.Parse(masterDataInfo.UPH);
//                                        schedulerSummary.HC = masterDataInfo.HC == null ? 0 : double.Parse(masterDataInfo.HC);
//                                        schedulerSummary.WeekNumber = weekNumber;
//                                        schedulerSummary.KeyUser = masterDataInfo.User;
//                                        schedulerSummary.CreateTime = DateTime.Now;
//                                        schedulerSummary.UpdateTime = DateTime.Now;
//                                        schedulerSummaries.Add(schedulerSummary);
//                                        if (!usedLineList.Contains(masterDataInfo.Line.Replace(" ", "").ToUpper()))
//                                        {
//                                            restHumanResource -= (double)schedulerSummary.HC;
//                                        }
//                                    }
//                                    else
//                                    {
//                                        schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate && x.MaterialNumber == item.MaterialNumber && x.Line == masterDataInfo.Line).ToList().ForEach(x => x.PlanedQuantity += targetQuantity);
//                                    }

//                                    //新开的线别满额拍程
//                                    usedLineList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.Line).Distinct().ToList();
//                                    usedMaterialNumberList = schedulerSummaries.Where(x => x.SchedulerDate == scheduleDate).Select(x => x.MaterialNumber).Distinct().ToList();
//                                }
//                            }

//                            if (restHumanResource < 1)
//                            {
//                                isFullSchedule = true;
//                                break;
//                            }
//                        }
//                    }
//                }
//            }
//            else
//            {
//                //没有找到前一天的排程数据
//                ScheduleErrorMessage scheduleErrorMessage = new ScheduleErrorMessage();
//                scheduleErrorMessage.MaterialNumber = "NA";
//                scheduleErrorMessage.ErrorMessage = "没有找到前一天的排程数据.";
//                scheduleErrorMessagelst.Add(scheduleErrorMessage);
//            }
//        }
//        #endregion

//        #region 将排程结果传递至前端
//        /// <summary>将数据格式进行转换,传递至前端</summary>
//        public SchedulerSummaryViewModel SchedulerSummaryTransforFEData(List<SchedulerSummary> schedulerSummaries)
//        {
//            SchedulerSummaryViewModel schedulerSummaryForUI = new SchedulerSummaryViewModel();
//            List<Hashtable> lst = new List<Hashtable>();
//            var distinctMaterialNumberlst = schedulerSummaries.Select(x => x.MaterialNumber?.Replace(" ", "").ToUpper()).OrderBy(x => x).Distinct().ToList();
//            var distinctLinelst = schedulerSummaries.Select(x => x.Line?.Replace(" ", "").ToUpper()).OrderBy(x => x).Distinct().ToList();
//            var startDate = firstMonday;

//            string[] weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
//            List<TitleElement> titleElements = new List<TitleElement>();
//            for (int i = 0; i < 28; i++)
//            {
//                var scheduleDate = startDate.AddDays(i);
//                var dateFormate = scheduleDate.ToString("MM-dd");
//                string week = weekdays[Convert.ToInt32(scheduleDate.DayOfWeek)];

//                Hashtable hashtable = new Hashtable();
//                hashtable.Add("title", $"{dateFormate}-{week}");
//                List<Hashtable> _children = [];
//                Hashtable hashtable1 = new Hashtable();
//                hashtable1.Add("key", $"{dateFormate}-Plan");
//                Hashtable hashtable2 = new Hashtable();
//                hashtable2.Add("key", $"{dateFormate}-Actual");
//                Hashtable hashtable3 = new Hashtable();
//                hashtable3.Add("key", $"{dateFormate}-Mark");
//                _children.Add(hashtable1);
//                _children.Add(hashtable2);
//                _children.Add(hashtable3);

//                titleElements.Add(new TitleElement() { title = hashtable, children = _children });
//            }
//            schedulerSummaryForUI.title = titleElements;

//            foreach (var line in distinctLinelst)
//            {
//                foreach (var materialNumber in distinctMaterialNumberlst)
//                {
//                    if (string.IsNullOrEmpty(materialNumber) || string.IsNullOrEmpty(line)) { continue; }

//                    //查询相同MaterialNumber,Line对应的排程计划数据
//                    var queryResultByMaterialNumberLinelst = schedulerSummaries.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber && x.Line?.Replace(" ", "").ToUpper() == line).DistinctBy(x => x.SchedulerDate).ToList();
//                    if (queryResultByMaterialNumberLinelst.Count != 0)
//                    {
//                        Hashtable hashtable = new Hashtable();
//                        foreach (var item in queryResultByMaterialNumberLinelst)
//                        {
//                            var dateFormate = item.SchedulerDate?.ToString("MM-dd");

//                            //basic info
//                            if (hashtable.Keys.Count == 0)
//                            {
//                                hashtable.Add("PN", materialNumber);
//                                hashtable.Add("key", line + materialNumber);
//                                hashtable.Add("Line", line);
//                                hashtable.Add("UPH", item.UPH);
//                                hashtable.Add("HC", item.HC);
//                                hashtable.Add("SPQ", item.SPQ);
//                                //hashtable.Add("Site", item.Site);
//                                //hashtable.Add("PCBA", item.PCBA);
//                                hashtable.Add("Description", item.Description);
//                            }

//                            //dynamic info
//                            if (!hashtable.ContainsKey($"{dateFormate}-Plan"))
//                            {
//                                hashtable.Add($"{dateFormate}-Plan", item.PlanedQuantity);
//                                hashtable.Add($"{dateFormate}-Actual", item.ActualQuantity);
//                                hashtable.Add($"{dateFormate}-Mark", item.Remark);
//                            }
//                        }
//                        lst.Add(hashtable);
//                    }
//                }
//            }
//            schedulerSummaryForUI.list = lst;
//            return schedulerSummaryForUI;
//        }
//        /// <summary>获取已经做过排程的记录</summary>
//        public SchedulerSummaryViewModel GetSchedulerSummary(DateTime startDate = default(DateTime))
//        {
//            if (startDate == default(DateTime))
//                startDate = firstMonday.AddDays(7);
//            var _scheduleSummaryResult = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerDate >= startDate).ToList();
//            SchedulerSummaryViewModel schedulerSummaryViewModel = new SchedulerSummaryViewModel();
//            if (_scheduleSummaryResult.Count > 0)
//            {
//                schedulerSummaryViewModel = SchedulerSummaryTransforFEData(_scheduleSummaryResult);
//            }
//            return schedulerSummaryViewModel;
//        }
//        /// <summary>更新排程计划</summary>
//        public List<UpdateSchedulerSummaryModel> UpdateSchedulerSummary(UpdateSchedulerSummaryViewModel updateSchedulerSummaryViewModel)
//        {
//            updateSchedulerSummaryViewModel.list.ToList();
//            List<Hashtable> _list = new List<Hashtable>();
//            List<UpdateSchedulerSummaryModel> schedulerSummaryModellst = new List<UpdateSchedulerSummaryModel>();
//            if (updateSchedulerSummaryViewModel.list.Count == 0) { return schedulerSummaryModellst; }

//            //通过PN+Line+Date查找
//            _list = updateSchedulerSummaryViewModel.list;

//            foreach (Hashtable item in _list)
//            {
//                if (!item.ContainsKey("PN") || !item.ContainsKey("Line")) { continue; }   //保存失败

//                foreach (var key in item.Keys)
//                {
//                    var _key = key.ToString();
//                    var _value = item[key] == null ? string.Empty : item[key].ToString();
//                    if (!_key.Contains('-')) { continue; }
//                    var _keySplit = _key.Split('-');
//                    var _dateStr = _keySplit.Length >= 4 ? _keySplit[0] + _keySplit[1] + _keySplit[2] : DateTime.Now.ToString("yyyy") + _keySplit[0] + _keySplit[1];
//                    var _schedulerId = item["PN"]?.ToString()?.Replace(" ", "").ToUpper() + item["Line"]?.ToString()?.Replace(" ", "").ToUpper() + _dateStr;

//                    var queryResult = schedulerSummaryModellst.Where(x => x.SchedulerId == _schedulerId).FirstOrDefault();
//                    if (queryResult != null)
//                    {
//                        if (_keySplit[^1].ToString().Replace(" ", "").Equals("PLAN", StringComparison.CurrentCultureIgnoreCase))
//                        {
//                            queryResult.PlanedQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
//                            continue;
//                        }

//                        if (_keySplit[^1].ToString().Replace(" ", "").Equals("ACTUAL", StringComparison.CurrentCultureIgnoreCase))
//                        {
//                            queryResult.ActualQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
//                            continue;
//                        }

//                        if (_keySplit[^1].ToString().Replace(" ", "").Equals("MARK", StringComparison.CurrentCultureIgnoreCase))
//                        {
//                            queryResult.Remark = _value;
//                            continue;
//                        }
//                    }
//                    else
//                    {
//                        UpdateSchedulerSummaryModel updateSchedulerSummaryModel = new UpdateSchedulerSummaryModel();
//                        updateSchedulerSummaryModel.SchedulerId = _schedulerId;
//                        if (_keySplit[_keySplit.Length - 1].ToString().Replace(" ", "").ToUpper() == "PLAN")
//                        {
//                            updateSchedulerSummaryModel.PlanedQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
//                            schedulerSummaryModellst.Add(updateSchedulerSummaryModel);
//                            continue;
//                        }

//                        if (_keySplit[_keySplit.Length - 1].ToString().Replace(" ", "").ToUpper() == "ACTUAL")
//                        {
//                            updateSchedulerSummaryModel.ActualQuantity = string.IsNullOrEmpty(_value) ? 0 : int.Parse(_value);
//                            schedulerSummaryModellst.Add(updateSchedulerSummaryModel);
//                            continue;
//                        }

//                        if (_keySplit[_keySplit.Length - 1].ToString().Replace(" ", "").ToUpper() == "MARK")
//                        {
//                            updateSchedulerSummaryModel.Remark = _value;
//                            schedulerSummaryModellst.Add(updateSchedulerSummaryModel);
//                            continue;
//                        }
//                    }
//                }
//            }

//            if (schedulerSummaryModellst.Count == 0) { return schedulerSummaryModellst; }

//            //更新数据
//            foreach (var item in schedulerSummaryModellst)
//            {
//                var queryResult = _apsDbContext.SchedulerSummary.Where(x => x.SchedulerId == item.SchedulerId).FirstOrDefault();
//                if (queryResult != null)
//                {
//                    queryResult.PlanedQuantity = item.PlanedQuantity;
//                    queryResult.ActualQuantity = item.ActualQuantity;
//                    queryResult.Remark = item.Remark;
//                    if (!string.IsNullOrEmpty(item.Remark))
//                    {

//                    }
//                    _apsDbContext.SchedulerSummary.Update(queryResult);

//                    //保存历史记录
//                    var queryHistoryResult = _apsDbContext.SchedulerSummaryHistory.Where(x => x.SchedulerId == item.SchedulerId).OrderByDescending(x => x.UpdateTime).FirstOrDefault();
//                    if (queryHistoryResult != null)
//                    {
//                        if (queryHistoryResult.PlanedQuantity != item.PlanedQuantity || queryHistoryResult.ActualQuantity != item.ActualQuantity || queryHistoryResult.Remark != item.Remark)
//                        {
//                            queryHistoryResult.PlanedQuantity = item.PlanedQuantity;
//                            queryHistoryResult.ActualQuantity = item.ActualQuantity;
//                            queryHistoryResult.Remark = item.Remark;
//                            _apsDbContext.SchedulerSummaryHistory.Update(queryHistoryResult);
//                        }
//                    }
//                }
//                _apsDbContext.SaveChanges();
//            }

//            return schedulerSummaryModellst;
//        }

//        #endregion

//        #region 获取PIR数据
//        /// <summary>获取原始PIR数据</summary>
//        public SapPIRInfoViewModel GetPirInfo()
//        {
//            List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Quantity >= 1 && x.Date != null).ToList();
//            List<MaterialMainLine> materialMainLinelst = _apsDbContext.MaterialMainLine.Where(x => x.MainLine != null).ToList();
//            var distinctLinelst = materialMainLinelst.Where(x => x.MainLine != null).Select(x => x.MainLine?.Replace(" ", "").ToUpper()).Distinct().ToList();
//            var distinctMaterialNumberlst = sapDemands.Select(x => x.MaterialNumber?.Replace(" ", "").ToUpper()).Distinct().ToList();

//            SapPIRInfoViewModel sapPIRInfoViewModel = new SapPIRInfoViewModel();
//            var distinctDateTotallst = sapDemands.Where(x => x.Date != null).Select(x => x.Date.Date.ToString("yyyy-MM-dd")).Distinct().OrderBy(x => x).ToList();
//            sapPIRInfoViewModel.title = distinctDateTotallst;

//            if (sapDemands.Count > 0 && distinctMaterialNumberlst.Count != 0)
//            {
//                List<Hashtable> _list = [];
//                List<Dictionary<string, string?>> dictionaries = new List<Dictionary<string, string?>>();
//                foreach (var materialNumber in distinctMaterialNumberlst)
//                {
//                    var distinctDatelst = sapDemands.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber).DistinctBy(x => x.Date).ToList();
//                    Dictionary<string, string?> keyValuePairs = [];
//                    if (!keyValuePairs.ContainsKey(materialNumber))
//                    {
//                        var queryResult = materialMainLinelst.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber).FirstOrDefault();
//                        if (queryResult != null)
//                        {
//                            keyValuePairs.Add("Line", queryResult.MainLine?.Replace(" ", "").ToUpper());
//                        }
//                        else
//                        {
//                            keyValuePairs.Add("Line", "NA");
//                        }
//                        keyValuePairs.Add("key", materialNumber);
//                        keyValuePairs.Add("PN", materialNumber);
//                    }
//                    if (distinctDatelst.Count != 0)
//                    {
//                        foreach (var item in distinctDatelst)
//                        {
//                            var _key = item.Date.ToString("yyyy-MM-dd");
//                            if (!keyValuePairs.ContainsKey(_key))
//                            {
//                                keyValuePairs.Add(_key, item.Quantity.ToString());
//                            }
//                        }
//                    }
//                    dictionaries.Add(keyValuePairs);
//                }
//                sapPIRInfoViewModel.current = 1;
//                sapPIRInfoViewModel.pageSize = 20;
//                sapPIRInfoViewModel.startDate = DateTime.Now.ToString("yyyy-MM-dd");

//                var sortedDictionaries = dictionaries.OrderBy(dict => dict["Line"]).ToList();
//                sapPIRInfoViewModel.total = dictionaries.Count;
//                sapPIRInfoViewModel.list = sortedDictionaries;
//            }
//            return sapPIRInfoViewModel;
//        }
//        /// <summary>获取原始PIR数据</summary>
//        public SapPIRInfoViewModel GetAllPirInfo()
//        {
//            List<SAPDemand> sapDemands = _apsDbContext.SAPDemand.Where(x => x.Quantity >= 1 && x.Date != null).ToList();
//            List<MaterialNumberLineMapRelations> materialNumberLineMapRelations = _apsDbContext.MaterialNumberLineMapRelations.Where(x => x.Line != null && x.Order == "0").ToList();
//            var distinctLinelst = materialNumberLineMapRelations.Where(x => x.Line != null).Select(x => x.Line?.Replace(" ", "").ToUpper()).Distinct().ToList();
//            var distinctMaterialNumberlst = sapDemands.Select(x => x.MaterialNumber?.Replace(" ", "").ToUpper()).Distinct().ToList();

//            SapPIRInfoViewModel sapPIRInfoViewModel = new SapPIRInfoViewModel();
//            var distinctDateTotallst = sapDemands.Where(x => x.Date != null).Select(x => x.Date.Date.ToString("yyyy-MM-dd")).Distinct().OrderBy(x => x).ToList();
//            sapPIRInfoViewModel.title = distinctDateTotallst;
//            //行转列
//            if (sapDemands.Count > 0 && distinctMaterialNumberlst.Count != 0)
//            {
//                List<Hashtable> _list = [];
//                List<Dictionary<string, string?>> dictionaries = new List<Dictionary<string, string?>>();
//                foreach (var materialNumber in distinctMaterialNumberlst)
//                {
//                    var distinctDatelst = sapDemands.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber).DistinctBy(x => x.Date).ToList();
//                    Dictionary<string, string?> keyValuePairs = [];
//                    if (!keyValuePairs.ContainsKey(materialNumber))
//                    {
//                        var queryResult = materialNumberLineMapRelations.Where(x => x.MaterialNumber?.Replace(" ", "").ToUpper() == materialNumber).FirstOrDefault();
//                        if (queryResult != null)
//                        {
//                            keyValuePairs.Add("Line", queryResult.Line?.Replace(" ", "").ToUpper());
//                        }
//                        else
//                        {
//                            keyValuePairs.Add("Line", "NA");
//                        }
//                        keyValuePairs.Add("key", materialNumber);
//                        keyValuePairs.Add("PN", materialNumber);
//                    }
//                    if (distinctDatelst.Count != 0)
//                    {
//                        foreach (var item in distinctDatelst)
//                        {
//                            var _key = item.Date.ToString("yyyy-MM-dd");
//                            if (!keyValuePairs.ContainsKey(_key))
//                            {
//                                keyValuePairs.Add(_key, item.Quantity.ToString());
//                            }
//                        }
//                    }
//                    dictionaries.Add(keyValuePairs);
//                }
//                sapPIRInfoViewModel.current = 1;
//                sapPIRInfoViewModel.pageSize = 20;
//                sapPIRInfoViewModel.startDate = DateTime.Now.ToString("yyyy-MM-dd");

//                var sortedDictionaries = dictionaries.OrderBy(dict => dict["Line"]).ToList();
//                sapPIRInfoViewModel.total = dictionaries.Count;
//                sapPIRInfoViewModel.list = sortedDictionaries;
//            }
//            return sapPIRInfoViewModel;
//        }
//        /// <summary>及时获取PIR数据</summary>
//        public List<SapOriginalPir> GetPirInfoInTime()
//        {
//            HttpClient _httpClient = new HttpClient();
//            _httpClient.Timeout = TimeSpan.FromSeconds(120);
//            List<SapOriginalPir> sapOriginalPirs = [];
//            var uri = "http://10.2.34.21:8100/api/sap/GetPIRInfo";
//            //var uri = _configuration.GetSection("SapPir").ToString(); 
//            var request = new HttpRequestMessage(HttpMethod.Get, uri);
//            HttpResponseMessage response = _httpClient.Send(request);
//            try
//            {
//                // 确保HTTP响应成功  
//                HttpResponseMessage resmsg = response.EnsureSuccessStatusCode();
//                if (response.IsSuccessStatusCode)
//                {
//                    var responseBody = response.Content.ReadAsStream();
//                    StreamReader reader = new(responseBody);
//                    string text = reader.ReadToEnd();
//                    var apiResult = JsonConvert.DeserializeObject<ApiResult>(text);
//                    if (apiResult != null && apiResult.Data != null)
//                    {
//                        sapOriginalPirs = JsonConvert.DeserializeObject<List<SapOriginalPir>>(apiResult.Data.ToString());
//                    }
//                }

//                if (sapOriginalPirs.Count == 0) { return sapOriginalPirs; }

//                List<SAPDemand> sapDemands = [];
//                foreach (var item in sapOriginalPirs)
//                {
//                    if (string.IsNullOrEmpty(item.Date.Replace(" ", "")) || string.IsNullOrEmpty(item.Quantity.Replace(" ", "").ToString())) { continue; }
//                    var date = DateTime.ParseExact(item.Date.Replace(" ", "").ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
//                    SAPDemand sAPDemand = new SAPDemand();
//                    sAPDemand.Plant = item.Plant;
//                    sAPDemand.MaterialNumber = item.MaterialNumber.Replace(" ", "").ToUpper();
//                    sAPDemand.Type = item.Type.Replace(" ", "").ToUpper();
//                    sAPDemand.Date = date;
//                    sAPDemand.Quantity = int.Parse(item.Quantity.Replace(" ", "").ToUpper());
//                    sAPDemand.ReleaseTime = DateTime.Now;
//                    sAPDemand.CreateTime = DateTime.Now;
//                    sAPDemand.UpdateTime = DateTime.Now;
//                    sapDemands.Add(sAPDemand);
//                }

//                if (sapDemands.Count != 0)
//                {
//                    //_apsDbContext.GetTableFromSql("TRUNCATE TABLE [AP_SuZhou_APS_DevDb].[APS].[SAPDemand]");
//                    _apsDbContext.Database.ExecuteSqlRaw("TRUNCATE TABLE [AP_SuZhou_APS_DevDb].[APS].[SAPDemand]");
//                    _apsDbContext.SAPDemand.UpdateRange(sapDemands);
//                    _apsDbContext.SaveChanges();
//                }
//            }
//            catch (Exception ex)
//            {
//                LogHelper.Error(ex.Message);
//                return sapOriginalPirs;
//            }
//            return sapOriginalPirs;
//        }
//        #endregion

//        #region 日，周达成率
//        /// <summary>每条线的损耗工时</summary>
//        public EachLineTotalTimeViewModel GetEachLineTotalTimesInfo()
//        {
//            //获取原始需求 //计算
//            DateTime dt = DateTime.Now;
//            var result = from x in _apsDbContext.SAPDemand
//                         join y in _apsDbContext.MaterialMainLine
//                         on x.MaterialNumber.Replace(" ", "").ToUpper() equals y.MaterialNumber.Replace(" ", "").ToUpper()
//                         where x.Date >= dt && x.Quantity >= 1 && x.MaterialNumber != null && y.MainLine != null
//                         select new
//                         {
//                             Date = x.Date,
//                             Quantity = x.Quantity,
//                             UPH = y.UPH,
//                             Line = y.MainLine.Replace(" ", "").ToUpper(),
//                             StandbyLineA = y.StandbyLineA == null ? y.StandbyLineA.Replace(" ", "").ToUpper() : string.Empty,
//                             StandbyLineB = y.StandbyLineB == null ? y.StandbyLineB.Replace(" ", "").ToUpper() : string.Empty,
//                             StandbyLineC = y.StandbyLineC == null ? y.StandbyLineC.Replace(" ", "").ToUpper() : string.Empty,
//                             MaterialNumber = y.MaterialNumber
//                         };
//            EachLineTotalTimeViewModel eachLineTotalTimeViewModel = new EachLineTotalTimeViewModel();
//            List<Hashtable> hashtables = [];

//            if (result.Count() != 0)
//            {
//                var distinctLineslst = result.Select(x => x.Line).Distinct().OrderBy(x => x).ToList();
//                if (distinctLineslst.Count() != 0)
//                {
//                    foreach (var line in distinctLineslst)
//                    {
//                        //相同线别,日期进行汇总
//                        var samelineResult = result.Where(x => x.Line == line).ToList();
//                        if (samelineResult.Count() != 0)
//                        {
//                            Hashtable hashtable = new Hashtable();
//                            var querySeminline = result.Where(x => x.Line == line && x.StandbyLineA != null).FirstOrDefault();

//                            var _lineQty = 1;
//                            var _line = line;

//                            if (querySeminline != null)
//                            {
//                                _line = querySeminline.Line + querySeminline.StandbyLineA;
//                                _lineQty = 2;
//                            }

//                            hashtable.Add("key", line);
//                            hashtable.Add("Line", line);
//                            hashtable.Add("Workday", 6);
//                            hashtable.Add("LineQty", _lineQty);
//                            hashtable.Add("OEE", 0.9);

//                            var distinctDateResult = samelineResult.Select(x => x.Date).Distinct().ToList();
//                            if (distinctDateResult.Count() != 0)
//                            {
//                                foreach (var date in distinctDateResult)
//                                {
//                                    //相同线体，日期，不同MaterialNumber
//                                    //var sameLineAndDateResult = samelineResult.Where(x => x.Date == item).ToList();
//                                    var sameLineAndDateResult = samelineResult.Where(x => x.Date < date.AddDays(7) && x.Date >= date).ToList();

//                                    if (sameLineAndDateResult.Count() != 0)
//                                    {
//                                        var values = sameLineAndDateResult.Select(x => new { _ = x.Quantity / x.UPH }).ToArray();
//                                        double totalCount = values.Sum(x => x._);

//                                        //累加至周一
//                                        //星期一为第一天  
//                                        int weeknow = Convert.ToInt32(date.DayOfWeek);

//                                        //因为是以星期一为第一天，所以要判断weeknow等于0时，要向前推6天。  
//                                        weeknow = (weeknow == 0 ? (7 - 1) : (weeknow - 1));
//                                        int daydiff = (-1) * weeknow;
//                                        //本周第一天  
//                                        DateTime? firstDay = date.AddDays(daydiff);

//                                        var _key = firstDay?.ToString("yyyy-MM-dd");
//                                        //hashtable.Add(item?.ToString("yyyy-MM-dd"), totalCount);
//                                        if (hashtable.ContainsKey(_key))
//                                        {
//                                            var _res = (double)hashtable[_key] + totalCount;
//                                            hashtable[_key] = _res;
//                                        }
//                                        else
//                                        {
//                                            hashtable.Add(firstDay?.ToString("yyyy-MM-dd"), totalCount);
//                                        }
//                                    }

//                                }
//                            }
//                            hashtables.Add(hashtable);
//                        }
//                        eachLineTotalTimeViewModel.current = 1;
//                        eachLineTotalTimeViewModel.pageSize = 20;
//                        eachLineTotalTimeViewModel.startDate = dt.Date.ToString("yyyy-MM-dd");

//                    }

//                    eachLineTotalTimeViewModel.total = hashtables.Count;
//                    eachLineTotalTimeViewModel.list = hashtables;
//                }
//            }
//            return eachLineTotalTimeViewModel;
//        }
//        #endregion

//        #region Download
//        public string DownloadPIRInfo(SapPIRInfoViewModel sapPIRInfoViewModel)
//        {
//            string excelName = "Template";
//            string excelPath = string.Empty;
//            try
//            {
//                //首先创建Excel文件对象
//                var workbook = new HSSFWorkbook();

//                //创建工作表，也就是Excel中的sheet，给工作表赋一个名称(Excel底部名称)
//                var sheet = workbook.CreateSheet("sheet1");

//                //sheet.DefaultColumnWidth = 20;//默认列宽

//                sheet.ForceFormulaRecalculation = false;//TODO:是否开始Excel导出后公式仍然有效（非必须）

//                //二级标题列样式设置
//                var listData = sapPIRInfoViewModel.list;
//                var headTopStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 15, true, 700, "楷体", true, false, false, true, FillPattern.SolidForeground, HSSFColor.Grey25Percent.Index, HSSFColor.Black.Index, FontUnderlineType.None, FontSuperScript.None, false);

//                var _headerlist = new List<string>() { "PN", "Line" };
//                _headerlist.AddRange(sapPIRInfoViewModel.title);
//                var maxRows = _headerlist.Count();
//                var headerArr = _headerlist.ToArray();
//                var row = NpoiExcelExportHelper._.CreateRow(sheet, 0, 24);  //第二行
//                var cell = row.CreateCell(0);
//                for (var i = 0; i < headerArr.Length; i++)
//                {
//                    cell = NpoiExcelExportHelper._.CreateCells(row, headTopStyle, i, headerArr[i]);
//                    sheet.SetColumnWidth(i, 5000); //设置单元格宽度
//                }

//                //单元格边框样式
//                var cellStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 10, true, 400);

//                var scheduleDate = DateTime.Now;
//                int rowNumber = 0;
//                foreach (var diclist in listData)
//                {
//                    rowNumber++;
//                    row = NpoiExcelExportHelper._.CreateRow(sheet, rowNumber, 24);  //第二行
//                    int columnNumber = 0;
//                    List<int> existedColumnIndexlist = [];
//                    foreach (var key in diclist.Keys.OrderBy(x => x.Length))
//                    {
//                        var content = diclist[key] == null ? "" : diclist[key].ToString();
//                        var columnIndex = _headerlist.FindIndex(x => x.Replace(" ", "").ToUpper() == key.Replace(" ", "").ToUpper());
//                        if (columnIndex == -1) { continue; }
//                        cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, columnIndex, content);
//                        existedColumnIndexlist.Add(columnIndex);
//                    }

//                    //补充空白
//                    var newMaxRowsCopy = maxRows;
//                    if (maxRows >= 1)
//                    {
//                        for (int i = 0; i < newMaxRowsCopy; i++)
//                        {
//                            if (!existedColumnIndexlist.Contains(i))
//                            {
//                                cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, i, string.Empty);
//                            }
//                        }
//                    }
//                }

//                string folder = DateTime.Now.ToString("yyyyMMdd");


//                var uploadPath = AppDomain.CurrentDomain.BaseDirectory + "UploadFile\\" + folder + "\\";

//                //excel保存文件名
//                string excelFileName = excelName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";

//                //创建目录文件夹
//                if (!Directory.Exists(uploadPath))
//                {
//                    Directory.CreateDirectory(uploadPath);
//                }

//                //Excel的路径及名称
//                excelPath = uploadPath + excelFileName;

//                //使用FileStream文件流来写入数据（传入参数为：文件所在路径，对文件的操作方式，对文件内数据的操作）
//                var fileStream = new FileStream(excelPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

//                //向Excel文件对象写入文件流，生成Excel文件
//                workbook.Write(fileStream);

//                //关闭文件流
//                fileStream.Close();

//                //释放流所占用的资源
//                fileStream.Dispose();

//                //excel文件保存的相对路径，提供前端下载
//                var relativePositioning = "/UploadFile/" + folder + "/" + excelFileName;
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(e.Message);
//            }
//            return excelPath;
//        }
//        public string DownloadStaffTimeByDayInfo(List<Dictionary<string, object>> listData)
//        {
//            string excelName = "Template";
//            string excelPath = string.Empty;
//            try
//            {
//                //首先创建Excel文件对象
//                var workbook = new HSSFWorkbook();

//                //创建工作表，也就是Excel中的sheet，给工作表赋一个名称(Excel底部名称)
//                var sheet = workbook.CreateSheet("sheet1");

//                //sheet.DefaultColumnWidth = 20;//默认列宽

//                sheet.ForceFormulaRecalculation = false;//TODO:是否开始Excel导出后公式仍然有效（非必须）

//                //二级标题列样式设置
//                var headTopStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 15, true, 700, "楷体", true, false, false, true, FillPattern.SolidForeground, HSSFColor.Grey25Percent.Index, HSSFColor.Black.Index,
//                FontUnderlineType.None, FontSuperScript.None, false);
//                var maxRows = listData.OrderByDescending(x => x.Count).FirstOrDefault().Keys.Count();
//                var headerlist = listData.OrderByDescending(x => x.Count).FirstOrDefault().Keys.OrderBy(x => x.Length).ToArray();  //表头名称

//                //Line 05 - 07 - Plan
//                var _headerlist = headerlist.Where(x => x.ToString() != "key" && x.ToString().EndsWith("Plan")).OrderBy(x => x).ToList();
//                _headerlist.Insert(0, "Line");
//                headerlist = _headerlist.ToArray();
//                var row = NpoiExcelExportHelper._.CreateRow(sheet, 0, 24);  //第二行
//                var cell = row.CreateCell(0);
//                for (var i = 0; i < headerlist.Length; i++)
//                {
//                    cell = NpoiExcelExportHelper._.CreateCells(row, headTopStyle, i, headerlist[i]);
//                    sheet.SetColumnWidth(i, 5000); //设置单元格宽度
//                }

//                //单元格边框样式
//                var cellStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 10, true, 400);

//                var scheduleDate = DateTime.Now;
//                int rowNumber = 0;
//                foreach (var diclist in listData)
//                {
//                    rowNumber++;
//                    row = NpoiExcelExportHelper._.CreateRow(sheet, rowNumber, 24);  //第二行
//                    int columnNumber = 0;
//                    List<int> existedColumnIndexlist = [];
//                    foreach (var key in diclist.Keys.OrderBy(x => x.Length))
//                    {
//                        if (key == "key") { continue; }
//                        var content = diclist[key] == null ? "" : diclist[key].ToString();

//                        var columnIndex = _headerlist.FindIndex(x => x.Replace(" ", "").ToUpper() == key.Replace(" ", "").ToUpper());
//                        if (columnIndex == -1) { continue; }
//                        cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, columnIndex, content);
//                        existedColumnIndexlist.Add(columnIndex);
//                    }

//                    //补充空白
//                    //var newMaxRowsCopy = maxRows - columnNumber;
//                    var newMaxRowsCopy = maxRows;
//                    if (maxRows >= 1)
//                    {
//                        for (int i = 0; i < newMaxRowsCopy; i++)
//                        {
//                            if (!existedColumnIndexlist.Contains(i))
//                            {
//                                cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, i, string.Empty);
//                            }
//                        }
//                    }
//                }

//                string folder = DateTime.Now.ToString("yyyyMMdd");


//                var uploadPath = AppDomain.CurrentDomain.BaseDirectory + "UploadFile\\" + folder + "\\";

//                //excel保存文件名
//                string excelFileName = excelName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";

//                //创建目录文件夹
//                if (!Directory.Exists(uploadPath))
//                {
//                    Directory.CreateDirectory(uploadPath);
//                }

//                //Excel的路径及名称
//                excelPath = uploadPath + excelFileName;

//                //使用FileStream文件流来写入数据（传入参数为：文件所在路径，对文件的操作方式，对文件内数据的操作）
//                var fileStream = new FileStream(excelPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

//                //向Excel文件对象写入文件流，生成Excel文件
//                workbook.Write(fileStream);

//                //关闭文件流
//                fileStream.Close();

//                //释放流所占用的资源
//                fileStream.Dispose();

//                //excel文件保存的相对路径，提供前端下载
//                var relativePositioning = "/UploadFile/" + folder + "/" + excelFileName;
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(e.Message);
//            }
//            return excelPath;
//        }
//        public string DownloadStaffTimeByWeekInfo(DownloadModel downloadModel)
//        {
//            string excelName = "Template";
//            string excelPath = string.Empty;
//            try
//            {
//                //首先创建Excel文件对象
//                var workbook = new HSSFWorkbook();

//                //创建工作表，也就是Excel中的sheet，给工作表赋一个名称(Excel底部名称)
//                var sheet = workbook.CreateSheet("sheet1");

//                //sheet.DefaultColumnWidth = 20;//默认列宽

//                sheet.ForceFormulaRecalculation = false;//TODO:是否开始Excel导出后公式仍然有效（非必须）

//                //二级标题列样式设置
//                var headTopStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 15, true, 700, "楷体", true, false, false, true, FillPattern.SolidForeground, HSSFColor.Grey25Percent.Index, HSSFColor.Black.Index,
//                FontUnderlineType.None, FontSuperScript.None, false);
//                var headerlist = downloadModel.title.ToArray();
//                var maxRows = headerlist.Count();
//                var row = NpoiExcelExportHelper._.CreateRow(sheet, 0, 24);  //第二行
//                var cell = row.CreateCell(0);
//                for (var i = 0; i < headerlist.Length; i++)
//                {
//                    cell = NpoiExcelExportHelper._.CreateCells(row, headTopStyle, i, headerlist[i]);
//                    sheet.SetColumnWidth(i, 5000); //设置单元格宽度
//                }

//                //单元格边框样式
//                var cellStyle = NpoiExcelExportHelper._.CreateStyle(workbook, HorizontalAlignment.Center, VerticalAlignment.Center, 10, true, 400);

//                var scheduleDate = DateTime.Now;
//                int rowNumber = 0;
//                var listData = downloadModel.data;
//                foreach (var diclist in listData)
//                {
//                    rowNumber++;
//                    row = NpoiExcelExportHelper._.CreateRow(sheet, rowNumber, 24);  //第二行
//                    int columnNumber = 0;
//                    List<int> existedColumnIndexlist = [];
//                    foreach (var key in diclist.Keys.OrderBy(x => x.Length))
//                    {
//                        var content = diclist[key] == null ? "" : diclist[key].ToString();
//                        var columnIndex = headerlist.ToList().FindIndex(x => x.Replace(" ", "").ToUpper() == key.Replace(" ", "").ToUpper());
//                        if (columnIndex == -1) { continue; }
//                        cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, columnIndex, content);
//                        existedColumnIndexlist.Add(columnIndex);
//                    }

//                    //补充空白
//                    var newMaxRowsCopy = maxRows;
//                    if (maxRows >= 1)
//                    {
//                        for (int i = 0; i < newMaxRowsCopy; i++)
//                        {
//                            if (!existedColumnIndexlist.Contains(i))
//                            {
//                                cell = NpoiExcelExportHelper._.CreateCells(row, cellStyle, i, string.Empty);
//                            }
//                        }
//                    }
//                }

//                string folder = DateTime.Now.ToString("yyyyMMdd");


//                var uploadPath = AppDomain.CurrentDomain.BaseDirectory + "UploadFile\\" + folder + "\\";

//                //excel保存文件名
//                string excelFileName = excelName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";

//                //创建目录文件夹
//                if (!Directory.Exists(uploadPath))
//                {
//                    Directory.CreateDirectory(uploadPath);
//                }

//                //Excel的路径及名称
//                excelPath = uploadPath + excelFileName;

//                //使用FileStream文件流来写入数据（传入参数为：文件所在路径，对文件的操作方式，对文件内数据的操作）
//                var fileStream = new FileStream(excelPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

//                //向Excel文件对象写入文件流，生成Excel文件
//                workbook.Write(fileStream);

//                //关闭文件流
//                fileStream.Close();

//                //释放流所占用的资源
//                fileStream.Dispose();

//                //excel文件保存的相对路径，提供前端下载
//                var relativePositioning = "/UploadFile/" + folder + "/" + excelFileName;
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(e.Message);
//            }
//            return excelPath;
//        }
//        #endregion
//    }
//}