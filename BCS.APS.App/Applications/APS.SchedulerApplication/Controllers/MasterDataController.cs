﻿using APS.SchedulerApplication.Entitys;
using APS.SchedulerApplication.Interfaces;
using APS.SchedulerApplication.Model;
using APS.Utils.Extensions;
using APS.Utils.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace APS.SchedulerApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [ApiExplorerSettings(GroupName = "BllScheduler")]
    public class MasterDataController : ControllerBase
    {
        private readonly ILogger<SchedulerController> _logger;
        private readonly IMasterDataService _masterDataService;

        public MasterDataController(ILogger<SchedulerController> logger, IServiceProvider provider, IMasterDataService masterDataService)
        {
            _logger = logger;
            _masterDataService = provider.CreateScope().ServiceProvider.GetRequiredService<IMasterDataService>();
        }

        [HttpGet("MaterialAllInfo"), AllowAnonymous]
        public ApiResult GetMaterialInfo()
        {
            ApiResult apiResult = new ApiResult();
            List<MaterialNumberLineMapRelations> materialNumberLineMapRelations = _masterDataService.GetMaterialInfo();
            if (materialNumberLineMapRelations.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到MasterData信息";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                //根据分页数据进行查询
                List<Hashtable> _list = new List<Hashtable>();
                foreach (var item in materialNumberLineMapRelations)
                {
                    Hashtable hashtable = new()
                    {
                        { "Id", item.Id },
                        { "PN", item.MaterialNumber },
                        { "Plant", item.Plant },
                        { "Line", item.Line },
                        { "Order", item.Order },
                        { "SPQ", item.SPQ },
                        { "UPH", item.UPH },
                        { "HC", item.HC },
                        { "Area", item.Area },
                        { "User", item.User },
                        { "Description", item.Description }
                    };
                    _list.Add(hashtable);
                }
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "success.";
                apiResult.Data = _list;
            }
            return apiResult;
        }
        [HttpPost("MaterialInfo"), AllowAnonymous]
        public ApiResult GetMaterialInfo(MaterialInfoViewModel materialInfoViewModel, int current = 1, int pageSize = 20)
        {
            ApiResult apiResult = new ApiResult();
            MaterialMainLineInfoViewModel _data = new MaterialMainLineInfoViewModel();
            List<MaterialNumberLineMapRelations> materialNumberLineMapRelations = _masterDataService.GetMaterialInfo(materialInfoViewModel);
            if (materialNumberLineMapRelations.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到MasterData信息";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                //根据分页数据进行查询
                List<Hashtable> _list = new List<Hashtable>();
                List<string> MaterialNumberlst = materialNumberLineMapRelations.OrderByDescending(x => x.Order).ThenBy(x => x.MaterialNumber).Select(x => x.MaterialNumber).Distinct().OrderBy(x => x).ToList();

                foreach (string materialNumber in MaterialNumberlst)
                {
                    var elementlst = materialNumberLineMapRelations.Where(x => x.MaterialNumber == materialNumber).OrderBy(x => x.Order).ToList();
                    Hashtable hashtableParent = new Hashtable();
                    List<Hashtable> hashtablelst = new List<Hashtable>();
                    foreach (var element in elementlst)
                    {
                        Hashtable hashtableChildren = new Hashtable();
                        if (element.Order == "0")
                        {
                            if (hashtableParent.ContainsKey("Id")) { continue; }
                            hashtableParent.Add("Id", element.Id);
                            hashtableParent.Add("PN", element.MaterialNumber);
                            hashtableParent.Add("Plant", element.Plant);
                            hashtableParent.Add("Line", element.Line);
                            hashtableParent.Add("Order", element.Order);
                            hashtableParent.Add("SPQ", element.SPQ);
                            hashtableParent.Add("UPH", element.UPH);
                            hashtableParent.Add("HC", element.HC);
                            hashtableParent.Add("Area", element.Area);
                            hashtableParent.Add("User", element.User);
                            hashtableParent.Add("Description", element.Description);
                        }
                        else
                        {
                            hashtableChildren.Add("Id", element.Id);
                            hashtableChildren.Add("PN", element.MaterialNumber);
                            hashtableChildren.Add("Plant", element.Plant);
                            hashtableChildren.Add("Line", element.Line);
                            hashtableChildren.Add("Order", element.Order);
                            hashtableChildren.Add("SPQ", element.SPQ);
                            hashtableChildren.Add("UPH", element.UPH);
                            hashtableChildren.Add("HC", element.HC);
                            hashtableChildren.Add("Area", element.Area);
                            hashtableChildren.Add("User", element.User);
                            hashtableChildren.Add("Description", element.Description);
                            hashtablelst.Add(hashtableChildren);
                        }
                    }

                    if (hashtablelst.Count >= 1) { hashtableParent.Add("children", hashtablelst); }
                    _list.Add(hashtableParent);
                }
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "success.";
                apiResult.Data = _list;

                var _templst = _list.Skip((current - 1) * pageSize).Take(pageSize).ToList();
                _data.list = _templst;
                _data.current = current;
                _data.pageSize = pageSize;
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "success.";
                apiResult.Data = _list;
            }
            return apiResult;
        }
        [HttpPost("UpdateMaterialInfo"), AllowAnonymous]
        public ApiResult UpdateMaterialInfo(MaterialInfoViewModel materialInfoViewModel)
        {
            MaterialMainLineInfoViewModel _data = new MaterialMainLineInfoViewModel();
            List<MaterialNumberLineMapRelations> materialNumberLineMapRelations = [];
            ApiResult apiResult = new ApiResult();
            materialNumberLineMapRelations = _masterDataService.UpdateMaterialInfoById(materialInfoViewModel);
            if (materialNumberLineMapRelations.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = "更新Masterial失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                //根据分页数据进行查询
                List<Hashtable> _list = new List<Hashtable>();
                foreach (var item in materialNumberLineMapRelations)
                {
                    Hashtable hashtable = new Hashtable
                    {
                        { "Id", item.Id },
                        { "PN", item.MaterialNumber },
                        { "Plant", item.Plant },
                        { "Line", item.Line },
                        { "Order", item.Order },
                        { "SPQ", item.SPQ },
                        { "UPH", item.UPH },
                        { "HC", item.HC },
                        { "Area", item.Area },
                        { "User", item.User },
                        { "Description", item.Description }
                    };
                    _list.Add(hashtable);
                }

                _data.list = _list;
                _data.current = 1;
                _data.pageSize = 20;
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "更新Masterial成功.";
                apiResult.Data = _list;
            }
            return apiResult;
        }
        [HttpPost("AddMaterialInfo"), AllowAnonymous]
        public ApiResult AddMaterialInfo(MaterialInfoViewModel materialInfoViewModel)
        {
            ApiResult apiResult = new ApiResult();
            List<MaterialNumberLineMapRelations> materialNumberLineMapRelations = _masterDataService.AddMaterialInfo(materialInfoViewModel);
            if (materialNumberLineMapRelations.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = "新增Material失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                //根据分页数据进行查询
                List<Hashtable> _list = new List<Hashtable>();
                foreach (var item in materialNumberLineMapRelations)
                {
                    Hashtable hashtable = new Hashtable
                    {
                         { "Id", item.Id },
                        { "PN", item.MaterialNumber },
                        { "Plant", item.Plant },
                        { "Line", item.Line },
                        { "Order", item.Order },
                        { "SPQ", item.SPQ },
                        { "UPH", item.UPH },
                        { "HC", item.HC },
                        { "Area", item.Area },
                        { "User", item.User },
                        { "Description", item.Description }
                    };
                    _list.Add(hashtable);
                }

                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "新增Material成功.";
                apiResult.Data = _list;
            }
            return apiResult;
        }
        [HttpPost("DownloadMaterialInfo")]
        public IActionResult MaterialInfoExportExcel([FromBody] object fromBody)
        {
            DownloadModel deserializedDownloadModel = JsonConvert.DeserializeObject<DownloadModel>(fromBody.ToString());
            var filePath = _masterDataService.DownloadMaterialInfo(deserializedDownloadModel);
            var contentType = "application/octet-stream";
            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            var fileDownloadName = Path.GetFileName(filePath);
            return File(fileBytes, contentType, fileDownloadName);
        }


        [HttpGet("FactoryCalendarInfo"), AllowAnonymous]
        public ApiResult GetFactoryCalendarInfo()
        {
            var result = _masterDataService.GetFactoryCalendarInfo();
            ApiResult apiResult = new ApiResult();
            MaterialMainLineInfoViewModel _data = new MaterialMainLineInfoViewModel();
            if (result.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到MasterData信息";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "success.";
                apiResult.Data = result;
            }
            return apiResult;
        }
        [HttpPost("UpdateFactoryCalendarInfo"), AllowAnonymous]
        public ApiResult UpdateFactoryCalendarInfo(FactoryCalendarViewModel factoryCalendarViewModels)
        {
            var result = _masterDataService.UpdateFactoryCalendarInfo(factoryCalendarViewModels);
            ApiResult apiResult = new ApiResult();
            if (result.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = $"更新工厂日历({factoryCalendarViewModels.Date})失败";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = $"更新工厂日历({factoryCalendarViewModels.Date})成功.";
                apiResult.Data = result;
            }
            return apiResult;
        }


        [HttpGet("HCResourceInfo"), AllowAnonymous]
        public ApiResult GetHCResource(string date, string quantity, string current, string pageSize)
        {
            ApiResult apiResult = new ApiResult();
            List<EachDayHumanQuantityViewModel> result = _masterDataService.GetHCResource();

            //筛选
            if (!string.IsNullOrEmpty(date))
            {
                result = result.Where(x => x.Date == date).ToList();
            }

            if (!string.IsNullOrEmpty(quantity))
            {
                result = result.Where(x => x.Quantity == quantity).ToList();
            }

            if (result.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到HC Resource信息";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                int currentInt = int.Parse(current);
                int pageSizeInt = int.Parse(pageSize);
                var _templst = result.Skip((currentInt - 1) * pageSizeInt).Take(pageSizeInt).ToList();
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Data = result;
                apiResult.IsSuccess = true;
                apiResult.Message = "获取到HC Resource信息成功.";
            }
            return apiResult;
        }
        [HttpPost("HCResourceInfoForJob"), AllowAnonymous]
        public ApiResult GetHCResourceForJob()
        {
            ApiResult apiResult = new ApiResult();
            string startDateTime = DateTime.Now.ToString("yyyyMMdd");
            string endDateTime = DateTime.Now.AddDays(30).ToString("yyyyMMdd");
            string costCenter = "53500P1184";
            List<EachDayHumanQuantity> result = _masterDataService.GetHCResourceForJob(startDateTime, endDateTime, costCenter);
            if (result.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到HC Resource信息";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                //根据分页数据进行查询
                //var _templst = result.Skip((current - 1) * pageSize).Take(pageSize).ToList();
                //_data.list = _templst;
                //_data.current = current;
                //_data.pageSize = pageSize;
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "获取到HC Resource信息成功.";
                apiResult.Data = result;
            }
            return apiResult;
        }


        [HttpGet("LineAllInfo"), AllowAnonymous]
        public ApiResult GetLineAllInfo()
        {
            ApiResult apiResult = new ApiResult();
            List<LineMaintain> result = _masterDataService.GetLineInfo();
            if (result.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到LineInfo信息";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                List<LineMaintainViewModel> newResult = [];
                foreach (LineMaintain lineMaintain in result)
                {
                    LineMaintainViewModel lineMaintainViewModel1 = new LineMaintainViewModel();
                    lineMaintainViewModel1.Id = lineMaintain.Id.ToString();
                    lineMaintainViewModel1.Line = lineMaintain.Line;
                    lineMaintainViewModel1.StartDate = lineMaintain.StartTime.ToString("yyyy-MM-dd");
                    lineMaintainViewModel1.EndDate = lineMaintain.EndTime.ToString("yyyy-MM-dd");
                    lineMaintainViewModel1.IsWork = lineMaintain.Enable ? "OK" : "NG";
                    lineMaintainViewModel1.User = lineMaintain.KeyUser;
                    lineMaintainViewModel1.Area = lineMaintain.Area;
                    newResult.Add(lineMaintainViewModel1);
                }

                apiResult.Code = 200;
                apiResult.Data = newResult;
                apiResult.IsSuccess = true;
                apiResult.Message = "获取到LineInfo信息成功.";
            }
            return apiResult;
        }
        [HttpPost("LineInfo"), AllowAnonymous]
        public ApiResult GetLineInfo(LineMaintainViewModel lineMaintainViewModel, int current = 1, int pageSize = 20)
        {
            ApiResult apiResult = new ApiResult();
            List<LineMaintain> result = _masterDataService.GetLineInfo(lineMaintainViewModel);
            if (result.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到LineInfo信息";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                List<LineMaintainViewModel> newResult = [];
                foreach (LineMaintain lineMaintain in result)
                {
                    LineMaintainViewModel lineMaintainViewModel1 = new LineMaintainViewModel();
                    lineMaintainViewModel1.Id = lineMaintain.Id.ToString();
                    lineMaintainViewModel1.Line = lineMaintain.Line;
                    lineMaintainViewModel1.StartDate = lineMaintain.StartTime.ToString("yyyy-MM-dd");
                    lineMaintainViewModel1.EndDate = lineMaintain.EndTime.ToString("yyyy-MM-dd");
                    lineMaintainViewModel1.IsWork = lineMaintain.Enable ? "OK" : "NG";
                    lineMaintainViewModel1.User = lineMaintain.KeyUser;
                    lineMaintainViewModel1.Area = lineMaintain.Area;
                    newResult.Add(lineMaintainViewModel1);
                }

                var _templst = newResult.Skip((current - 1) * pageSize).Take(pageSize).ToList();
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Data = _templst;
                apiResult.IsSuccess = true;
                apiResult.Message = "获取到LineInfo信息成功.";
            }
            return apiResult;
        }
        [HttpPost("UpdateLineInfo"), AllowAnonymous]
        public ApiResult UpdateLineInfo(LineMaintainViewModel lineMaintainViewModel)
        {
            var result = _masterDataService.UpdateLineInfo(lineMaintainViewModel);
            ApiResult apiResult = new ApiResult();
            if (result.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = $"更新线体{lineMaintainViewModel.Line}信息失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = $"更新线体{lineMaintainViewModel.Line}信息成功.";
                apiResult.Data = result;
            }
            return apiResult;
        }
        [HttpPost("AddLineInfo"), AllowAnonymous]
        public ApiResult AddLineInfo(LineMaintainViewModel lineMaintainViewModel)
        {
            var result = _masterDataService.AddLineInfo(lineMaintainViewModel);
            ApiResult apiResult = new ApiResult();
            if (result.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = $"新增线体{lineMaintainViewModel.Line}信息失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = $"新增线体{lineMaintainViewModel.Line}信息成功.";
                apiResult.Data = result;
            }
            return apiResult;
        }
        [HttpPost("DownloadLineInfo")]
        public IActionResult LineAllInfoExportExcel([FromBody] object fromBody)
        {
            DownloadModel deserializedDownloadModel = JsonConvert.DeserializeObject<DownloadModel>(fromBody.ToString());
            var filePath = _masterDataService.DownloadLineInfo(deserializedDownloadModel);
            var contentType = "application/octet-stream";
            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            var fileDownloadName = Path.GetFileName(filePath);
            return File(fileBytes, contentType, fileDownloadName);
        }

        [HttpGet("SendScheduleLineInfoToSapBom"), AllowAnonymous]
        public ApiResult SendScheduleLineInfoToSapBom()
        {
            ApiResult apiResult = new ApiResult();
            var result = _masterDataService.SendMasterDataToSapBom();
            return apiResult;
        }
        [HttpGet("GetSapBomInfo"), AllowAnonymous]
        public ApiResult GetSapBomInfo(SapBomListModel sapBomListModel, int current = 1, int pageSize = 20)
        {
            ApiResult apiResult = new ApiResult();
            var result = _masterDataService.GetSapBomInfo(sapBomListModel);
            if (result.Count == 0)
            {
                apiResult.Code = 400;
                apiResult.Message = "获取SAP BOM数据失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                var _templst = result.Skip((current - 1) * pageSize).Take(pageSize).ToList();
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Data = _templst;
                apiResult.IsSuccess = true;
                apiResult.Message = "获取SAP BOM数据成功.";
            }
            return apiResult;
        }


        [HttpGet("TestFunc"), AllowAnonymous]
        public ApiResult TestFunc()
        {
            var result = _masterDataService.GetMaterialInfoFromSapThenInsertIntoAPSDb();
            return new ApiResult();
        }

    }
}
