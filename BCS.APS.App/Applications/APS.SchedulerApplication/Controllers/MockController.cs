﻿//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Text.Json;
//using System.Threading.Tasks;

//namespace APS.SchedulerApplication.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class MockController : ControllerBase
//    {
//        protected int getNumber(Hashtable Body, string Key, int DefaultValue)
//        {
//            int Value = DefaultValue;
//            var C = Body[Key];
//            if (C != null)
//            {
//                //Value = ((JsonElement)C).GetInt32();
//                Value = Convert.ToInt32(C);
//            }
//            return Value;
//        }
//        protected R handleListToPage(List<Hashtable> list, Hashtable body)
//        {
//            int current = getNumber(body, "current", 1) - 1;
//            int pageSize = getNumber(body, "pageSize", 20);
//            list = list.Where(x =>
//            {
//                foreach (DictionaryEntry kv in body)
//                {
//                    if (x.ContainsKey(kv.Key) && !x[kv.Key].ToString().Contains(kv.Value.ToString()))
//                    {
//                        return false;
//                    }
//                }
//                return true;
//            }).ToList();
//            var Count = list.Count;
//            list = list.Skip(current * pageSize).Take(pageSize).ToList();
//            Page<Hashtable> page = new Page<Hashtable>(list, pageSize, current + 1, Count, new DateTime(2023, 1, 29));
//            R R = new R() { success = true, code = 200, data = page };
//            return R;
//        }
//        [HttpPost("hr/page")]
//        public R GetHRPage(Hashtable body)
//        {
//            List<Hashtable> list = new List<Hashtable>();
//            DateTime date = DateTime.Now;
//            Random random = new Random();
//            for (int i = 0; i < 30; i++)
//            {
//                Hashtable hashtable = new Hashtable();
//                hashtable.Add("id", i);
//                hashtable.Add("Date", date.ToString("yyyy-MM-dd"));
//                hashtable.Add("Number", 800 + random.Next(100));
//                list.Add(hashtable);
//                date = date.AddDays(1);

//            }
//            return handleListToPage(list, body);
//        }
//    }
//    public class R
//    {
//        public int code { get; set; }
//        public bool success { get; set; }
//        public string message { get; set; }
//        public Object data { get; set; }
//    }
//    public class Page<T>
//    {
//        public int pageSize { get; set; }
//        public int current { get; set; }
//        public DateTime startDate { get; set; }
//        public List<T> list { get; set; }
//        public int total { get; set; }
//        public Page(List<T> list, int pageSize, int current, int total, DateTime startDate)
//        {
//            this.list = list;
//            this.pageSize = pageSize;
//            this.current = current;
//            this.total = total;
//            this.startDate = startDate;
//        }
//    }
//    }
