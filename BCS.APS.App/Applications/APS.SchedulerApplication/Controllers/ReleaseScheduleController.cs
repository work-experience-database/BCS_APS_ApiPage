﻿using APS.SchedulerApplication.Application;
using APS.SchedulerApplication.Interfaces;
using APS.SchedulerApplication.Model;
using APS.Utils.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [ApiExplorerSettings(GroupName = "BllScheduler")]
    public class ReleaseScheduleController : ControllerBase
    {
        private readonly ILogger<ReleaseScheduleController> _logger;
        private readonly IReleaseScheduleSummaryService _releaseScheduleSummaryService;
        public ReleaseScheduleController(ILogger<ReleaseScheduleController> logger, IServiceProvider provider, IReleaseScheduleSummaryService releaseScheduleSummaryService)
        {
            _logger = logger;
            _releaseScheduleSummaryService = provider.CreateScope().ServiceProvider.GetRequiredService<IReleaseScheduleSummaryService>();
        }

        /// <summary>
        /// 释放排程至SAP
        /// </summary>
        /// <param name="releaseSchedulerSummaryToSapViewModel"></param>
        /// <returns></returns>
        [HttpPost("GetSchedulerSummaryInfoReleaseToSap"), AllowAnonymous]
        public ApiResult GetReleaseToSapSchedulerSummaryInfo(ReleaseSchedulerSummaryToSapViewModel releaseSchedulerSummaryToSapViewModel)
        {
            ApiResult apiResult = new ApiResult();
            if (releaseSchedulerSummaryToSapViewModel.selectDate.Count != 2)
            {
                apiResult.Code = 400;
                apiResult.Message = "获取即将释放给Sap的排程记录失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }

            var _data = _releaseScheduleSummaryService.GetSchedulerSummaryInfoReleaseToSap(releaseSchedulerSummaryToSapViewModel);
            if (_data == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "获取即将释放给Sap的排程记录失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = $"获取即将释放给Sap的排程{_data.Count}条记录成功.";
                apiResult.Data = _data;
            }
            return apiResult;
        }

        [HttpPost("ExecuteSchedulerSummaryReleaseToSap"), AllowAnonymous]
        public ApiResult ExecuteReleaseToSapSchedulerSummary(ReleaseSchedulerSummaryToSapViewModel releaseSchedulerSummaryToSapViewModel)
        {
            ApiResult apiResult = new ApiResult();
            if (releaseSchedulerSummaryToSapViewModel.selectDate.Count != 2)
            {
                apiResult.Code = 400;
                apiResult.Message = "获取即将释放给Sap的排程记录失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            var _data = _releaseScheduleSummaryService.ExecuteSchedulerSummaryReleaseToSap(releaseSchedulerSummaryToSapViewModel);

            if (_data == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "将排程记录释放给Sap失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = $"将排程记录释放给Sap成功.";
                apiResult.Data = _data;
            }
            return apiResult;
        }

        /// <summary>
        /// 释放排程至IMES
        /// </summary>
        /// <param name="releaseSchedulerSummaryToSapViewModel"></param>
        /// <returns></returns>
        [HttpPost("GetSchedulerSummaryInfoReleaseToIMes"), AllowAnonymous]
        public ApiResult GetReleaseToIMesSchedulerSummaryInfo(ReleaseSchedulerSummaryToSapViewModel releaseSchedulerSummaryToSapViewModel)
        {
            ApiResult apiResult = new ApiResult();
            if (releaseSchedulerSummaryToSapViewModel.selectDate.Count != 2)
            {
                apiResult.Code = 400;
                apiResult.Message = "获取即将释放给Sap的排程记录失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }

            var _data = _releaseScheduleSummaryService.GetSchedulerSummaryInfoReleaseToSap(releaseSchedulerSummaryToSapViewModel);
            if (_data == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "获取即将释放给IMES的排程记录失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = $"获取即将释放给IMES的排程{_data.Count}条记录成功.";
                apiResult.Data = _data;
            }
            return apiResult;
        }

        [HttpPost("ExecuteSchedulerSummaryReleaseToIMes"), AllowAnonymous]
        public ApiResult ExecuteReleaseToIMesSchedulerSummary(ReleaseSchedulerSummaryToSapViewModel releaseSchedulerSummaryToSapViewModel)
        {
            ApiResult apiResult = new ApiResult();
            if (releaseSchedulerSummaryToSapViewModel.selectDate.Count != 2)
            {
                apiResult.Code = 400;
                apiResult.Message = "获取即将释放给Sap的排程记录失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            var _data = _releaseScheduleSummaryService.ExecuteSchedulerSummaryReleaseToSap(releaseSchedulerSummaryToSapViewModel);

            if (_data == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "将排程记录释放给Sap失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = $"将排程记录释放给Sap成功.";
                apiResult.Data = _data;
            }
            return apiResult;
        }

        [HttpGet("GetLinesAndKeyUsers"), AllowAnonymous]
        public ApiResult GetLinesAndKeyUsers()
        {
            ApiResult apiResult = new ApiResult();
            //var ip1 = HttpContext.Request.Headers["Cdn-Src-Ip"].FirstOrDefault();
            //var ip2 = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault();
            //var ip3 = HttpContext.Connection.RemoteIpAddress.ToString();
            //LogHelper.Info("FS");
            //LogHelper.Info(ip1);
            //LogHelper.Info(ip2);
            //LogHelper.Info(ip3);

            var _data = _releaseScheduleSummaryService.GetLineAndKeyUserSelectedModelInfo();

            if (_data == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "获取线体以及人员失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = $"获取线体以及人员成功.";
                apiResult.Data = _data;
            }
            return apiResult;
        }
    }
}
