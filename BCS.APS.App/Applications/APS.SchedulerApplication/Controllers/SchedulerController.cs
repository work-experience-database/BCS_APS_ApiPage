﻿using APS.Utils.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using APS.SchedulerApplication.Interfaces;
using APS.SchedulerApplication.Model;
using APS.Utils.Helper;
using APS.SchedulerApplication.Application;
using Newtonsoft.Json;

namespace APS.SchedulerApplication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [ApiExplorerSettings(GroupName = "BllScheduler")]
    public class SchedulerController : ControllerBase
    {
        private readonly ILogger<SchedulerController> _logger;
        private readonly ISchedulerService _schedulerService;

        public SchedulerController(ILogger<SchedulerController> logger, IServiceProvider provider, ISchedulerService schedulerService)
        {
            _logger = logger;
            _schedulerService = provider.CreateScope().ServiceProvider.GetRequiredService<ISchedulerService>();
        }

        [HttpGet("PirAllInfo"), AllowAnonymous]
        public ApiResult GetPirAllInfo()
        {
            ApiResult apiResult = new ApiResult();
            SapPIRInfoViewModel _data = new SapPIRInfoViewModel();
            _data = _schedulerService.GetPirInfo();
            if (_data == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到每条线的每日消耗工时";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                DateTime dt = DateTime.Now;
                _data.startDate = dt.ToString("yyyy-MM-dd");
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "success.";
                apiResult.Data = _data;
            }
            return apiResult;
        }
        [HttpGet("PirInfo"), AllowAnonymous]
        public ApiResult GetPirInfo(string materialNumber = "", string line = "", int current = 1, int pageSize = 20)
        {
            ApiResult apiResult = new ApiResult();
            SapPIRInfoViewModel _data = new SapPIRInfoViewModel();
            _data = _schedulerService.GetPirInfo();
            if (_data == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到每条线的每日消耗工时";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                //分页查询前，做数据筛选
                if (!string.IsNullOrEmpty(line.ToString()))
                {
                    var _line = line.Replace(" ", "").ToUpper();
                    _data.list = _data.list.Where(x => x["Line"].ToString().Replace(" ", "").ToUpper().Contains(_line)).ToList();
                }

                if (!string.IsNullOrEmpty(materialNumber.ToString()))
                {
                    var _materialNumber = materialNumber.Replace(" ", "").ToUpper();
                    _data.list = _data.list.Where(x => x["PN"].ToString().Replace(" ", "").ToUpper().Contains(_materialNumber)).ToList();
                }
                //根据分页数据进行查询
                //_data.total = _data.list.Count;
                var _templst = _data.list.Skip((current - 1) * pageSize).Take(pageSize).ToList();
                _data.total = _data.list.Count;
                _data.list = _data.list;
                _data.current = current;
                _data.pageSize = pageSize;
                DateTime dt = DateTime.Now;
                _data.startDate = dt.ToString("yyyy-MM-dd");
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "success.";
                apiResult.Data = _data;
            }
            return apiResult;
        }
        [HttpGet("GetPirInfoInTime"), AllowAnonymous]
        public ApiResult GetPirInfoInTime()
        {
            ApiResult apiResult = new ApiResult();
            var _data = _schedulerService.GetPirInfoInTime();

            if (_data == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "获取最新PIR失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Message = "获取最新PIR成功.";
                apiResult.Data = _data;
            }

            return apiResult;
        }
        [HttpPost("DownloadPirInfo")]
        public IActionResult PirInfoExportExcel([FromBody] object fromBody)
        {
            SapPIRInfoViewModel deserializedSapPIRInfoViewModel = JsonConvert.DeserializeObject<SapPIRInfoViewModel>(fromBody.ToString());
            var filePath = _schedulerService.DownloadPIRInfo(deserializedSapPIRInfoViewModel);
            var contentType = "application/octet-stream";
            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            var fileDownloadName = Path.GetFileName(filePath);
            return File(fileBytes, contentType, fileDownloadName);
        }

        [HttpGet("EachLineTotalTimeInfo"), AllowAnonymous]
        public ApiResult GetEachLineTotalTimeInfo(int current = 1, int pageSize = 20)
        {
            ApiResult apiResult = new ApiResult();
            EachLineTotalTimeViewModel _data = new EachLineTotalTimeViewModel();
            //首先判断缓存有无
            var resultCache = RedisHelper.StringGet<EachLineTotalTimeViewModel>("EachLineTotalTimeInfo");
            if (resultCache != null)
            {
                if (resultCache.list.Count >= 1)
                {
                    _data = resultCache;
                }
            }
            else
            {
                _data = _schedulerService.GetEachLineTotalTimesInfo();
                RedisHelper.StringSet<EachLineTotalTimeViewModel>("EachLineTotalTimeInfo", _data, 10 * 60);
            }
            if (_data == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到每条线的每日消耗工时";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                //根据分页数据进行查询
                var _templst = _data.list.Skip((current - 1) * pageSize).Take(pageSize).ToList();
                _data.list = _data.list;
                _data.current = current;
                _data.pageSize = pageSize;
                DateTime dt = DateTime.Now;

                //星期一为第一天  
                int weeknow = Convert.ToInt32(dt.DayOfWeek);

                //因为是以星期一为第一天，所以要判断weeknow等于0时，要向前推6天。  
                weeknow = (weeknow == 0 ? (7 - 1) : (weeknow - 1));
                int daydiff = (-1) * weeknow;
                //本周第一天  
                DateTime firstDay = dt.AddDays(daydiff);
                _data.startDate = firstDay.ToString("yyyy-MM-dd");
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "success.";
                apiResult.Data = _data;
            }
            return apiResult;
        }
        [HttpPost("DownloadStaffTimeByDayInfo"), AllowAnonymous]
        public IActionResult StaffTimeByDayInfoExportExcel([FromBody] object fromBody)
        {
            List<Dictionary<string, object>> deserializedList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(fromBody.ToString());
            var filePath = _schedulerService.DownloadStaffTimeByDayInfo(deserializedList);
            var contentType = "application/octet-stream";
            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            var fileDownloadName = Path.GetFileName(filePath);
            return File(fileBytes, contentType, fileDownloadName);
        }
        [HttpPost("DownloadStaffTimeByWeekInfo"), AllowAnonymous]
        public IActionResult StaffTimeByWeekInfoExportExcel([FromBody] object fromBody)
        {
            DownloadModel deserializedDownloadModel = JsonConvert.DeserializeObject<DownloadModel>(fromBody.ToString());
            var filePath = _schedulerService.DownloadStaffTimeByWeekInfo(deserializedDownloadModel);
            var contentType = "application/octet-stream";
            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            var fileDownloadName = Path.GetFileName(filePath);
            return File(fileBytes, contentType, fileDownloadName);
        }

       
        [HttpGet("ExecuteSchedulerSummary"), AllowAnonymous]
        public ApiResult ExecuteSchedulerSummary(DateTime startDate, DateTime endDate)
        {
            //起始日期更改
            startDate = DateTime.Now.Date;
            double oee = 0.9;
            int days = 1;
            var result = _schedulerService.ExecuteSchedulerSummary(startDate, endDate,oee,days);
            ApiResult apiResult = new ApiResult();
            if (result == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到排程信息";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "success.";
                apiResult.Data = result;
            }
            return apiResult;
        }
        [HttpGet("SchedulerSummaryInfo"), AllowAnonymous]
        public ApiResult GetSchedulerSummary()
        {
            //起始日期更改
            DateTime startDate = default(DateTime);
            DateTime endDate = default(DateTime);
            //var result = _schedulerService.GetSchedulerSummary(startDate);
            double oee = 0.9;
            int days = 1;
            var result = _schedulerService.ExecuteSchedulerSummary(startDate, endDate,oee,days);
            ApiResult apiResult = new ApiResult();
            if (result == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "未获取到排程信息";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = "success.";
                apiResult.Data = result;
            }
            return apiResult;
        }


        [HttpGet("SchedulerSummary1"), AllowAnonymous]
        public ApiResult GetSchedulerSummary1() {
            ApiResult apiResult =  ApiResult.OperationSuccessful;
            apiResult.Data=_schedulerService.TestScheduler().ToVO();
           
            return apiResult;

        }
        [HttpGet("SchedulerSummaryTest"), AllowAnonymous]
        public ApiResult GetSchedulerSummaryTest(string m)
        {
            ApiResult apiResult = ApiResult.OperationSuccessful;
            apiResult.Data = _schedulerService.TestScheduler(m);

            return apiResult;

        }
        [HttpPost("UpdateSchedulerSummary"), AllowAnonymous]
        public ApiResult UpdateSchedulerSummary(UpdateSchedulerSummaryViewModel viewModel)
        {
            var _data = _schedulerService.UpdateSchedulerSummary(viewModel);
            ApiResult apiResult = new ApiResult();
            if (_data == null)
            {
                apiResult.Code = 400;
                apiResult.Message = "更新排程数据失败.";
                apiResult.IsSuccess = false;
                apiResult.Data = null;
                return apiResult;
            }
            else
            {
                apiResult.IsSuccess = true;
                apiResult.Code = 200;
                apiResult.Message = $"更新{_data.Count}条排程数据成功.";
                apiResult.Data = _data;
            }
            return apiResult;
        }
    }
}