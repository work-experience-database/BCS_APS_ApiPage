﻿using APS.EFCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Entitys
{
    public class APSDbContext : AppDbContext
    {
        public APSDbContext(DbContextOptions<APSDbContext> options) : base(options)
        {
        }

        public DbSet<SchedulerSummary> SchedulerSummary { get; set; }
        public DbSet<SchedulerSummaryHistory> SchedulerSummaryHistory { get; set; }
        public DbSet<MaterialPickUpDate> MaterialPickUpDate { get; set; }
        public DbSet<MaterialMainLine> MaterialMainLine { get; set; }
        public DbSet<MaterialNumberLineMapRelations> MaterialNumberLineMapRelations { get; set; }
        public DbSet<SAPDemand> SAPDemand { get; set; }
        public DbSet<SAPMaterial> SAPMaterial { get; set; }
        public DbSet<SAPMaterial> DisableDateOfScheduler { get; set; }
        public DbSet<FactoryCalendar> FactoryCalendar { get; set; }
        public DbSet<EachDayHumanQuantity> EachDayHumanQuantity { get; set; }
        public DbSet<LineMaintain> LineMaintain { get; set; }
        
        public DbSet<TestTable> TestTable { get; set; }
        public DbSet<SchedulerSummaryHistory> SchedulerSummaryTest { get; set; }
    }
}
