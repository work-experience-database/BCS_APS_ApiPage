﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Entitys
{
    [Table("EachDayHumanQuantity", Schema = "APS")]
    public class EachDayHumanQuantity : DefaultEntityLong
    {
        public string? CostCenter { get; set; }
        public string? EmployeeGroup { get; set; }
        public DateTime Date { get; set; }
        public double Quantity { get; set; }
        public double OEE { get; set; }
        public string? KeyUser { get; set; }
        public string? Remark { get; set; }
    }
}
