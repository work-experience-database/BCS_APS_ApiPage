﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Entitys
{
    [Table("FactoryCalendar", Schema ="APS")]
    public class FactoryCalendar : DefaultEntityLong
    {
        public DateTime Date { get; set; }
        public string? KeyUser { get; set; }
        public string? Type { get; set; }
        public string? Comments { get; set; }
        public bool IsWork { get; set; }
    }
}
