﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Entitys
{
    [Table("LineMaintain", Schema = "APS")]
    public class LineMaintain : DefaultEntityLong
    {
        public string Line { get; set; }
        public string? Area { get; set; }
        public string? KeyUser { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool Enable { get; set; }
    }
}
