﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Entitys
{
    [Table("MaterialMainLine", Schema ="APS")]
    public class MaterialMainLine : DefaultEntityLong
    {
        public string? MaterialNumber { get; set; }
        public string? Plant { get; set; }
        public string? MainLine { get; set; }
        public string? StandbyLineA { get; set; }
        public string? StandbyLineB { get; set; }
        public string? StandbyLineC { get; set; }
        public int SPQ { get; set; }
        public double UPH { get; set; }
        public double HC { get; set; }
        public string? Site { get; set; }
        public string? Area { get; set; }
        public string? KeyUser { get; set; }
        public string? PCBA { get; set; }
        public string? Description { get; set; }
    }
}
