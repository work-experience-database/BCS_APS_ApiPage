﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Entitys
{
    [Table("MaterialNumberLineMapRelations", Schema = "APS")]
    public class MaterialNumberLineMapRelations : DefaultEntityLong
    {
        public string? Plant { get; set; }
        public string MaterialNumber { get; set; }
        public string Line { get; set; }
        public string Order { get; set; }
        public string Area { get; set; }
        public string UPH { get; set; }
        public string SPQ { get; set; }
        public string HC { get; set; }
        public string? User { get; set; }
        public string? Description { get; set; }
    }
}
