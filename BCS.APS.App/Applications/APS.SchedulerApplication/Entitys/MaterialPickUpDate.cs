﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Entitys
{
    [Table("MaterialPickUpDate", Schema ="APS")]
    public class MaterialPickUpDate : DefaultEntityLong
    {
        public string? MaterialNumber { get; set; }
        public bool Sun { get; set; }
        public bool Mon { get; set; }
        public bool Tues { get; set; }
        public bool Wed { get; set; }
        public bool Thur { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }
    }
}
