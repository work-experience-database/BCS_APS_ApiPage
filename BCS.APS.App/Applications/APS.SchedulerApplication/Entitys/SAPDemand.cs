﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Entitys
{
    [Table("SAPDemand", Schema ="APS")]
    public class SAPDemand : DefaultEntityLong
    {
        public string? MaterialNumber { get; set; }
        public string? Plant { get; set; }
        public string? Type { get; set; }
        public int Quantity { get; set; }
        public DateTime Date { get; set; }
        public DateTime? ReleaseTime { get; set; }
    }
}
