﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Entitys
{
    [Table("SAPMaterial", Schema ="APS")]
    public class SAPMaterial : DefaultEntityLong
    {
        public string? MaterialNumber { get; set; }
        public string? Plant { get; set; }
        public string? MaterialType { get; set; }
        public string? MaterialDescription { get; set; }
        public string? ProcurementType { get; set; }
        public string? SpecialProcurementType { get; set; }
        public string? ProductionSupervisor { get; set; }
        public string? ProductionSupervisorName { get; set; }
        public string? PostToInspectionStock { get; set; }
        public string? MRPController { get; set; }
        public string? MRPControllerName { get; set; }
        public string? PackingMaterialNumber { get; set; }
        public string? PackingQuantityOfSPQ { get; set; }
    }
}
