﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Entitys
{
    [Table("SchedulerSummaryHistory", Schema = "APS")]
    public class SchedulerSummaryHistory : DefaultEntityLong
    {
        public string? SchedulerId { get; set; }
        public string? MaterialNumber { get; set; }
        public string? Plant { get; set; }
        public string? Line { get; set; }
        public int? SPQ { get; set; }
        public double? UPH { get; set; }
        public double? HC { get; set; }
        public string? Area { get; set; }
        public string? Site { get; set; }
        public string? PCBA { get; set; }
        public string? Description { get; set; }
        public int? WeekNumber { get; set; }
        public string? Status { get; set; } = "CREATED";
        public string? KeyUser { get; set; }
        public int? PlanedQuantity { get; set; }
        public int? ActualQuantity { get; set; }
        public string? Remark { get; set; }
        public string? PlanOrder { get; set; }
        public DateTime? SchedulerDate { get; set; }
        public DateTime? ReleaseToSAP { get; set; }
        public DateTime? ReleaseToIMES { get; set; }

    }
}
