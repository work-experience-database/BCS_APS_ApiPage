﻿using APS.Utils.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Entitys
{
    [Table("TestTable", Schema ="APS")]
    public class TestTable: DefaultEntityLong
    {
        public string? Name { get; set; }
        public DateTime? ShortDate { get; set; }
    }
}
