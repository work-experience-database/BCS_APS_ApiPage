﻿using APS.SchedulerApplication.Entitys;
using APS.SchedulerApplication.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Interfaces
{
    public interface IMasterDataService
    {
        List<MaterialNumberLineMapRelations> GetMaterialInfo();
        List<MaterialNumberLineMapRelations> GetMaterialInfo(MaterialInfoViewModel materialInfoViewModel);
        List<MaterialNumberLineMapRelations> AddMaterialInfo(MaterialInfoViewModel materialInfoViewModel);
        List<MaterialNumberLineMapRelations> UpdateMaterialInfoById(MaterialInfoViewModel materialInfoViewModel);
        string DownloadMaterialInfo(DownloadModel downloadModel);


        List<FactoryCalendar> GetFactoryCalendarInfo();
        List<FactoryCalendar> UpdateFactoryCalendarInfo(FactoryCalendarViewModel factoryCalendarViewModels);

        List<EachDayHumanQuantityViewModel> GetHCResource();
        List<EachDayHumanQuantity> GetHCResourceForJob(string startDateTime, string endDateTime, string costCenter);

        List<LineMaintain> GetLineInfo();
        List<LineMaintain> GetLineInfo(LineMaintainViewModel lineMaintainViewModel);
        List<LineMaintain> AddLineInfo(LineMaintainViewModel lineMaintainViewModel);
        List<LineMaintain> UpdateLineInfo(LineMaintainViewModel lineMaintainViewModel);
        string DownloadLineInfo(DownloadModel downloadModel);

        List<SAPMaterial> GetMaterialInfoFromSapThenInsertIntoAPSDb();

        //BOM
        List<SapBomModel> SendMasterDataToSapBom();
        List<SapBomListModel> GetSapBomInfo(SapBomListModel sapBomListModel);
    }
}
