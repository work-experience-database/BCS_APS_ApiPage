﻿using APS.SchedulerApplication.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Interfaces
{
    public interface IReleaseScheduleSummaryService
    {
        LineAndKeyUserSelectedModel GetLineAndKeyUserSelectedModelInfo();

        List<T_PLDORD> GetSchedulerSummaryInfoReleaseToSap(ReleaseSchedulerSummaryToSapViewModel releaseSchedulerSummaryToSapViewModel);
        List<T_PLDORD> ExecuteSchedulerSummaryReleaseToSap(ReleaseSchedulerSummaryToSapViewModel releaseSchedulerSummaryToSapViewModel);

        List<T_PLDORD> GetSchedulerSummaryInfoReleaseToIMes(ReleaseSchedulerSummaryToIMesViewModel releaseSchedulerSummaryToIMesViewModel);
        List<T_PLDORD> ExecuteSchedulerSummaryReleaseToIMes(ReleaseSchedulerSummaryToIMesViewModel releaseSchedulerSummaryToIMesViewModel);
    }
}
