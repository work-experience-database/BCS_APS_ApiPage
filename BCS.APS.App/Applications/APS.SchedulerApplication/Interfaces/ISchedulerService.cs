﻿using APS.Contracts.Model;
using APS.SchedulerApplication.Entitys;
using APS.SchedulerApplication.Model;
using APS.SchedulerApplication.Model.BO;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Interfaces
{
    public interface ISchedulerService
    {
        SchedulerSummaryViewModel ExecuteSchedulerSummary(DateTime startDate, DateTime endDate, double oee, int days);
        SchedulerSummaryViewModel GetSchedulerSummary(DateTime endDate);
        List<UpdateSchedulerSummaryModel> UpdateSchedulerSummary(UpdateSchedulerSummaryViewModel updateSchedulerSummaryViewModel);


        SapPIRInfoViewModel GetPirInfo();
        SapPIRInfoViewModel GetAllPirInfo();
        List<SapOriginalPir> GetPirInfoInTime();


        EachLineTotalTimeViewModel GetEachLineTotalTimesInfo();

        //FactoryPlan TestScheduler();
        //TestVO TestScheduler(string m); 

        string DownloadPIRInfo(SapPIRInfoViewModel sapPIRInfoViewModel);
        string DownloadStaffTimeByDayInfo(List<Dictionary<string, object>> listData);
        string DownloadStaffTimeByWeekInfo(DownloadModel downloadModel);
    }
}
