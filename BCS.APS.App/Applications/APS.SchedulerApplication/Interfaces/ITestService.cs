﻿    using APS.Contracts.Model;
using APS.SchedulerApplication.Entitys;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Interfaces
{
    public interface ITestService
    {
        List<SAPDemand> GetSAPDemandListAsync();
    }
}
