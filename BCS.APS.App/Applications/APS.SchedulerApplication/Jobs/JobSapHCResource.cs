﻿using APS.Jobs;
using APS.SchedulerApplication.Entitys;
using APS.SchedulerApplication.Interfaces;
using APS.Utils.Models;
using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Jobs
{
    public class JobSapHCResource : IJobSchedule
    {
        private IMasterDataService _masterDataService;
        public JobSapHCResource(IMasterDataService masterDataService)
        {
            _masterDataService = masterDataService;
        }
        public string Name { get; set; } = "APS.SchedulerApplication|UpdateHCResource";

        public void ScheduleConfig(Schedule schedule)
        {
            schedule.ToRunEvery(1).Days().At(00, 10);
        }

        public void Execute()
        {
            string startDateTime = DateTime.Now.ToString("yyyyMMdd");
            string endDateTime = DateTime.Now.AddDays(30).ToString("yyyyMMdd");
            string costCenter = "53500P1184";
            _masterDataService.GetHCResourceForJob(startDateTime, endDateTime, costCenter);
        }
    }
}
