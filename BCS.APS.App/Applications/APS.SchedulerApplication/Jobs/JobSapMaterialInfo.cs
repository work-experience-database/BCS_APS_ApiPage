﻿using APS.Jobs;
using APS.SchedulerApplication.Interfaces;
using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Jobs
{
    public class JobSapMaterialInfo : IJobSchedule
    {
        private IMasterDataService _masterDataService;
        public JobSapMaterialInfo(IMasterDataService masterDataService)
        {
            _masterDataService = masterDataService;
        }
        public string Name { get; set; } = "APS.SchedulerApplication|UpdateMaterialFromSapThenInsertIntoAPSDb";

        public void ScheduleConfig(Schedule schedule)
        {
            schedule.ToRunEvery(1).Days().At(00, 10);
        }

        public void Execute()
        {
            _masterDataService.GetMaterialInfoFromSapThenInsertIntoAPSDb();
        }
    }
}
