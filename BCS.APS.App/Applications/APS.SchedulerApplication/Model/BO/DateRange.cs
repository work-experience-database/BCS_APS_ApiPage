﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model.BO
{
    internal class DateRange
    {

        public DateOnly StartDate { get; set; }
        public DateOnly EndDate { get; set; }
    }
}
