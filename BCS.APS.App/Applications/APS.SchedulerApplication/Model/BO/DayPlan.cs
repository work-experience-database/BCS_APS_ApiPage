﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace APS.SchedulerApplication.Model.BO
{
    public class DayPlan
    {
        public DayPlan(string line, DateOnly date)
        {
            Line = line;
            Date = date;
        }

        public const double MaxDayTime = 22.28;
        public string Line { get; set; }
        public DateOnly Date { get; set; }
        private bool isFull;
        public Dictionary<string, MaterialPlan> MaterialPlanDictionary { get; set; } = new Dictionary<string, MaterialPlan>();

        public DayPlan(MaterialPlan materialPlan)
        {
            this.Line = materialPlan.Line;
            this.MaterialPlanDictionary.Add(materialPlan.Material, materialPlan);
            this.Date = materialPlan.Date;
        }

        //本日计划需要的人力
        public double GetNeedPersonNumber()
        {
            if (MaterialPlanDictionary.Count <= 0)
            {
                return 0;
            }
            return MaterialPlanDictionary.Values.Max(obj => obj.HC);
        }

        //日计划已满（非常接近最大工时或者超过最大工时）
        public bool IsFull()
        {
            if (GetProduceNeedTime()< MaxDayTime-0.02) {
                isFull=false;
            }
            return isFull || IsUnfinished();
        }

        //本日计划是无法完成的（需要时间超出每天的最大工时）
        public bool IsUnfinished()
        {
            return MaterialPlanDictionary.Values.Sum(obj => obj.GetProduceTime()) > MaxDayTime;
        }

        public bool IsEmpty()
        {
            return GetProduceNeedTime() < 0.001;
        }

        //本日剩余可用时间
        public double GetAvailableTime()
        {
            return MaxDayTime - GetProduceNeedTime();
        }

        //本日生产需要时间
        public double GetProduceNeedTime()
        {
            return MaterialPlanDictionary.Values.Sum(obj => obj.GetProduceTime());
        }

        public double GetProduceNeedTime(string m)
        {
            return MaterialPlanDictionary.Values.Where(ele=>ele.Material==m).Sum(obj => obj.GetProduceTime());
        }
        //日计划中增加一个物料计划（如果日计划已经存在此物料，则更新物料计划的数量，可以超出最大工时）
        public void AddMaterialPlan(MaterialPlan materialPlan)
        {
            if (MaterialPlanDictionary.ContainsKey(materialPlan.Material))
            {
                MaterialPlanDictionary[materialPlan.Material].Number = materialPlan.Number + MaterialPlanDictionary[materialPlan.Material].Number;
            }
            else
            {
                MaterialPlanDictionary.Add(materialPlan.Material, materialPlan);
            }
        }

        public MaterialPlan? TryAddSame(DayPlan nextDayPlan, List<DateOnly> canUseDateList, List<string> yesterdayMaterialList)
        {
            //本日排程物料列表为空白，尝试使用参数物料列表
            List<string> todayMaterialList = MaterialPlanDictionary.Keys.ToList();
            if (todayMaterialList == null || todayMaterialList.Count == 0)
            {
                todayMaterialList = yesterdayMaterialList;
            }
            if (todayMaterialList == null || todayMaterialList.Count == 0)
            {
                todayMaterialList = nextDayPlan.MaterialPlanDictionary.Keys.ToList();
            }
            var sortList= nextDayPlan.MaterialPlanDictionary.Values.ToList();
            sortList.Sort((p1,p2)=>p1.Number.CompareTo(p2.Number));
            foreach (var k in sortList)
            {
                string key = k.Material;
                if (IsFull())
                {
                    break;
                }
                if (todayMaterialList.Contains(key))
                {
                    MaterialPlan nextDayMaterialPlan = nextDayPlan.MaterialPlanDictionary[key];
                    double canUseTime = GetAvailableTime() + nextDayPlan.GetAvailableTime() + canUseDateList.Where(ele => ele > Date && ele < nextDayPlan.Date).Count() * MaxDayTime+ nextDayMaterialPlan.GetProduceTime();
                    if (canUseTime > nextDayMaterialPlan.GetProduceTime())
                    {
                        MaterialPlan result = AddMaterial(nextDayMaterialPlan);
                        nextDayPlan.MaterialPlanDictionary.Remove(key);
                        if (result != null && result.Number > 0)
                        {
                            return result;
                        }
                    }
                }
            }
            return null;
        }
        public SchedulerResult TryAddSameMaterial(DayPlan nextDayPlan, List<DateOnly> canUseDateList, string material,SchedulerResult src)
        {
            clear();
            List<string> todayMaterialList = MaterialPlanDictionary.Keys.ToList();
            if (todayMaterialList == null || todayMaterialList.Count == 0)
            {
                if (src.UnFinishedMaterial != null)
                {
                    todayMaterialList = new List<string>() { src.UnFinishedMaterial };
                }
                else {
                    todayMaterialList = src.TodayMaterialList;
                }
                
            }
            if (todayMaterialList == null || todayMaterialList.Count == 0)
            {
                todayMaterialList = nextDayPlan.MaterialPlanDictionary.Keys.ToList();
            }
            src.UnFinishedMaterial = null;
            src = TryAddSameMaterial0(nextDayPlan, canUseDateList, material, src, todayMaterialList);
           
            return src;
        }
        private SchedulerResult TryAddSameMaterial0(DayPlan nextDayPlan, List<DateOnly> canUseDateList, string material, SchedulerResult src,List<string> list) {
            
            foreach (string key in list)
            {
                if (IsFull())
                {
                    break;
                }
                if (key == material && nextDayPlan.MaterialPlanDictionary.ContainsKey(key))
                {
                    MaterialPlan nextDayMaterialPlan = nextDayPlan.MaterialPlanDictionary[key];
                    double canUseTime = GetAvailableTime() + nextDayPlan.GetAvailableTime() + canUseDateList.Where(ele => ele > Date && ele < nextDayPlan.Date).Count() * MaxDayTime;
                    if (canUseTime > nextDayMaterialPlan.GetProduceTime())
                    {
                        MaterialPlan? result = AddMaterial(nextDayMaterialPlan);
                       

                        src.TodayMaterialList = MaterialPlanDictionary.Keys.ToList();
                        if (result != null && result.Number > 0)
                        {
                            src.UnFinishedMaterial = result.Material;
                            return src;
                        }
                    }
                }
            }
            return src;
        }

        //日计划添加物料计划，不能超过日最大工时
        public MaterialPlan? AddMaterial(MaterialPlan material)
        {
            bool isOld = MaterialPlanDictionary.ContainsKey(material.Material);
            MaterialPlan m = MaterialPlanDictionary.GetValueOrDefault(material.Material, new MaterialPlan(material));
            m.Date = Date;
            int maxNumber = (int)(GetAvailableTime() * m.UPH);
            if (maxNumber < material.Number)
            {
                m.Number = m.Number + maxNumber;
                material.Number = material.Number - maxNumber;
                SetFull();
            }
            else
            {
                m.Number = m.Number + material.Number;
                material.Number = 0;
            }
            if (!isOld)
            {
                MaterialPlanDictionary.Add(material.Material, m);
            }
            if (material.Number>0) {
                return material;
            }
            return null;
        }

        private void SetFull()
        {
            isFull = true;
            clear();
        }

        public void clear()
        {
            foreach (var key in MaterialPlanDictionary.Keys)
            {
                if (MaterialPlanDictionary[key].Number == 0)
                {
                    MaterialPlanDictionary.Remove(key);
                }
            }
        }

        public void FillUseNext(DayPlan nextDayPlan)
        {
            List<MaterialPlan> sortList = nextDayPlan.MaterialPlanDictionary.Values.OrderByDescending(obj => obj.Number
           ).ToList();
            foreach (var item in sortList)
            {
                if (GetAvailableTime() < 0.01)
                {
                    break;
                }
                AddMaterial(item);
            }
            clear();
        }
        public void ChangeDate(DateOnly date) { 
            Date= date;
            foreach (var item in MaterialPlanDictionary.Values) {
                item.Date = date;
            }
        }
    }
}