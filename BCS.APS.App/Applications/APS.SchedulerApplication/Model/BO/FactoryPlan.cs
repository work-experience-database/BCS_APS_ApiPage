﻿using APS.SchedulerApplication.Entitys;
using APS.Utils.Helper;
using System.Collections;

namespace APS.SchedulerApplication.Model.BO
{
    public class FactoryPlan
    {
        public Dictionary<string, LinePlan> LinePlanDictionary { get; set; } = new Dictionary<string, LinePlan>();
        public ISet<DateOnly> RestDaySet { get; set; } = new HashSet<DateOnly>() { };
        public Dictionary<string, ISet<DateOnly>> LineRestDayDictionary { get; set; } = new Dictionary<string, ISet<DateOnly>>() { };
        public DateOnly FiexdDate { get; set; }

        //页面展示的开始日期，一般为周一
        public DateOnly StartDate { get; set; }

        public DateOnly EndDate { get; set; }
        public DateOnly DownloadPirDate { get; set; }

        public FactoryPlan(List<SchedulerSummary> schedulerSummaryList)
        {
            List<MaterialPlan> materialPlanList = schedulerSummaryList.Select(p => new MaterialPlan(p)).ToList();
            foreach (var p in materialPlanList)
            {
                AddMaterialPlan(p);
            }
        }

        //参数要求：
        //fixedPlan应该为pir下载日期（包括）到锁定日期（包括）范围的数据
        //downloadPirDate为pir下载日期
        public FactoryPlan(List<SAPDemand> pirList, List<SchedulerSummary> fixedPlan, Dictionary<string, MaterialMainLine> dictionary, DateOnly FiexdDate)
        {
            //使用上期排程计划更新pir数量
            this.FiexdDate = FiexdDate;
            List<MaterialPlan> materialPlanList = fixedPlan.Select(p => new MaterialPlan(p)).ToList();
            List<MaterialPlan> materialPlanList2 = fixedPlan.Select(p => new MaterialPlan(p)).ToList();
            Dictionary<string, List<MaterialPlan>> stockMap = materialPlanList.GroupBy(p => p.Material).ToDictionary(g => g.Key, g => g.ToList());
            foreach (var item in pirList)
            {
                List<MaterialPlan> basePlanListNotAfterPIR = stockMap.GetValueOrDefault(item.MaterialNumber, new List<MaterialPlan>());

                int maxNumber = basePlanListNotAfterPIR.Sum(x => x.Number);
                if (maxNumber <= 0 || item.Quantity <= 0)
                {
                    continue;
                }
                foreach (var p in basePlanListNotAfterPIR)
                {
                    int changeNumber = Math.Min(p.Number, item.Quantity);
                    p.Number = p.Number - changeNumber;
                    item.Quantity = item.Quantity - changeNumber;
                    if (item.Quantity <= 0)
                    {
                        break;
                    }
                }
            }
            //将fixed的计划插入本期计划
            foreach (var p in materialPlanList2)
            {
                AddMaterialPlan(p);
            }
            //将更新后的PIR插入本期计划，默认使用主线插入，主线超负载时插入副线
            pirList = pirList.Where(ele => ele.Quantity > 0).ToList();
            foreach (var p in pirList)
            {
                if (DateOnly.FromDateTime((DateTime)p.Date) < FiexdDate.AddDays(2))
                {
                    p.Date = FiexdDate.AddDays(2).ToDateTime(TimeOnly.MinValue);
                }
                TryAddPIR(p, dictionary);
            }
        }

        public void DebugScheduler(Dictionary<DateOnly, double> hcDictionary)
        {
            //使用人力划分线体可用日期
            ReadySchedulerAllLine();
            UpdateAllLineCanUseDate(hcDictionary);
            //线体单独排程优化
            RevampSchedulerAllLine();
            Console.WriteLine("s");
        }

        private void ReadySchedulerAllLine()
        {
            foreach (var item in LinePlanDictionary.Values)
            {
                item.ReadyScheduler(FiexdDate.AddDays(1), EndDate, ListLineRestDay(item.Line));
            }
        }

        private void UpdateAllLineCanUseDate(Dictionary<DateOnly, double> hcDictionary)
        {
            for (var date = FiexdDate.AddDays(1); date < EndDate.AddDays(14); date = date.AddDays(1))
            {
                double personNumber = hcDictionary[date];
                ISet<string> useLineSet = new HashSet<string>();
                for (var testDate = date; testDate < EndDate.AddDays(14); testDate = testDate.AddDays(1))
                {
                    //如果当天（date）可用人力为0或者当天可以排满全部线体，则开始排下一天
                    if (personNumber <= 0 || useLineSet.Count == LinePlanDictionary.Values.Count)
                    {
                        break;
                    }
                    foreach (var item in LinePlanDictionary.Values)
                    {
                        if (useLineSet.Contains(item.Line))
                        {
                            continue;
                        }
                        if (item.FirstNeedSchedulerDate == testDate)
                        {
                            double lineHC = item.TryStartLine(date, personNumber, ListLineRestDay(item.Line));
                            if (lineHC >= 0.001)
                            {
                                useLineSet.Add(item.Line);
                                personNumber = personNumber - lineHC;
                            }
                        }
                    }
                }
            }
        }

        private void RevampSchedulerAllLine()
        {
            foreach (var item in LinePlanDictionary.Values)
            {
                Console.WriteLine(item.Line);
                item.RevampScheduler();
            }
        }

        public void Scheduler(Dictionary<DateOnly, double> hcDictionary)
        {
            //每条线体做出工作日最晚排程
            foreach (var item in LinePlanDictionary.Values)
            {
                item.ReadyScheduler(FiexdDate.AddDays(1), EndDate, ListLineRestDay(item.Line));
            }

            //使用人力划分线体可用日期

            for (var date = FiexdDate; date < EndDate; date = date.AddDays(1))
            {
                double personNumber = hcDictionary[date];
                ISet<string> useLineSet = new HashSet<string>();
                for (var testDate = date; testDate < EndDate; testDate = testDate.AddDays(1))
                {
                    //如果当天（date）可用人力为0或者当天可以排满全部线体，则开始排下一天
                    if (personNumber <= 0 || useLineSet.Count == LinePlanDictionary.Values.Count)
                    {
                        break;
                    }
                    foreach (var item in LinePlanDictionary.Values)
                    {
                        if (useLineSet.Contains(item.Line))
                        {
                            continue;
                        }
                        if (item.FirstNeedSchedulerDate == testDate)
                        {
                            double lineHC = item.TryStartLine(testDate, personNumber, ListLineRestDay(item.Line));
                            if (lineHC >= 0.001)
                            {
                                useLineSet.Add(item.Line);
                                personNumber = personNumber - lineHC;
                            }
                        }
                    }
                }
            }

            //线体单独排程优化
            foreach (var item in LinePlanDictionary.Values)
            {
                item.RevampScheduler();
            }
        }

        private List<DateOnly> ListLineRestDay(string line)
        {
            HashSet<DateOnly> set1 = new HashSet<DateOnly>(RestDaySet);
            ISet<DateOnly> set2 = LineRestDayDictionary.GetValueOrDefault(line, new HashSet<DateOnly>());
            set1.UnionWith(set2);
            return set1.OrderBy(x => x).ToList();
        }

        //将PIR分配线体
        private void TryAddPIR(SAPDemand p, Dictionary<string, MaterialMainLine> dictionary)
        {
            //没有设置线体的料号不做处理
            if (dictionary.ContainsKey(p.MaterialNumber))
            {
                MaterialMainLine materialMainLine = dictionary[p.MaterialNumber];
                string line = materialMainLine.MainLine;
                MaterialPlan materialPlan = new MaterialPlan(p, dictionary, line);
                if (!LinePlanDictionary.ContainsKey(line) || materialMainLine.StandbyLineA == null || LinePlanDictionary[line].CanAddMaterialPlan(materialPlan, ListLineRestDay(line), FiexdDate.AddDays(1)))
                {
                    AddMaterialPlan(materialPlan);
                }
                else
                {
                    line = materialMainLine.StandbyLineA;
                    materialPlan = new MaterialPlan(p, dictionary, line);
                    AddMaterialPlan(materialPlan);
                }
            }
        }

        private void AddMaterialPlan(MaterialPlan materialPlan)
        {
            string line = materialPlan.Line;

            if (LinePlanDictionary.ContainsKey(line))
            {
                LinePlanDictionary[line].AddMaterialPlan(materialPlan);
            }
            else
            {
                LinePlan linePlan = new LinePlan();
                linePlan.Line = line;
                linePlan.AddMaterialPlan(materialPlan);
                LinePlanDictionary.Add(line, linePlan);
            }
        }

        public List<SchedulerSummary> ToSchedulerSummaryList()
        {
            List<SchedulerSummary> list = new List<SchedulerSummary>();
            foreach (var linePlan in LinePlanDictionary.Values)
            {
                foreach (var dayPlan in linePlan.DayPlanList)
                {
                    foreach (var item in dayPlan.MaterialPlanDictionary.Values)
                    {
                        if (item.Number>0) {
                            var _weekNumber = WeekHelper.GetWeekNumber(item.Date.ToDateTime(TimeOnly.MinValue));
                            list.Add(new SchedulerSummary()
                            {
                                SchedulerDate = item.Date.ToDateTime(TimeOnly.MinValue),
                                MaterialNumber = item.Material,
                                Plant = "535A",
                                Line = item.Line,
                                SPQ = item.SPQ,
                                UPH = item.UPH,
                                HC = item.HC,
                                Area = item.Area,
                                Site = item.Site,
                                WeekNumber = _weekNumber,
                                KeyUser = item.KeyUser,
                                PlanedQuantity = item.Number,
                            });
                        }
                        
                    }
                }
            }
            return list;
        }

        public FactoryPlanVO ToVO()
        {
            FactoryPlanVO result = new FactoryPlanVO();
            List<Hashtable> list = new List<Hashtable>();

            Hashtable hashtable = new Hashtable();
            hashtable.Add("title", StartDate.ToString("MM-dd-dddd"));

            result.title = new List<TitleVO>() {
            new TitleVO(){
            title=hashtable } };
            DateOnly s = StartDate;
            DateOnly e = EndDate.AddDays(14);
            List<LineSort> lineSortList = new List<LineSort>();
            foreach (LinePlan linePlan in LinePlanDictionary.Values)
            {
                MaterialPlan MaterialPlanAAA = linePlan.DayPlanList.First.Value.MaterialPlanDictionary.Values.First();
                lineSortList.Add(new LineSort()
                {
                    Line = MaterialPlanAAA.Line,
                    Area = MaterialPlanAAA.Area,
                    KyeUser = MaterialPlanAAA.KeyUser,
                });
            }

            lineSortList.Sort((p1, p2) =>
            {
                int result = p1.Area.CompareTo(p2.Area);
                if (result == 0)
                {
                    result = p1.KyeUser.CompareTo(p2.KyeUser);
                }
                if (result == 0)
                {
                    result = int.Parse(p1.Line.Replace("L", "")).CompareTo(int.Parse(p2.Line.Replace("L", "")));
                }
                return result;
            });
            foreach (var lineSortEle in lineSortList)
            {
                string key = lineSortEle.Line;
                var item = LinePlanDictionary[key];
                //if (item.Line == "L150" || item.Line == "L26")
                {
                    list.AddRange(item.ToVO(s, e));
                }
            }
            result.list = list;
            return result;
        }
    }

    public class FactoryPlanVO
    {
        public List<Hashtable> list { get; set; }
        public List<TitleVO> title { get; set; }
    }

    public class TitleVO()
    {
        public Hashtable title { get; set; }
    }

    public class TestVO()
    {
        public List<TestVOEle> pir { get; set; }
        public int pirNumber { get; set; }
        public List<TestVOEle> plan { get; set; }
        public int planNumber { get; set; }
    }

    public class TestVOEle()
    {
        public DateOnly Date { get; set; }
        public int Number { get; set; }
    }

    internal class LineSort
    {
        public string Area { get; set; }
        public string KyeUser { get; set; }
        public string Line { get; set; }
    }
}