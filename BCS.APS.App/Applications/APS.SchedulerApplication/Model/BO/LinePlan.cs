﻿using System.Collections;

namespace APS.SchedulerApplication.Model.BO
{
    public class LinePlan
    {
        public string Line { get; set; }
        public LinkedList<DayPlan> DayPlanList { get; set; } = new LinkedList<DayPlan>();
        public DateOnly FirstNeedSchedulerDate { get; set; }
        public List<DateOnly> CanUseDateList { get; set; } = new List<DateOnly>();


        public void AddMaterialPlan(MaterialPlan materialPlan)
        {
            if (Line.Equals(materialPlan.Line))
            {
                if (DayPlanList.Count == 0 || DayPlanList.First?.Value.Date > materialPlan.Date)
                {
                    DayPlanList.AddFirst(new DayPlan(materialPlan));
                    return;
                }
                if (DayPlanList.Last?.Value.Date < materialPlan.Date)
                {
                    DayPlanList.AddLast(new DayPlan(materialPlan));
                    return;
                }
                LinkedListNode<DayPlan>? currentNode = DayPlanList.First;
                while (currentNode != null)
                {
                    if (currentNode.Value.Date == materialPlan.Date)
                    {
                        currentNode.Value.AddMaterialPlan(materialPlan);
                        return;
                    }
                    if (currentNode.Value.Date > materialPlan.Date)
                    {
                        var previousNode = currentNode.Previous;
                        if (previousNode == null)
                        {
                            return;
                        }
                        LinkedListNode<DayPlan> newNode = new LinkedListNode<DayPlan>(new DayPlan(materialPlan));
                        DayPlanList.AddBefore(currentNode, newNode);
                        break;
                    }
                    currentNode = currentNode.Next;
                }
            }
        }

        //如果物料需求日期之前工作日的可用时间足够容纳
        public bool CanAddMaterialPlan(MaterialPlan materialPlan, List<DateOnly> lineRestDaySet, DateOnly startDate)
        {
            List<DateOnly> finalDateRange = ListCanUseDate(startDate, materialPlan.Date, lineRestDaySet);
            double maxTime = finalDateRange.Count * DayPlan.MaxDayTime;
            double usedTime = DayPlanList.Where(ele => ele.Date >= materialPlan.Date).Sum(ele => ele.GetProduceNeedTime());
            return maxTime >= (usedTime + materialPlan.GetProduceTime());
        }

        private List<DateOnly> ListCanUseDate(DateOnly startDate, DateOnly endDate, List<DateOnly> lineRestDaySet)
        {
            List<DateOnly> dateRange = Enumerable.Range(0, (endDate.DayNumber - startDate.DayNumber) + 1)
                                            .Select(day => startDate.AddDays(day))
                                            .ToList();
            return dateRange.Except(lineRestDaySet).ToList();
        }

        //尝试开线（开线成功后将线体FirstNeedSchedulerDate后移，返回需求人数）
        public double TryStartLine(DateOnly date, double personNumber, List<DateOnly> lineRestDayList)
        {
            //需要开工的最早日期已经比线体需要的最晚日期更晚，不需要开线
            if (DayPlanList.Count == 0 || (DayPlanList.Last.Value.Date < FirstNeedSchedulerDate) || (lineRestDayList.Contains(date)))
            {
                return 0;
            }
            LinkedListNode<DayPlan>? currentNode = DayPlanList.First;
            while (currentNode != null)
            {
                if (currentNode.Value.Date >= date)
                {
                    break;
                }
                currentNode = currentNode.Next;
            }
            if (currentNode == null || currentNode.Value == null)
            {
                return 0;
            }
            double needPersonNumber = currentNode.Value.GetNeedPersonNumber();
            if (needPersonNumber <= personNumber)
            {
                currentNode = currentNode.Next;
                if (currentNode == null)
                {
                    FirstNeedSchedulerDate = date.AddDays(1);
                }
                else
                {
                    FirstNeedSchedulerDate = currentNode.Value.Date;
                }
                CanUseDateList.Add(date);
                return needPersonNumber;
            }
            return 0;
        }

        public void ReadyScheduler(DateOnly startDate, DateOnly endDate, List<DateOnly> lineRestDaySet)
        {
            FirstNeedSchedulerDate = startDate;
            //周末前置
            MoveRestDayToFront(startDate, lineRestDaySet);
            //物料合并（在不增加天数的前提下，可以放在一起做的物料，合并在一起）
            LinkedListNode<DayPlan>? currentNode = DayPlanList.First;
            while (currentNode != null)
            {
                if (currentNode.Value.Date < startDate)
                {
                    currentNode = currentNode.Next;
                    continue;
                }
                MegerNodeSameMaterial(currentNode);
                currentNode = currentNode.Next;
            }
            Clear();
        }

        //将本身有物料计划的日计划的下一个日计划的相同物料合并至本日，
        private void MegerNodeSameMaterial(LinkedListNode<DayPlan> currentNode)
        {
            LinkedListNode<DayPlan>? nextNode = currentNode.Next;

            if (nextNode == null || currentNode.Value.IsEmpty() || currentNode.Value.IsUnfinished())
            {
                return;
            }
            while (nextNode != null)
            {
                if (nextNode.Value.IsEmpty())
                {
                    nextNode = nextNode.Next;
                    continue;
                }
                List<MaterialPlan> materialPlanList = nextNode.Value.MaterialPlanDictionary.Values.ToList();
                materialPlanList.Sort((p1, p2) => p2.Number.CompareTo(p1.Number));
                foreach (MaterialPlan materialPlan in materialPlanList)
                {
                    if (currentNode.Value.MaterialPlanDictionary.ContainsKey(materialPlan.Material))
                    {
                        currentNode.Value.AddMaterial(materialPlan);
                    }
                }
                nextNode = nextNode.Next;
            }
        }

        private void MoveRestDayToFront(DateOnly startDate, List<DateOnly> lineRestDaySet)
        {
            LinkedListNode<DayPlan>? currentNode = DayPlanList.Last;
            while (currentNode != null)
            {
                if (currentNode.Value.Date < startDate)
                {
                    break;
                }
                if (lineRestDaySet.Contains(currentNode.Value.Date))
                {
                    LinkedListNode<DayPlan>? previousNode = currentNode.Previous;
                    DateOnly previousDate = currentNode.Value.Date;
                    while (lineRestDaySet.Contains(previousDate))
                    {
                        previousDate = previousDate.AddDays(-1);
                    }
                    if (previousDate < startDate)
                    {
                        break;
                    }
                    if (previousNode != null && previousNode.Value.Date == previousDate)
                    {

                        foreach (MaterialPlan m in currentNode.Value.MaterialPlanDictionary.Values)
                        {
                            previousNode.Value.AddMaterialPlan(m);
                            m.Number = 0;
                        }
                    }
                    else
                    {
                        //currentNode.Value.ChangeDate(previousDate);
                    }
                }
                currentNode = currentNode.Previous;
            }
            Clear();
        }

        private void Clear()
        {
            var node = DayPlanList.First; // 获取头节点
            while (node != null)
            {
                var nextNode = node.Next;
                if (node.Value.GetProduceNeedTime() < 0.001)
                {
                    DayPlanList.Remove(node);
                }
                node = nextNode;
            }
        }

        private SchedulerResult? SchedulerForDate(DateOnly date, SchedulerResult? src)
        {
            if (src == null)
            {
                src = new SchedulerResult();
            }
            if (Line == "L26" && date == new DateOnly(2024, 3, 18))
            {
                Console.WriteLine("pause");
            }
            Clear();
            LinkedListNode<DayPlan>? node = GetNode(date);
            if (node == null)
            {
                node = new LinkedListNode<DayPlan>(new DayPlan(Line, date));
                AddNode(node);
            }
            DayPlan dayPlan = node.Value;
            if (dayPlan.IsFull())
            {
                src.TodayMaterialList = dayPlan.MaterialPlanDictionary.Keys.ToList();
                return src;
            }
            //优先对未处理完的物料再次处理
            LinkedListNode<DayPlan>? nextNode = node.Next;

            //尝试添加能全部放入该时间段（本日到下个排程日之间）的物料

            //后期优化可以对物料根据时间进行排序
            List<string> todayMaterialList = new List<string>();
            if (src.UnFinishedMaterial != null)
            {
                todayMaterialList.Add(src.UnFinishedMaterial);
            }
            if (dayPlan.MaterialPlanDictionary != null && dayPlan.MaterialPlanDictionary.Count != 0)
            {
                todayMaterialList.AddRange(dayPlan.MaterialPlanDictionary.Keys);
            }
            else
            {
                todayMaterialList.AddRange(src.TodayMaterialList ?? new List<string>());
            }
            Dictionary<string, double> keyValuePairs = new Dictionary<string, double>();
            nextNode = node.Next;
            while (nextNode != null)
            {
                foreach (var m in nextNode.Value.MaterialPlanDictionary.Keys)
                {
                    if (!todayMaterialList.Contains(m))
                    {
                        todayMaterialList.Add(m);
                    }
                    if (keyValuePairs.ContainsKey(m))
                    {
                        keyValuePairs[m] = keyValuePairs[m] + nextNode.Value.MaterialPlanDictionary[m].GetProduceTime();
                    }
                    else
                    {
                        keyValuePairs.Add(m, nextNode.Value.MaterialPlanDictionary[m].GetProduceTime());
                    }
                }
                nextNode = nextNode.Next;
            }

            foreach (var m in todayMaterialList)
            {
                nextNode = node.Next;
                //while (nextNode != null)
                //{
                //    if (!nextNode.Value.IsEmpty() && nextNode.Value.MaterialPlanDictionary.Values.Where(x => x.Material!=m&&x.Number > 0).Count() > 0)
                //    {
                //        break;
                //    }
                //    nextNode = nextNode.Next;
                //}
                if (nextNode == null)
                {
                    return src;
                }
                bool canAdd = false;



                double canUseTime = dayPlan.GetAvailableTime() + nextNode.Value.GetAvailableTime() + CanUseDateList.Where(ele => ele > dayPlan.Date && ele < nextNode.Value.Date).Count() * DayPlan.MaxDayTime + nextNode.Value.GetProduceNeedTime(m);
                canAdd = keyValuePairs.ContainsKey(m) && canUseTime >= keyValuePairs[m] && keyValuePairs[m] > 0.01;
                nextNode = node.Next;
                if (canAdd)
                {
                    while (nextNode != null)
                    {
                        if (nextNode.Value.MaterialPlanDictionary.ContainsKey(m))
                        {
                            MaterialPlan? materialPlan = dayPlan.AddMaterial(nextNode.Value.MaterialPlanDictionary[m]);
                            if (materialPlan != null && materialPlan.Number > 0)
                            {
                                src.UnFinishedMaterial = materialPlan.Material;
                                src.TodayMaterialList = dayPlan.MaterialPlanDictionary.Keys.ToList();
                                return src;
                            }
                        }
                        nextNode = nextNode.Next;
                    }
                }
            }

            //尝试添加可以部分放入的物料
            foreach (var m in keyValuePairs.Keys)
            {
                nextNode = node.Next;
                while (nextNode != null && !nextNode.Value.IsFull())
                {
                    src = node.Value.TryAddSameMaterial(nextNode.Value, CanUseDateList, m, src);
                    if (src.UnFinishedMaterial != null)
                    {
                        return src;
                    }
                    nextNode = nextNode.Next;
                }
            }

            //填充剩余
            nextNode = node.Next;
            while (nextNode != null)
            {
                node.Value.FillUseNext(nextNode.Value);
                nextNode = nextNode.Next;
            }
            return null;
        }


        private void AddNode(LinkedListNode<DayPlan> node)
        {
            LinkedListNode<DayPlan>? currentNode = DayPlanList.First;
            while (currentNode != null)
            {
                if (currentNode.Value.Date > node.Value.Date)
                {
                    DayPlanList.AddBefore(currentNode, node);
                    break;
                }
                currentNode = currentNode.Next;
            }
        }

        private LinkedListNode<DayPlan>? GetNode(DateOnly date)
        {
            LinkedListNode<DayPlan>? currentNode = DayPlanList.First;
            while (currentNode != null)
            {
                if (currentNode.Value.Date == date)
                {
                    return currentNode;
                }
                currentNode = currentNode.Next;
            }
            return null;
        }

        public void RevampScheduler()
        {
            //在真实可用日期的情况下，尝试对有多余时间的排程再次优化
            SchedulerResult? schedulerResult = null;
            foreach (var date in CanUseDateList)
            {
                if (DayPlanList.Last == null || date >= DayPlanList.Last.Value.Date)
                {
                    break;
                }

                schedulerResult = SchedulerForDate(date, schedulerResult);
            }
            Clear();
        }

        public List<Hashtable> ToVO(DateOnly startTime, DateOnly endTime)
        {
            List<Hashtable> hashtables = new List<Hashtable>();
            Hashtable hashtable = new Hashtable();
            var format = "MM-dd";

            HashSet<string> hashSet = new HashSet<string>();
            List<MaterialPlan> list = new List<MaterialPlan>();
            foreach (var item in DayPlanList)
            {
                foreach (var item2 in item.MaterialPlanDictionary.Values)
                {
                    if (!hashSet.Contains(item2.Material))
                    {
                        hashSet.Add(item2.Material);
                        list.Add(item2);
                    }
                }
            }
            int index = 2;
            foreach (var item in list)
            {
                hashtable = new Hashtable();
                hashtable.Add("key", Line + index);
                hashtable.Add("Line", Line);
                hashtable.Add("PN", item.Material);
                hashtable.Add("UPH", item.UPH);
                hashtable.Add("HC", item.HC);
                hashtable.Add("Area", item.Area);
                hashtable.Add("KeyUser", item.KeyUser);
                for (DateOnly date = startTime; date < endTime; date = date.AddDays(1))
                {
                    hashtable.Add(date.ToString(format) + "-Plan", "");
                    hashtable.Add(date.ToString(format) + "-Actual", "");
                    hashtable.Add(date.ToString(format) + "-Mark", "");
                    foreach (var d in DayPlanList)
                    {
                        if (d.Date.Equals(date) && d.MaterialPlanDictionary.ContainsKey(item.Material))
                        {
                            hashtable[date.ToString(format) + "-Plan"] = d.MaterialPlanDictionary[item.Material].Number;
                            break;
                        }
                    }
                }
                hashtables.Add(hashtable);
                index++;
            }
            return hashtables;
        }
    }

    public class SchedulerResult
    {
        //如果这个值不为null,下个日期先处理这个物料
        public string? UnFinishedMaterial { get; set; }
        public List<string> TodayMaterialList { get; set; }
    }
}