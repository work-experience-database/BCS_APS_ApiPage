﻿using APS.SchedulerApplication.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model.BO
{
    public class MaterialPlan
    {
        public string Material { get; set; }
        public string Line { get; set; }
        public DateOnly Date { get; set; }
        public int Number { get; set; }
        public double UPH { get; set; }
        public double HC { get; set; }
        public string KeyUser { get; set; }
        public string Area { get; set; }
        public string Site { get; set; }
        public int? SPQ { get; set; }
        //获取该物料生产计划需要的时间
        public double GetProduceTime()
        {
            return ((double)Number) / UPH;
        }

        public MaterialPlan(SAPDemand pir, Dictionary<string, MaterialMainLine> dictionary, String line)
        {
            if (pir.Date == null || pir.MaterialNumber == null)
            {
                throw new Exception("ERROR PIR DATA");
            }
            this.Date = DateOnly.FromDateTime((DateTime)pir.Date).AddDays(-1);
            MaterialMainLine materialMainLine = dictionary[pir.MaterialNumber];
            this.UPH = materialMainLine.UPH;
            this.HC = materialMainLine.HC;
            this.Number = pir.Quantity;
            this.Line = line;
            this.Material = pir.MaterialNumber;
            this.KeyUser = materialMainLine.KeyUser??"";
            this.Area = materialMainLine.Area ?? "";
            this.SPQ = materialMainLine.SPQ;
            this.Site = materialMainLine.Site;
        }
        public MaterialPlan(SchedulerSummary schedulerSummary)
        {
            this.Date = DateOnly.FromDateTime((DateTime)schedulerSummary.SchedulerDate);   
            this.UPH =(double) schedulerSummary.UPH;
            this.HC = (double)schedulerSummary.HC;
            this.Number =(int) schedulerSummary.PlanedQuantity;
            this.Line = schedulerSummary.Line;
            this.Material = schedulerSummary.MaterialNumber;
            this.KeyUser = schedulerSummary.KeyUser ?? "";
            this.Area = schedulerSummary.Area ?? "";
            this.SPQ = schedulerSummary.SPQ;
            this.Site = schedulerSummary.Site;
        }
        public MaterialPlan(MaterialPlan m) {
            this.Material = m.Material;
            this.Line = m.Line;
            this.UPH = m.UPH;
            this.HC = m.HC;
            this.KeyUser = m.KeyUser ;
            this.Area = m.Area ;
            this.SPQ = m.SPQ;
            this.Site   = m.Site;
        }
    }
}