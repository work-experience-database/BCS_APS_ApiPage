﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class BOMScheduleModel
    {
        public string? MATNR { get; set; }
        public string? WERKS { get; set; }
        public string? ARBPL { get; set; }
        public string? ALPRF { get; set; }
        public string? UPH { get; set; }
        public string? LKENZ { get; set; }
    }
}
