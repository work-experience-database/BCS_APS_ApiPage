﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class DownloadModel
    {
        public List<string> title { get; set; }
        public List<Dictionary<string, object>> data { get; set; }

    }
}
