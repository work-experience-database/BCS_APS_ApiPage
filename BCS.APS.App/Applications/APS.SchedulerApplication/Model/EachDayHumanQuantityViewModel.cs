﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class EachDayHumanQuantityViewModel
    {
        public string? CostCenter { get; set; }
        public string? EmployeeGroup { get; set; }
        public string Date { get; set; }
        public string? Quantity { get; set; }
        public string? OEE { get; set; }
        public string? KeyUser { get; set; }
        public string? Remark { get; set; }
    }
}
