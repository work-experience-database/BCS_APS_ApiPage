﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class EachLineTotalTimeViewModel
    {
        public string startDate { get; set; }
        public int current { get; set; }
        public int pageSize { get; set; }
        public int total { get; set; }
        public List<Hashtable> list { get; set; }
    }
}
