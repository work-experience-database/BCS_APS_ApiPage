﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class FactoryCalendarViewModel
    {
        public DateTime Date { get; set; }
        public bool IsWork { get; set; }
        public string? KeyUser { get; set; }
        public string? Type { get; set; }
        public string? Comments { get; set; }
    }
}
