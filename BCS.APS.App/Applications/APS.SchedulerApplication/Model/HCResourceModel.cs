﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace APS.SchedulerApplication.Model
{
    public class HCResourceModel
    {
        public string KOSTL { get; set; }
        public string PERSG { get; set; }
        public string DATUM { get; set; }
        public string COUNTER { get; set; }
    }
}
