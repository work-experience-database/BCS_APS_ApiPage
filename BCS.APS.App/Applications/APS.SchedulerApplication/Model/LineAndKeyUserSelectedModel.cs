﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class LineAndKeyUserSelectedModel
    {
        public List<string> Lines { get; set; }
        public List<string> KeyUsers { get; set; }
    }
}
