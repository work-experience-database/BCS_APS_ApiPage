﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class LineMaintainViewModel
    {
        public string Id { get; set; }
        public string Line { get; set; }
        public string? Area { get; set; }
        public string? User { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string IsWork { get; set; }
    }
}
