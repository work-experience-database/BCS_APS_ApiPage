﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class MaterialInfoViewModel
    {
        public string Id { get; set; }
        public string? Plant { get; set; }
        public string? PN { get; set; }
        public string? Line { get; set; }
        public string? Order { get; set; }
        public string? Area { get; set; }
        public string? UPH { get; set; }
        public string? SPQ { get; set; }
        public string? HC { get; set; }
        public string? User { get; set; }
        public string? Description { get; set; }
    }
}
