﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class NewSapPirModel
    {
        public int Flag { get; set; }
        public string? Plant { get; set; }
        public int WeekNumber { get; set; }
        public DateTime Date { get; set; }
        public List<PIRDemand> PIRDemands { get; set; }
    }

    public class PIRDemand
    {
        public int Flag { get; set; } = 0;
        public int Quantity { get; set; }
        public string? Line { get; set; }
        public string? MaterialNumber { get; set; }
    }
}