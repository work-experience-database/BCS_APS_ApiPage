﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class ReleaseSchedulerSummaryToIMesViewModel
    {
        public List<string> Line { get; set; }
        public List<string> PP { get; set; }
        public List<string> selectDate { get; set; }
    }
}
