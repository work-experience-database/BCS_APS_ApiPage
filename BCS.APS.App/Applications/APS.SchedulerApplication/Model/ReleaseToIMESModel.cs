﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class ReleaseToIMESModel
    {
        //"AUFNR": "L12917040205-022024010401", //工单
        //"PLNBEZ": "17040205-02", //料号
        //"WERKS": "535A", //工厂
        //"AUART": "ZPK4", //工单类型
        //"GAMNG": 200, //订单数量总计（工单目标量）
        //"GSTRP": "20240104", //基本开始日期
        //"GSUZP": "083000", //基本开始时间
        //"ZJKSRQ": "2024/1/4 08:30:00", //基本开始日期时间
        //"GLTRP": "20240104", //基本完成日期
        //"GLUZP": "203000", //基本完成时间
        //"ZJJSRQ": "2024/1/4 20:30:00", //基本结束日期时间
        //"ERDAT": "20240104", //订单创建日期
        //"ERFZEIT": "101800", //订单创建时间
        //"ZDCJRQ": "2024/1/4 10:18:00", //订单创建日期时间
        //"ZZLINE": "L129", //线别
        //"ERNAM": "0493", //创建者
        //"ZZCMCC": "L129" //机种
        public string? AUFNR { get; set; }
        public string? PLNBEZ { get; set; }
        public string? WERKS { get; set; }
        public string? AUART { get; set; }
        public string? GAMNG { get; set; }
        public string? GSTRP { get; set; }
        public string? GSUZP { get; set; }
        public string? ZJKSRQ { get; set; }
        public string? GLTRP { get; set; }
        public string? GLUZP { get; set; }
        public string? ZJJSRQ { get; set; }
        public string? ERDAT { get; set; }
        public string? ZDCJRQ { get; set; }
        public string? ZZLINE { get; set; }
        public string? ERNAM { get; set; }
        public string? ZZCMCC { get; set; }

    }
}
