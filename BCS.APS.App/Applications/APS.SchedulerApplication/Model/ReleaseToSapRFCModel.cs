﻿using APS.SchedulerApplication.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class ReleaseToSapRFCModel
    {
        public string IV_WERKS { get; set; }
        public string IV_COLLECT { get; set; } = "N";
        public string IV_PSTTR { get; set; }
        public string IV_PEDTR { get; set; }

        public List<T_PLDORD> T_PLDORDList { get; set; }
    }

    public class T_PLDORD
    {
        public string ORDER { get; set; }
        public string MATNR { get; set; }
        public string WERKS { get; set; }
        public string ARBPL { get; set; }
        public string PSTTR { get; set; }
        public string PEDTR { get; set; }
        public string GSMNG { get; set; }
        public string PLNUM { get; set; }
        public string TYPE { get; set; }
        public string MESSAGE { get; set; }
    }
}
