﻿using APS.SchedulerApplication.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class SapDemandExtension : SAPDemand
    {
        public string? Line { get; set; }
        public int? ActualQuantity { get; set; }
        public int? SPQ { get; set; }
        public double? UPH { get; set; }
        public double? HC { get; set; }
        public string? Site { get; set; }
        public string? Area { get; set; }
        public string? KeyUser { get; set; }
        public string? PCBA { get; set; }
        public string? Description { get; set; }
    }
}
