﻿using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class SapOriginalMaterial
    {
        public string? MATNR { get; set; }
        public string? WERKS { get; set; }
        public string? MTART { get; set; }
        public string? MAKTX { get; set; }
        public string? BESKZ { get; set; }
        public string? SOBSL { get; set; }
        public string? FEVOR { get; set; }
        public string? TXT { get; set; }
        public string? INSMK { get; set; }
        public string? DISPO { get; set; }
        public string? DSNAM { get; set; }
        public string? PMATNR1 { get; set; }
        public string? TRGQTY1 { get; set; }
    }
}
