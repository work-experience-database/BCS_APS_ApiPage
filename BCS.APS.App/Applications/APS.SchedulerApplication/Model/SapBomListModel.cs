﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace APS.SchedulerApplication.Model
{
    public class SapBomListModel
    {
        public string MATNR { get; set; }   //料号/上阶物料
        public string WERKS { get; set; }   //资料类型
        public string POSNR { get; set; }   //BOM序号
        public string IDNRK { get; set; }   //组件/下阶物料
        public string MENGE { get; set; }   //组件用量
        public string MEINS { get; set; }   //用量单位
        public string SOBSL { get; set; }   //特殊采购类型
        public string DUMPS { get; set; }   //虚拟件
        public string DATUV { get; set; }   //生效日期
        public string DATUB { get; set; }   //失效日期
        public string MMSTA { get; set; }   //物料工厂状态
        public string AUSCH { get; set; }   //组件报废率
    }
}
