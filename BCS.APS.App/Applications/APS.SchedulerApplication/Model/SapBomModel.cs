﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class SapBomModel
    {
        public string MATNR { get; set; }   //料号
        public string WERKS { get; set; }   //工厂
        public string ARBPL { get; set; }   //线体
        public string ALPRF { get; set; }   //优先级，主线1，辅线2
        public string UPH { get; set; }     //UPH
        public string LKENZ { get; set; }   //删除标记
    }
}
