﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace APS.SchedulerApplication.Model
{
    public class SapOriginalPir
    {
        //public string MATNR { get; set; }
        //public string TYPE { get; set; }
        //public string DATUM { get; set; }
        //public string PLNMG { get; set; }
        public string Plant { get; set; }
        public string MaterialNumber { get; set; }
        public string Type { get; set; }
        public string Date { get; set; }
        public string Quantity { get; set; }
        public string ReleaseTime { get; set; }
    }
}
