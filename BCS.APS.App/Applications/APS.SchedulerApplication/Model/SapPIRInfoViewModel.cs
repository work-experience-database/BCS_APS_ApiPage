﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class SapPIRInfoViewModel
    {
        public string startDate { get; set; }
        public int current { get; set; }
        public int pageSize { get; set; }
        public int total { get; set; }
        public List<string> title { get; set; }
        public List<Dictionary<string, string?>> list { get; set; }
    }
}
