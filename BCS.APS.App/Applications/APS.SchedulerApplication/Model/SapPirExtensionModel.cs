﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace APS.SchedulerApplication.Model
//{
//    public class SapPirExtensionModel
//    {
//        public string Plant { get; set; }
//        public int Flag { get; set; } = 0;
//        public DateTime Date { get; set; }
//        public int LineQty { get; set; }
//        public double StandardTime { get; set; } = 22.28;
//        public double EachDayTime { get; set; }
//        public double TotalHCOfEachLine { get; set; }
//        public List<LineInfo> LineInfoList { get; set; }
//    }

//    public class LineInfo
//    {
//        public string? Line { get; set; }
//        public int Flag { get; set; } = 0;
//        public int MaterialNumberQty { get; set; }
//        public double StandardTime { get; set; } = 22.28;
//        public double EachLineSpareTime { get; set; } = 22.28;
//        public double EachLineTime { get; set; }
//        public double EachLineHC { get; set; }
//        public List<MaterialInfo> MaterialInfoList { get; set; }
//    }

//    public class MaterialInfo
//    {
//        public string? Subline { get; set; }
//        public string? MaterialNumber { get; set; }
//        public int Flag { get; set; } = 0;
//        public int PlanedQuantity { get; set; }
//        public int? ActualQuantity { get; set; }
//        public int? SPQ { get; set; }
//        public double UPH { get; set; }
//        public double HC { get; set; }
//        public string? Site { get; set; }
//        public string? Area { get; set; }
//        public string? KeyUser { get; set; }
//        public string? PCBA { get; set; }
//        public string? Description { get; set; }
//        public string? Remark { get; set; }

//        public double StandardTime { get; set; } = 22.28;

//        public double EachMaterialTime { get; set; }
//    }
//}
