﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class SapPirModel
    {
        public int Flag { get; set; }

        public bool Deleted { get; set; } = false;
        public DateTime? UpdateTime { get; set; }
        public DateTime? CreateTime { get; set; } = DateTime.Now;

        public string? MaterialNumber { get; set; }
        public string Plant { get; set; }
        public string? Type { get; set; }
        public int PlanedQuantity { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime? ReleaseTime { get; set; }

        public string? Line { get; set; }
        public string? MainLine { get; set; }
        public string? StandbyLineA { get; set; }
        public string? StandbyLineB { get; set; }
        public string? StandbyLineC { get; set; }
        

        public int? ActualQuantity { get; set; }
        public int? SPQ { get; set; }
        public double UPH { get; set; }
        public double HC { get; set; }
        public string? Site { get; set; }
        public string? Area { get; set; }
        public string? KeyUser { get; set; }
        public string? PCBA { get; set; }
        public string? Description { get; set; }
    }
}
