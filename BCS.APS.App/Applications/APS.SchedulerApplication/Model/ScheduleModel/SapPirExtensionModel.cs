﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model.ScheduleModel
{
    public class SapPirExtensionModel
    {
        public int Flag { get; set; }
        public string Plant { get; set; } = "535A";
        public int WeekNumber { get; set; }
        public DateTime Date { get; set; }
        public int Quantity { get; set; }
        public double UPH { get; set; }
        public string Line { get; set; }
        public string MaterialNumber { get; set; }
    }
}
