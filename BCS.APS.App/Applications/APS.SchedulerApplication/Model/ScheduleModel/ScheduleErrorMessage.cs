﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model.ScheduleModel
{
    public class ScheduleErrorMessage
    {
        public string MaterialNumber { get; set; }
        public Types Type { get; set; } = Types.Error;
        public string ErrorMessage { get; set; } = string.Empty;
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;
    }

    public enum Types
    {
        Info,
        Warning,
        Error,
    }
}
