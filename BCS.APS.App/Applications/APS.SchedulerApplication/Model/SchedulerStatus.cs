﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public enum SchedulerStatus
    {
        Created,
        ReleasedToSap,
        ReleasedToIMes,
        WorkInProcess,
        Hold,
        Completed,
    }
}
