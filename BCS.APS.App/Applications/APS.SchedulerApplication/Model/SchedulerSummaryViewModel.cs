﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class SchedulerSummaryViewModel
    {
        public List<Hashtable> list { get; set; }
        public List<TitleElement> title { get; set; }
    }

    public class TitleElement
    {
        public Hashtable title { get; set; }
        public List<Hashtable> children { get; set; }
    }
}
