﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.SchedulerApplication.Model
{
    public class UpdateSchedulerSummaryViewModel
    {
        public List<Hashtable> list { get; set; }
    }

    public class UpdateSchedulerSummaryModel
    {
        public string? SchedulerId { get; set; }
        public int PlanedQuantity { get; set; }
        public int ActualQuantity { get; set; }
        public string? Remark { get; set; }
    }
}
