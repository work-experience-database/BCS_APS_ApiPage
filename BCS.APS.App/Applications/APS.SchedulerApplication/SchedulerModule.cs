﻿using APS.AspNetCore.Services;
using APS.SchedulerApplication.Entitys;
using APS.Utils.Helper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace APS.SchedulerApplication
{
    public class SchedulerModule : BaseModule
    {
        public override void ConfigServices(IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddDbContext<APSDbContext>(op =>
            {
                op.UseSqlServer(configuration.GetConnectionString("APSDb"));
            });
        }

        /// <summary>初始化</summary>
        public override void AppFirstInit()
        {
            var commdb = HostServiceExtension.ServiceProvider.GetService<APSDbContext>();
            try
            {
                commdb.Database.EnsureCreated();
            }
            catch (Exception ex)
            {
                ConsoleHelper.Err($"创建CommonDbContext异常 ex:{ex.Message}");
            }

            try
            {
                var databaseCreator = commdb.GetService<IRelationalDatabaseCreator>();
                databaseCreator.CreateTables();
            }
            catch (Exception ex)
            {
                ConsoleHelper.Err($"创建CommonDb表异常 ex:{ex.Message}");
            }
        }
    }
}
