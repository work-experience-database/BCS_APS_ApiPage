﻿using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.Jobs
{
    public class JobSchedule
    {
        private readonly bool IsInited = false;

        public JobSchedule(params Registry[] registrys)
        {
            try
            {
                if (registrys.Length == 0)
                {
                    registrys = new Registry[] { new JobRegistry() };
                }
                JobManager.Initialize(registrys);
                JobManager.JobEnd += JobManager_JobEnd; ;
                IsInited = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public event Action<JobEndInfo> JobEnd;

        private void JobManager_JobEnd(JobEndInfo obj)
        {
            JobEnd?.Invoke(obj);
        }

        public void Start()
        {
            JobManager.Start();
        }

        public void Stop()
        {
            JobManager.Stop();
        }
    }
}
