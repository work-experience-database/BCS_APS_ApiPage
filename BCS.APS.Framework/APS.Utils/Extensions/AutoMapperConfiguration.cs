﻿//using AutoMapper;
//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace APS.Utils.Extensions
//{
//   public  class AutoMapperConfiguration:Profile
//    {
//        /// <summary>
//        /// Initialize mapper
//        /// </summary>
//        /// <param name="configurationActions">Configuration actions</param>
//        public IMapper CreateMapper()
//        {
//            Action<IMapperConfigurationExpression> configurationAction = GetConfiguration();
//            var  _mapperConfiguration = new MapperConfiguration(cfg =>
//            {
//                configurationAction(cfg);
//            });

//            return _mapperConfiguration.CreateMapper();
//        }


//        /// <summary>
//        /// Get configuration
//        /// </summary>
//        /// <returns>Mapper configuration action</returns>
//        public  Action<IMapperConfigurationExpression> GetConfiguration()
//        {
//            Action<IMapperConfigurationExpression> action = cfg =>
//            {
//                cfg.CreateMap<TemplateItemModel, MAPTemplateDataItem>();
//            };
//            return action;
//        }
//    }
//}
