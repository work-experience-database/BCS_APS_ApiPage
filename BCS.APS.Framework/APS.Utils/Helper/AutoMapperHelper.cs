﻿using APS.Utils.Extensions;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.Utils.Helper
{
    public static class AutoMapperHelper
    {
        public static T1 Map<T, T1>(T t)
        {
            var source = Activator.CreateInstance(typeof(T));
            var result = Activator.CreateInstance(typeof(T1));
            if (source.GetType().Name == "List`1" || result.GetType().Name == "List`1")
            {
                throw new Exception("形参有误！，请使用对象。");
                //throw new Exception("列表映射请使用 MapList 方法");
            }
            var tpropertyInfos = source.GetType().GetProperties();
            var t1propertyInfos = result.GetType().GetProperties();
            foreach (var tinfo in tpropertyInfos)
            {
                foreach (var t1info in t1propertyInfos)
                {
                    if (t1info.PropertyType.IsValueType || t1info.PropertyType.Name.StartsWith("String"))
                    {
                        if (tinfo.Name == t1info.Name)
                        {
                            try
                            {
                                object value = tinfo.GetValue(t, null);
                                var property = typeof(T1).GetProperty(tinfo.Name);
                                if (property != null && property.CanWrite && !(value is DBNull))
                                {
                                    property.SetValue(result, value, null);
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                }

            }
            return (T1)result;
        }
    }
}
