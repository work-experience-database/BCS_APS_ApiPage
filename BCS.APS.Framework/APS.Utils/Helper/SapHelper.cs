﻿using APS.Utils.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace APS.Utils.Helper
{
    public class SapHelper
    {
        readonly RfcConfigParametersConfig rfcConfigParametersConfig;
        public SapHelper()
        {
            rfcConfigParametersConfig.Name = "FSTest100";
            rfcConfigParametersConfig.User = "RFC_APS";
            rfcConfigParametersConfig.Password = "Welcome#12";
            rfcConfigParametersConfig.SystemID = "BAQ";
            rfcConfigParametersConfig.Client = "100";
            rfcConfigParametersConfig.Language = "ZH";
            rfcConfigParametersConfig.AppServerHost = "10.16.0.1";
            rfcConfigParametersConfig.SystemNumber = "20";
        }

        /// <summary>
        ///  输入参数为字符串
        /// </summary>
        /// <param name="inputParameters"></param>
        public void GetSapInfoByParameters(InputParameters inputParameters)
        {
            var jsonContent = JsonConvert.SerializeObject(inputParameters);
            var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
            var uri = "http://10.2.34.21:8100/api/sap/GetPlanOrder";
            var request = new HttpRequestMessage(HttpMethod.Post, uri) { Content = content };

            HttpClient client = new HttpClient();
            //HttpResponseMessage response = client.SendAsync(request);
            //var responseBody = response.Content.ReadAsStringAsync();

            //// 确保HTTP响应成功  
            //response.EnsureSuccessStatusCode();
            //var result = JsonConvert.DeserializeObject<ApiResult>();
            //if (result.Data != null)
            //{
            //    var JsonConvert.DeserializeObject<object>(result.Data.ToString());
            //}
        }


        /// <summary>
        /// 输入参数为table
        /// </summary>
        /// <param name="inputParametersAndTable"></param>
        public void GetSapInfoByParametersAndTable(InputParametersAndTable inputParametersAndTable)
        {

        }

        /// <summary>
        /// 输入参数为Structure
        /// </summary>
        /// <param name="inputParametersAndStructure"></param>
        public void GetSapInfoByParametersAndStructure(InputParametersAndStructure inputParametersAndStructure)
        {

        }
    }

    public class InputParameters
    {
        public string RfcName { get; set; }
        public string MsessageType { get; set; }
        public string Msessage { get; set; }
        public Dictionary<string, string> Parametersdic { get; set; }
    }

    public class InputParametersAndTable
    {
        public string RfcName { get; set; }
        public string MsessageType { get; set; }
        public string Msessage { get; set; }
        public Dictionary<string, string> Parametersdic { get; set; }

    }

    public class InputParametersAndStructure
    {
        public string RfcName { get; set; }
        public string MsessageType { get; set; }
        public string Msessage { get; set; }
        public Dictionary<string, string> Parametersdic { get; set; }

    }


    public class RfcConfigParametersConfig
    {
        public string Name { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string SystemID { get; set; }
        public string Client { get; set; }
        public string Language { get; set; }
        public string AppServerHost { get; set; }
        public string SystemNumber { get; set; }

    }
}