﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS.Utils.Helper
{
    public class WeekHelper
    {
        public static DateTime GetMondayDate(DateTime date)
        {
            while (date.DayOfWeek != DayOfWeek.Monday)
            {
                date = date.AddDays(-1);
            }
            return date;
        }

        public static int GetWeekNumber(DateTime date)
        {
            // 使用当前线程的CultureInfo来获取Calendar对象  
            Calendar calendar = CultureInfo.CurrentCulture.Calendar;

            // 调用Calendar的GetWeekOfYear方法获取周数  
            // 注意：某些Calendar可能需要指定FirstDayOfWeek和CalendarWeekRule  
            // 这里使用GregorianCalendar作为示例，它通常是默认Calendar  
            int weekOfYear = calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Monday);

            // 打印结果  
            //DateTime firstDayOfYear = new DateTime(date.Year, 1, 1);
            //int daysDifference = date.Date.Subtract(firstDayOfYear).Days;
            //int weekNumber = (daysDifference + 3) / 7;
            return weekOfYear;
        }
    }
}
